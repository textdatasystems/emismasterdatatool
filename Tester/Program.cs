﻿using Processor.ControlObjects;
using Processor.Entities;
using Processor.Entities.PM.disposal;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tester
{
    class Program
    {

        static void Main(string[] args)
        {

            timotTest();

            //loadAppData(DatabaseHandler.dbConnection());

            // testSaveCbActivity();
            // testGetCbActivity(1);
        }

        private static void timotTest()
        {


            new DatabaseHandler().getManagementLetterSections(DatabaseHandler.dbConnection());            
            Console.ReadLine();

        }

        private static void testGetCbActivity(int activityID)
        {
            var db = DatabaseHandler.dbConnection();

            var activity = new DatabaseHandler().findCustomCbActivityByActivityId(db, activityID);

        }

        public static void testSaveCbActivity()
        {
            var db = DatabaseHandler.dbConnection();

            CbActivity activity = new CbActivity();

            int id = db.Insert(activity);

        }



        private static void loadAppData(SQLiteConnection dbConnection)
        {


            Console.WriteLine("******** Started loading data *********");

            DataLoader dataLoader = new DataLoader();

            //insert the financial years into the system
            //dataLoader.loadFinancialYears();
            //dataLoader.loadEntities();      
            //dataLoader.loadCbaActivities();

            //dataLoader.fetchAuditProcessToolSections();
            //dataLoader.fetchAuditSystemsToolSections();
            //dataLoader.fetchAuditDisposalToolSections();
            dataLoader.fetchComplianceProcessToolSections();


            Console.WriteLine("******** Finished loading data *********");
            Console.ReadLine();

        }
       
    }


}

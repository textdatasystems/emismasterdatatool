﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class PmPlan
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int financial_year_id { get; set; }

        [IgnoreAttribute]
        public String displayName { get; set; }
        
    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class BudgetItem
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public String item_name { get; set; }

    }
}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class BudgetItemAmount
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int budget_item_id { get; set; }
        public int commentable_id { get; set; }
        public String commentable_type { get; set; }
        public String note { get; set; }
        public double unit_price { get; set; }
        public double quantity { get; set; }
        public double amount { get; set; }

    }
}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class PmMeetingAttendances
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }        
        public int emisId { get; set; }
        public int pm_activity_id { get; set; }
        public String audit_activity { get; set; }
        public String commentable_type { get; set; }
        public int commentable_id { get; set; }
        public int audit_meeting_role_id { get; set; }

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class PmAuditSampleFile
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int pm_activity_id { get; set; }
        public String procurement_reference { get; set; }
        public String procurement_subject { get; set; }
        public String procurement_method { get; set; }
        public String provider { get; set; }
        public String date_of_award { get; set; }
        public double contract_value { get; set; }
        public int user_id { get; set; }
        
    }

}

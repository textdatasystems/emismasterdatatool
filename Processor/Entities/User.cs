﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class User
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        public String title { get; set; }
        public String email { get; set; }
        public String password { get; set; }

        //foreign key IDs
        public int department_id { get; set; }
        public int region_id { get; set; }
        public int role_id { get; set; }
        public int unit_id { get; set; }

        
        public String fullName;
        [Ignore]
        public string FullName
        {
            get
            {
                return first_name + " " + last_name;
              }
            set { fullName = value; }
        }

    }

}

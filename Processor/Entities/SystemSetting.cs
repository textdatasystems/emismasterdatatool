﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class SystemSetting
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String groupCode { get; set; }
        public String valueCode { get; set; }
        public String value { get; set; }
        public String description { get; set; }

    }

}

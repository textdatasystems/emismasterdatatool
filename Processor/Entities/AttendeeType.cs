﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class AttendeeType
    {
        public String commentableType { get; set; }
        public String displayValue { get; set; }

        public AttendeeType(String commentableType, String displayValue)
        {
            this.commentableType = commentableType;
            this.displayValue = displayValue;
        }
    }
}

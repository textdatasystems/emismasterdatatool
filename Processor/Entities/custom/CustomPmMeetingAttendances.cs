﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.custom
{
    public class CustomPmMeetingAttendances : PmMeetingAttendances
    {
            public String firstName { get; set; }
            public String lastName { get; set; }
            public String title { get; set; }
            public String email { get; set; }
            public String phoneNo { get; set; }
            public String entityName { get; set; }
            public String roleName { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.custom
{
    public class CustomPmAuditSampleFile : PmAuditSampleFile
    {
        public String assigneeName { get; set; }
        public String status { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.custom
{
    public class CustomCbActivity
    {
        public int activityId { get; set; }
        public String activityName { get; set; }
        public String cordinatorName { get; set; }
        public String districtName { get; set; }
        public String startDate { get; set; }
        public String endDate { get; set; }
        public String activitySubject { get; set; }
        public String activityCategory;
                public string ActivityCategory
        {
            get
            {
                return String.IsNullOrEmpty(activityCategory) ? null :
                   System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(activityCategory.ToLower().Replace("_", " "));
            }
            set { activityCategory = value; }
        }

        public String noOfPeople { get; set; }
        public String noOfdays { get; set; }
        public String responsibleOffice { get; set; }
        public String activityTypeName { get; set; }
        public String fundingSourceName { get; set; }
        
        public CustomCbActivity child { get; set; }
        public CustomCbActivity parent { get; set; }


        public List<CustomCbActivityModule> modules { get; set; }
        public List<CustomCbActivityTrainer> trainersInternal { get; set; }
        public List<CustomCbActivityTrainer> trainersExternal { get; set; }
        public List<CustomBudgetItemAmount> budgetItemAmounts { get; set; }
        public List<CbExpectedOutcome> expectedOutcomes { get; set; }

    }

}

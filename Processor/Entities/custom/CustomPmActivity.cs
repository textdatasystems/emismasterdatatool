﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.custom
{
    public class CustomPmActivity : PmActivity
    {

        public String entityName { get; set; }
        public String responsibleOffice { get; set; }
        public String fundingSourceName { get; set; }
        public String startDate { get; set; }
        public String endDate { get; set; }

        public List<CustomBudgetItemAmount> budgetItemAmounts { get; set; }

        public CustomPmTeam pmTeam { get; set; }

        public FinacialYear financialYear { get; set; }

        public String displayName()
        {            
            return financialYear == null ? "Performance Monitoring Activity  | " + entityName : "Performance Monitoring Activity  |  " + financialYear.financial_year + "  |  " + entityName;
        } 

    }

}

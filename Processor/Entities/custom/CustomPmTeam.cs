﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.custom
{
    public class CustomPmTeam : PmTeam
    {
        public String teamLeaderName { get; set; }
        public String startDate { get; set; }
        public String endDate { get; set; }
        public double budget { get; set; }
        public List<CustomPmTeamMember> teamMembers { get; set; }

    }

}

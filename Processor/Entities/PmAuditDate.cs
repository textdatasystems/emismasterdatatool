﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class PmAuditDate
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int pm_activity_id { get; set; }
        public String activity { get; set; }
        public String activity_date  { get; set; }
        public String activity_time { get; set; }
        public String tag { get; set; }

    }

}

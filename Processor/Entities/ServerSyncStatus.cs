﻿using Processor.ControlObjects;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class ServerSyncStatus
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String dataTypeCategory { get; set; }
        public String dataType { get; set; }
        public String lastUpdateDate { get; set; }

        //public string DisplayText
        //{
        //    get
        //    {

        //        if (dataTypeCategory == Globals.SYNC_DATA_TYPE_CAT_PM_ACTIVITY)
        //        {
        //            return "PM Activities for " + dataType;
        //        }
        //        else if (dataTypeCategory == Globals.SYNC_DATA_TYPE_CAT_CB_ACTIVITY)
        //        {
        //            return "CB Activities for " + dataType;
        //        }
        //        else
        //        {
        //            return dataType;
        //        }

        //    }
        //    set { dataType = value; }
        //}

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class CbActivity
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int cbPlanId { get; set; }
        public int parentId { get; set; }
        public int userId { get; set; }
        public int districtId { get; set; }
        public int activityTypeId { get; set; }
        public int ppdaOfficeId { get; set; }
        public int activityCategoryId { get; set; }
        public int fundingSourceId { get; set; }
        public String activityname { get; set; }
        public String activitySubject { get; set; }
        public String activityCategory;

        public string ActivityCategory
        {
            get
            {
                return String.IsNullOrEmpty(activityCategory) ? null :
                   System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(activityCategory.ToLower().Replace("_", " "));
            }
            set { activityCategory = value; }
        }

        public String town { get; set; }
        public String startDate { get; set; }
        public String endDate { get; set; }
        public String trainingProgram { get; set; }
        public int noOfdays { get; set; }
        public int noOfPeople { get; set; }

    }
}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class ManagementLetterSectionException
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int mgt_letter_section_id { get; set; }
        public String exception_title { get; set; }
        public String exception_standard_implication { get; set; }
        public String exception_standard_recommendation { get; set; }

    }

}

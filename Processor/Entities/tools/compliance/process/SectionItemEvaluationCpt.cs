﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.compliance.process
{
    public class SectionItemEvaluationCpt
    {

        public long sectionItemEmisId;
        public long sectionEmisId;

        public long yesRank;
        public long noRank;
        public long naRank;

        public String case1FieldName;
        public String case1FieldValue;

        public String case2FieldName;
        public String case2FieldValue;

        public String case3FieldName;
        public String case3FieldValue;

        public String case4FieldName;
        public String case4FieldValue;

        public String case5FieldName;
        public String case5FieldValue;

        public String case6FieldName;
        public String case6FieldValue;

        public String case7FieldName;
        public String case7FieldValue;

        public String case8FieldName;
        public String case8FieldValue;

        public String case9FieldName;
        public String case9FieldValue;

        public String case10FieldName;
        public String case10FieldValue;
        

        public String evaluationFieldType;
        public String evaluationFieldName;
        public String evaluationFieldValue;
        public String evaluationFieldRole;

        public String findingFieldName;
        public String findingFieldValue;

        public String exceptionFieldName;
        public String exceptionFieldValue;

        

    }

}

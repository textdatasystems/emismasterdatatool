﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.compliance.process
{
    public class ApiDataPmComplianceProcessToolSection
    {
        public int id { get; set; }
        public string rank_no { get; set; }
        public string name { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public List<ApiDataPmComplianceProcessToolSectionItem> pm_compliance_process_tool_section_items { get; set; }

    }

}

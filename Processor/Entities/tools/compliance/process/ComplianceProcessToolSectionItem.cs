﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.compliance.process
{
    public class ComplianceProcessToolSectionItem
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public String description { get; set; }
        public int complianceProcessToolSectionId { get; set; }
        public double rank { get; set; }
        public String responseType { get; set; }
        public int yesRank { get; set; }
        public int noRank { get; set; }
        public int naRank { get; set; }
        public String fieldRole { get; set; }

    }
}

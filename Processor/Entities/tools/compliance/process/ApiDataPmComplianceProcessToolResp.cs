﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.compliance.process
{
    public class ApiDataPmComplianceProcessToolResp
    {
        public int statusCode { get; set; }
        public string statusDescription { get; set; }
        public List<ApiDataPmComplianceProcessToolSection> data { get; set; }

    }

}

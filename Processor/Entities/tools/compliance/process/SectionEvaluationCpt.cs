﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.compliance.process
{
    public class SectionEvaluationCpt
    {

        public long sectionEmisId;
        public String name;
        public String sectionTableFieldName;
        public String sectionTotalFieldName;
        public long sectionYesTotalValue;
        public long sectionNoTotalValue;

        public CaseScores caseScores = new CaseScores();

        public Dictionary<long, SectionItemEvaluationCpt> sectionItemEvaluations = new Dictionary<long, SectionItemEvaluationCpt>();

    }
}

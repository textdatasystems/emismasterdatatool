﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.compliance.process
{
    public class ComplianceProcessToolSection
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public double rank { get; set; }
        public String name { get; set; }

        [Ignore]
        public List<ComplianceProcessToolSectionItem> items { get; set; }

    }

}

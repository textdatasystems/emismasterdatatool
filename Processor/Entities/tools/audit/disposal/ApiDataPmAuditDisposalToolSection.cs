﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.disposal
{
    public class ApiDataPmAuditDisposalToolSection
    {
        public int id { get; set; }
        public string rank_no { get; set; }
        public string name { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public List<ApiDataPmAuditDisposalToolSectionItem> pm_audit_disposal_tool_section_items { get; set; }

    }
}

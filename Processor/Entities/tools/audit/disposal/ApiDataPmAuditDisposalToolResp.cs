﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.disposal
{
    public class ApiDataPmAuditDisposalToolResp
    {
        public int statusCode { get; set; }
        public string statusDescription { get; set; }
        public List<ApiDataPmAuditDisposalToolSection> data { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.disposal
{
    public class ApiDataPmAuditDisposalToolSectionItem
    {
        public int id { get; set; }
        public int tool_section_id { get; set; }
        public string rank { get; set; }
        public string description { get; set; }
        public string response_type { get; set; }
        public int yes_rank { get; set; }
        public int no_rank { get; set; }
        public int na_rank { get; set; }
        public object more_info { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }
}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.disposal
{
    public class AuditDisposalToolSectionItem { 

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public String description { get; set; }
        public int auditDisposalToolSectionId { get; set; }
        public double rank { get; set; }
        public String responseType { get; set; }
        public int yesRank { get; set; }
        public int noRank { get; set; }
        public int naRank { get; set; }
        public String fieldRole { get; set; }

    }
}

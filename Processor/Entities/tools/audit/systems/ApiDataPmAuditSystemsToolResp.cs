﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.systems
{
    public class ApiDataPmAuditSystemsToolResp
    {

        public String statusDescription { get; set; }
        public List<ApiDataPmAuditSystemsToolSection> data { get; set; }
        public String statusCode { get; set; }

    }

}

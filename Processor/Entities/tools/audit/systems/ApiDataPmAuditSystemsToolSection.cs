﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.systems
{
    public class ApiDataPmAuditSystemsToolSection
    {
        public List<ApiDataPmAuditSystemsToolSectionItem> pm_audit_systems_tool_section_items { get; set; }
        public String updated_at { get; set; }
        public String rank_no { get; set; }
        public String name { get; set; }
        public String created_at { get; set; }
        public String id { get; set; }

    }

}

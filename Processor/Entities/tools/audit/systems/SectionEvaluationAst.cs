﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.systems
{
    public class SectionEvaluationAst
    {
        public long sectionEmisId;
        public String name;
        public String sectionTableFieldName;
        public String sectionTotalFieldName;
        public long sectionYesTotalValue;
        public long sectionNoTotalValue;

        public Dictionary<long, SectionItemEvaluationAst> sectionItemEvaluations = new Dictionary<long, SectionItemEvaluationAst>();


    }

}

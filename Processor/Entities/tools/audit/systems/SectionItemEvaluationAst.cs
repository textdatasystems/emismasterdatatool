﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.systems
{
    public class SectionItemEvaluationAst
    {
        public long sectionItemEmisId;
        public long sectionEmisId;

        public String description; 

        public String evaluationFieldType; 
        public String evaluationFieldName;
        public String evaluationFieldValue;
        public String evaluationFieldRole;

        public String findingFieldName;
        public String findingFieldValue;

        public String exceptionFieldName;
        public String exceptionFieldValue;

        public String managementLetterSectionFieldName;
        public long managementLetterSectionFieldValue;

        public String managementLetterSectionItemFieldName;
        public long managementLetterSectionItemFieldValue;

        public long yesRank;
        public long noRank;
        public long naRank;

    }
}

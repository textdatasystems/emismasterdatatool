﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM.systems
{
    public class AuditSystemsToolSection
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public double rank { get; set; }
        public String name { get; set; }

        [Ignore]
        public List<AuditSystemsToolSectionItem> items { get; set; }
    }
}

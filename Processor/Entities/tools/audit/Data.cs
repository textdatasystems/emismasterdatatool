﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM
{

    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Data
    {
        [JsonProperty("statusCode")]
        public long StatusCode { get; set; }

        [JsonProperty("statusDescription")]
        public string StatusDescription { get; set; }

        [JsonProperty("data")]
        public Datum[] DataData { get; set; }
    }

    public partial class Datum
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("rank_no")]
        public long RankNo { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("pm_process_tool_section_items")]
        public PmProcessToolSectionItem[] PmProcessToolSectionItems { get; set; }
    }

    public partial class PmProcessToolSectionItem
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("pm_process_tool_section_id")]
        public long PmProcessToolSectionId { get; set; }

        [JsonProperty("rank")]
        public string Rank { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("response_type")]
        public ResponseType ResponseType { get; set; }

        [JsonProperty("yes_rank")]
        public long? YesRank { get; set; }

        [JsonProperty("no_rank")]
        public long? NoRank { get; set; }

        [JsonProperty("na_rank")]
        public long? NaRank { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("role")]
        public string role { get; set; }

    }

    public enum ResponseType { Date, Text, Yesnona };

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                ResponseTypeConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ResponseTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ResponseType) || t == typeof(ResponseType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "date":
                    return ResponseType.Date;
                case "text":
                    return ResponseType.Text;
                case "yesnona":
                    return ResponseType.Yesnona;
            }
            throw new Exception("Cannot unmarshal type ResponseType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ResponseType)untypedValue;
            switch (value)
            {
                case ResponseType.Date:
                    serializer.Serialize(writer, "date");
                    return;
                case ResponseType.Text:
                    serializer.Serialize(writer, "text");
                    return;
                case ResponseType.Yesnona:
                    serializer.Serialize(writer, "yesnona");
                    return;
            }
            throw new Exception("Cannot marshal type ResponseType");
        }

        public static readonly ResponseTypeConverter Singleton = new ResponseTypeConverter();
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM
{
    public class SectionEvaluation
    {

        public long sectionEmisId;
        public String name;
        public String sectionTableFieldName;
        public String sectionTotalFieldName;
        public long sectionYesTotalValue;
        public long sectionNoTotalValue;

        public Dictionary<long,SectionItemEvaluation> sectionItemEvaluations = new Dictionary<long, SectionItemEvaluation>();
        
    }

}

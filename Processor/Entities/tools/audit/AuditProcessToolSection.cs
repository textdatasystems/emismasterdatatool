﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.PM
{
    public class AuditProcessToolSection
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public double rank { get; set; }
        public String name { get; set; }

        [Ignore]
        public List<AuditProcessToolSectionItem> items { get; set; }




    }

}

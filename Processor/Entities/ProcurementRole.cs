﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class ProcurementRole
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int emisId { get; set; }
        public String role { get; set; }
        
        public ProcurementRole(int emisId, String role)
        {
            this.emisId = emisId;
            this.role = role;
        }

    }

}

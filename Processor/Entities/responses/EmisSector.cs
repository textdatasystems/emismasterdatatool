﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisSector
    {
        public int id { get; set; }
        public string sector_name { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }

}

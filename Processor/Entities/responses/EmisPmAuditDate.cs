﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisPmAuditDate
    {
        public int id { get; set; }
        public int pm_activity_id { get; set; }
        public string activity { get; set; }
        public string activity_date { get; set; }
        public string activity_time { get; set; }
        public string tag { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }

    }
}

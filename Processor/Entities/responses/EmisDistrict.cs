﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisDistrict
    {
        public int id { get; set; }
        public string district_name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisPmPlan
    {

        public int id { get; set; }
        public int financial_year_id { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public EmisFinancialYear financial_year { get; set; }
        public List<EmisPmTeam> pm_teams { get; set; }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisPpdaOffice
    {
        public int id { get; set; }
        public string office_name { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }

    }
}

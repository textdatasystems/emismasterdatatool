﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisActivityTeam
    {

        public int id { get; set; }
        public int pm_activity_id { get; set; }
        public int pm_team_id { get; set; }
        public string pm_activity_start_date { get; set; }
        public string pm_activity_end_date { get; set; }
        public int pm_activity_budget { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public EmisPmTeam pm_team { get; set; }

    }
}

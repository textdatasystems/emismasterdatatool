﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class GetProcurementRolesResp
    {

        public bool success { get; set; }
        public List<EmisProcurementRole> payload { get; set; }

    }

}

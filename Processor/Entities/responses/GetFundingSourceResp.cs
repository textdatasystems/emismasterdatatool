﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public  class GetFundingSourceResp
    {
        public bool success { get; set; }
        public List<EmisFundingSource> payload { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisPmSampleFile
    {

        public int id { get; set; }
        public int pm_activity_id { get; set; }
        public string proc_ref_number { get; set; }
        public string subject_of_proc { get; set; }
        public int procurement_method_id { get; set; }
        public string contract_value { get; set; }
        public string provider { get; set; }
        public string date_of_award { get; set; }
        public int user_id { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public EmisProcurementMethod procurement_method { get; set; }

    }

}

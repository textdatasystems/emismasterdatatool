﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisBranch
    {

        public int id { get; set; }
        public String branch_name { get; set; }
        public int String { get; set; }
        public int entity_id { get; set; }
        public String district_id { get; set; }
        public String town { get; set; }
        public String created_at { get; set; }
        public String updated_at { get; set; }

    }
}

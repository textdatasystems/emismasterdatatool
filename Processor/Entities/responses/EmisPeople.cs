﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisPeople
    {
        public int id { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        public String title { get; set; }
        public String gender { get; set; }
        public String email { get; set; }
        public String phone_number { get; set; }
        public string branch_id { get; set; }
        public String person_role_id { get; set; }
        public int entity_id { get; set; }
        public int is_external_facilitator { get; set; }
        public String note { get; set; }
        public String created_at { get; set; }
        public String updated_at { get; set; }
    }

}


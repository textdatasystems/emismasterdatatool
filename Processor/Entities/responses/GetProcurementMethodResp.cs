﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class GetProcurementMethodResp
    {

        public bool success { get; set; }
        public List<EmisProcurementMethod> payload { get; set; }

    }
}

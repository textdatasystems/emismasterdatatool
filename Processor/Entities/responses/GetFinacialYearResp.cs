﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class GetFinacialYearResp
    {
        public bool success { get; set; }
        public List<EmisFinancialYear> payload { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisFinancialYear
    {

        public int id { get; set; }
        public String financial_year { get; set; }
        public String from_date { get; set; }
        public String to_date { get; set; }
        public String created_at { get; set; }
        public String updated_at { get; set; }

    }

}

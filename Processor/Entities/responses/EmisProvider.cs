﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisProvider
    {
        public int id { get; set; }
        public String orgname { get; set; }
        public String source { get; set; }

    }

}

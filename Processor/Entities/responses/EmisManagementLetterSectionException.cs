﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisManagementLetterSectionException
    {
        public int id { get; set; }
        public String exception_title { get; set; }
        public String implication { get; set; }
        public String recommendation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisProcurementMethod
    {
        public int id { get; set; }
        public string method_name { get; set; }
    }
}

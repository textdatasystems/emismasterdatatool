﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisManagementLetterSection
    {
        public int id { get; set; }
        public String section_title { get; set; }
        public List<EmisManagementLetterSectionException> exceptions { get; set; }
    }

}

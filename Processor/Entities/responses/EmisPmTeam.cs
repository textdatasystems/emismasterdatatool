﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisPmTeam
    {

        public int id { get; set; }
        public int pm_plan_id { get; set; }
        public String team_type { get; set; }
        public string team_name { get; set; }
        public int user_id { get; set; }
        public string note { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public EmisUser user { get; set; }
        public List<EmisMember> members { get; set; }

    }
}

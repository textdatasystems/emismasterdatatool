﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisPmActivity
    {

        public int id { get; set; }
        public int entity_id { get; set; }
        public String ppda_office_id { get; set; }
        public int pm_plan_id { get; set; }
        public String pm_activity_type_id { get; set; }
        public String funding_source_id { get; set; }
        public String procurement_budget { get; set; }
        public String oag_opinion { get; set; }
        public String audit_type { get; set; }
        public String last_audited { get; set; }
        public String previous_performance_score { get; set; }
        public String previous_performance_category { get; set; }
        public String created_at { get; set; }
        public String updated_at { get; set; }
        public EmisPmPlan pm_plan { get; set; }
        public EmisEntity entity { get; set; }
        public String funding_source { get; set; }
        //public List<object> pm_audit_files { get; set; }
        public List<EmisPmAuditDate> pm_audit_dates { get; set; }
        public EmisActivityTeam activity_team { get; set; }
        public List<EmisBudgetItemAmount> budget_item_amounts { get; set; }
        public List<EmisPmSampleFile> pm_sample_files { get; set; }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisEntity
    {

        public int id { get; set; }
        public string entity_name { get; set; }
        public string proc_code { get; set; }
        public string physical_address { get; set; }
        public string contact_number { get; set; }
        public string webiste { get; set; }
        public string email { get; set; }
        public int? sector_id { get; set; }
        public int? body_category_id { get; set; }
        public int spend_category_id { get; set; }
        public int entity_type_id { get; set; }
        public int? ppda_office_id { get; set; }
        public int usmid_status { get; set; }
        public int egp_status { get; set; }
        public int gpp_status { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public EmisPpdaOffice ppda_office { get; set; }
        //public BodyCategory body_category { get; set; }
        //public object pm_category { get; set; }
        public EmisSector sector { get; set; }
        //public object audit_category { get; set; }
        //public EntityType entity_type { get; set; }
        //public List<object> entity_allowances_budgets { get; set; }
        //public List<object> entity_fuel_budgets { get; set; }
        //public List<object> entity_audit_history { get; set; }
        public List<EmisPeople> people { get; set; }
        public List<EmisBranch> branches { get; set; }

    }

}

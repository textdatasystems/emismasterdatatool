﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class PmActivitiesGet
    {

        public bool success { get; set; }
        public String message { get; set; }
        public List<EmisPmActivity> payload { get; set; }

    }

}

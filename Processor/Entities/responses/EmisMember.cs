﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisMember
    {

        public int id { get; set; }
        public int pm_team_id { get; set; }
        public int user_id { get; set; }
        public int leader { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public EmisUser user { get; set; }

    }

}

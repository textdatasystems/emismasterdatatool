﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.responses
{
    public class EmisProcurementRole
    {
        public int id { get; set; }
        public String role_name { get; set; }
    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class People
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int  entityEmisId { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String title { get; set; }
        public String roleInProcurement { get; set; }
        public String gender { get; set; }
        public String email { get; set; }
        public String phoneNo { get; set; }
        public String note { get; set; }

        public int branch_id { get; set; }
        public int person_role_id { get; set; }
        public int is_external_facilitator { get; set; }

        public String fullName;
        [Ignore]
        public string FullName
        {
            get
            {
                return firstName + " " + lastName;
            }
            set { fullName = value; }
        }

    }

}

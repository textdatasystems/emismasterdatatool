﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class PmTeam
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int pm_plan_id { get; set; }
        public String team_type { get; set; }
        public String team_name { get; set; }
        public int user_id { get; set; }
        public String note { get; set; }

    }

}

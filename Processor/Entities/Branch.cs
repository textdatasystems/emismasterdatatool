﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class Branch
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int entityEmisId { get; set; }
        public String branchName { get; set; }
        public String district { get; set; }
        public int districtEmisId { get; set; }
        public String town { get; set; }

    }

}

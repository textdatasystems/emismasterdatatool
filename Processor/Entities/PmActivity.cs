﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class PmActivity
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int entityId { get; set; }
        public int ppdaOfficeId { get; set; }
        public int pmPlanId { get; set; }
        public int pmActivityTypeId { get; set; }
        public int fundingSourceId { get; set; }
        public String procurementBudget { get; set; }
        public String oagOpinion { get; set; }

        public String auditType;// { get;set; }

        public string AuditType
        {                       
            get { return String.IsNullOrEmpty(auditType) ? null :
                     System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(auditType.ToLower().Replace("_"," ")); }
            set { auditType = value; }
        }

        public String lastAudited { get; set; }
        public String previousPerformanceScore { get; set; }
        public String previousPerformanceCategory { get; set; }

    }

}

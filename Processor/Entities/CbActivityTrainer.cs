﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class CbActivityTrainer
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int cb_activity_id { get; set; }
        public int trainer_id { get; set; }
        public int trainer_type { get; set; }

    }
}

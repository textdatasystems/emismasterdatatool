﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class CbActivityPeople
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int cb_activity_id { get; set; }
        public int person_id { get; set; }
        public int person_local_id { get; set; }

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmCompStructToolDepts
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public String deptName { get; set; }

        public ApiPmCompStructToolDepts()
        {

        }

        public ApiPmCompStructToolDepts(String tool, String name)
        {
            this.toolReference = tool;
            this.deptName = name;
        }
    }
}

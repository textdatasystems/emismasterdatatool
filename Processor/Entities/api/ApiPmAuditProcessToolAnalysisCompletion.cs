﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessToolAnalysisCompletion
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

        //completion analysis
        public String numberOfBiddersInvited { get; set; }
        public String numberOfBidsReceived { get; set; }
        public String numberOfBidsPassedPreliminaryExam { get; set; }
        public String numberOfTechnicallyResponsiveBids { get; set; }
        public String bidSubmissionRate { get; set; }
        public String bidResponsiveRate { get; set; }

    }

}

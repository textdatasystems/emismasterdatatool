﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessToolAnalysisCompliance
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

       //ComplianceAnalysis
        public String complianceAnalyisYesScore { get; set; }
        public String complianceAnalyisNoScore { get; set; }
        public String complianceAnalyisTotalScore { get; set; }
        public String complianceAnalyisPercentScore { get; set; }

        public String proPlanningAndInitiationYesScore { get; set; }
        public String proPlanningAndInitiationNoScore { get; set; }
        public String proPlanningAndInitiationTotalScore { get; set; }
        public String proPlanningAndInitiationPercentScore { get; set; }

        public String biddingDocumentYesScore { get; set; }
        public String biddingDocumentNoScore { get; set; }
        public String biddingDocumentTotalScore { get; set; }
        public String biddingDocumentPercentScore { get; set; }

        public String biddingYesScore { get; set; }
        public String biddingNoScore { get; set; }
        public String biddingTotalScore { get; set; }
        public String biddingPercentScore { get; set; }

        public String bidEvaluationYesScore { get; set; }
        public String bidEvaluationNoScore { get; set; }
        public String bidEvaluationTotalScore { get; set; }
        public String bidEvaluationPercentScore { get; set; }

        public String procContractingYesScore { get; set; }
        public String procContractingNoScore { get; set; }
        public String procContractingTotalScore { get; set; }
        public String procContractingPercentScore { get; set; }

        public String contractManagementYesScore { get; set; }
        public String contractManagementNoScore { get; set; }
        public String contractManagementTotalScore { get; set; }
        public String contractManagementPercentScore { get; set; }

        public String recordsKeepingYesScore { get; set; }
        public String recordsKeepingNoScore { get; set; }
        public String recordsKeepingTotalScore { get; set; }
        public String recordsKeepingPercentScore { get; set; }               

        public String procProcessComplianceLevel { get; set; }
             
    }

}

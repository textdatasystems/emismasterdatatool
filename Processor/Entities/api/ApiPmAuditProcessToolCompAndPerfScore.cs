﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessToolCompAndPerfScore
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public long procurementProcessComplianceLevel { get; set; }
        public long procureRatio { get; set; }
        public long completionRatio { get; set; }
        public long paymentRatio { get; set; }
        public long numberOfBidsReceived { get; set; }
        public long bidSubmissionRatio { get; set; }
        public long bidResponsiveRate { get; set; }
        public long planRatio { get; set; }
        public long costRatio { get; set; }

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmCompProcessToolSection
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public long sectionEmisId { get; set; }
        public String toolReference { get; set; }
        public String name { get; set; }

        public long case1Yes { get; set; }
        public long case1No { get; set; }
        public long case1AvgPercent { get; set; }

        public long case2Yes { get; set; }
        public long case2No { get; set; }
        public long case2AvgPercent { get; set; }

        public long case3Yes { get; set; }
        public long case3No { get; set; }
        public long case3AvgPercent { get; set; }

        public long case4Yes { get; set; }
        public long case4No { get; set; }
        public long case4AvgPercent { get; set; }

        public long case5Yes { get; set; }
        public long case5No { get; set; }
        public long case5AvgPercent { get; set; }

        public long case6Yes { get; set; }
        public long case6No { get; set; }
        public long case6AvgPercent { get; set; }

        public long case7Yes { get; set; }
        public long case7No { get; set; }
        public long case7AvgPercent { get; set; }

        public long case8Yes { get; set; }
        public long case8No { get; set; }
        public long case8AvgPercent { get; set; }

        public long case9Yes { get; set; }
        public long case9No { get; set; }
        public long case9AvgPercent { get; set; }

        public long case10Yes { get; set; }
        public long case10No { get; set; }
        public long case10AvgPercent { get; set; }

        [Ignore]
        public List<ApiPmCompProcessToolSectionItem> items { get; set; }

    }

}

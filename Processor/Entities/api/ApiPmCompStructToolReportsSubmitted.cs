﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmCompStructToolReportsSubmitted
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public String report { get; set; }
        public String dateSubmitted { get; set; }

        public ApiPmCompStructToolReportsSubmitted()
        {

        }

        public ApiPmCompStructToolReportsSubmitted(String tool, String report, String date)
        {
            this.toolReference = tool;
            this.report = report;
            this.dateSubmitted = date;
        }


    }
}

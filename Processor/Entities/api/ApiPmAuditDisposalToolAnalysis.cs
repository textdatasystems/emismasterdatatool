﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditDisposalToolAnalysis
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public String scorePlan { get; set; }
        public String scoreInitiation { get; set; }
        public String scorePublicBidding { get; set; }
        public String scorePublicAuction { get; set; }
        public String scoreDirectNegotiations { get; set; }
        public String scorePublicOfficers { get; set; }
        public String scoreDestruction { get; set; }
        public String scoreConversion { get; set; }
        public String scoreTradeIn { get; set; }
        public String scoreTransfer { get; set; }
        public String scoreDonation { get; set; }
        public String scoreDelivery { get; set; }        
        public String scoreRecords { get; set; }

        public String scoreAverage { get; set; }

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{

    public class ApiPmAuditSystemsTool
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolName { get; set; }
        public String toolReference { get; set; }
        public int activityId { get; set; }
        public String createdOn { get; set; }
        public String updatedOn { get; set; }
        public String createdBy { get; set; }
        public String toolType { get; set; }

        [Ignore]
        public ApiPmAuditSystemsToolIndentification identification { get; set; }
        [Ignore]
        public ApiPmAuditSystemsToolAnalysis analysis { get; set; }
        [Ignore]
        public ApiPmAuditSystemsToolFindingConclusionsAndRecommendations findings { get; set; }
        [Ignore]
        public List<ApiPmAuditSystemsToolSection> sections { get; set; }

    }

}

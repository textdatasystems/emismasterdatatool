﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessToolAnalysisCost
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

        //cost analysis
        public String procPlanTotalCostValue { get; set; }
        public String accountingOfficerMarketPrice { get; set; }
        public String bestBidder1Name { get; set; }
        public String bestBidder1Price { get; set; }
        public String bestBidder2Name { get; set; }
        public String bestBidder2Price { get; set; }
        public String bestBidder3Name { get; set; }
        public String bestBidder3Price { get; set; }
        public String bestBidder4Name { get; set; }
        public String bestBidder4Price { get; set; }
        public String bestBidder5Name { get; set; }
        public String bestBidder5Price { get; set; }
        public String bestBidder6Name { get; set; }
        public String bestBidder6Price { get; set; }
        public String bestBidder7Name { get; set; }
        public String bestBidder7Price { get; set; }

        public String totalContractAwardPrice { get; set; }
        public String finalContractCostOnCompletion { get; set; }
        public String costOverunAmount { get; set; }
        public String budgetVarianceAmount { get; set; }
        public String budgetVariancePercentage { get; set; }
        public String costOVerunPercentage { get; set; }
        public String planRatio { get; set; }
        public String costRatio { get; set; }

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{

    public class ApiPmCompStructTool
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolName { get; set; }
        public String toolReference { get; set; }
        public int activityId { get; set; }
        public String createdOn { get; set; }
        public String updatedOn { get; set; }
        public String email { get; set; }
        public String toolType { get; set; }
        


[Ignore]
        public ApiPmCompStructToolGeneralInfo generalInfo { get; set; }
        [Ignore]
        public List<ApiPmCompStructToolContractsCommitteMembers> contractsCommitteeMembers { get; set; }
        [Ignore]
        public List<ApiPmCompStructToolPduMembers> pduMembers { get; set; }
        [Ignore]
        public List<ApiPmCompStructToolDepts> departments { get; set; }
        [Ignore]
        public List<ApiPmCompStructToolReportsSubmitted> reports { get; set; }
        [Ignore]
        public ApiPmCompStructToolSpend spend { get; set; }

    }
}

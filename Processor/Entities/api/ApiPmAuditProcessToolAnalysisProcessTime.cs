﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessToolAnalysisProcessTime
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

        //process analysis times
        public String initiationStartDelayTime { get; set; }
        public String requistionTime { get; set; }
        public String bidSubmissionPeriod { get; set; }
        public String bidEvaluationTime { get; set; }
        public String contractAwardPeriod { get; set; }
        public String noBEBDelayPeriod { get; set; }
        public String contractingTime { get; set; }
        public String actualProcTime { get; set; }
        public String plannedProcTime { get; set; }
        public String procTimeOverun { get; set; }
        public String contractualCompletionTime { get; set; }
        public String actualCompletionTime { get; set; }
        public String completionTimeOverun { get; set; }
        public String contractualPaymentPeriodInDays { get; set; }
        public String actualPaymentPeriodInDays { get; set; }
        public String paymentPeriodOverun { get; set; }
        public String procTimeOverunPercentage { get; set; }
        public String completionTimeOverunPercentage { get; set; }
        public String paymentPeriodOverunPercentage { get; set; }
        public String procureRatio { get; set; }
        public String completionRatio { get; set; }
        public String paymentRatio { get; set; }


    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmCompStructToolSpend
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

        public String macroJan { get; set; }
        public String macroFeb { get; set; }
        public String macroMar { get; set; }
        public String macroApr { get; set; }
        public String macroMay { get; set; }
        public String macroJun { get; set; }
        public String macroJul { get; set; }
        public String macroAug { get; set; }
        public String macroSept { get; set; }
        public String macroOct { get; set; }
        public String macroNov { get; set; }
        public String macroDec { get; set; }

        public String microJan { get; set; }
        public String microFeb { get; set; }
        public String microMar { get; set; }
        public String microApr { get; set; }
        public String microMay { get; set; }
        public String microJun { get; set; }
        public String microJul { get; set; }
        public String microAug { get; set; }
        public String microSept { get; set; }
        public String microOct { get; set; }
        public String microNov { get; set; }
        public String microDec { get; set; }

        public String macroTotal { get; set; }
        public String microTotal { get; set; }
        public String totalMacroAndMicro { get; set; }
        
    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmCompStructToolGeneralInfo
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public String nameOfEntity { get; set; }
        public String sector { get; set; }
        public String dateOfOperationzation { get; set; }
        public String numberOfStaff { get; set; }
        public String geographicalSpread { get; set; }
        public String totalEntityBudget { get; set; }
        public String totalProcBugdet { get; set; }
        public String volOfProcBudgetAsPercentOfPdeBudget { get; set; }
        public String fundingSource { get; set; }
        public String previousRatings { get; set; }
        public String nameOfAccountingOfficer { get; set; }
        public String hasSpaceAndInternet { get; set; }
        public String areContractsCommiteeMembersApproved { get; set; }
        public String hasListOfPrequalifiedProviders { get; set; }
        public String hasApprovedProcPlan { get; set; }
        public String usesFrameworkContracts { get; set; }
        public String caseHaveStandardRefNumbering { get; set; }
        public String hasAbridgedVersionOfProcAds { get; set; }
        public String nameOfInternalAuditors { get; set; }
        public String auditorHasUnderstaningOfRole { get; set; }
        public String wereReportsSubmittedOnTime { get; set; }
        public String uploadsDataOnGpp { get; set; }
        public String reportsSubmittedInStandardFormat { get; set; }
        public String ppdaQueriesResolved { get; set; }
        public String hasDisposedOffObsoleteItems { get; set; }
        public String avgProcStrucutureScore { get; set; }
        public String reviewBy { get; set; }
        public String dateOfReview { get; set; }

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessToolSection
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public long sectionEmisId { get; set; }
        public String toolReference { get; set; }
        public String name { get; set; }
        public long yesTotal { get; set; }
        public long noTotal { get; set; }

        //
        //below fields are not saved to the db as part of this object, they are just used for retrieving data
        //from the individual tables
        //
        [Ignore]
        public List<ApiPmAuditProcessToolSectionItem> items { get; set; }

    }

}

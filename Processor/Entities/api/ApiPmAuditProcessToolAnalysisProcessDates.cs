﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessToolAnalysisProcessDates
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

        //process dates
        public String startDatePerProcPlan { get; set; }
        public String dateOfInitiationRequest { get; set; }
        public String dateOfBidInvitationNote { get; set; }
        public String dateOfBidSubmission { get; set; }
        public String dateOfBidEvaluationReport { get; set; }
        public String dateOfContractAward { get; set; }
        public String dateOfNoticeOfBestBidder { get; set; }
        public String dateOfExpiryOfNoBEB { get; set; }
        public String dateOfContractSigning { get; set; }
        public String dateOfContractSigningPerProcPlan { get; set; }
        public String contractualCompletionDate { get; set; }
        public String actualCompletionDate { get; set; }
        public String dateOfCertificationOfInvoice { get; set; }
        public String dateInvoicePaid { get; set; }

    }

}

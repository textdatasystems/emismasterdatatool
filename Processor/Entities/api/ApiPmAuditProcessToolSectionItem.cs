﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessToolSectionItem
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

        public long sectionEmisId{ get; set; }
        public long itemEmisId { get; set; }       
        public string evaluationFieldType { get; set; }        
        public string evaluationFieldValue { get; set; }
        public string evaluationFieldRole { get; set; }
        public String finding { get; set; }

        public string description { get; set; }
        public String exception { get; set; }
        public long managementLetterSectionId { get; set; }
        public long managementLetterSectionItemId { get; set; }

        public long yesRank { get; set; }
        public long noRank { get; set; }
        public long naRank { get; set; }

    }
}

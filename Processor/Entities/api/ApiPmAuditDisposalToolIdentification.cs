﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditDisposalToolIdentification
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public String nameOfEntity { get; set; }
        public String referenceNo { get; set; }        
        public String auditPeriod { get; set; }
        public String assetDescription { get; set; }
        public String nameOfBuyer { get; set; }
        public String disposalValue { get; set; }
        public String valuationAmount { get; set; }
        public String disposalMethod { get; set; }

    }

}

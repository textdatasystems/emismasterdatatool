﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmCompProcessToolAnalysis
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

        //records
        public String RAvgScore { get; set; }
        public String RCase10 { get; set; }
        public String RCase9 { get; set; }
        public String RCase8 { get; set; }
        public String RCase7 { get; set; }
        public String RCase6 { get; set; }
        public String RCase5 { get; set; }
        public String RCase4 { get; set; }
        public String RCase3 { get; set; }
        public String RCase2 { get; set; }
        public String RCase1 { get; set; }

        //contract managegemt
        public String CMAvgScore { get; set; }
        public String CMCase10 { get; set; }
        public String CMCase9 { get; set; }
        public String CMCase8 { get; set; }
        public String CMCase7 { get; set; }
        public String CMCase6 { get; set; }
        public String CMCase5 { get; set; }
        public String CMCase4 { get; set; }
        public String CMCase3 { get; set; }
        public String CMCase2 { get; set; }
        public String CMCase1 { get; set; }


        //contracting
        public String CAvgScore { get; set; }
        public String CCase10 { get; set; }
        public String CCase9 { get; set; }
        public String CCase8 { get; set; }
        public String CCase7 { get; set; }
        public String CCase6 { get; set; }
        public String CCase5 { get; set; }
        public String CCase4 { get; set; }
        public String CCase3 { get; set; }
        public String CCase2 { get; set; }
        public String CCase1 { get; set; }


        //evaluation
        public String EAvgScore { get; set; }
        public String ECase10 { get; set; }
        public String ECase9 { get; set; }
        public String ECase8 { get; set; }
        public String ECase7 { get; set; }
        public String ECase6 { get; set; }
        public String ECase5 { get; set; }
        public String ECase4 { get; set; }
        public String ECase3 { get; set; }
        public String ECase2 { get; set; }
        public String ECase1 { get; set; }


        //biddng
        public String BAvgScore { get; set; }
        public String BCase10 { get; set; }
        public String BCase9 { get; set; }
        public String BCase8 { get; set; }
        public String BCase7 { get; set; }
        public String BCase6 { get; set; }
        public String BCase5 { get; set; }
        public String BCase4 { get; set; }
        public String BCase3 { get; set; }
        public String BCase2 { get; set; }
        public String BCase1 { get; set; }

        //bid document
        public String BDAvgScore { get; set; }
        public String BDCase10 { get; set; }
        public String BDCase9 { get; set; }
        public String BDCase8 { get; set; }
        public String BDCase7 { get; set; }
        public String BDCase6 { get; set; }
        public String BDCase5 { get; set; }
        public String BDCase4 { get; set; }
        public String BDCase3 { get; set; }
        public String BDCase2 { get; set; }
        public String BDCase1 { get; set; }

        //planning and initiation
        public String PIAvgScore { get; set; }
        public String PICase10 { get; set; }
        public String PICase9 { get; set; }
        public String PICase8 { get; set; }
        public String PICase7 { get; set; }
        public String PICase6 { get; set; }
        public String PICase5 { get; set; }
        public String PICase4 { get; set; }
        public String PICase3 { get; set; }
        public String PICase2 { get; set; }
        public String PICase1 { get; set; }

        public String AvgAvgScore { get; set; }

        //procurement structure
        public String PSAvgScore { get; set; }

        public String AvgCase1 { get; set; }
        public String AvgCase2 { get; set; }
        public String AvgCase3 { get; set; }
        public String AvgCase4 { get; set; }
        public String AvgCase5 { get; set; }
        public String AvgCase6 { get; set; }
        public String AvgCase7 { get; set; }
        public String AvgCase8 { get; set; }
        public String AvgCase9 { get; set; }
        public String AvgCase10 { get; set; }

        public String MirrorPIAvgScore { get; set; }
        public String MirrorBDAvgScore { get; set; }
        public String MirrorBAvgScore { get; set; }
        public String MirrorEAvgScore { get; set; }
        public String MirrorCAvgScore { get; set; }
        public String MirrorCMAvgScore { get; set; }
        public String MirrorRAvgScore { get; set; }
        public String MirrorPSAvgScore { get; set; }


    }

}

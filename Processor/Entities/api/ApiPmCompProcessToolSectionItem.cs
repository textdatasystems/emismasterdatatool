﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{

    public class ApiPmCompProcessToolSectionItem
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

        public long sectionEmisId { get; set; }
        public long itemEmisId { get; set; }

        public long yesRank { get; set; }
        public long noRank { get; set; }
        public long naRank { get; set; }

        public String case1FieldValue{ get; set; }
        public String case2FieldValue{ get; set; }
        public String case3FieldValue{ get; set; }
        public String case4FieldValue{ get; set; }
        public String case5FieldValue{ get; set; }
        public String case6FieldValue{ get; set; }
        public String case7FieldValue{ get; set; }
        public String case8FieldValue{ get; set; }
        public String case9FieldValue{ get; set; }
        public String case10FieldValue{ get; set; }
        
    }


}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessToolIdentification
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public String nameOfEntity { get; set; }
        public String sector { get; set; }
        public String auditPeriod { get; set; }
        public String referenceNo { get; set; }
        public String contractDesc { get; set; }
        public String nameOfProvider { get; set; }
        public String totalContractValue { get; set; }
        public String typeOfProvider { get; set; }
        public String processStage { get; set; }
        public String procurementMethod { get; set; }
        public String consultantSelectionMethod { get; set; }
        public String procurementCategory { get; set; }
        public String bidderInvivationMethod { get; set; }
        public String bidSubmissionMethod { get; set; }
        public String contractType { get; set; }

    }
}

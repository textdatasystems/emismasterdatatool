﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditSystemsToolIndentification
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public String nameOfEntity { get; set; }
        public String town { get; set; }
        public String sector { get; set; }        
        public String auditPeriod { get; set; }

    }

}

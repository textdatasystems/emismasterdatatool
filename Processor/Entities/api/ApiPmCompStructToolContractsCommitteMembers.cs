﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmCompStructToolContractsCommitteMembers
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public String name { get; set; }
        public String positionOnCommittee { get; set; }
        public String positionInPde { get; set; }
        public String approvalDate { get; set; }

        public ApiPmCompStructToolContractsCommitteMembers()
        {

        }

        public ApiPmCompStructToolContractsCommitteMembers(String tool, String name, String position, String positionInPde, String date)
        {
            this.toolReference = tool;
            this.name = name;
            this.positionOnCommittee = position;
            this.positionInPde = positionInPde;
            this.approvalDate = date;
        }

    }
}

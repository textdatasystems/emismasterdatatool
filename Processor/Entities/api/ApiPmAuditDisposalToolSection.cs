﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditDisposalToolSection
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public long sectionEmisId { get; set; }
        public String toolReference { get; set; }
        public String name { get; set; }
        public long yesTotal { get; set; }
        public long noTotal { get; set; }

        [Ignore]
        public List<ApiPmAuditDisposalToolSectionItem> items { get; set; }

    }
}

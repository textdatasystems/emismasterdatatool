﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditSystemsToolFindingConclusionsAndRecommendations
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }

        public String accountingOfficerPositiveFindings { get; set; }
        public String accountingOfficerNegativeFindings { get; set; }
        public String accountingOfficerRecommendations { get; set; }

        public String pduPositiveFindings { get; set; }
        public String pduNegativeFindings { get; set; }
        public String pduRecommendations { get; set; }


        public String contractsCommitteePositiveFindings { get; set; }
        public String contractsCommitteeNegativeFindings { get; set; }
        public String contractsCommitteeRecommendations { get; set; }


        public String userDeptPositiveFindings { get; set; }
        public String userDeptNegativeFindings { get; set; }
        public String userDeptRecommendations { get; set; }

        public String evalCommitteePositiveFindings { get; set; }
        public String evalCommitteeNegativeFindings { get; set; }
        public String evalCommitteeRecommendations { get; set; }

        public String internalControlsPositiveFindings { get; set; }
        public String internalControlsNegativeFindings { get; set; }
        public String internalControlsRecommendations { get; set; }

        public String procPlanningPositiveFindings { get; set; }
        public String procPlanningNegativeFindings { get; set; }
        public String procPlanningRecommendations { get; set; }

        public String recordsReportsPositiveFindings { get; set; }
        public String recordsReportsNegativeFindings { get; set; }
        public String recordsReportsRecommendations { get; set; }

        public String complainceScorePositiveFindings { get; set; }
        public String complainceScoreNegativeFindings { get; set; }
        public String complainceScoreRecommendations { get; set; }

        public String budgetAbsorptionPositiveFindings { get; set; }
        public String budgetAbsorptionNegativeFindings { get; set; }
        public String budgetAbsorptionRecommendations { get; set; }

        public String openBiddingvaluePositiveFindings { get; set; }
        public String openBiddingvalueNegativeFindings { get; set; }
        public String openBiddingvalueRecommendations { get; set; }
    }

}

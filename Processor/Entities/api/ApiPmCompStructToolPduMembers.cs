﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmCompStructToolPduMembers
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }
        public String name { get; set; }
        public String qualification { get; set; }
        public String positionOnUnit { get; set; }
        public String appointmentDate { get; set; }

        public ApiPmCompStructToolPduMembers()
        {
        }

        public ApiPmCompStructToolPduMembers(String tool, String name, String qualification, String position, String date)
        {
            this.toolReference = tool;
            this.name = name;
            this.qualification = qualification;
            this.positionOnUnit = position;
            this.appointmentDate = date;
        }

    }
}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditSystemsToolAnalysis
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolReference { get; set; }


        public String accountingOfficerYesTotal { get; set; }
        public String accountingOfficerNoTotal { get; set; }
        public String accountingOfficerTotal { get; set; }
        public String accountingOfficerPercentScore { get; set; }

        public String pduYesTotal { get; set; }
        public String pduNoTotal { get; set; }
        public String pduTotal { get; set; }
        public String pduPercentScore { get; set; }
        
        public String contractsCommitteYesTotal { get; set; }
        public String contractsCommitteNoTotal { get; set; }
        public String contractsCommitteTotal { get; set; }
        public String contractsCommittePercentScore { get; set; }

        public String userDeptsYesTotal { get; set; }
        public String userDeptsNoTotal { get; set; }
        public String userDeptsTotal { get; set; }
        public String userDeptsPercentScore { get; set; }

        public String evalCommitteeYesTotal { get; set; }
        public String evalCommitteeNoTotal { get; set; }
        public String evalCommitteeTotal { get; set; }
        public String evalCommitteePercentScore { get; set; }

        public String internalControlsYesTotal { get; set; }
        public String internalControlsNoTotal { get; set; }
        public String internalControlsTotal { get; set; }
        public String internalControlsPercentScore { get; set; }

        public String complianceLevelPercentScore { get; set; }

        public String totalProcBudget { get; set; }
        public String totalActualProcs { get; set; }
        public String procBudgetAbsorptionRate { get; set; }


        public String totalValueOfProcs { get; set; }
        public String totalvalueOfOpenBidContracts { get; set; }
        public String proportionValueOfContractsOnOpenBid { get; set; }

    }

}

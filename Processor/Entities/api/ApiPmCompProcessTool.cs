﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmCompProcessTool
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolName { get; set; }
        public String toolReference { get; set; }
        public int activityId { get; set; }
        public int sampleFileLocalId { get; set; }
        public String createdOn { get; set; }
        public String updatedOn { get; set; }
        public String email { get; set; }
        public String toolType { get; set; }
        

[Ignore]
        public ApiPmCompProcessToolAnalysis analysis { get; set; }
        [Ignore]
        public List<ApiPmCompProcessToolSection> sections { get; set; }

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditProcessTools
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolName { get; set; }
        public String toolReference { get; set; }
        public int activityId { get; set; }
        public int sampleFileLocalId { get; set; }
        public String createdOn { get; set; }
        public String updatedOn { get; set; }
        public String createdBy { get; set; }
        public String toolType { get; set; }

        //
        //below fields are not saved to the db as part of this object, they are just used for retrieving data
        //from the individual tables
        //
        [Ignore]
        public ApiPmAuditProcessToolIdentification identification { get; set; }       
        [Ignore]
        public ApiPmAuditProcessToolCompAndPerfScore compAndPerfScore { get; set; }
        [Ignore]
        public List<ApiPmAuditProcessToolSection> sections { get; set; }

        [Ignore]
        public ApiPmAuditProcessToolAnalysisCompliance analysisCompliance { get; set; }

        [Ignore]
        public ApiPmAuditProcessToolAnalysisCompletion analysisCompletion { get; set; }

        [Ignore]
        public ApiPmAuditProcessToolAnalysisCost analysisCost { get; set; }

        [Ignore]
        public ApiPmAuditProcessToolAnalysisProcessDates analysisProcessDates { get; set; }

        [Ignore]
        public ApiPmAuditProcessToolAnalysisProcessTime analysisProcessTimes { get; set; }

    }
}

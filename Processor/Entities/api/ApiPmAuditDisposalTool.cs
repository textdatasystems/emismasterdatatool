﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities.api
{
    public class ApiPmAuditDisposalTool
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String toolName { get; set; }
        public String toolReference { get; set; }
        public int activityId { get; set; }
        public String createdOn { get; set; }
        public String updatedOn { get; set; }
        public String createdBy { get; set; }
        public String toolType { get; set; }


        [Ignore]
        public ApiPmAuditDisposalToolIdentification identification { get; set; }

        [Ignore]
        public ApiPmAuditDisposalToolAnalysis analysis { get; set; }

        [Ignore]
        public List<ApiPmAuditDisposalToolSection> sections { get; set; }

    }

}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class Entity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public string entityName { get; set; }
        public string procCode { get; set; }
        public string physicalAddress { get; set; }
        public string contactNumber { get; set; }
        public string website { get; set; }
        public string email { get; set; }
        public bool egpStatus { get; set; }
        public bool gppStatus { get; set; }
        public bool usmidStatus { get; set; }

    }

}

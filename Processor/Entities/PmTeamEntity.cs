﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.Entities
{
    public class PmTeamEntity
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int emisId { get; set; }
        public int pm_activity_id { get; set; }
        public int pm_team_id { get; set; }
        public String pm_activity_start_date { get; set; }
        public String pm_activity_end_date { get; set; }
        public double pm_activity_budget { get; set; }

    }

}

﻿using Processor.Entities;
using Processor.Entities.api;
using Processor.Entities.custom;
using Processor.Entities.PM;
using Processor.Entities.PM.compliance.process;
using Processor.Entities.PM.disposal;
using Processor.Entities.PM.systems;
using SQLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.ControlObjects
{
    public class DatabaseHandler
    {

        public static SQLiteConnection dbConnection()
        {

            SQLiteConnection sqlConnection;

            if (!(new DatabaseHandler().databaseExists(Globals.DATABASE_PATH)))
            {

            }


            sqlConnection = new SQLiteConnection(Globals.DATABASE_PATH, true,Globals.DATABASE_PASS_PHRASE);


            if (isApplicationFirstRun())
            {

                //create the tables
                createTheTables(sqlConnection);

            }

            return sqlConnection;

        }

       
        private static void createTheTables(SQLiteConnection sqlConnection)
        {

            sqlConnection.CreateTable<SystemSetting>();
            sqlConnection.CreateTable<ServerSyncStatus>();
            sqlConnection.CreateTable<ManagementLetterSection>();
            sqlConnection.CreateTable<ManagementLetterSectionException>();
            sqlConnection.CreateTable<Region>();
            sqlConnection.CreateTable<District>();
            sqlConnection.CreateTable<FinacialYear>(); 
            sqlConnection.CreateTable<Provider>(); 
            sqlConnection.CreateTable<ProcurementRole>();
            sqlConnection.CreateTable<ProcurementMethod>();
            sqlConnection.CreateTable<Entity>();
            sqlConnection.CreateTable<Branch>();
            sqlConnection.CreateTable<People>();

            sqlConnection.CreateTable<PmActivity>();
            sqlConnection.CreateTable<CbActivity>();
            sqlConnection.CreateTable<ActivityType>();
            sqlConnection.CreateTable<CbPlan>();
            sqlConnection.CreateTable<PmPlan>();
            sqlConnection.CreateTable<FundingSource>();
            sqlConnection.CreateTable<PpdaOffice>();
            sqlConnection.CreateTable<User>();

            sqlConnection.CreateTable<Module>();
            sqlConnection.CreateTable<CbActivityModule>();
            sqlConnection.CreateTable<CbActivityTrainer>();
            sqlConnection.CreateTable<CbExpectedOutcome>();
            sqlConnection.CreateTable<CbActivityPeople>();

            sqlConnection.CreateTable<BudgetItem>();
            sqlConnection.CreateTable<BudgetItemAmount>();

            sqlConnection.CreateTable<PmTeam>();
            sqlConnection.CreateTable<PmTeamMember>();
            sqlConnection.CreateTable<PmTeamEntity>();
            sqlConnection.CreateTable<PmAuditDate>();
            sqlConnection.CreateTable<PmAuditSampleFile>();
            sqlConnection.CreateTable<PmAuditMeetingRole>();
            sqlConnection.CreateTable<PmMeetingAttendances>();


            //PM Tool tables
            createPmToolsTables(sqlConnection);
            

        }

       
        private static void createPmToolsTables(SQLiteConnection sqlConnection)
        {

            //tables for the Process tool
            sqlConnection.CreateTable<AuditProcessToolSection>();
            sqlConnection.CreateTable<AuditProcessToolSectionItem>();

            //tables for filled in audit process tools
            sqlConnection.CreateTable<ApiPmAuditProcessTools>();
            sqlConnection.CreateTable<ApiPmAuditProcessToolSection>();
            sqlConnection.CreateTable<ApiPmAuditProcessToolSectionItem>();
            sqlConnection.CreateTable<ApiPmAuditProcessToolIdentification>();
            sqlConnection.CreateTable<ApiPmAuditProcessToolCompAndPerfScore>();
            //analysis tab tables
            sqlConnection.CreateTable<ApiPmAuditProcessToolAnalysisCompliance>();
            sqlConnection.CreateTable<ApiPmAuditProcessToolAnalysisCompletion>();
            sqlConnection.CreateTable<ApiPmAuditProcessToolAnalysisCost>();
            sqlConnection.CreateTable<ApiPmAuditProcessToolAnalysisProcessDates>();
            sqlConnection.CreateTable<ApiPmAuditProcessToolAnalysisProcessTime>();


            //tables for the Systems tool
            sqlConnection.CreateTable<AuditSystemsToolSection>();
            sqlConnection.CreateTable<AuditSystemsToolSectionItem>();

            //tables for filled in audit system tools
            sqlConnection.CreateTable<ApiPmAuditSystemsTool>();
            sqlConnection.CreateTable<ApiPmAuditSystemsToolSection>();
            sqlConnection.CreateTable<ApiPmAuditSystemsToolSectionItem>();
            sqlConnection.CreateTable<ApiPmAuditSystemsToolIndentification>();
            sqlConnection.CreateTable<ApiPmAuditSystemsToolAnalysis>();
            sqlConnection.CreateTable<ApiPmAuditSystemsToolSection>();
            sqlConnection.CreateTable<ApiPmAuditSystemsToolFindingConclusionsAndRecommendations>();


            //tables for the disposal tool
            sqlConnection.CreateTable<AuditDisposalToolSection>();
            sqlConnection.CreateTable<AuditDisposalToolSectionItem>();

            //tables for filled in audit disposal tools
            sqlConnection.CreateTable<ApiPmAuditDisposalTool>();
            sqlConnection.CreateTable<ApiPmAuditDisposalToolSection>();
            sqlConnection.CreateTable<ApiPmAuditDisposalToolSectionItem>();
            sqlConnection.CreateTable<ApiPmAuditDisposalToolIdentification>();
            sqlConnection.CreateTable<ApiPmAuditDisposalToolAnalysis>();
            sqlConnection.CreateTable<ApiPmAuditDisposalToolSection>();



            //
            // compliance tables
            //

            //tables for compliance process tool
            sqlConnection.CreateTable<ComplianceProcessToolSection>();
            sqlConnection.CreateTable<ComplianceProcessToolSectionItem>();

            //tables for filled compliance process tool
            sqlConnection.CreateTable<ApiPmCompProcessTool>();
            sqlConnection.CreateTable<ApiPmCompProcessToolAnalysis>();
            sqlConnection.CreateTable<ApiPmCompProcessToolSection>();
            sqlConnection.CreateTable<ApiPmCompProcessToolSectionItem>();

            //tables for filled compliance structure tol
            sqlConnection.CreateTable<ApiPmCompStructTool>();
            sqlConnection.CreateTable<ApiPmCompStructToolGeneralInfo>();
            sqlConnection.CreateTable<ApiPmCompStructToolDepts>();
            sqlConnection.CreateTable<ApiPmCompStructToolContractsCommitteMembers>();
            sqlConnection.CreateTable<ApiPmCompStructToolPduMembers>();
            sqlConnection.CreateTable<ApiPmCompStructToolReportsSubmitted>();
            sqlConnection.CreateTable<ApiPmCompStructToolSpend>();

        }

        internal void updatePmActivityPmTeamEntity(SQLiteConnection dbConn, PmTeamEntity teamEntity, int activityId)
        {
            
            if(teamEntity == null)
            {
                return;
            }

            var existentPmTeamEntity = dbConn.Table<PmTeamEntity>().Where(v => v.pm_activity_id == activityId).FirstOrDefault();
            if(existentPmTeamEntity != null)
            {
                teamEntity.Id = existentPmTeamEntity.Id;
                dbConn.Update(teamEntity);
            }
            else
            {
                dbConn.Insert(teamEntity);
            }            
            
        }

        public static int addCbActivityPeople(SQLiteConnection sQLiteConnection, CbActivityPeople activityPeople)
        {
            sQLiteConnection.Insert(activityPeople);
            return activityPeople.Id;
        }

        public static int addPmAuditSampleFile(SQLiteConnection db, PmAuditSampleFile sample)
        {
            db.Insert(sample);
            return sample.Id;
        }

        public static void deletePerson(SQLiteConnection db, int personLocalId)
        {
            db.Delete<People>(personLocalId);
        }

        public Entity findEntityByEmisId(SQLiteConnection sQLiteConnection, int entityEmisId)
        {
            var entity = sQLiteConnection.Table<Entity>().Where(v => v.emisId == entityEmisId).FirstOrDefault();
            return entity;
        }

        private static bool isApplicationFirstRun()
        {
            return true;
        }

        private bool databaseExists(string databasePath)
        {
            return File.Exists(databasePath);
        }


        public static int addEntity(SQLiteConnection db, Entity entity)
        {

            db.Insert(entity);
            return entity.Id;
        }

        internal void updatePmActivities(SQLiteConnection sqlConnection, List<PmActivity> activities)
        {
            if (activities.Count == 0) return;

            //delete all existing data
            sqlConnection.DeleteAll<PmActivity>();

            //insert new data
            sqlConnection.InsertAll(activities);

        }


        internal void updatePmActivityAuditDates(SQLiteConnection sqlConnection, int activityId, List<PmAuditDate> auditDates)
        {

            if (auditDates.Count == 0) return;

            sqlConnection.Table<PmAuditDate>().Where(v => v.pm_activity_id == activityId).Delete();
            sqlConnection.InsertAll(auditDates);

        }

        internal void updatePmActivityBugdetItems(SQLiteConnection sqlConnection, int activityId, List<BudgetItemAmount> budgetItemAmounts)
        {

            if (budgetItemAmounts.Count == 0) return;

            sqlConnection.Table<BudgetItemAmount>().Where(v => v.commentable_id == activityId).Where(v => v.commentable_type == Globals.ACTIVITY_TYPE_PM).Delete();
            sqlConnection.InsertAll(budgetItemAmounts);

        }

        internal void updatePmActivityAuditSampleFiles(SQLiteConnection sqlConnection, int activityId, List<PmAuditSampleFile> sampleFiles)
        {

            if (sampleFiles.Count == 0) return;

            sqlConnection.Table<PmAuditSampleFile>().Where(v => v.pm_activity_id == activityId).Delete();
            sqlConnection.InsertAll(sampleFiles);

        }

        internal void updateCbActivities(SQLiteConnection sqlConnection, List<CbActivity> activities)
        {
            if (activities.Count == 0) return;

            //delete all existing data
            sqlConnection.DeleteAll<CbActivity>();

            //insert new data
            sqlConnection.InsertAll(activities);

        }

        public List<Entity> allEntities(SQLiteConnection dbConn)
        {

            var entities = dbConn.Table<Entity>().ToList();
            return entities;

        }

        internal void updateEntities(SQLiteConnection sqlConnection, List<Entity> entities)
        {
            if (entities.Count == 0) return;

            sqlConnection.DeleteAll<Entity>();
            sqlConnection.InsertAll(entities);
        }

        internal void updateEntityPeople(SQLiteConnection sqlConnection, int entityEmisId, List<People> people)
        {

            if (people.Count == 0) return;

            sqlConnection.Table<People>().Where(v => v.entityEmisId == entityEmisId).Delete();
            sqlConnection.InsertAll(people);

        }


        internal void updateManagementLetterSectionExceptions(SQLiteConnection sqlConnection, int sectionEmisId, List<ManagementLetterSectionException> exceptions)
        {

            if (exceptions.Count == 0) return;

            sqlConnection.Table<ManagementLetterSectionException>().Where(v => v.mgt_letter_section_id == sectionEmisId).Delete();
            sqlConnection.InsertAll(exceptions);

        }


        internal void updatePpdaUsers(SQLiteConnection sqlConnection, List<User> users)
        {

            if (users.Count == 0) return;

            sqlConnection.DeleteAll<User>();
            sqlConnection.InsertAll(users);

        }

        public static int addPerson(SQLiteConnection db, People person)
        {

            db.Insert(person);
            return person.Id;
        }

        public List<People> allPeople(SQLiteConnection dbConn, int entityEmisId = 0)
        {

            List<People> people;

            if(entityEmisId == 0)
            {
                people = dbConn.Table<People>().ToList();
            }
            else
            {
                people = dbConn.Table<People>().Where(v => v.entityEmisId == entityEmisId).ToList();
            }

            return people;

        }

        public static int addPmMeetingAttendance(SQLiteConnection dbConn,PmMeetingAttendances attendence)
        {
            dbConn.Insert(attendence);
            return attendence.Id;
        }

        public List<User> allUsers(SQLiteConnection dbConn)
        {
            var users = dbConn.Table<User>().ToList();
            return users;
        }


        public List<PmAuditMeetingRole> allPmAuditMeetingRoles(SQLiteConnection dbConn)
        {
            var roles = dbConn.Table<PmAuditMeetingRole>().ToList();
            return roles;
        }


        internal void updateBranches(SQLiteConnection sqlConnection, List<Branch> branches)
        {
            if (branches.Count == 0) return;

            Branch branch = branches[0];
            sqlConnection.Table<Branch>().Where(v => v.entityEmisId == branch.entityEmisId).Delete();

            //sqlConnection.DeleteAll<Branch>();
            sqlConnection.InsertAll(branches);
        }


        public List<Branch> allBranches(SQLiteConnection dbConn, int entityEmisId)
        {
            var branches = dbConn.Table<Branch>().Where(v => v.entityEmisId == entityEmisId).ToList();
            return branches;
        }

        public List<FinacialYear> allFinancialYears(SQLiteConnection sQLiteConnection)
        {
            var list = sQLiteConnection.Table<FinacialYear>().OrderByDescending(v => v.emisId).ToList();
            return list;
        }

        internal void updateFinancialYears(SQLiteConnection sqlConnection, List<FinacialYear> financialYears)
        {

            if (financialYears.Count == 0) return;

            //delete all existing data
            sqlConnection.DeleteAll<FinacialYear>();

            //insert new data
            sqlConnection.InsertAll(financialYears);

        }


        internal void updateProviders(SQLiteConnection sqlConnection, List<Provider> records)
        {

            if (records.Count == 0) return;

            //delete all existing data
            sqlConnection.DeleteAll<Provider>();

            //insert new data
            sqlConnection.InsertAll(records);

        }

        internal void updateFundingSources(SQLiteConnection sqlConnection, List<FundingSource> records)
        {

            if (records.Count == 0) return;
            sqlConnection.DeleteAll<FundingSource>();
            sqlConnection.InsertAll(records);

        }

        internal void updateProcurementRoles(SQLiteConnection sqlConnection, List<ProcurementRole> records)
        {
            if (records.Count == 0) return;
            sqlConnection.DeleteAll<ProcurementRole>();
            sqlConnection.InsertAll(records);
        }

        internal void updateProcurementMethods(SQLiteConnection sqlConnection, List<ProcurementMethod> records)
        {
            if (records.Count == 0) return;
            sqlConnection.DeleteAll<ProcurementMethod>();
            sqlConnection.InsertAll(records);
        }

        internal void updatePpdaOffices(SQLiteConnection sqlConnection, List<PpdaOffice> records)
        {
            if (records.Count == 0) return;
            sqlConnection.DeleteAll<PpdaOffice>();
            sqlConnection.InsertAll(records);
        }

        internal void updateManagementLetterExceptionSections(SQLiteConnection sqlConnection, List<ManagementLetterSection> records)
        {

            if (records.Count == 0) return;

            //delete all existing data
            sqlConnection.DeleteAll<ManagementLetterSection>();

            //insert new data
            sqlConnection.InsertAll(records);

        }

        public List<District> allDistricts(SQLiteConnection dbConn)
        {

            var districts = dbConn.Table<District>().ToList();
            return districts;

        }

        internal void updateDistricts(SQLiteConnection sqlConnection, List<District> districts)
        {
            if (districts.Count == 0) return;

            sqlConnection.DeleteAll<District>();
            sqlConnection.InsertAll(districts);
        }

        public static int addBranch(SQLiteConnection db, Branch branch)
        {

            db.Insert(branch);
            return branch.Id;
        }

        public List<CbActivity> getCbActivities(SQLiteConnection sQLiteConnection, int passedCbPlanId)
        {

            var activities = sQLiteConnection.Table<CbActivity>().Where(v => v.cbPlanId == passedCbPlanId).ToList();                       
            return activities;

        }


        public List<CustomCbActivity> getCustomCbActivities(SQLiteConnection db, int passedCbPlanId)
        {

            var activities = db.Query<CustomCbActivity>("select CBA.emisId as activityId, CBA.activityname as activityName, U.first_name || ' ' || U.last_name as cordinatorName, D.districtName as districtName, CBA.startDate, CBA.endDate from CbActivity CBA inner join User U on CBA.userId = U.emisId left join District D on CBA.districtId = D.emisId where CBA.cbPlanId = ?", passedCbPlanId);
            return activities;

        }


        public List<CustomPmActivity> getCustomPmActivities(SQLiteConnection db, int passedPmPlanId)
        {

            var activities = db.Query<CustomPmActivity>("select PMA.*, E.entityName, CASE WHEN PMA.ppdaOfficeId IS NULL THEN 'N/A' ELSE PO.office_name END responsibleOffice, CASE WHEN PMA.fundingSourceId IS NULL THEN 'N/A' ELSE FS.source_name END fundingSourceName, FY.from_date as startDate, FY.to_date as endDate from PmActivity PMA inner join PmPlan PP on PMA.pmPlanId = PP.emisId inner join Entity E on PMA.entityId = E.emisId left join FundingSource FS on PMA.fundingSourceId = FS.emisId left join PpdaOffice PO on PMA.ppdaOfficeId = Po.emisId left join FinacialYear FY on PP.financial_year_id = FY.emisId where PMA.pmPlanId = ?", passedPmPlanId);
            return activities;

        }


        public CustomCbActivity findCustomCbActivityByActivityId(SQLiteConnection db, int activityId)
        {

            var activity = db.Query<CustomCbActivity>("select CBA.emisId as activityId, CBA.activityname as activityName, U.first_name || ' ' || U.last_name as cordinatorName, D.districtName as districtName, CBA.startDate, CBA.endDate,CBA.activitySubject, CBA.activityCategory, CBA.noOfPeople, CBA.noOfdays ,PO.office_name as responsibleOffice ,Act.activity_name as activityTypeName ,FS.source_name as fundingSourceName from CbActivity CBA inner join User U on CBA.userId = U.emisId left join District D on CBA.districtId = D.emisId left  join PpdaOffice PO on CBA.ppdaOfficeId = PO.emisId left join ActivityType Act on CBA.activityTypeId = Act.emisId  left join FundingSource FS on CBA.fundingSourceId = FS.emisId where CBA.emisId = ?", activityId).FirstOrDefault();

            if(activity == null)
            {
                return null;
            }

            //get the modules
            activity.modules = getCbActivityModules(db, activityId);

            //get trainers
            activity.trainersExternal = getCbActivityTrainers(db, activityId, "external");
            activity.trainersInternal = getCbActivityTrainers(db, activityId, "internal");

            //get the budget item amounts
            activity.budgetItemAmounts = getBudgetItemAmounts(db, Globals.ACTIVITY_TYPE_CB, activityId);

            //expected outcome
            activity.expectedOutcomes = getCbActivityExpectedOutcomes(db, activityId);

            //the child record, that has the actual
            var child = db.Query<CustomCbActivity>("select CBA.emisId as activityId, CBA.activityname as activityName, U.first_name || ' ' || U.last_name as cordinatorName, D.districtName as districtName, CBA.startDate, CBA.endDate,CBA.activitySubject, CBA.activityCategory, CBA.noOfPeople, CBA.noOfdays ,PO.office_name as responsibleOffice ,Act.activity_name as activityTypeName ,FS.source_name as fundingSourceName from CbActivity CBA inner join User U on CBA.userId = U.emisId left join District D on CBA.districtId = D.emisId left  join PpdaOffice PO on CBA.ppdaOfficeId = PO.emisId left join ActivityType Act on CBA.activityTypeId = Act.emisId  left join FundingSource FS on CBA.fundingSourceId = FS.emisId where CBA.parentId = ?", activityId).FirstOrDefault();

            if(child != null)
            {
                child.trainersInternal = getCbActivityTrainers(db, child.activityId, "internal");
                child.trainersExternal = getCbActivityTrainers(db, child.activityId, "external");
                child.modules = getCbActivityModules(db, child.activityId);
                child.budgetItemAmounts = getBudgetItemAmounts(db, Globals.ACTIVITY_TYPE_CB, child.activityId);
                child.expectedOutcomes = getCbActivityExpectedOutcomes(db, child.activityId);
            }           

            activity.child = child;                       

            return activity;

        }


        public List<CustomCbActivityModule> getCbActivityModules(SQLiteConnection db, int activityId)
        {
            var modules = db.Query<CustomCbActivityModule>("select CBAM.*,M.module_name as moduleName from CbActivityModule CBAM inner join Module M on CBAM.module_id = M.emisId where CBAM.cb_activity_id = ?", activityId).ToList();
            return modules;
        }

        public List<CbExpectedOutcome> getCbActivityExpectedOutcomes(SQLiteConnection db, int activityId)
        {

            var expectedOutcomes = db.Table<CbExpectedOutcome>().Where(v => v.cb_activity_id == activityId).ToList();
            return expectedOutcomes;

        }

        public List<CustomCbActivityTrainer> getCbActivityTrainers(SQLiteConnection db, int activityId, String trainerType = "internal")
        {

            List<CustomCbActivityTrainer> trainers;

            object[] param = new object[] { trainerType, activityId };
            if (trainerType != "internal")
            {  
                
                trainers = db.Query<CustomCbActivityTrainer>("select CBAT.*, U.first_name || ' ' || U.last_name as trainerName from CbActivityTrainer CBAT inner join User U on CBAT.trainer_id = U.emisId where CBAT.trainer_type = ? and CBAT.cb_activity_id = 1", param).ToList();
                return trainers;
            }

            //the other option is external trainers
            trainers = db.Query<CustomCbActivityTrainer>("select CBAT.*, U.firstName || ' ' || U.lastName as trainerName from CbActivityTrainer CBAT inner join People U on CBAT.trainer_id = U.emisId where CBAT.trainer_type = ? and CBAT.cb_activity_id = 1", param).ToList();
            return trainers;

        }


        public CustomPmActivity findCustomPmActivityByActivityId(SQLiteConnection db, int activityId)
        {
            var activity = db.Query<CustomPmActivity>("select PMA.*, E.entityName, CASE WHEN PMA.ppdaOfficeId IS NULL THEN 'N/A' ELSE PO.office_name END responsibleOffice, CASE WHEN PMA.fundingSourceId IS NULL THEN 'N/A' ELSE FS.source_name END fundingSourceName, FY.from_date as startDate, FY.to_date as endDate from PmActivity PMA inner join PmPlan PP on PMA.pmPlanId = PP.emisId inner join Entity E on PMA.entityId = E.emisId left join FundingSource FS on PMA.fundingSourceId = FS.emisId left join PpdaOffice PO on PMA.ppdaOfficeId = Po.emisId left join FinacialYear FY on PP.financial_year_id = FY.emisId where PMA.emisId = ?", activityId).FirstOrDefault();

            if(activity == null)
            {
                return activity;
            }

            //financial year
            var plan = db.Table<PmPlan>().Where(v => v.emisId == activity.pmPlanId).FirstOrDefault();
            var financialYear = plan == null ? null : db.Table<FinacialYear>().Where(v => v.emisId == plan.financial_year_id).FirstOrDefault();
            activity.financialYear = financialYear;

            //budget items
            activity.budgetItemAmounts = getBudgetItemAmounts(db, Globals.ACTIVITY_TYPE_PM, activityId);
            //pm team

            CustomPmTeam customPmTeam = getPmActivityTeam(db, activityId);
            activity.pmTeam = customPmTeam;

            return activity;

        }

        private CustomPmTeam getPmActivityTeam(SQLiteConnection db, int activityId)
        {

            CustomPmTeam pmTeam = db.Query<CustomPmTeam>("select PTE.pm_activity_start_date as startDate, PTE.pm_activity_start_date endDate, pm_activity_budget budget, PT.* from PmTeamEntity PTE inner join PmTeam PT on PTE.pm_team_id = PT.emisId where PTE.pm_activity_id = ?", activityId).FirstOrDefault();

            if (pmTeam == null)
            {
                return pmTeam;
            }

            //get team members
            var members = db.Query<CustomPmTeamMember>("select PTM.*, U.first_name || ' ' || U.last_name as fullName from PmTeamMember PTM inner join User U on PTM.user_id = U.emisId where PTM.pm_team_id = ?", pmTeam.emisId).ToList();
            pmTeam.teamMembers = members;

            return pmTeam;

        }

        public bool clearAuditSystemToolOldData(SQLiteConnection dbConn, string toolReference)
        {

            //most poor code, I have ever seen, what happens when you fail to delete

            //
            //identification
            //analysis
            //findings
            //sections
            //section items
            //

            dbConn.Table<ApiPmAuditSystemsToolIndentification>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditSystemsToolAnalysis>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditSystemsToolFindingConclusionsAndRecommendations>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditSystemsToolSection>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditSystemsToolSectionItem>().Where(v => v.toolReference == toolReference).Delete();
            return true;

        }

        public bool clearAuditDisposalToolOldData(SQLiteConnection dbConn, string toolReference)
        {

            //most poor code, I have ever seen, what happens when you fail to delete

            //
            //identification
            //analysis
            //sections
            //section items
            //

            dbConn.Table<ApiPmAuditDisposalToolIdentification>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditDisposalToolAnalysis>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditDisposalToolSection>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditDisposalToolSectionItem>().Where(v => v.toolReference == toolReference).Delete();
            return true;

        }


        public bool clearAuditProcessToolOldData(SQLiteConnection dbConn, string toolReference)
        {

            //most poor code, I have ever seen, what happens when you fail to delete

            dbConn.Table<ApiPmAuditProcessToolIdentification>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditProcessToolSection>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditProcessToolSectionItem>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditProcessToolCompAndPerfScore>().Where(v => v.toolReference == toolReference).Delete();
            //analysis
            dbConn.Table<ApiPmAuditProcessToolAnalysisProcessTime>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditProcessToolAnalysisProcessDates>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditProcessToolAnalysisCost>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditProcessToolAnalysisCompliance>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmAuditProcessToolAnalysisCompletion>().Where(v => v.toolReference == toolReference).Delete();

            return true;

        }

        public bool clearCompStructToolOldData(SQLiteConnection dbConn, string toolReference)
        {

            //most poor code, I have ever seen, what happens when you fail to delete
            
            dbConn.Table<ApiPmCompStructToolGeneralInfo>().Where(v => v.toolReference == toolReference).Delete();

            dbConn.Table<ApiPmCompStructToolContractsCommitteMembers>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmCompStructToolDepts>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmCompStructToolPduMembers>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmCompStructToolReportsSubmitted>().Where(v => v.toolReference == toolReference).Delete();

            dbConn.Table<ApiPmCompStructToolSpend>().Where(v => v.toolReference == toolReference).Delete();

            return true;

        }

        public bool clearCompProcessToolOldData(SQLiteConnection dbConn, string toolReference)
        {

            //most poor code, I have ever seen, what happens when you fail to delete
            dbConn.Table<ApiPmCompProcessToolAnalysis>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmCompProcessToolSection>().Where(v => v.toolReference == toolReference).Delete();
            dbConn.Table<ApiPmCompProcessToolSectionItem>().Where(v => v.toolReference == toolReference).Delete();

            return true;

        }

        public CbPlan getCbPlanForFinancialYear(SQLiteConnection sQLiteConnection, int fyEmisId)
        {
            var plan = sQLiteConnection.Table<CbPlan>().Where(v => v.financial_year_id == fyEmisId).FirstOrDefault();
            return plan;
        }

        public PmPlan getPmPlanForFinancialYear(SQLiteConnection sQLiteConnection, int fyEmisId)
        {
            var plan = sQLiteConnection.Table<PmPlan>().Where(v => v.financial_year_id == fyEmisId).FirstOrDefault();
            return plan;
        }

        public FinacialYear getPmFinancialYearByFyFullText(SQLiteConnection sQLiteConnection, String financialYear)
        {
            var fy = sQLiteConnection.Table<FinacialYear>().Where(v => v.financial_year == financialYear).FirstOrDefault();
            return fy;
        }


        public List<PmActivity> getPmActivities(SQLiteConnection sQLiteConnection, int passedPmPlanId)
        {

            var activities = sQLiteConnection.Table<PmActivity>().Where(v => v.pmPlanId == passedPmPlanId).ToList();
            return activities;

        }


        public List<CustomBudgetItemAmount> getBudgetItemAmounts(SQLiteConnection db, String activityType, int activityId)
        {
            object[] param = new object[] { activityId, activityType };
            var budgetItems = db.Query<CustomBudgetItemAmount>("select BIA.*, BI.item_name as itemName from BudgetItemAmount BIA left join BudgetItem BI on BIA.budget_item_id = BI.emisId where commentable_id = ? and commentable_type = ?", param).ToList();
            return budgetItems;
        }

        public List<CustomPmMeetingAttendances> getCustomPmMeetingAttendances(SQLiteConnection db, int activityId, string activityType)
        {

            object[] param = new object[] { activityId, activityType, activityId,activityType };
            var attendees = db.Query<CustomPmMeetingAttendances>("select PMA.pm_activity_id, PMA.Id, PMA.emisId, P.firstName, P.lastName, P.title, P.email, P.phoneNo, E.entityName as entityName, PAMR.role_name as roleName from PmMeetingAttendances PMA inner join People P on PMA.commentable_id = P.emisId left join Entity E on P.entityEmisId = E.emisId left join PmAuditMeetingRole PAMR on PMA.audit_meeting_role_id = PAMR.emisId where PMA.commentable_type = 'App\\Person' and PMA.pm_activity_id = ? and PMA.audit_activity = ? UNION select PMA.pm_activity_id, PMA.Id, PMA.emisId, U.first_name as firstName, U.last_name as lastName, U.title, U.email, '' as phoneNo, 'PPDA' as entityName, PAMR.role_name as roleName from PmMeetingAttendances PMA inner join User U on PMA.commentable_id = U.emisId left join PmAuditMeetingRole PAMR on PMA.audit_meeting_role_id = PAMR.emisId where PMA.commentable_type = 'App\\User' and PMA.pm_activity_id = ? and PMA.audit_activity = ?", param).ToList();
       
            return attendees;
        }

        public Dictionary<string, PmAuditDate> getPmActivityAuditDates(SQLiteConnection sQLiteConnection, int activtyEmisId)
        {

            Dictionary<string, PmAuditDate> dictAuditDates = new Dictionary<string, PmAuditDate>();

            var activities = sQLiteConnection.Table<PmAuditDate>().Where(v => v.pm_activity_id == activtyEmisId).ToList();

            foreach(var item in activities)
            {

                if(item.tag == Globals.PM_AUDIT_DATE_TAG_ENTRY_MEETING && !dictAuditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_ENTRY_MEETING))
                {
                    dictAuditDates.Add(Globals.PM_AUDIT_DATE_TAG_ENTRY_MEETING, item);
                }
                else if (item.tag == Globals.PM_AUDIT_DATE_TAG_START_DATE && !dictAuditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_START_DATE))
                {
                    dictAuditDates.Add(Globals.PM_AUDIT_DATE_TAG_START_DATE, item);
                }
                else if (item.tag == Globals.PM_AUDIT_DATE_TAG_CUTT_OFF && !dictAuditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_CUTT_OFF))
                {
                    dictAuditDates.Add(Globals.PM_AUDIT_DATE_TAG_CUTT_OFF, item);
                }
                else if (item.tag == Globals.PM_AUDIT_DATE_TAG_DEBRIEF && !dictAuditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_DEBRIEF))
                {
                    dictAuditDates.Add(Globals.PM_AUDIT_DATE_TAG_DEBRIEF, item);
                }
                else if (item.tag == Globals.PM_AUDIT_DATE_TAG_END_DATE && !dictAuditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_END_DATE))
                {
                    dictAuditDates.Add(Globals.PM_AUDIT_DATE_TAG_END_DATE, item);
                }
                else if (item.tag == Globals.PM_AUDIT_DATE_TAG_LETTER_SUBMISSION && !dictAuditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_LETTER_SUBMISSION))
                {
                    dictAuditDates.Add(Globals.PM_AUDIT_DATE_TAG_LETTER_SUBMISSION, item);
                }
                else if (item.tag == Globals.PM_AUDIT_DATE_TAG_RECEIPT_DATE && !dictAuditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_RECEIPT_DATE))
                {
                    dictAuditDates.Add(Globals.PM_AUDIT_DATE_TAG_RECEIPT_DATE, item);
                }
                else if (item.tag == Globals.PM_AUDIT_DATE_TAG_EXIT_MEETING && !dictAuditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_EXIT_MEETING))
                {
                    dictAuditDates.Add(Globals.PM_AUDIT_DATE_TAG_EXIT_MEETING, item);
                }

            }

            return dictAuditDates;

        }

        public List<CustomPmAuditSampleFile> getCustomPmAuditSampleFiles(SQLiteConnection db, int activityId)
        {
            var samples = db.Query<CustomPmAuditSampleFile>("Select PASF.*, U.first_name || ' ' || U.last_name as assigneeName, CASE WHEN PT.sampleFileLocalId IS NULL THEN 'Not Yet Started' ELSE 'Last edited on ' || PT.updatedOn END status from PmAuditSampleFile PASF left join User U on PASF.user_id = U.emisId left join ApiPmAuditProcessTools PT on PASF.Id = PT.sampleFileLocalId where PASF.pm_activity_id = ?", activityId);
            return samples;
        }

        public CustomPmAuditSampleFile getCustomPmAuditSampleFileByActivityIdAndLocalId(SQLiteConnection db, int activityId, int sampleFileLocalId)
        {
            var sampleFile = db.Query<CustomPmAuditSampleFile>("Select PASF.*, U.first_name || ' ' || U.last_name as assigneeName, 'Not Yet Started' as status from PmAuditSampleFile PASF left join User U on PASF.user_id = U.emisId where PASF.pm_activity_id = ? and PASF.Id = ?", activityId, sampleFileLocalId).FirstOrDefault();
            return sampleFile;
        }

        public List<CustomCbActivityPeople> getCustomCbActivityPeople(SQLiteConnection db, int activityId)
        {
            var people = db.Query<CustomCbActivityPeople>("select CAP.*, P.firstName as firstName, P.lastName as lastName, P.email as email, P.title as title, E.entityName as entityName from CbActivityPeople CAP inner join People P on CAP.person_local_id = P.Id left join Entity E on P.entityEmisId = E.emisId where CAP.cb_activity_id = ?", activityId);
            return people;
        }


        public int saveAuditProcessToolSection(SQLiteConnection db, AuditProcessToolSection toolSection)
        {

            db.Insert(toolSection);
            return toolSection.Id;

        }


        public int saveAuditProcessToolSectionItem(SQLiteConnection db, AuditProcessToolSectionItem sectionItem)
        {

            db.Insert(sectionItem);
            return sectionItem.Id;
        }

        public int savePmApt(SQLiteConnection sQLiteConnection, ApiPmAuditProcessTools tool)
        {
            sQLiteConnection.Insert(tool);
            return tool.Id;
        }

        public int updatePmApt(SQLiteConnection sQLiteConnection, ApiPmAuditProcessTools tool)
        {
            return sQLiteConnection.Update(tool);
        }

        public int savePmAdt(SQLiteConnection sQLiteConnection, ApiPmAuditDisposalTool tool)
        {
            sQLiteConnection.Insert(tool);
            return tool.Id;
        }

        public int updatePmAdt(SQLiteConnection sQLiteConnection, ApiPmAuditDisposalTool tool)
        {
            return sQLiteConnection.Update(tool);
        }

        public int savePmAst(SQLiteConnection sQLiteConnection, ApiPmAuditSystemsTool tool)
        {
            sQLiteConnection.Insert(tool);
            return tool.Id;
        }

        public int updatePmAst(SQLiteConnection sQLiteConnection, ApiPmAuditSystemsTool tool)
        {
            return sQLiteConnection.Update(tool);            
        }


        public static int saveApiPmAuditProcessToolSection(SQLiteConnection sQLiteConnection, ApiPmAuditProcessToolSection apiToolSection)
        {
            sQLiteConnection.Insert(apiToolSection);
            return apiToolSection.Id;
        }

        public static int saveApiPmAuditSystemsToolSection(SQLiteConnection sQLiteConnection, ApiPmAuditSystemsToolSection apiToolSection)
        {
            sQLiteConnection.Insert(apiToolSection);
            return apiToolSection.Id;
        }

        public static int saveApiPmAuditDisposalToolSection(SQLiteConnection sQLiteConnection, ApiPmAuditDisposalToolSection apiToolSection)
        {
            sQLiteConnection.Insert(apiToolSection);
            return apiToolSection.Id;
        }

        public static void saveApiPmAuditProcessToolSectionItem(SQLiteConnection sQLiteConnection, List<ApiPmAuditProcessToolSectionItem> apiToolSectionItems)
        {
            sQLiteConnection.InsertAll(apiToolSectionItems);
        }

        public static void saveApiPmAuditSystemsToolSectionItem(SQLiteConnection sQLiteConnection, List<ApiPmAuditSystemsToolSectionItem> apiToolSectionItems)
        {
            sQLiteConnection.InsertAll(apiToolSectionItems);
        }

        public static void saveApiPmAuditDisposalToolSectionItem(SQLiteConnection sQLiteConnection, List<ApiPmAuditDisposalToolSectionItem> apiToolSectionItems)
        {
            sQLiteConnection.InsertAll(apiToolSectionItems);
        }

        public int saveApiPmAuditProcessToolIdentification(SQLiteConnection sQLiteConnection, ApiPmAuditProcessToolIdentification toolIdentification)
        {

            sQLiteConnection.Insert(toolIdentification);
            return toolIdentification.Id;

        }

        public int saveApiPmAuditSystemsToolIdentification(SQLiteConnection sQLiteConnection, ApiPmAuditSystemsToolIndentification toolIdentification)
        {

            sQLiteConnection.Insert(toolIdentification);
            return toolIdentification.Id;

        }

        public int saveApiPmAuditDisposalToolIdentification(SQLiteConnection sQLiteConnection, ApiPmAuditDisposalToolIdentification toolIdentification)
        {
            sQLiteConnection.Insert(toolIdentification);
            return toolIdentification.Id;

        }

        public bool auditProcessToolNameExists(SQLiteConnection sQLiteConnection, string toolName)
        {
            var tool = sQLiteConnection.Table<ApiPmAuditProcessTools>().Where(v => v.toolName == toolName).FirstOrDefault();
            return tool != null;
        }

        public bool auditSystemsToolNameExists(SQLiteConnection sQLiteConnection, string toolName)
        {
            var tool = sQLiteConnection.Table<ApiPmAuditSystemsTool>().Where(v => v.toolName == toolName).FirstOrDefault();
            return tool != null;
        }

        public bool auditDisposalToolNameExists(SQLiteConnection sQLiteConnection, string toolName)
        {
            var tool = sQLiteConnection.Table<ApiPmAuditDisposalTool>().Where(v => v.toolName == toolName).FirstOrDefault();
            return tool != null;
        }

        public static int saveApiPmAuditProcessToolCompAndPerfScore(SQLiteConnection sQLiteConnection, ApiPmAuditProcessToolCompAndPerfScore compAndPerfScore)
        {
            sQLiteConnection.Insert(compAndPerfScore);
            return compAndPerfScore.Id;
        }

        public static int saveApiPmAuditSystemsToolAnalysis(SQLiteConnection sQLiteConnection, ApiPmAuditSystemsToolAnalysis analysis)
        {
            sQLiteConnection.Insert(analysis);
            return analysis.Id;
        }

        public static int saveApiPmAuditDisposalToolAnalysis(SQLiteConnection sQLiteConnection, ApiPmAuditDisposalToolAnalysis analysis)
        {
            sQLiteConnection.Insert(analysis);
            return analysis.Id;
        }

        public static int saveApiPmAuditSystemsToolFindingsConclusionsAndRecom(SQLiteConnection sQLiteConnection, ApiPmAuditSystemsToolFindingConclusionsAndRecommendations findingsConclusionsRecoms)
        {
            sQLiteConnection.Insert(findingsConclusionsRecoms);
            return findingsConclusionsRecoms.Id;
        }

        public int saveAuditSystemsToolSection(SQLiteConnection db, AuditSystemsToolSection toolSection)
        {

            db.Insert(toolSection);
            return toolSection.Id;

        }

        public int saveAuditDisposalToolSection(SQLiteConnection db, AuditDisposalToolSection toolSection)
        {

            db.Insert(toolSection);
            return toolSection.Id;

        }


        public int saveAuditSystemsToolSectionItem(SQLiteConnection db, AuditSystemsToolSectionItem sectionItem)
        {

            db.Insert(sectionItem);
            return sectionItem.Id;
        }

        public int saveAuditDisposalToolSectionItem(SQLiteConnection db, AuditDisposalToolSectionItem sectionItem)
        {

            db.Insert(sectionItem);
            return sectionItem.Id;
        }

        public List<AuditSystemsToolSection> getPmAuditSystemToolSections(SQLiteConnection dbConn)
        {

            List<AuditSystemsToolSection> updatedSections = new List<AuditSystemsToolSection>();
            List<AuditSystemsToolSection> toolSections = dbConn.Table<AuditSystemsToolSection>().ToList();

            foreach (AuditSystemsToolSection section in toolSections)
            {

                section.items = dbConn.Table<AuditSystemsToolSectionItem>().Where(v => v.auditSystemsToolSectionId == section.Id).ToList();
                updatedSections.Add(section);

            }

            return updatedSections;

        }


        public List<AuditDisposalToolSection> getPmAuditDisposalToolSections(SQLiteConnection dbConn)
        {

            List<AuditDisposalToolSection> updatedSections = new List<AuditDisposalToolSection>();
            List<AuditDisposalToolSection> toolSections = dbConn.Table<AuditDisposalToolSection>().ToList();

            foreach (AuditDisposalToolSection section in toolSections)
            {

                section.items = dbConn.Table<AuditDisposalToolSectionItem>().Where(v => v.auditDisposalToolSectionId == section.Id).ToList();
                updatedSections.Add(section);

            }

            return updatedSections;

        }


        public ApiPmAuditProcessTools getPmAuditProcessToolByIdOrActivityIdAndSampleFileLocalId(SQLiteConnection dbConn, int activityId = 0, int sampleFileLocalId = 0, long toolId = 0)
        {

            ApiPmAuditProcessTools tool;
            
            tool = activityId == 0 ? tool = dbConn.Table<ApiPmAuditProcessTools>().Where(v => v.Id == toolId).FirstOrDefault() :
                                     tool = dbConn.Table<ApiPmAuditProcessTools>().Where(v => v.activityId == activityId).Where(v => v.sampleFileLocalId == sampleFileLocalId).FirstOrDefault();

            //we didnt find the tool
            if (tool == null)
            {
                return tool;
            }

            String toolRef = tool.toolReference;

            //get the identification
            tool.identification = dbConn.Table<ApiPmAuditProcessToolIdentification>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //get the comp & perf score
            tool.compAndPerfScore = dbConn.Table<ApiPmAuditProcessToolCompAndPerfScore>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //analysis
            tool.analysisCompliance = dbConn.Table<ApiPmAuditProcessToolAnalysisCompliance>().Where(v => v.toolReference == toolRef).FirstOrDefault();
            tool.analysisCompletion = dbConn.Table<ApiPmAuditProcessToolAnalysisCompletion>().Where(v => v.toolReference == toolRef).FirstOrDefault();
            tool.analysisCost = dbConn.Table<ApiPmAuditProcessToolAnalysisCost>().Where(v => v.toolReference == toolRef).FirstOrDefault();
            tool.analysisProcessDates = dbConn.Table<ApiPmAuditProcessToolAnalysisProcessDates>().Where(v => v.toolReference == toolRef).FirstOrDefault();
            tool.analysisProcessTimes = dbConn.Table<ApiPmAuditProcessToolAnalysisProcessTime>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //get the sections 
            List<ApiPmAuditProcessToolSection> sections = getApiAuditProcessToolSections(dbConn, toolRef);
            tool.sections = sections;

            return tool;

        }

        private static List<ApiPmAuditProcessToolSection> getApiAuditProcessToolSections(SQLiteConnection dbConn, string toolRef)
        {

            List<ApiPmAuditProcessToolSection> sections = dbConn.Table<ApiPmAuditProcessToolSection>().Where(v => v.toolReference == toolRef).ToList();
            var updatedSections = new List<ApiPmAuditProcessToolSection>();

            foreach (var sec in sections)
            {

                long emisId = sec.sectionEmisId;
                List<ApiPmAuditProcessToolSectionItem> items = dbConn.Table<ApiPmAuditProcessToolSectionItem>().Where(v => v.toolReference == toolRef).Where(v => v.sectionEmisId == emisId).ToList();
                sec.items = items;
                updatedSections.Add(sec);

            }

            return updatedSections;

        }


        private static List<ApiPmAuditSystemsToolSection> getApiAuditSystemsToolSections(SQLiteConnection dbConn, string toolRef)
        {

            List<ApiPmAuditSystemsToolSection> sections = dbConn.Table<ApiPmAuditSystemsToolSection>().Where(v => v.toolReference == toolRef).ToList();
            var updatedSections = new List<ApiPmAuditSystemsToolSection>();

            foreach (var sec in sections)
            {

                long emisId = sec.sectionEmisId;
                List<ApiPmAuditSystemsToolSectionItem> items = dbConn.Table<ApiPmAuditSystemsToolSectionItem>().Where(v => v.toolReference == toolRef).Where(v => v.sectionEmisId == emisId).ToList();
                sec.items = items;
                updatedSections.Add(sec);

            }

            return updatedSections;

        }


        private static List<ApiPmAuditDisposalToolSection> getApiAuditDisposalToolSections(SQLiteConnection dbConn, string toolRef)
        {

            List<ApiPmAuditDisposalToolSection> sections = dbConn.Table<ApiPmAuditDisposalToolSection>().Where(v => v.toolReference == toolRef).ToList();
            var updatedSections = new List<ApiPmAuditDisposalToolSection>();

            foreach (var sec in sections)
            {

                long emisId = sec.sectionEmisId;
                List<ApiPmAuditDisposalToolSectionItem> items = dbConn.Table<ApiPmAuditDisposalToolSectionItem>().Where(v => v.toolReference == toolRef).Where(v => v.sectionEmisId == emisId).ToList();
                sec.items = items;
                updatedSections.Add(sec);

            }

            return updatedSections;

        }



        public ApiPmAuditSystemsTool getPmAuditSystemsToolByIdOrToolReference(SQLiteConnection dbConn, int activityId = 0, long toolId = 0)
        {

            ApiPmAuditSystemsTool tool;

            //if the activity is 0 we use the toolID else we use the tool activity
            tool = activityId == 0 ? tool = dbConn.Table<ApiPmAuditSystemsTool>().Where(v => v.Id == toolId).FirstOrDefault() :
                                     tool = dbConn.Table<ApiPmAuditSystemsTool>().Where(v => v.activityId == activityId).FirstOrDefault();

            //we didnt find the tool
            if (tool == null)
            {
                return tool;
            }

            String toolRef = tool.toolReference;

            //get the identification
            tool.identification = dbConn.Table<ApiPmAuditSystemsToolIndentification>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //get the analysis
            tool.analysis = dbConn.Table<ApiPmAuditSystemsToolAnalysis>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //get the findinds
            tool.findings = dbConn.Table<ApiPmAuditSystemsToolFindingConclusionsAndRecommendations>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //get the sections 
            List<ApiPmAuditSystemsToolSection> sections = getApiAuditSystemsToolSections(dbConn, toolRef);
            tool.sections = sections;

            return tool;

        }


        public ApiPmAuditDisposalTool getPmAuditDisposalToolByIdOrToolReference(SQLiteConnection dbConn, int activityId = 0, long toolId = 0)
        {

            ApiPmAuditDisposalTool tool;
            
            tool = activityId == 0 ? tool = dbConn.Table<ApiPmAuditDisposalTool>().Where(v => v.Id == toolId).FirstOrDefault() :
                                     tool = dbConn.Table<ApiPmAuditDisposalTool>().Where(v => v.activityId == activityId).FirstOrDefault();

            //we didnt find the tool
            if (tool == null)
            {
                return tool;
            }

            String toolRef = tool.toolReference;

            //get the identification
            tool.identification = dbConn.Table<ApiPmAuditDisposalToolIdentification>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //get the analysis
            tool.analysis = dbConn.Table<ApiPmAuditDisposalToolAnalysis>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //get the sections 
            List<ApiPmAuditDisposalToolSection> sections = getApiAuditDisposalToolSections(dbConn, toolRef);
            tool.sections = sections;

            return tool;

        }

        public int saveApiPmAudiProcessToolAnalysisCompliance(SQLiteConnection sQLiteConnection, ApiPmAuditProcessToolAnalysisCompliance data)
        {
            sQLiteConnection.Insert(data);
            return data.Id;
        }

        public int saveApiPmAuditProcessToolAnalysisCompletion(SQLiteConnection sQLiteConnection, ApiPmAuditProcessToolAnalysisCompletion data)
        {
            sQLiteConnection.Insert(data);
            return data.Id;
        }

        public int saveApiPmAuditProcessToolAnalysisCost(SQLiteConnection sQLiteConnection, ApiPmAuditProcessToolAnalysisCost data)
        {
            sQLiteConnection.Insert(data);
            return data.Id;
        }

        public int saveApiPmAuditProcessToolAnalysisProcessDates(SQLiteConnection sQLiteConnection, ApiPmAuditProcessToolAnalysisProcessDates data)
        {
            sQLiteConnection.Insert(data);
            return data.Id;
        }

        public int saveApiPmAuditProcessToolAnalysisProcessTime(SQLiteConnection sQLiteConnection, ApiPmAuditProcessToolAnalysisProcessTime data)
        {
            sQLiteConnection.Insert(data);
            return data.Id;
        }

        public List<AuditProcessToolSection> getPmAuditProcessToolSections(SQLiteConnection dbConn)
        {

            List<AuditProcessToolSection> updatedSections = new List<AuditProcessToolSection>();
            List<AuditProcessToolSection> toolSections = dbConn.Table<AuditProcessToolSection>().ToList();

            foreach (AuditProcessToolSection section in toolSections)
            {

                section.items = dbConn.Table<AuditProcessToolSectionItem>().Where(v => v.auditProcessToolSectionId == section.Id).ToList();
                updatedSections.Add(section);

            }

            return updatedSections;

        }

        public ApiPmCompProcessTool getPmCompProcessToolByIdOrActivityIdAndSampleFileLocalId(SQLiteConnection dbConn, int activityId = 0, int sampleFileLocalId = 0, long toolId = 0)
        {

            ApiPmCompProcessTool tool;
            tool = activityId == 0 ? tool = dbConn.Table<ApiPmCompProcessTool>().Where(v => v.Id == toolId).FirstOrDefault() :
                                     tool = dbConn.Table<ApiPmCompProcessTool>().Where(v => v.activityId == activityId).Where(v => v.sampleFileLocalId == sampleFileLocalId).FirstOrDefault();

            //we didnt find the tool
            if (tool == null)
            {
                return tool;
            }

            //we didnt find the tool
            if (tool == null)
            {
                return tool;
            }

            String toolRef = tool.toolReference;

            //get the identification
            tool.analysis = dbConn.Table<ApiPmCompProcessToolAnalysis>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //get the sections 
            List<ApiPmCompProcessToolSection> sections = getApiCompProcessToolSections(dbConn, toolRef);
            tool.sections = sections;

            return tool;

        }

        private List<ApiPmCompProcessToolSection> getApiCompProcessToolSections(SQLiteConnection dbConn, string toolRef)
        {

            List<ApiPmCompProcessToolSection> sections = dbConn.Table<ApiPmCompProcessToolSection>().Where(v => v.toolReference == toolRef).ToList();
            var updatedSections = new List<ApiPmCompProcessToolSection>();

            foreach (var sec in sections)
            {

                long emisId = sec.sectionEmisId;
                List<ApiPmCompProcessToolSectionItem> items = dbConn.Table<ApiPmCompProcessToolSectionItem>().Where(v => v.toolReference == toolRef).Where(v => v.sectionEmisId == emisId).ToList();
                sec.items = items;
                updatedSections.Add(sec);

            }

            return updatedSections;

        }

        public int savePmCpt(SQLiteConnection sQLiteConnection, ApiPmCompProcessTool tool)
        {
            sQLiteConnection.Insert(tool);
            return tool.Id;
        }

        public int updatePmCpt(SQLiteConnection sQLiteConnection, ApiPmCompProcessTool tool)
        {
            return sQLiteConnection.Update(tool);
        }

        public static int saveApiPmCompProcessToolSection(SQLiteConnection sQLiteConnection, ApiPmCompProcessToolSection apiToolSection)
        {
            sQLiteConnection.Insert(apiToolSection);
            return apiToolSection.Id;
        }

        public static void saveApiPmCompProcessToolSectionItem(SQLiteConnection sQLiteConnection, List<ApiPmCompProcessToolSectionItem> apiToolSectionItems)
        {
            sQLiteConnection.InsertAll(apiToolSectionItems);
        }

        public int saveApiPmCompProcessToolAnayisis(SQLiteConnection sQLiteConnection, ApiPmCompProcessToolAnalysis toolIdentification)
        {

            sQLiteConnection.Insert(toolIdentification);
            return toolIdentification.Id;

        }

        public bool compProcessToolNameExists(SQLiteConnection sQLiteConnection, string toolName)
        {
            var tool = sQLiteConnection.Table<ApiPmCompProcessTool>().Where(v => v.toolName == toolName).FirstOrDefault();
            return tool != null;
        }

        public int saveComplianceProcessToolSection(SQLiteConnection db, ComplianceProcessToolSection toolSection)
        {

            db.Insert(toolSection);
            return toolSection.Id;

        }

        public int saveComplianceProcessToolSectionItem(SQLiteConnection db, ComplianceProcessToolSectionItem sectionItem)
        {

            db.Insert(sectionItem);
            return sectionItem.Id;
        }

        public List<ComplianceProcessToolSection> getPmComplianceProcessToolSections(SQLiteConnection dbConn)
        {

            List<ComplianceProcessToolSection> updatedSections = new List<ComplianceProcessToolSection>();
            List<ComplianceProcessToolSection> toolSections = dbConn.Table<ComplianceProcessToolSection>().ToList();

            foreach (ComplianceProcessToolSection section in toolSections)
            {

                section.items = dbConn.Table<ComplianceProcessToolSectionItem>().Where(v => v.complianceProcessToolSectionId == section.Id).ToList();
                updatedSections.Add(section);

            }

            return updatedSections;

        }

        public bool compStructToolNameExists(SQLiteConnection sQLiteConnection, string toolName)
        {
            var tool = sQLiteConnection.Table<ApiPmCompStructTool>().Where(v => v.toolName == toolName).FirstOrDefault();
            return tool != null;
        }

        public int savePmCst(SQLiteConnection sQLiteConnection, ApiPmCompStructTool tool)
        {
            sQLiteConnection.Insert(tool);
            return tool.Id;
        }

        public int updatePmCst(SQLiteConnection sQLiteConnection, ApiPmCompStructTool tool)
        {
            return sQLiteConnection.Update(tool);
        }

        public int savePmCstGeneralInfo(SQLiteConnection sQLiteConnection, ApiPmCompStructToolGeneralInfo data)
        {
            sQLiteConnection.Insert(data);
            return data.Id;
        }

        public int savePmCstSpend(SQLiteConnection sQLiteConnection, ApiPmCompStructToolSpend data)
        {
            sQLiteConnection.Insert(data);
            return data.Id;
        }

        public void savePmCstDepts(SQLiteConnection sQLiteConnection, List<ApiPmCompStructToolDepts> data)
        {
            sQLiteConnection.InsertAll(data);
        }

        public void savePmCstCCMembers(SQLiteConnection sQLiteConnection, List<ApiPmCompStructToolContractsCommitteMembers> data)
        {
            sQLiteConnection.InsertAll(data);
        }

        public void savePmCstPduMembers(SQLiteConnection sQLiteConnection, List<ApiPmCompStructToolPduMembers> data)
        {
            sQLiteConnection.InsertAll(data);
        }

        public void savePmCstReportsSubmitted(SQLiteConnection sQLiteConnection, List<ApiPmCompStructToolReportsSubmitted> data)
        {
            sQLiteConnection.InsertAll(data);
        }


        public ApiPmCompStructTool getPmCompStructToolByIdOrActivityId(SQLiteConnection dbConn, int activityId = 0, long toolId = 0)
        {

            ApiPmCompStructTool tool;
            
            tool = activityId == 0 ? tool = dbConn.Table<ApiPmCompStructTool>().Where(v => v.Id == toolId).FirstOrDefault() :
                                     tool = dbConn.Table<ApiPmCompStructTool>().Where(v => v.activityId == activityId).FirstOrDefault();

            //we didnt find the tool
            if (tool == null)
            {
                return tool;
            }

            String toolRef = tool.toolReference;

            //CC members
            tool.contractsCommitteeMembers = dbConn.Table<ApiPmCompStructToolContractsCommitteMembers>().Where(v => v.toolReference == toolRef).ToList();

            //departments
            tool.departments = dbConn.Table<ApiPmCompStructToolDepts>().Where(v => v.toolReference == toolRef).ToList();

            //general info
            tool.generalInfo = dbConn.Table<ApiPmCompStructToolGeneralInfo>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            //pdu members
            tool.pduMembers = dbConn.Table<ApiPmCompStructToolPduMembers>().Where(v => v.toolReference == toolRef).ToList();

            //reports
            tool.reports = dbConn.Table<ApiPmCompStructToolReportsSubmitted>().Where(v => v.toolReference == toolRef).ToList();

            //spend
            tool.spend = dbConn.Table<ApiPmCompStructToolSpend>().Where(v => v.toolReference == toolRef).FirstOrDefault();

            return tool;

        }

        public int saveSystemSetting(SQLiteConnection sQLiteConnection, SystemSetting systemParameter)
        {
            sQLiteConnection.Insert(systemParameter);
            return systemParameter.Id;
        }

        public int updateSystemSetting(SQLiteConnection sQLiteConnection, SystemSetting systemParameter)
        {
             int rowsAffected = sQLiteConnection.Update(systemParameter);
            return rowsAffected;
        }


        public SystemSetting getSystemParameter(SQLiteConnection dbConn, String groupCode, String valueCode)
        {
            var parameter = dbConn.Table<SystemSetting>().Where(v => v.groupCode == groupCode).Where(v => v.valueCode == valueCode).FirstOrDefault();
            return parameter;
        }
        

        public List<ServerSyncStatus> getServerSyncStatus(SQLiteConnection dbConn, string dataTypeCategory)
        {
            var results = dbConn.Table<ServerSyncStatus>().Where(v => v.dataTypeCategory == dataTypeCategory).ToList();
            return results;
        }

        public ServerSyncStatus getServerSyncStatus(SQLiteConnection dbConn, string dataTypeCategory, string dataType)
        {
            var result = dbConn.Table<ServerSyncStatus>().Where(v => v.dataTypeCategory == dataTypeCategory).Where(v => v.dataType == dataType).FirstOrDefault();
            return result;
        }


        public int saveServerSyncStatus(SQLiteConnection dbConn, ServerSyncStatus serverSyncStatus)
        {
            dbConn.Insert(serverSyncStatus);
            return serverSyncStatus.Id;
        }


        public int updateServerSyncStatus(SQLiteConnection dbConn, ServerSyncStatus serverSyncStatus)
        {
            int affectedRows = dbConn.Update(serverSyncStatus);
            return affectedRows;
        }

        internal void saveServerSyncStatus(SQLiteConnection dbConn, ServerSyncStatus serverSyncStatus, string dataTypeCategory, string dataType)
        {

            var existentStatus = dbConn.Table<ServerSyncStatus>().Where(v => v.dataTypeCategory == dataTypeCategory).Where(v => v.dataType == dataType).FirstOrDefault();
            if(existentStatus != null)
            {
                serverSyncStatus.Id = existentStatus.Id;
                dbConn.Update(serverSyncStatus);
            }
            else
            {
                dbConn.Insert(serverSyncStatus);
            }

        }

        public List<ManagementLetterSection> getManagementLetterSections(SQLiteConnection dbConn)
        {
            var magamentLetterSections = dbConn.Table<ManagementLetterSection>().ToList();
            return magamentLetterSections;
        }

        public List<ManagementLetterSectionException> getManagementLetterSectionExceptions(SQLiteConnection dbConn, int managementLetterSectionEmisId)
        {
            var sectionExceptions = dbConn.Table<ManagementLetterSectionException>().Where(v=>v.mgt_letter_section_id == managementLetterSectionEmisId).ToList();
            return sectionExceptions;
        }
        
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.ControlObjects
{
    public class EndPoints
    {

        public const String ENTITIES_GET = "/entity_query/get_entities";
        public const String ENTITIES_GET_BY_ID = "/entity_query/get_by_id/";
        
        public const String PM_ACTIVITIES_GET_BY_PM_PLAN_ID = "/pm_query/get_financial_year_activities/";
        public const String PM_ACTIVITIES_GET_BY_ACTIVITY_ID = "/pm_query/activity/";

        public const String CB_ACTIVITIES_GET_BY_FY = "/cb_query/get_financial_year_activities/";
        public const String CB_ACTIVITIES_GET_BY_ACTIVITY_ID = "/cb_query/activity/";

        public const String USERS_GET = "/user_query/get_ppda_users";
        public const String FINANCIAL_YEARS_GET = "/general_query/get_financial_years";
        public const String DISTRICTS_GET = "/general_query/get_districts";
        public const String FUNDING_SOURCES_GET = "/general_query/get_funding_sources";
        public const String PPDA_OFFICES_GET = "/general_query/get_ppda_offices";
        public const String PROCUREMENT_ROLES_GET = "/general_query/get_procurement_roles";
        public const String PROCUREMENT_METHODS_GET = "/general_query/get_procurement_methods";
        public const String PROVIDERS_GET = "/general_query/get_providers";

        public const String MGT_LETTER_EXCEPTION_SECTIONS_GET = "/pm_query/get_management_letter_sections";

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processor.Entities;
using Processor.Entities.responses;

namespace Processor.ControlObjects
{
    public class DataFormatter
    {
        internal static List<Entity> formatEmisEntities(List<EmisEntity> emisEntites)
        {

            List<Entity> entities = new List<Entity>();

            foreach(var item in emisEntites)
            {

                var entity = new Entity();
                entity.emisId = item.id;
                entity.contactNumber = item.contact_number;
                entity.egpStatus = item.egp_status != 0 ;
                entity.email = item.email;
                entity.entityName = item.entity_name;
                entity.gppStatus = item.gpp_status != 0;
                entity.physicalAddress = item.physical_address;
                entity.procCode = item.proc_code;
                entity.usmidStatus = item.usmid_status != 0;

                var emisEntityPeople = item.people;
                List<People> people = formatEmisPeople(emisEntityPeople, item.id);

                //update the entity people
                new DatabaseHandler().updateEntityPeople(DatabaseHandler.dbConnection(), item.id, people);
                                
                entities.Add(entity);

            }

            return entities;
        }

        private static List<People> formatEmisPeople(List<EmisPeople> emisEntityPeople, int entityId)
        {

            List<People> people = new List<People>();

            foreach(var item in emisEntityPeople)
            {

                var person = new People();
                person.emisId = item.id;
                person.firstName = item.first_name;
                person.lastName = item.last_name;
                person.entityEmisId = entityId;
               // person.branch_id = item.branch_id;
                person.email = item.email;
                person.gender = item.gender;
                person.is_external_facilitator = item.is_external_facilitator;
                person.note = item.note;

                int roleId = 0;
                person.person_role_id = int.TryParse(item.person_role_id, out roleId) ? roleId : 0;
                person.phoneNo = item.phone_number;
                person.title = item.title;               

                people.Add(person);

            }

            return people;
        }

        internal static List<PmActivity> formatEmisPmActivities(List<EmisPmActivity> emisPmActivities)
        {

            List<PmActivity> pmActivities = new List<PmActivity>();

            foreach (var item in emisPmActivities)
            {
                var activity = new PmActivity();

                activity.auditType = item.audit_type;
                activity.emisId = item.id;
                activity.entityId = item.entity_id;

                int fundingSourceId = 0;
                activity.fundingSourceId =int.TryParse(item.funding_source, out fundingSourceId) ? fundingSourceId : 0;
                activity.lastAudited = item.last_audited;
                activity.oagOpinion = item.oag_opinion;
                activity.pmActivityTypeId = int.TryParse(item.pm_activity_type_id, out int activity_type_id) ? activity_type_id : 0;
                activity.pmPlanId = item.pm_plan_id;
                activity.ppdaOfficeId = int.TryParse(item.ppda_office_id, out int ppda_office_id) ? ppda_office_id : 0;
                activity.previousPerformanceCategory = item.previous_performance_category;
                activity.previousPerformanceScore = item.previous_performance_score;
                activity.procurementBudget = item.procurement_budget;

                DatabaseHandler dbHandler = new DatabaseHandler();
                var dbConn = DatabaseHandler.dbConnection();

                //audit dates
                var emisAuditDates = item.pm_audit_dates;
                List<PmAuditDate> auditDates = formatEmisAuditDates(emisAuditDates, item.id);
                dbHandler.updatePmActivityAuditDates(dbConn, item.id, auditDates);

                //actitiy team
                var emisActivityTeam = item.activity_team;
                PmTeamEntity teamEntity = formatEmisActivityTeam(emisActivityTeam, item.id);
                dbHandler.updatePmActivityPmTeamEntity(dbConn,teamEntity, item.id);

                //budget item amounts
                var emisBudgetItemAmounts = item.budget_item_amounts;
                List<BudgetItemAmount> budgetItemAmounts = formatEmisBudgetItemAmounts(emisBudgetItemAmounts);
                dbHandler.updatePmActivityBugdetItems(dbConn, item.id, budgetItemAmounts);

                //sample files
                var emisSampleFiles = item.pm_sample_files;
                List<PmAuditSampleFile> sampleFiles = formatEmisSampleFiles(emisSampleFiles, item.id);
                dbHandler.updatePmActivityAuditSampleFiles(dbConn, item.id, sampleFiles);

                pmActivities.Add(activity);

            }

            return pmActivities;

        }

        internal static List<ManagementLetterSection> formatMagamentLetterSectionexceptions(List<EmisManagementLetterSection> records)
        {
            List<ManagementLetterSection> data = new List<ManagementLetterSection>();

            foreach (var item in records)
            {
                var dataItem = new ManagementLetterSection();
                dataItem.emisId = item.id;
                dataItem.section_title = item.section_title;

                var exceptions = new List<ManagementLetterSectionException>();
                foreach(var emisException in item.exceptions)
                {

                    var newException = new ManagementLetterSectionException();
                    newException.emisId = emisException.id;
                    newException.exception_title = emisException.exception_title;
                    newException.exception_standard_implication = emisException.implication;
                    newException.exception_standard_recommendation = emisException.recommendation;
                    newException.mgt_letter_section_id = item.id;

                    exceptions.Add(newException);
                }

                //save the exceptions for this section
                new DatabaseHandler().updateManagementLetterSectionExceptions(DatabaseHandler.dbConnection(), item.id, exceptions);

                data.Add(dataItem);

            }
            return data;
        }

        private static PmTeamEntity formatEmisActivityTeam(EmisActivityTeam emisActivityTeam, int id)
        {

            if(emisActivityTeam == null)
            {
                return null;
            }

            PmTeamEntity pmTeamEntity = new PmTeamEntity();
            pmTeamEntity.emisId = emisActivityTeam.id;
            pmTeamEntity.pm_activity_budget = emisActivityTeam.pm_activity_budget;
            pmTeamEntity.pm_activity_end_date = emisActivityTeam.pm_activity_end_date;
            pmTeamEntity.pm_activity_id = emisActivityTeam.pm_activity_id;
            pmTeamEntity.pm_activity_start_date = emisActivityTeam.pm_activity_start_date;
            pmTeamEntity.pm_team_id = emisActivityTeam.pm_team_id;
            
            return pmTeamEntity;

        }

        private static List<PmAuditSampleFile> formatEmisSampleFiles(List<EmisPmSampleFile> emisSampleFiles, int activityEmisId)
        {
            List<PmAuditSampleFile> auditSampleFiles = new List<PmAuditSampleFile>();

            foreach(var item in emisSampleFiles)
            {
                var sample = new PmAuditSampleFile();
                sample.contract_value = double.TryParse(item.contract_value, out double contractValue) ? contractValue : 0;
                sample.date_of_award = item.date_of_award;
                sample.emisId = item.id;
                sample.pm_activity_id = activityEmisId;
                sample.procurement_method = item.procurement_method_id.ToString();
                sample.procurement_reference = item.proc_ref_number;
                sample.procurement_subject = item.subject_of_proc;
                sample.provider = item.provider;
                sample.user_id = item.user_id;

                auditSampleFiles.Add(sample);
            }

            return auditSampleFiles;
        }

        private static List<BudgetItemAmount> formatEmisBudgetItemAmounts(List<EmisBudgetItemAmount> emisBudgetItemAmounts)
        {

            List<BudgetItemAmount> budgetItemAmounts = new List<BudgetItemAmount>();

            foreach (var item in emisBudgetItemAmounts)
            {

                var budgetItemAmount = new BudgetItemAmount();
                budgetItemAmount.emisId = item.Id;
                budgetItemAmount.amount = item.amount;
                budgetItemAmount.budget_item_id = item.budget_item_id;
                budgetItemAmount.commentable_id = item.commentable_id;
                budgetItemAmount.commentable_type = item.commentable_type;
                budgetItemAmount.note = item.note;
                budgetItemAmount.quantity = item.quantity;
                budgetItemAmount.unit_price = item.unit_price;

                budgetItemAmounts.Add(budgetItemAmount);

            }

            return budgetItemAmounts;

        }

        private static List<PmAuditDate> formatEmisAuditDates(List<EmisPmAuditDate> emisAuditDates, int activityEmisId)
        {
            List<PmAuditDate> pmAuditDates = new List<PmAuditDate>();

            foreach(var item in emisAuditDates)
            {
                PmAuditDate auditDate = new PmAuditDate();
                auditDate.pm_activity_id = activityEmisId;
                auditDate.activity = item.activity;
                auditDate.activity_date = item.activity_date;
                auditDate.activity_time = item.activity_time;
                auditDate.tag = item.tag;

                pmAuditDates.Add(auditDate);
            }

            return pmAuditDates;
        }

        internal static List<User> formatEmisUsers(List<EmisUser> emisUsers)
        {

            List<User> users = new List<User>();

            foreach(var item in emisUsers)
            {
                var user = new User();
                user.emisId = item.id;
                user.email = item.email;
                user.first_name = item.first_name;
                user.last_name = item.last_name;
                user.title = item.title;
                users.Add(user);
            }
            return users;
        }

        internal static List<FinacialYear> formatEmisFinancialYears(List<EmisFinancialYear> records)
        {

            List<FinacialYear> fy = new List<FinacialYear>();

            foreach (var item in records)
            {
                var financialYr = new FinacialYear();
                financialYr.emisId = item.id;
                financialYr.financial_year = item.financial_year;
                fy.Add(financialYr);
            }
            return fy;
        }

        internal static List<Provider> formatEmisProviders(List<EmisProvider> records)
        {

            List<Provider> data = new List<Provider>();

            foreach (var item in records)
            {
                var dataItem = new Provider();
                dataItem.emisId = item.id;
                dataItem.orgname = item.orgname;
                dataItem.source = item.source;
                data.Add(dataItem);
            }
            return data;
        }

        internal static List<FundingSource> formatEmisFundingSources(List<EmisFundingSource> records)
        {

            List<FundingSource> data = new List<FundingSource>();

            foreach (var item in records)
            {
                var dataItem = new FundingSource();
                dataItem.emisId = item.id;
                dataItem.source_name = item.source_name;
                data.Add(dataItem);
            }
            return data;
        }


        internal static List<ProcurementRole> formatEmisProcurementRoles(List<EmisProcurementRole> records)
        {

            List<ProcurementRole> data = new List<ProcurementRole>();

            foreach (var item in records)
            {
                var dataItem = new ProcurementRole(item.id, item.role_name);
                data.Add(dataItem);
            }
            return data;
        }

        internal static List<ProcurementMethod> formatEmisProcuremenMethods(List<EmisProcurementMethod> records)
        {

            List<ProcurementMethod> data = new List<ProcurementMethod>();

            foreach (var item in records)
            {
                var dataItem = new ProcurementMethod();
                dataItem.emisId = item.id;
                dataItem.method_name = item.method_name;
                data.Add(dataItem);
            }
            return data;
        }

        internal static List<PpdaOffice> formatEmisPpdaOffices(List<EmisPpdaOffice> records)
        {

            List<PpdaOffice> data = new List<PpdaOffice>();

            foreach (var item in records)
            {
                var dataItem = new PpdaOffice();
                dataItem.emisId = item.id;
                dataItem.office_name = item.office_name;
                data.Add(dataItem);
            }
            return data;
        }


        internal static List<District> formatEmisDistricts(List<EmisDistrict> records)
        {

            List<District> data = new List<District>();

            foreach (var item in records)
            {
                var dataItem = new District();
                dataItem.emisId = item.id;
                dataItem.districtName = item.district_name;
                data.Add(dataItem);
            }
            return data;
        }

    }

}

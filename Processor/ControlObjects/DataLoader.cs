﻿using Processor.Entities;
using Processor.Entities.PM;
using Processor.Entities.PM.compliance.process;
using Processor.Entities.PM.disposal;
using Processor.Entities.PM.systems;
using Processor.Entities.responses;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.ControlObjects
{
    public class DataLoader
    {

        public void loadEntities()
        {

            List<Entity> entities = !Globals.IN_DEBUG ? apiFetchEntities() : DataGenerator.mockEntities();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();
            db.updateEntities(dbConn, entities);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_ENTITIES;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_ENTITIES);

        }

        private List<Entity> apiFetchEntities()
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if(setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.ENTITIES_GET;

            var resp = RestUtility.CallServiceAsync<EntitiesGetResp>(apiUrl, null, "GET") as EntitiesGetResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisEntites = resp.payload;
            return DataFormatter.formatEmisEntities(emisEntites);

        }

        public void loadFinancialYears()
        {

            //fecth financial years
            List<FinacialYear> financialYears = !Globals.IN_DEBUG ? apiFetchFinancialYears() : DataGenerator.mockFinancialYears();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            db.updateFinancialYears(dbConn, financialYears);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_FINANCIAL_YEARS;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_FINANCIAL_YEARS);

        }


        public void loadProviders()
        {
            
            List<Provider> providers = !Globals.IN_DEBUG ? apiFetchProviders() : new List<Provider>();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            db.updateProviders(dbConn, providers);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_PROVIDERS;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_PROVIDERS);

        }

        public void loadFundingSources()
        {

            List<FundingSource> providers = !Globals.IN_DEBUG ? apiFetchFundingSources() : new List<FundingSource>();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            db.updateFundingSources(dbConn, providers);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_FUNDING_SOURCES;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_FUNDING_SOURCES);

        }

        public void loadProcurementRoles()
        {

            List<ProcurementRole> procRoles = !Globals.IN_DEBUG ? apiFetchProcurementRoles() : new List<ProcurementRole>();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            db.updateProcurementRoles(dbConn, procRoles);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_PROCUREMENT_ROLES;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_PROCUREMENT_ROLES);

        }

        public void loadProcurementMethods()
        {

            List<ProcurementMethod> procMethods = !Globals.IN_DEBUG ? apiFetchProcurementMethods() : new List<ProcurementMethod>();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            db.updateProcurementMethods(dbConn, procMethods);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_PROCUREMENT_METHODS;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_PROCUREMENT_METHODS);

        }

        public void loadPpdaOffices()
        {

            List<PpdaOffice> offices = !Globals.IN_DEBUG ? apiFetchPpdaOffcies() : new List<PpdaOffice>();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            db.updatePpdaOffices(dbConn, offices);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_PPDA_OFFICES;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_PPDA_OFFICES);

        }

        private List<FundingSource> apiFetchFundingSources()
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.FUNDING_SOURCES_GET;

            var resp = RestUtility.CallServiceAsync<GetFundingSourceResp>(apiUrl, null, "GET") as GetFundingSourceResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisFundingSources = resp.payload;
            return DataFormatter.formatEmisFundingSources(emisFundingSources);

        }


        private List<ProcurementRole> apiFetchProcurementRoles()
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.PROCUREMENT_ROLES_GET;

            var resp = RestUtility.CallServiceAsync<GetProcurementRolesResp>(apiUrl, null, "GET") as GetProcurementRolesResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisProcRoles = resp.payload;
            return DataFormatter.formatEmisProcurementRoles(emisProcRoles);

        }


        private List<ProcurementMethod> apiFetchProcurementMethods()
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.PROCUREMENT_METHODS_GET;

            var resp = RestUtility.CallServiceAsync<GetProcurementMethodResp>(apiUrl, null, "GET") as GetProcurementMethodResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisProcMethods = resp.payload;
            return DataFormatter.formatEmisProcuremenMethods(emisProcMethods);

        }


        private List<PpdaOffice> apiFetchPpdaOffcies()
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.PPDA_OFFICES_GET;

            var resp = RestUtility.CallServiceAsync<GetPpdaOfficeResp>(apiUrl, null, "GET") as GetPpdaOfficeResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisPpdaOffices = resp.payload;
            return DataFormatter.formatEmisPpdaOffices(emisPpdaOffices);

        }



        private List<Provider> apiFetchProviders()
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.PROVIDERS_GET;

            var resp = RestUtility.CallServiceAsync<GetProviderResp>(apiUrl, null, "GET") as GetProviderResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisProviders = resp.payload;
            return DataFormatter.formatEmisProviders(emisProviders);

        }

        public void loadManagementLetterSections()
        {

            //fecth financial years
            List<ManagementLetterSection> exceptionSections = !Globals.IN_DEBUG ? apiFetchManagementLetterExceptionSections() : DataGenerator.mockManagementLetterExceptionSections();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            db.updateManagementLetterExceptionSections(dbConn, exceptionSections);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_MGT_LETTER_SECTIONS;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_MGT_LETTER_SECTIONS);

        }

        private List<ManagementLetterSection> apiFetchManagementLetterExceptionSections()
        {
            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.MGT_LETTER_EXCEPTION_SECTIONS_GET;

            var resp = RestUtility.CallServiceAsync<GetManagementLetterSectionsResp>(apiUrl, null, "GET") as GetManagementLetterSectionsResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisMagamentLetterSectionexceptions = resp.payload;
            return DataFormatter.formatMagamentLetterSectionexceptions(emisMagamentLetterSectionexceptions);

        }

        private List<FinacialYear> apiFetchFinancialYears()
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.FINANCIAL_YEARS_GET;

            var resp = RestUtility.CallServiceAsync<GetFinacialYearResp>(apiUrl, null, "GET") as GetFinacialYearResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisFinancialYears = resp.payload;
            return DataFormatter.formatEmisFinancialYears(emisFinancialYears);

        }


        public void loadDistricts()
        {

            List<District> districts = !Globals.IN_DEBUG ? apiFetchDistricts() : DataGenerator.mockDistricts();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();
            db.updateDistricts(dbConn, districts);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_DISTRICTS;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_DISTRICTS);
            
        }

        private List<District> apiFetchDistricts()
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.DISTRICTS_GET;

            var resp = RestUtility.CallServiceAsync<GetDistrictsResp>(apiUrl, null, "GET") as GetDistrictsResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisDistricts = resp.payload;
            return DataFormatter.formatEmisDistricts(emisDistricts);

        }



        public void loadPmActivities(String financialYear)
        {

            List<PmActivity> activities = !Globals.IN_DEBUG ? apiFetchPmActivities(financialYear) : DataGenerator.mockPmActivities();

            DatabaseHandler db = new DatabaseHandler();
            db.updatePmActivities(DatabaseHandler.dbConnection(), activities);


            //update the server sync status
            var serverSyncStatus = new ServerSyncStatus();            
            serverSyncStatus.dataType = financialYear;
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_PM_ACTIVITY;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);
            db.saveServerSyncStatus(DatabaseHandler.dbConnection(), serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_PM_ACTIVITY, financialYear);

        }

        private List<PmActivity> apiFetchPmActivities(String financialYear)
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.PM_ACTIVITIES_GET_BY_PM_PLAN_ID + financialYear; 

            var resp = RestUtility.CallServiceAsync<PmActivitiesGet>(apiUrl, null, "GET") as PmActivitiesGet;

            //we got the data

            if (!resp.success)
            {
                throw new Exception(resp.message);
            }

            var emisPmActivities = resp.payload;
            return DataFormatter.formatEmisPmActivities(emisPmActivities);
            
        }


        public void loadCbActivities()
        {

            List<CbActivity> activities = !Globals.IN_DEBUG ? apiFetchCbActivities() : DataGenerator.mockCbActivities();

            DatabaseHandler db = new DatabaseHandler();
            db.updateCbActivities(DatabaseHandler.dbConnection(), activities);

        }

        private List<CbActivity> apiFetchCbActivities()
        {
            return new List<CbActivity>();
        }

        public void fetchAuditProcessToolSections()
        {

             const string urlPmAuditToolSections = "http://192.168.43.49:7002/api/pm-tools/audit-process-tool";
            //const string urlPmAuditToolSections = "http://192.168.1.123:7002/api/pm-tools/audit-process-tool";

            var data = RestUtility.CallServiceAsync<Data>(urlPmAuditToolSections, null, "GET") as Data;

            //we got the data

            if (data.StatusCode != 0)
            {
                throw new Exception("Error on getting section data " + data.StatusDescription);
            }


            var apiSections = data.DataData;


            //clear existing section data
            var sQLiteConnection = DatabaseHandler.dbConnection();
            sQLiteConnection.DeleteAll<AuditProcessToolSection>();
            sQLiteConnection.DeleteAll<AuditProcessToolSectionItem>();


            //loop through the new data and save it
            foreach (Datum apiSection in apiSections)
            {
                var name = apiSection.Name;
                var emisId = apiSection.Id;
                var rank = apiSection.RankNo;
                var apiSectionItems = apiSection.PmProcessToolSectionItems;

                var customSection = new AuditProcessToolSection();
                customSection.name = name;
                customSection.rank = rank;

                var db = new DatabaseHandler();
                var sectionId = db.saveAuditProcessToolSection(sQLiteConnection, customSection);

                saveAuditProcessToolSectionItems(db, sQLiteConnection, sectionId, apiSectionItems);


            }

        }

        private void saveAuditProcessToolSectionItems(DatabaseHandler db, SQLiteConnection sQLiteConnection, int sectionId, PmProcessToolSectionItem[] apiSectionItems)
        {


            foreach (PmProcessToolSectionItem item in apiSectionItems)
            {

                var emisId = item.Id;
                var rank = item.Rank;
                var responseType = item.ResponseType;
                var yesRank = item.YesRank;
                var noRank = item.NoRank;
                var naRank = item.NaRank;
                var description = item.Description;
                var role = item.role;

                var sectionItem = new AuditProcessToolSectionItem();
                sectionItem.emisId = (int)emisId;
                sectionItem.rank = Double.Parse(rank);
                sectionItem.description = description;
                sectionItem.yesRank = yesRank == null ? 0 : (int)yesRank;
                sectionItem.noRank = noRank == null ? 0 : (int)noRank;
                sectionItem.naRank = naRank == null ? 0 : (int)naRank;
                sectionItem.auditProcessToolSectionId = sectionId;
                sectionItem.responseType = responseType.ToString().ToLower();
                sectionItem.fieldRole = role;

                db.saveAuditProcessToolSectionItem(sQLiteConnection, sectionItem);

            }

        }

        public void fetchAuditSystemsToolSections()
        {

            // const string urlPmAuditSystemsToolSections = "http://192.168.43.49:7002/api/pm-tools/audit-systems-tool";
            const string urlPmAuditSystemsToolSections = "http://192.168.43.49:7002/api/pm-tools/audit-systems-tool";

            var data = RestUtility.CallServiceAsync<ApiDataPmAuditSystemsToolResp>(urlPmAuditSystemsToolSections, null, "GET") as ApiDataPmAuditSystemsToolResp;

            //we got the data

            if (data.statusCode != "0")
            {
                throw new Exception("Error on getting section data " + data.statusDescription);
            }


            var apiSections = data.data;

            //clear existing section data
            var sQLiteConnection = DatabaseHandler.dbConnection();
            sQLiteConnection.DeleteAll<AuditSystemsToolSection>();
            sQLiteConnection.DeleteAll<AuditSystemsToolSectionItem>();


            //loop through the new data and save it
            foreach (var apiSection in apiSections)
            {
                var name = apiSection.name;
                var emisId = apiSection.id;
                var rank = apiSection.rank_no;
                var apiSectionItems = apiSection.pm_audit_systems_tool_section_items;

                var customSection = new AuditSystemsToolSection();
                customSection.name = name;
                customSection.rank = Double.Parse(rank);

                var db = new DatabaseHandler();
                var sectionId = db.saveAuditSystemsToolSection(sQLiteConnection, customSection);

                saveAuditSystemsToolSectionItems(db, sQLiteConnection, sectionId, apiSectionItems);

            }

        }

        private void saveAuditSystemsToolSectionItems(DatabaseHandler db, SQLiteConnection sQLiteConnection, int sectionId, List<ApiDataPmAuditSystemsToolSectionItem> apiSectionItems)
        {

            foreach (ApiDataPmAuditSystemsToolSectionItem item in apiSectionItems)
            {

                var emisId = item.id;
                var rank = item.rank;
                var responseType = item.response_type;
                var yesRank = item.yes_rank;
                var noRank = item.no_rank;
                var naRank = item.na_rank;
                var description = item.description;
                //var role = item.role;

                var sectionItem = new AuditSystemsToolSectionItem();
                sectionItem.emisId = (int)emisId;
                sectionItem.rank = Double.Parse(rank);
                sectionItem.description = description;
                sectionItem.yesRank = String.IsNullOrEmpty(yesRank) ? 0 : int.Parse(yesRank);
                sectionItem.noRank = String.IsNullOrEmpty(noRank) ? 0 : int.Parse(noRank);
                sectionItem.naRank = String.IsNullOrEmpty(naRank) ? 0 : int.Parse(naRank);
                sectionItem.auditSystemsToolSectionId = sectionId;
                sectionItem.responseType = responseType.ToString().ToLower();
                //sectionItem.fieldRole = role;

                db.saveAuditSystemsToolSectionItem(sQLiteConnection, sectionItem);

            }

        }

        public void fetchAuditDisposalToolSections()
        {

            // const string urlPmAuditDisposalToolSections = "http://192.168.43.49:7002/api/pm-tools/audit-disposal-tool";
            const string urlPmAuditDisposalToolSections = "http://192.168.43.49:7002/api/pm-tools/audit-disposal-tool";

            var data = RestUtility.CallServiceAsync<ApiDataPmAuditDisposalToolResp>(urlPmAuditDisposalToolSections, null, "GET") as ApiDataPmAuditDisposalToolResp;

            //we got the data

            if (data.statusCode != 0)
            {
                throw new Exception("Error on getting section data " + data.statusDescription);
            }


            var apiSections = data.data;

            //clear existing section data
            var sQLiteConnection = DatabaseHandler.dbConnection();
            sQLiteConnection.DeleteAll<AuditDisposalToolSection>();
            sQLiteConnection.DeleteAll<AuditDisposalToolSectionItem>();


            //loop through the new data and save it
            foreach (var apiSection in apiSections)
            {
                var name = apiSection.name;
                var emisId = apiSection.id;
                var rank = apiSection.rank_no;
                var apiSectionItems = apiSection.pm_audit_disposal_tool_section_items;

                var customSection = new AuditDisposalToolSection();
                customSection.name = name;
                customSection.rank = Double.Parse(rank);

                var db = new DatabaseHandler();
                var sectionId = db.saveAuditDisposalToolSection(sQLiteConnection, customSection);

                saveAuditDisposalToolSectionItems(db, sQLiteConnection, sectionId, apiSectionItems);

            }

        }

        private void saveAuditDisposalToolSectionItems(DatabaseHandler db, SQLiteConnection sQLiteConnection, int sectionId, List<ApiDataPmAuditDisposalToolSectionItem> apiSectionItems)
        {

            foreach (ApiDataPmAuditDisposalToolSectionItem item in apiSectionItems)
            {

                var emisId = item.id;
                var rank = item.rank;
                var responseType = item.response_type;
                var yesRank = item.yes_rank;
                var noRank = item.no_rank;
                var naRank = item.na_rank;
                var description = item.description;
                //var role = item.role;

                var sectionItem = new AuditDisposalToolSectionItem();
                sectionItem.emisId = (int)emisId;
                sectionItem.rank = Double.Parse(rank);
                sectionItem.description = description;
                sectionItem.yesRank = yesRank;//String.IsNullOrEmpty(yesRank) ? 0 : int.Parse(yesRank);
                sectionItem.noRank = noRank;// String.IsNullOrEmpty(noRank) ? 0 : int.Parse(noRank);
                sectionItem.naRank = naRank;// String.IsNullOrEmpty(naRank) ? 0 : int.Parse(naRank);
                sectionItem.auditDisposalToolSectionId = sectionId;
                sectionItem.responseType = responseType.ToString().ToLower();
                //sectionItem.fieldRole = role;

                db.saveAuditDisposalToolSectionItem(sQLiteConnection, sectionItem);

            }

        }

        public void fetchComplianceProcessToolSections()
        {

           // const string urlPmComplianceProcessToolSections = "http://192.168.1.123:7002/api/pm-tools/compliance-process-tool";
            const string urlPmComplianceProcessToolSections = "http://192.168.43.49:7002/api/pm-tools/compliance-process-tool";

            var data = RestUtility.CallServiceAsync<ApiDataPmComplianceProcessToolResp>(urlPmComplianceProcessToolSections, null, "GET") as ApiDataPmComplianceProcessToolResp;

            //we got the data

            if (data.statusCode != 0)
            {
                throw new Exception("Error on getting section data " + data.statusDescription);
            }


            var apiSections = data.data;

            //clear existing section data
            var sQLiteConnection = DatabaseHandler.dbConnection();
            sQLiteConnection.DeleteAll<ComplianceProcessToolSection>();
            sQLiteConnection.DeleteAll<ComplianceProcessToolSectionItem>();


            //loop through the new data and save it
            foreach (var apiSection in apiSections)
            {
                var name = apiSection.name;
                var emisId = apiSection.id;
                var rank = apiSection.rank_no;
                var apiSectionItems = apiSection.pm_compliance_process_tool_section_items;

                var customSection = new ComplianceProcessToolSection();
                customSection.name = name;
                customSection.rank = Double.Parse(rank);

                var db = new DatabaseHandler();
                var sectionId = db.saveComplianceProcessToolSection(sQLiteConnection, customSection);

                saveComplianceProcessToolSectionItems(db, sQLiteConnection, sectionId, apiSectionItems);

            }

        }

        private void saveComplianceProcessToolSectionItems(DatabaseHandler db, SQLiteConnection sQLiteConnection, int sectionId, List<ApiDataPmComplianceProcessToolSectionItem> apiSectionItems)
        {

            foreach (ApiDataPmComplianceProcessToolSectionItem item in apiSectionItems)
            {

                var emisId = item.id;
                var rank = item.rank;
                var responseType = item.response_type;
                var yesRank = item.yes_rank;
                var noRank = item.no_rank;
                var naRank = item.na_rank;
                var description = item.description;

                var sectionItem = new ComplianceProcessToolSectionItem();
                sectionItem.emisId = (int)emisId;
                sectionItem.rank = Double.Parse(rank);
                sectionItem.description = description;
                sectionItem.yesRank = yesRank;
                sectionItem.noRank = noRank;
                sectionItem.naRank = naRank;
                sectionItem.complianceProcessToolSectionId = sectionId;
                sectionItem.responseType = responseType.ToString().ToLower();

                db.saveComplianceProcessToolSectionItem(sQLiteConnection, sectionItem);

            }

        }


        public void loadUsers()
        {

            List<User> users = !Globals.IN_DEBUG ? apiFetchUsers() : DataGenerator.mockUsers();

            DatabaseHandler db = new DatabaseHandler();
            SQLiteConnection dbConn = DatabaseHandler.dbConnection();
            db.updatePpdaUsers(dbConn, users);

            //update last sync date
            var serverSyncStatus = new ServerSyncStatus();
            serverSyncStatus.dataTypeCategory = Globals.SYNC_DATA_TYPE_CAT_GENERAL;
            serverSyncStatus.dataType = Globals.SYNC_DATA_TYPE_PPDA_USERS;
            serverSyncStatus.lastUpdateDate = DateTime.Now.ToString(Globals.DATE_FORMAT);

            db.saveServerSyncStatus(dbConn, serverSyncStatus, Globals.SYNC_DATA_TYPE_CAT_GENERAL, Globals.SYNC_DATA_TYPE_PPDA_USERS);

        }

        private List<User> apiFetchUsers()
        {

            SystemSetting setting = new DatabaseHandler().getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if (setting == null)
            {
                throw new Exception("Unable to determine the API server base URL");
            }

            String baseUrl = setting.value;
            string apiUrl = baseUrl + EndPoints.USERS_GET;

            var resp = RestUtility.CallServiceAsync<GetUsersResp>(apiUrl, null, "GET") as GetUsersResp;

            //we got the data

            if (!resp.success)
            {
                throw new Exception("Error on getting api data ");
            }

            var emisUsers = resp.payload;
            return DataFormatter.formatEmisUsers(emisUsers);

        }

    }

}

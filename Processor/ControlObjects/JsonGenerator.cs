﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.ControlObjects
{
    public class JsonGenerator
    {


        public bool generateAuditSystemToolJson(String dir, int activityId, out String result)
        {

            try
            {

                result = "";
                var tool = new DatabaseHandler().getPmAuditSystemsToolByIdOrToolReference(DatabaseHandler.dbConnection(), activityId);
                if (tool == null)
                {
                    result = "Tool for activity ID [" + activityId + "] not found";
                    return false;
                }

                string toolReference = tool.toolReference.Replace("/","_");
                String filePath = dir + @"\" + toolReference + ".emis";
                var toolJson = JsonConvert.SerializeObject(tool, Formatting.Indented);
                File.WriteAllText(filePath, toolJson);

                result = filePath;
                return true;

            }
            catch(Exception exception)
            {
                result = exception.Message;
                return false;
            }

        }

        public bool generateAuditDisposalToolJson(String dir, int activityId, out String result)
        {

            try
            {

                result = "";
                var tool = new DatabaseHandler().getPmAuditDisposalToolByIdOrToolReference(DatabaseHandler.dbConnection(), activityId);
                if (tool == null)
                {
                    result = "Tool for activity ID [" + activityId + "] not found";
                    return false;
                }

                string toolReference = tool.toolReference.Replace("/", "_");
                String filePath = dir + @"\" + toolReference + ".emis";
                var toolJson = JsonConvert.SerializeObject(tool, Formatting.Indented);
                File.WriteAllText(filePath, toolJson);

                result = filePath;
                return true;

            }
            catch (Exception exception)
            {
                result = exception.Message;
                return false;
            }

        }

        public bool generateAuditProcessToolJson(String dir, int activityId, int sampleFileLocalId, out String result)
        {

            try
            {

                result = "";
                var tool = new DatabaseHandler().getPmAuditProcessToolByIdOrActivityIdAndSampleFileLocalId(DatabaseHandler.dbConnection(), activityId, sampleFileLocalId);
                if (tool == null)
                {
                    result = "Tool for activity ID [" + activityId + "] not found";
                    return false;
                }

                string toolReference = tool.toolReference.Replace("/", "_");
                String filePath = dir + @"\" + toolReference + ".emis";
                var toolJson = JsonConvert.SerializeObject(tool, Formatting.Indented);
                File.WriteAllText(filePath, toolJson);

                result = filePath;
                return true;

            }
            catch (Exception exception)
            {
                result = exception.Message;
                return false;
            }

        }

        public bool generateCompStructureToolJson(String dir, int activityId, out String result)
        {

            try
            {

                result = "";
                var tool = new DatabaseHandler().getPmCompStructToolByIdOrActivityId(DatabaseHandler.dbConnection(), activityId);
                if (tool == null)
                {
                    result = "Tool for activity ID [" + activityId + "] not found";
                    return false;
                }

                string toolReference = tool.toolReference.Replace("/", "_");
                String filePath = dir + @"\" + toolReference + ".emis";
                var toolJson = JsonConvert.SerializeObject(tool, Formatting.Indented);
                File.WriteAllText(filePath, toolJson);

                result = filePath;
                return true;

            }
            catch (Exception exception)
            {
                result = exception.Message;
                return false;
            }

        }

        public bool generateCompProcessToolJson(String dir, int activityId, int sampleFileLocalId, out String result)
        {

            try
            {

                result = "";
                var tool = new DatabaseHandler().getPmCompProcessToolByIdOrActivityIdAndSampleFileLocalId(DatabaseHandler.dbConnection(), activityId, sampleFileLocalId);
                if (tool == null)
                {
                    result = "Tool for activity ID [" + activityId + "] not found";
                    return false;
                }

                string toolReference = tool.toolReference.Replace("/", "_");
                String filePath = dir + @"\" + toolReference + ".emis";
                var toolJson = JsonConvert.SerializeObject(tool, Formatting.Indented);
                File.WriteAllText(filePath, toolJson);

                result = filePath;
                return true;

            }
            catch (Exception exception)
            {
                result = exception.Message;
                return false;
            }

        }



    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor.ControlObjects
{
    public class Globals
    {

        public const string DATABASE_PATH = @"D:\TestDb\emisdb1.sqlite";
        public const string DATABASE_PASS_PHRASE = "T3rr1613";
        public const bool IN_DEBUG = false;

        public const String ACTIVITY_TYPE_CB = @"App\\CbActivity";
        public const String ACTIVITY_TYPE_PM = @"App\\PmActivity";

        public const String ATTENDEE_TYPE_PPDA_USER = @"App\User";
        public const String ATTENDEE_TYPE_ENTITY_PERSON = @"App\Person";

        public const String PM_AUDIT_DATE_TAG_ENTRY_MEETING = "entry_meeting";
        public const String PM_AUDIT_DATE_TAG_START_DATE = "start_date";
        public const String PM_AUDIT_DATE_TAG_CUTT_OFF = "cutt_off";
        public const String PM_AUDIT_DATE_TAG_DEBRIEF = "debrief";
        public const String PM_AUDIT_DATE_TAG_END_DATE = "end_date";
        public const String PM_AUDIT_DATE_TAG_LETTER_SUBMISSION = "letter_submission";
        public const String PM_AUDIT_DATE_TAG_RECEIPT_DATE = "receipt_deadline";
        public const String PM_AUDIT_DATE_TAG_EXIT_MEETING = "exit_meeting";

        public static string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

        public static string GROUP_CODE_EMIS_SERVER = "EMIS_SERVER";
        public static string VALUE_CODE_EMIS_SERVER_URL = "SERVER_URL";
        public static string VALUE_CODE_EMIS_SERVER_USERNAME = "SERVER_USERNAME";
        public static string VALUE_CODE_EMIS_SERVER_PASSWORD = "SERVER_PASSWORD";

        public static string SYNC_DATA_TYPE_CAT_GENERAL = "GENERAL";
        public static string SYNC_DATA_TYPE_CAT_PM_ACTIVITY = "PM_ACTIVITY";
        public static string SYNC_DATA_TYPE_CAT_CB_ACTIVITY = "CB_ACTIVITY";

        public static string SYNC_DATA_TYPE_ENTITIES = "ENTITIES";
        public static string SYNC_DATA_TYPE_PROCUREMENT_METHODS = "PROCUREMENT_METHODS";
        public static string SYNC_DATA_TYPE_PPDA_USERS = "PPDA_USERS";
        public static string SYNC_DATA_TYPE_ENTITY_PEOPLE = "ENTITY_PEOPLE";
        public static string SYNC_DATA_TYPE_PROVIDERS = "PROVIDERS";
        public static string SYNC_DATA_TYPE_PROCUREMENT_ROLES = "PROCUREMENT_ROLES";
        public static string SYNC_DATA_TYPE_FINANCIAL_YEARS = "FINANCIAL_YEARS";
        public static string SYNC_DATA_TYPE_DISTRICTS = "DISTRICTS";
        public static string SYNC_DATA_TYPE_FUNDING_SOURCES = "FUNDING_SOURCES";
        public static string SYNC_DATA_TYPE_PPDA_OFFICES = "PPDA_OFFICES";
        public static string SYNC_DATA_TYPE_MGT_LETTER_SECTIONS = "MGT_LETTER_SECTIONS";

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processor.Entities;

namespace Processor.ControlObjects
{
    public class DataGenerator
    {
        public static List<Entity> mockEntities()
        {

            List<Entity> entities = new List<Entity>();

            Entity entity1 = new Entity();
            entity1.emisId = 1;
            entity1.procCode = "PROC UNRA";
            entity1.entityName = "UNRA";
            entity1.contactNumber = "0789112299";
            entity1.email = "ed@unra.go.ug";
            entity1.physicalAddress = "Kampala Uganda";
            entity1.website = "test.com";

            //save the mock branches
            List<Branch> entity1Branches = mockBranches(entity1.emisId);
            new DatabaseHandler().updateBranches(DatabaseHandler.dbConnection(), entity1Branches);
            
            Entity entity2 = new Entity();
            entity2.emisId = 2;
            entity2.procCode = "PROC PPDA";
            entity2.entityName = "PPDA";
            entity2.contactNumber = "0701991122";
            entity2.email = "ed@ppda.go.ug";
            entity2.physicalAddress = "Kampala Uganda";
            entity2.website = "ppda.go.ug";

            //save the mock braches
            List<Branch> entity2Branches = mockBranches(entity2.emisId);
            new DatabaseHandler().updateBranches(DatabaseHandler.dbConnection(), entity2Branches);

            entities.Add(entity1);
            entities.Add(entity2);

            return entities;

        }

        internal static List<District> mockDistricts()
        {

            List<District> districts = new List<District>();

            District d1 = new District();
            d1.emisId = 1;
            d1.districtName = "Kampala";
            d1.regionEmisId = 1;


            District d2 = new District();
            d2.emisId = 2;
            d2.districtName = "Wakiso";
            d2.regionEmisId = 1;

            districts.Add(d1);
            districts.Add(d2);

            return districts;

        }

        public static List<AttendeeType> attendeeTypes()
        {
            List<AttendeeType> types = new List<AttendeeType>();
            types.Add(new AttendeeType(@"App\User", "PPDA User"));
            types.Add(new AttendeeType(@"App\Person", "Entity Person"));
            return types;
        }

        internal static List<PmActivity> mockPmActivities()
        {
            return new List<PmActivity>();
        }

        internal static List<CbActivity> mockCbActivities()
        {
            return new List<CbActivity>();
        }

        internal static List<FinacialYear> mockFinancialYears()
        {
            List<FinacialYear> years = new List<FinacialYear>();

            var year1 = new FinacialYear();
            var year2 = new FinacialYear();
            var year3 = new FinacialYear();
            var year4 = new FinacialYear();
            var year5 = new FinacialYear();
            var year6 = new FinacialYear();

            year1.financial_year = "2017/2018"; year1.emisId = 1; years.Add(year1);
            year2.financial_year = "2018/2019"; year2.emisId = 2; years.Add(year2);
            year3.financial_year = "2019/2020"; year3.emisId = 3; years.Add(year3);
            year4.financial_year = "2020/2021"; year4.emisId = 4; years.Add(year4);
            year5.financial_year = "2021/2022"; year5.emisId = 5; years.Add(year5);
            year6.financial_year = "2022/2023"; year6.emisId = 6; years.Add(year6);

            return years;
        }

        internal static List<ManagementLetterSection> mockManagementLetterExceptionSections()
        {
            return new List<ManagementLetterSection>();
        }

        private static List<Branch> mockBranches(int emisId)
        {
            List<Branch> branches = new List<Branch>();

            Branch branch1 = new Branch();
            branch1.entityEmisId = emisId;
            branch1.branchName = "HEAD OFFICE";
            branch1.town = "Nakawa";
            branch1.district = "Kampala";

            branches.Add(branch1);
            return branches;            
        }

        public static List<ProcurementRole> mockProcurementRoles()
        {
            List<ProcurementRole> roles = new List<ProcurementRole>();
            roles.Add(new ProcurementRole(1, "Head PDU"));
            roles.Add(new ProcurementRole(2, "Procurement Officer"));
            roles.Add(new ProcurementRole(3, "Accounting Officer"));
            roles.Add(new ProcurementRole(4, "Procurement Auditor"));

            return roles;
        }

        public static List<String> procurementMethods()
        {
            List<String> data = new List<string>();
            data.Add("Open Domestic Bidding");
            data.Add("Open International Bidding");
            data.Add("Restricted Domestic Bidding");
            data.Add("Restricted International Bidding");
            data.Add("RFQ");
            data.Add("Selective Bidding");
            data.Add("Direct Procurement");
            data.Add("Micro");
            data.Add("NA");
            return data;
        }

        public static List<String> mockProviders()
        {
            List<String> data = new List<string>();
            data.Add("NIRA");
            data.Add("KCCA");
            return data;
        }

        internal static List<User> mockUsers()
        {
            return new List<User>();
        }
    }

}

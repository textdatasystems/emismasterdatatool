﻿namespace EmisMasterDataTool
{
    partial class SubDialogPmAuditLaunch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubDialogPmAuditLaunch));
            this.panelAuditLauch = new System.Windows.Forms.Panel();
            this.tabAuditLauch = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gridViewAttendees = new System.Windows.Forms.DataGridView();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Entity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telephone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MeetingRole = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.btnRegisterAttendee = new System.Windows.Forms.Button();
            this.labelActivityName = new System.Windows.Forms.Label();
            this.panelAuditLauch.SuspendLayout();
            this.tabAuditLauch.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAttendees)).BeginInit();
            this.SuspendLayout();
            // 
            // panelAuditLauch
            // 
            this.panelAuditLauch.Controls.Add(this.tabAuditLauch);
            this.panelAuditLauch.Controls.Add(this.labelActivityName);
            this.panelAuditLauch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAuditLauch.Location = new System.Drawing.Point(0, 0);
            this.panelAuditLauch.Name = "panelAuditLauch";
            this.panelAuditLauch.Padding = new System.Windows.Forms.Padding(10);
            this.panelAuditLauch.Size = new System.Drawing.Size(1019, 502);
            this.panelAuditLauch.TabIndex = 0;
            // 
            // tabAuditLauch
            // 
            this.tabAuditLauch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabAuditLauch.Controls.Add(this.tabPage1);
            this.tabAuditLauch.Controls.Add(this.tabPage2);
            this.tabAuditLauch.Location = new System.Drawing.Point(13, 39);
            this.tabAuditLauch.Name = "tabAuditLauch";
            this.tabAuditLauch.SelectedIndex = 0;
            this.tabAuditLauch.Size = new System.Drawing.Size(1001, 450);
            this.tabAuditLauch.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(993, 424);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Brief Notes";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.richTextBox1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(987, 418);
            this.panel2.TabIndex = 2;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(17, 30);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(955, 373);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Brief Notes";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(993, 424);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Participant Registration";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.gridViewAttendees);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.btnRegisterAttendee);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(10);
            this.panel3.Size = new System.Drawing.Size(987, 418);
            this.panel3.TabIndex = 3;
            // 
            // gridViewAttendees
            // 
            this.gridViewAttendees.AllowUserToAddRows = false;
            this.gridViewAttendees.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridViewAttendees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewAttendees.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FirstName,
            this.LastName,
            this.Title,
            this.Entity,
            this.Email,
            this.Telephone,
            this.MeetingRole});
            this.gridViewAttendees.Location = new System.Drawing.Point(17, 44);
            this.gridViewAttendees.Name = "gridViewAttendees";
            this.gridViewAttendees.ReadOnly = true;
            this.gridViewAttendees.Size = new System.Drawing.Size(955, 362);
            this.gridViewAttendees.TabIndex = 2;
            // 
            // FirstName
            // 
            this.FirstName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FirstName.DataPropertyName = "firstName";
            this.FirstName.FillWeight = 2F;
            this.FirstName.HeaderText = "First Name";
            this.FirstName.Name = "FirstName";
            this.FirstName.ReadOnly = true;
            // 
            // LastName
            // 
            this.LastName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LastName.DataPropertyName = "lastName";
            this.LastName.FillWeight = 2F;
            this.LastName.HeaderText = "Last Name";
            this.LastName.Name = "LastName";
            this.LastName.ReadOnly = true;
            // 
            // Title
            // 
            this.Title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Title.DataPropertyName = "title";
            this.Title.FillWeight = 2F;
            this.Title.HeaderText = "Title";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            // 
            // Entity
            // 
            this.Entity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Entity.DataPropertyName = "entityName";
            this.Entity.FillWeight = 4F;
            this.Entity.HeaderText = "Entity";
            this.Entity.Name = "Entity";
            this.Entity.ReadOnly = true;
            // 
            // Email
            // 
            this.Email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Email.DataPropertyName = "email";
            this.Email.FillWeight = 3F;
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            // 
            // Telephone
            // 
            this.Telephone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Telephone.DataPropertyName = "phoneNo";
            this.Telephone.FillWeight = 2F;
            this.Telephone.HeaderText = "Telephone";
            this.Telephone.Name = "Telephone";
            this.Telephone.ReadOnly = true;
            // 
            // MeetingRole
            // 
            this.MeetingRole.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MeetingRole.DataPropertyName = "roleName";
            this.MeetingRole.FillWeight = 3F;
            this.MeetingRole.HeaderText = "Meeting Role";
            this.MeetingRole.Name = "MeetingRole";
            this.MeetingRole.ReadOnly = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(154, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(130, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Print Attendee List";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnRegisterAttendee
            // 
            this.btnRegisterAttendee.Location = new System.Drawing.Point(17, 14);
            this.btnRegisterAttendee.Name = "btnRegisterAttendee";
            this.btnRegisterAttendee.Size = new System.Drawing.Size(130, 23);
            this.btnRegisterAttendee.TabIndex = 0;
            this.btnRegisterAttendee.Text = "Register Attendee";
            this.btnRegisterAttendee.UseVisualStyleBackColor = true;
            this.btnRegisterAttendee.Click += new System.EventHandler(this.btnRegisterAttendee_Click_1);
            // 
            // labelActivityName
            // 
            this.labelActivityName.AutoSize = true;
            this.labelActivityName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivityName.Location = new System.Drawing.Point(17, 12);
            this.labelActivityName.Margin = new System.Windows.Forms.Padding(10);
            this.labelActivityName.Name = "labelActivityName";
            this.labelActivityName.Size = new System.Drawing.Size(384, 13);
            this.labelActivityName.TabIndex = 2;
            this.labelActivityName.Text = "Uganda Police Force - Performance Audits - FY 2019-2020 - Audit Launch";
            // 
            // SubDialogPmAuditLaunch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 502);
            this.Controls.Add(this.panelAuditLauch);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SubDialogPmAuditLaunch";
            this.Text = "Audit Launch";
            this.panelAuditLauch.ResumeLayout(false);
            this.panelAuditLauch.PerformLayout();
            this.tabAuditLauch.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAttendees)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelAuditLauch;
        private System.Windows.Forms.TabControl tabAuditLauch;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label labelActivityName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView gridViewAttendees;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Entity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telephone;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeetingRole;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnRegisterAttendee;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label2;
    }
}
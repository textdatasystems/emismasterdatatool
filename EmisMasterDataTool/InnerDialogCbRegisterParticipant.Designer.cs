﻿namespace EmisMasterDataTool
{
    partial class InnerDialogCbRegisterParticipant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InnerDialogCbRegisterParticipant));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSaveParticipant = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txvTitle = new System.Windows.Forms.TextBox();
            this.txvLastName = new System.Windows.Forms.TextBox();
            this.txvFirstName = new System.Windows.Forms.TextBox();
            this.cbxProcurementRole = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txvNote = new System.Windows.Forms.RichTextBox();
            this.txvPhoneNo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txvEmail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbtnGenderFemale = new System.Windows.Forms.RadioButton();
            this.rbtnGenderMale = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbxEntities = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSaveParticipant);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(735, 460);
            this.panel1.TabIndex = 1;
            // 
            // btnSaveParticipant
            // 
            this.btnSaveParticipant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveParticipant.Location = new System.Drawing.Point(617, 386);
            this.btnSaveParticipant.Name = "btnSaveParticipant";
            this.btnSaveParticipant.Size = new System.Drawing.Size(106, 27);
            this.btnSaveParticipant.TabIndex = 2;
            this.btnSaveParticipant.Text = "Save Participant";
            this.btnSaveParticipant.UseVisualStyleBackColor = true;
            this.btnSaveParticipant.Click += new System.EventHandler(this.btnSaveParticipant_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txvTitle, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txvLastName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txvFirstName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbxProcurementRole, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.txvNote, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txvPhoneNo, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.txvEmail, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbxEntities, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 52);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(707, 313);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(222, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "First Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(221, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 35);
            this.label3.TabIndex = 1;
            this.label3.Text = "Last Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(252, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 35);
            this.label4.TabIndex = 2;
            this.label4.Text = "Title";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(176, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 35);
            this.label5.TabIndex = 3;
            this.label5.Text = "Role in Procurement";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvTitle
            // 
            this.txvTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvTitle.Location = new System.Drawing.Point(285, 73);
            this.txvTitle.Name = "txvTitle";
            this.txvTitle.Size = new System.Drawing.Size(162, 20);
            this.txvTitle.TabIndex = 10;
            // 
            // txvLastName
            // 
            this.txvLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvLastName.Location = new System.Drawing.Point(285, 38);
            this.txvLastName.Name = "txvLastName";
            this.txvLastName.Size = new System.Drawing.Size(162, 20);
            this.txvLastName.TabIndex = 9;
            // 
            // txvFirstName
            // 
            this.txvFirstName.Dock = System.Windows.Forms.DockStyle.Left;
            this.txvFirstName.Location = new System.Drawing.Point(285, 3);
            this.txvFirstName.Name = "txvFirstName";
            this.txvFirstName.Size = new System.Drawing.Size(162, 20);
            this.txvFirstName.TabIndex = 8;
            // 
            // cbxProcurementRole
            // 
            this.cbxProcurementRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxProcurementRole.FormattingEnabled = true;
            this.cbxProcurementRole.Location = new System.Drawing.Point(285, 108);
            this.cbxProcurementRole.Name = "cbxProcurementRole";
            this.cbxProcurementRole.Size = new System.Drawing.Size(162, 21);
            this.cbxProcurementRole.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(249, 280);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 35);
            this.label9.TabIndex = 7;
            this.label9.Text = "Note";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txvNote
            // 
            this.txvNote.Location = new System.Drawing.Point(285, 283);
            this.txvNote.Name = "txvNote";
            this.txvNote.Size = new System.Drawing.Size(162, 20);
            this.txvNote.TabIndex = 14;
            this.txvNote.Text = "";
            this.txvNote.TextChanged += new System.EventHandler(this.txvNote_TextChanged);
            // 
            // txvPhoneNo
            // 
            this.txvPhoneNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvPhoneNo.Location = new System.Drawing.Point(285, 248);
            this.txvPhoneNo.Name = "txvPhoneNo";
            this.txvPhoneNo.Size = new System.Drawing.Size(162, 20);
            this.txvPhoneNo.TabIndex = 11;
            this.txvPhoneNo.TextChanged += new System.EventHandler(this.txvPhoneNo_TextChanged);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(201, 245);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 35);
            this.label8.TabIndex = 6;
            this.label8.Text = "Phone Number";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvEmail
            // 
            this.txvEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvEmail.Location = new System.Drawing.Point(285, 213);
            this.txvEmail.Name = "txvEmail";
            this.txvEmail.Size = new System.Drawing.Size(162, 20);
            this.txvEmail.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(247, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 35);
            this.label7.TabIndex = 5;
            this.label7.Text = "Email";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rbtnGenderFemale);
            this.panel2.Controls.Add(this.rbtnGenderMale);
            this.panel2.Location = new System.Drawing.Point(285, 178);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(342, 29);
            this.panel2.TabIndex = 3;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // rbtnGenderFemale
            // 
            this.rbtnGenderFemale.AutoSize = true;
            this.rbtnGenderFemale.Location = new System.Drawing.Point(103, 6);
            this.rbtnGenderFemale.Name = "rbtnGenderFemale";
            this.rbtnGenderFemale.Size = new System.Drawing.Size(59, 17);
            this.rbtnGenderFemale.TabIndex = 1;
            this.rbtnGenderFemale.Text = "Female";
            this.rbtnGenderFemale.UseVisualStyleBackColor = true;
            // 
            // rbtnGenderMale
            // 
            this.rbtnGenderMale.AutoSize = true;
            this.rbtnGenderMale.Checked = true;
            this.rbtnGenderMale.Location = new System.Drawing.Point(12, 6);
            this.rbtnGenderMale.Name = "rbtnGenderMale";
            this.rbtnGenderMale.Size = new System.Drawing.Size(48, 17);
            this.rbtnGenderMale.TabIndex = 0;
            this.rbtnGenderMale.TabStop = true;
            this.rbtnGenderMale.Text = "Male";
            this.rbtnGenderMale.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(237, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 35);
            this.label6.TabIndex = 4;
            this.label6.Text = "Gender";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(246, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 35);
            this.label10.TabIndex = 15;
            this.label10.Text = "Entity";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbxEntities
            // 
            this.cbxEntities.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxEntities.FormattingEnabled = true;
            this.cbxEntities.Location = new System.Drawing.Point(285, 143);
            this.cbxEntities.Name = "cbxEntities";
            this.cbxEntities.Size = new System.Drawing.Size(162, 21);
            this.cbxEntities.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "PARTICIPANT DETAILS";
            // 
            // InnerDialogCbRegisterParticipant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 460);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InnerDialogCbRegisterParticipant";
            this.Text = "REGISTER PARTICIPANT";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSaveParticipant;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbtnGenderFemale;
        private System.Windows.Forms.RadioButton rbtnGenderMale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txvPhoneNo;
        private System.Windows.Forms.TextBox txvEmail;
        private System.Windows.Forms.TextBox txvTitle;
        private System.Windows.Forms.TextBox txvLastName;
        private System.Windows.Forms.TextBox txvFirstName;
        private System.Windows.Forms.ComboBox cbxProcurementRole;
        private System.Windows.Forms.RichTextBox txvNote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbxEntities;
    }
}
﻿namespace EmisMasterDataTool
{
    partial class InnerDialogCustomRegisterMeetingAttendee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InnerDialogCustomRegisterMeetingAttendee));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSaveAttendee = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnAddEntityPerson = new System.Windows.Forms.Button();
            this.cbxPeople = new System.Windows.Forms.ComboBox();
            this.cbxUsers = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelPpdaUser = new System.Windows.Forms.Label();
            this.labelEntityPerson = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxMeetingRole = new System.Windows.Forms.ComboBox();
            this.cbxCommentableType = new System.Windows.Forms.ComboBox();
            this.labelActivityName = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSaveAttendee);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.labelActivityName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(657, 298);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnSaveAttendee
            // 
            this.btnSaveAttendee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAttendee.Location = new System.Drawing.Point(389, 223);
            this.btnSaveAttendee.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.btnSaveAttendee.Name = "btnSaveAttendee";
            this.btnSaveAttendee.Size = new System.Drawing.Size(127, 23);
            this.btnSaveAttendee.TabIndex = 2;
            this.btnSaveAttendee.Text = "Save Attendee";
            this.btnSaveAttendee.UseVisualStyleBackColor = true;
            this.btnSaveAttendee.Click += new System.EventHandler(this.btnSaveAttendee_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbxUsers, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelPpdaUser, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelEntityPerson, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbxCommentableType, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbxMeetingRole, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 64);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(628, 136);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnAddEntityPerson);
            this.panel2.Controls.Add(this.cbxPeople);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(254, 73);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(371, 29);
            this.panel2.TabIndex = 3;
            // 
            // btnAddEntityPerson
            // 
            this.btnAddEntityPerson.Location = new System.Drawing.Point(252, 3);
            this.btnAddEntityPerson.Name = "btnAddEntityPerson";
            this.btnAddEntityPerson.Size = new System.Drawing.Size(75, 23);
            this.btnAddEntityPerson.TabIndex = 9;
            this.btnAddEntityPerson.Text = "Add";
            this.btnAddEntityPerson.UseVisualStyleBackColor = true;
            this.btnAddEntityPerson.Click += new System.EventHandler(this.btnAddEntityPerson_Click);
            // 
            // cbxPeople
            // 
            this.cbxPeople.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.cbxPeople.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPeople.FormattingEnabled = true;
            this.cbxPeople.Location = new System.Drawing.Point(0, 3);
            this.cbxPeople.Name = "cbxPeople";
            this.cbxPeople.Size = new System.Drawing.Size(246, 21);
            this.cbxPeople.TabIndex = 8;
            // 
            // cbxUsers
            // 
            this.cbxUsers.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbxUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxUsers.FormattingEnabled = true;
            this.cbxUsers.Location = new System.Drawing.Point(254, 38);
            this.cbxUsers.Name = "cbxUsers";
            this.cbxUsers.Size = new System.Drawing.Size(246, 21);
            this.cbxUsers.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(171, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "Attendee Type";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPpdaUser
            // 
            this.labelPpdaUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPpdaUser.AutoSize = true;
            this.labelPpdaUser.Location = new System.Drawing.Point(187, 35);
            this.labelPpdaUser.Name = "labelPpdaUser";
            this.labelPpdaUser.Size = new System.Drawing.Size(61, 35);
            this.labelPpdaUser.TabIndex = 1;
            this.labelPpdaUser.Text = "PPDA Staff";
            this.labelPpdaUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEntityPerson
            // 
            this.labelEntityPerson.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelEntityPerson.AutoSize = true;
            this.labelEntityPerson.Location = new System.Drawing.Point(179, 70);
            this.labelEntityPerson.Name = "labelEntityPerson";
            this.labelEntityPerson.Size = new System.Drawing.Size(69, 35);
            this.labelEntityPerson.TabIndex = 2;
            this.labelEntityPerson.Text = "Entity Person";
            this.labelEntityPerson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(132, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 35);
            this.label5.TabIndex = 3;
            this.label5.Text = "Attendee Meeting Role";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbxMeetingRole
            // 
            this.cbxMeetingRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMeetingRole.FormattingEnabled = true;
            this.cbxMeetingRole.Location = new System.Drawing.Point(254, 108);
            this.cbxMeetingRole.Name = "cbxMeetingRole";
            this.cbxMeetingRole.Size = new System.Drawing.Size(246, 21);
            this.cbxMeetingRole.TabIndex = 4;
            // 
            // cbxCommentableType
            // 
            this.cbxCommentableType.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbxCommentableType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCommentableType.FormattingEnabled = true;
            this.cbxCommentableType.Location = new System.Drawing.Point(254, 3);
            this.cbxCommentableType.Name = "cbxCommentableType";
            this.cbxCommentableType.Size = new System.Drawing.Size(246, 21);
            this.cbxCommentableType.TabIndex = 5;
            this.cbxCommentableType.SelectedIndexChanged += new System.EventHandler(this.cbxCommentableType_SelectedIndexChanged);
            // 
            // labelActivityName
            // 
            this.labelActivityName.AutoSize = true;
            this.labelActivityName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivityName.Location = new System.Drawing.Point(12, 23);
            this.labelActivityName.Name = "labelActivityName";
            this.labelActivityName.Size = new System.Drawing.Size(225, 17);
            this.labelActivityName.TabIndex = 0;
            this.labelActivityName.Text = "Register Attendee for Audit launch";
            // 
            // InnerDialogCustomRegisterMeetingAttendee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 298);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InnerDialogCustomRegisterMeetingAttendee";
            this.Text = "REGISTER ATTENDEE";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelActivityName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnSaveAttendee;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPpdaUser;
        private System.Windows.Forms.Label labelEntityPerson;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbxUsers;
        private System.Windows.Forms.ComboBox cbxMeetingRole;
        private System.Windows.Forms.ComboBox cbxCommentableType;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnAddEntityPerson;
        private System.Windows.Forms.ComboBox cbxPeople;
    }
}
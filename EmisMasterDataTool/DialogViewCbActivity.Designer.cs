﻿namespace EmisMasterDataTool
{
    partial class DialogViewCbActivity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogViewCbActivity));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.listActOutcomes = new System.Windows.Forms.ListView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.listActTrainerExt = new System.Windows.Forms.ListView();
            this.listActTrainerInt = new System.Windows.Forms.ListView();
            this.listActBudget = new System.Windows.Forms.ListView();
            this.listActModules = new System.Windows.Forms.ListView();
            this.label3 = new System.Windows.Forms.Label();
            this.txvActualFundingSource = new System.Windows.Forms.Label();
            this.txvActualOwner = new System.Windows.Forms.Label();
            this.txvActualResponsibleOffice = new System.Windows.Forms.Label();
            this.txvActualNoParticipants = new System.Windows.Forms.Label();
            this.txvActualActivityCategory = new System.Windows.Forms.Label();
            this.txvActualActivityName = new System.Windows.Forms.Label();
            this.txvActualDate = new System.Windows.Forms.Label();
            this.txvActualNoOfDays = new System.Windows.Forms.Label();
            this.txvActualActivityType = new System.Windows.Forms.Label();
            this.txvActualSubject = new System.Windows.Forms.Label();
            this.txvActualActivityDays = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listPlnOutcomes = new System.Windows.Forms.ListView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.listPlnTrainerExt = new System.Windows.Forms.ListView();
            this.listPlnTrainerInt = new System.Windows.Forms.ListView();
            this.listPlnBudget = new System.Windows.Forms.ListView();
            this.listPlnModules = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.txvPlnFundingSource = new System.Windows.Forms.Label();
            this.txvPlnOwner = new System.Windows.Forms.Label();
            this.txvPlnNoParticipants = new System.Windows.Forms.Label();
            this.txvPlnActivityName = new System.Windows.Forms.Label();
            this.txvPlnNoOfDays = new System.Windows.Forms.Label();
            this.txvPlnSubject = new System.Windows.Forms.Label();
            this.txvPlnActivityDays = new System.Windows.Forms.Label();
            this.txvPlnActivityType = new System.Windows.Forms.Label();
            this.txvPlnDate = new System.Windows.Forms.Label();
            this.txvPlnActivityCategory = new System.Windows.Forms.Label();
            this.txvPlnResponsibleOffice = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnOpenParticpantFeedbackSurvey = new System.Windows.Forms.Button();
            this.btnParticipantRegistrastion = new System.Windows.Forms.Button();
            this.labelNavActivityName = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.panel4, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 73);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 608F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1160, 609);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.SystemColors.Menu;
            this.panel4.Controls.Add(this.listActOutcomes);
            this.panel4.Controls.Add(this.tableLayoutPanel3);
            this.panel4.Controls.Add(this.listActBudget);
            this.panel4.Controls.Add(this.listActModules);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.txvActualFundingSource);
            this.panel4.Controls.Add(this.txvActualOwner);
            this.panel4.Controls.Add(this.txvActualResponsibleOffice);
            this.panel4.Controls.Add(this.txvActualNoParticipants);
            this.panel4.Controls.Add(this.txvActualActivityCategory);
            this.panel4.Controls.Add(this.txvActualActivityName);
            this.panel4.Controls.Add(this.txvActualDate);
            this.panel4.Controls.Add(this.txvActualNoOfDays);
            this.panel4.Controls.Add(this.txvActualActivityType);
            this.panel4.Controls.Add(this.txvActualSubject);
            this.panel4.Controls.Add(this.txvActualActivityDays);
            this.panel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(699, 4);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(10);
            this.panel4.Size = new System.Drawing.Size(457, 601);
            this.panel4.TabIndex = 2;
            // 
            // listActOutcomes
            // 
            this.listActOutcomes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listActOutcomes.Location = new System.Drawing.Point(15, 252);
            this.listActOutcomes.Name = "listActOutcomes";
            this.listActOutcomes.Size = new System.Drawing.Size(426, 72);
            this.listActOutcomes.TabIndex = 30;
            this.listActOutcomes.UseCompatibleStateImageBehavior = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.listActTrainerExt, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.listActTrainerInt, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(13, 408);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(426, 73);
            this.tableLayoutPanel3.TabIndex = 37;
            // 
            // listActTrainerExt
            // 
            this.listActTrainerExt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listActTrainerExt.GridLines = true;
            this.listActTrainerExt.Location = new System.Drawing.Point(216, 3);
            this.listActTrainerExt.Name = "listActTrainerExt";
            this.listActTrainerExt.Size = new System.Drawing.Size(207, 67);
            this.listActTrainerExt.TabIndex = 28;
            this.listActTrainerExt.UseCompatibleStateImageBehavior = false;
            // 
            // listActTrainerInt
            // 
            this.listActTrainerInt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listActTrainerInt.GridLines = true;
            this.listActTrainerInt.Location = new System.Drawing.Point(3, 3);
            this.listActTrainerInt.Name = "listActTrainerInt";
            this.listActTrainerInt.Size = new System.Drawing.Size(207, 67);
            this.listActTrainerInt.TabIndex = 27;
            this.listActTrainerInt.UseCompatibleStateImageBehavior = false;
            // 
            // listActBudget
            // 
            this.listActBudget.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listActBudget.GridLines = true;
            this.listActBudget.Location = new System.Drawing.Point(13, 487);
            this.listActBudget.Name = "listActBudget";
            this.listActBudget.Size = new System.Drawing.Size(426, 87);
            this.listActBudget.TabIndex = 28;
            this.listActBudget.UseCompatibleStateImageBehavior = false;
            // 
            // listActModules
            // 
            this.listActModules.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listActModules.GridLines = true;
            this.listActModules.Location = new System.Drawing.Point(13, 329);
            this.listActModules.Name = "listActModules";
            this.listActModules.Size = new System.Drawing.Size(427, 72);
            this.listActModules.TabIndex = 36;
            this.listActModules.UseCompatibleStateImageBehavior = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "ACTUAL";
            // 
            // txvActualFundingSource
            // 
            this.txvActualFundingSource.AutoSize = true;
            this.txvActualFundingSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualFundingSource.Location = new System.Drawing.Point(14, 223);
            this.txvActualFundingSource.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualFundingSource.Name = "txvActualFundingSource";
            this.txvActualFundingSource.Size = new System.Drawing.Size(82, 13);
            this.txvActualFundingSource.TabIndex = 34;
            this.txvActualFundingSource.Text = "Funding Source";
            // 
            // txvActualOwner
            // 
            this.txvActualOwner.AutoSize = true;
            this.txvActualOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualOwner.Location = new System.Drawing.Point(14, 53);
            this.txvActualOwner.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualOwner.Name = "txvActualOwner";
            this.txvActualOwner.Size = new System.Drawing.Size(38, 13);
            this.txvActualOwner.TabIndex = 24;
            this.txvActualOwner.Text = "Owner";
            // 
            // txvActualResponsibleOffice
            // 
            this.txvActualResponsibleOffice.AutoSize = true;
            this.txvActualResponsibleOffice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualResponsibleOffice.Location = new System.Drawing.Point(14, 138);
            this.txvActualResponsibleOffice.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualResponsibleOffice.Name = "txvActualResponsibleOffice";
            this.txvActualResponsibleOffice.Size = new System.Drawing.Size(96, 13);
            this.txvActualResponsibleOffice.TabIndex = 29;
            this.txvActualResponsibleOffice.Text = "Responsible Office";
            // 
            // txvActualNoParticipants
            // 
            this.txvActualNoParticipants.AutoSize = true;
            this.txvActualNoParticipants.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualNoParticipants.Location = new System.Drawing.Point(14, 206);
            this.txvActualNoParticipants.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualNoParticipants.Name = "txvActualNoParticipants";
            this.txvActualNoParticipants.Size = new System.Drawing.Size(114, 13);
            this.txvActualNoParticipants.TabIndex = 33;
            this.txvActualNoParticipants.Text = "Number of Participants";
            // 
            // txvActualActivityCategory
            // 
            this.txvActualActivityCategory.AutoSize = true;
            this.txvActualActivityCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualActivityCategory.Location = new System.Drawing.Point(14, 121);
            this.txvActualActivityCategory.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualActivityCategory.Name = "txvActualActivityCategory";
            this.txvActualActivityCategory.Size = new System.Drawing.Size(86, 13);
            this.txvActualActivityCategory.TabIndex = 28;
            this.txvActualActivityCategory.Text = "Activity Category";
            // 
            // txvActualActivityName
            // 
            this.txvActualActivityName.AutoSize = true;
            this.txvActualActivityName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualActivityName.Location = new System.Drawing.Point(14, 70);
            this.txvActualActivityName.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualActivityName.Name = "txvActualActivityName";
            this.txvActualActivityName.Size = new System.Drawing.Size(72, 13);
            this.txvActualActivityName.TabIndex = 25;
            this.txvActualActivityName.Text = "Activity Name";
            // 
            // txvActualDate
            // 
            this.txvActualDate.AutoSize = true;
            this.txvActualDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualDate.Location = new System.Drawing.Point(14, 155);
            this.txvActualDate.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualDate.Name = "txvActualDate";
            this.txvActualDate.Size = new System.Drawing.Size(30, 13);
            this.txvActualDate.TabIndex = 30;
            this.txvActualDate.Text = "Date";
            // 
            // txvActualNoOfDays
            // 
            this.txvActualNoOfDays.AutoSize = true;
            this.txvActualNoOfDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualNoOfDays.Location = new System.Drawing.Point(14, 189);
            this.txvActualNoOfDays.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualNoOfDays.Name = "txvActualNoOfDays";
            this.txvActualNoOfDays.Size = new System.Drawing.Size(83, 13);
            this.txvActualNoOfDays.TabIndex = 32;
            this.txvActualNoOfDays.Text = "Number of Days";
            // 
            // txvActualActivityType
            // 
            this.txvActualActivityType.AutoSize = true;
            this.txvActualActivityType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualActivityType.Location = new System.Drawing.Point(14, 104);
            this.txvActualActivityType.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualActivityType.Name = "txvActualActivityType";
            this.txvActualActivityType.Size = new System.Drawing.Size(68, 13);
            this.txvActualActivityType.TabIndex = 27;
            this.txvActualActivityType.Text = "Activity Type";
            // 
            // txvActualSubject
            // 
            this.txvActualSubject.AutoSize = true;
            this.txvActualSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualSubject.Location = new System.Drawing.Point(14, 87);
            this.txvActualSubject.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualSubject.Name = "txvActualSubject";
            this.txvActualSubject.Size = new System.Drawing.Size(80, 13);
            this.txvActualSubject.TabIndex = 26;
            this.txvActualSubject.Text = "Activity Subject";
            // 
            // txvActualActivityDays
            // 
            this.txvActualActivityDays.AutoSize = true;
            this.txvActualActivityDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvActualActivityDays.Location = new System.Drawing.Point(14, 172);
            this.txvActualActivityDays.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvActualActivityDays.Name = "txvActualActivityDays";
            this.txvActualActivityDays.Size = new System.Drawing.Size(68, 13);
            this.txvActualActivityDays.TabIndex = 31;
            this.txvActualActivityDays.Text = "Activity Days";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.Gainsboro;
            this.panel3.Controls.Add(this.listPlnOutcomes);
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Controls.Add(this.listPlnBudget);
            this.panel3.Controls.Add(this.listPlnModules);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.txvPlnFundingSource);
            this.panel3.Controls.Add(this.txvPlnOwner);
            this.panel3.Controls.Add(this.txvPlnNoParticipants);
            this.panel3.Controls.Add(this.txvPlnActivityName);
            this.panel3.Controls.Add(this.txvPlnNoOfDays);
            this.panel3.Controls.Add(this.txvPlnSubject);
            this.panel3.Controls.Add(this.txvPlnActivityDays);
            this.panel3.Controls.Add(this.txvPlnActivityType);
            this.panel3.Controls.Add(this.txvPlnDate);
            this.panel3.Controls.Add(this.txvPlnActivityCategory);
            this.panel3.Controls.Add(this.txvPlnResponsibleOffice);
            this.panel3.Location = new System.Drawing.Point(236, 4);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(10);
            this.panel3.Size = new System.Drawing.Size(456, 601);
            this.panel3.TabIndex = 1;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // listPlnOutcomes
            // 
            this.listPlnOutcomes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listPlnOutcomes.Location = new System.Drawing.Point(17, 252);
            this.listPlnOutcomes.Name = "listPlnOutcomes";
            this.listPlnOutcomes.Size = new System.Drawing.Size(426, 72);
            this.listPlnOutcomes.TabIndex = 29;
            this.listPlnOutcomes.UseCompatibleStateImageBehavior = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.listPlnTrainerExt, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.listPlnTrainerInt, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(17, 408);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(426, 73);
            this.tableLayoutPanel2.TabIndex = 28;
            // 
            // listPlnTrainerExt
            // 
            this.listPlnTrainerExt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPlnTrainerExt.GridLines = true;
            this.listPlnTrainerExt.Location = new System.Drawing.Point(216, 3);
            this.listPlnTrainerExt.Name = "listPlnTrainerExt";
            this.listPlnTrainerExt.Size = new System.Drawing.Size(207, 67);
            this.listPlnTrainerExt.TabIndex = 26;
            this.listPlnTrainerExt.UseCompatibleStateImageBehavior = false;
            // 
            // listPlnTrainerInt
            // 
            this.listPlnTrainerInt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listPlnTrainerInt.GridLines = true;
            this.listPlnTrainerInt.Location = new System.Drawing.Point(3, 3);
            this.listPlnTrainerInt.Name = "listPlnTrainerInt";
            this.listPlnTrainerInt.Size = new System.Drawing.Size(207, 67);
            this.listPlnTrainerInt.TabIndex = 25;
            this.listPlnTrainerInt.UseCompatibleStateImageBehavior = false;
            // 
            // listPlnBudget
            // 
            this.listPlnBudget.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listPlnBudget.GridLines = true;
            this.listPlnBudget.Location = new System.Drawing.Point(17, 487);
            this.listPlnBudget.Name = "listPlnBudget";
            this.listPlnBudget.Size = new System.Drawing.Size(426, 87);
            this.listPlnBudget.TabIndex = 27;
            this.listPlnBudget.UseCompatibleStateImageBehavior = false;
            // 
            // listPlnModules
            // 
            this.listPlnModules.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listPlnModules.GridLines = true;
            this.listPlnModules.Location = new System.Drawing.Point(17, 329);
            this.listPlnModules.Name = "listPlnModules";
            this.listPlnModules.Size = new System.Drawing.Size(426, 72);
            this.listPlnModules.TabIndex = 24;
            this.listPlnModules.UseCompatibleStateImageBehavior = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "PLANNED";
            // 
            // txvPlnFundingSource
            // 
            this.txvPlnFundingSource.AutoSize = true;
            this.txvPlnFundingSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnFundingSource.Location = new System.Drawing.Point(14, 223);
            this.txvPlnFundingSource.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnFundingSource.Name = "txvPlnFundingSource";
            this.txvPlnFundingSource.Size = new System.Drawing.Size(82, 13);
            this.txvPlnFundingSource.TabIndex = 22;
            this.txvPlnFundingSource.Text = "Funding Source";
            // 
            // txvPlnOwner
            // 
            this.txvPlnOwner.AutoSize = true;
            this.txvPlnOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnOwner.Location = new System.Drawing.Point(14, 53);
            this.txvPlnOwner.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnOwner.Name = "txvPlnOwner";
            this.txvPlnOwner.Size = new System.Drawing.Size(38, 13);
            this.txvPlnOwner.TabIndex = 12;
            this.txvPlnOwner.Text = "Owner";
            // 
            // txvPlnNoParticipants
            // 
            this.txvPlnNoParticipants.AutoSize = true;
            this.txvPlnNoParticipants.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnNoParticipants.Location = new System.Drawing.Point(14, 206);
            this.txvPlnNoParticipants.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnNoParticipants.Name = "txvPlnNoParticipants";
            this.txvPlnNoParticipants.Size = new System.Drawing.Size(114, 13);
            this.txvPlnNoParticipants.TabIndex = 21;
            this.txvPlnNoParticipants.Text = "Number of Participants";
            // 
            // txvPlnActivityName
            // 
            this.txvPlnActivityName.AutoSize = true;
            this.txvPlnActivityName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnActivityName.Location = new System.Drawing.Point(14, 70);
            this.txvPlnActivityName.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnActivityName.Name = "txvPlnActivityName";
            this.txvPlnActivityName.Size = new System.Drawing.Size(72, 13);
            this.txvPlnActivityName.TabIndex = 13;
            this.txvPlnActivityName.Text = "Activity Name";
            // 
            // txvPlnNoOfDays
            // 
            this.txvPlnNoOfDays.AutoSize = true;
            this.txvPlnNoOfDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnNoOfDays.Location = new System.Drawing.Point(14, 189);
            this.txvPlnNoOfDays.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnNoOfDays.Name = "txvPlnNoOfDays";
            this.txvPlnNoOfDays.Size = new System.Drawing.Size(83, 13);
            this.txvPlnNoOfDays.TabIndex = 20;
            this.txvPlnNoOfDays.Text = "Number of Days";
            // 
            // txvPlnSubject
            // 
            this.txvPlnSubject.AutoSize = true;
            this.txvPlnSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnSubject.Location = new System.Drawing.Point(14, 87);
            this.txvPlnSubject.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnSubject.Name = "txvPlnSubject";
            this.txvPlnSubject.Size = new System.Drawing.Size(80, 13);
            this.txvPlnSubject.TabIndex = 14;
            this.txvPlnSubject.Text = "Activity Subject";
            // 
            // txvPlnActivityDays
            // 
            this.txvPlnActivityDays.AutoSize = true;
            this.txvPlnActivityDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnActivityDays.Location = new System.Drawing.Point(14, 172);
            this.txvPlnActivityDays.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnActivityDays.Name = "txvPlnActivityDays";
            this.txvPlnActivityDays.Size = new System.Drawing.Size(68, 13);
            this.txvPlnActivityDays.TabIndex = 19;
            this.txvPlnActivityDays.Text = "Activity Days";
            // 
            // txvPlnActivityType
            // 
            this.txvPlnActivityType.AutoSize = true;
            this.txvPlnActivityType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnActivityType.Location = new System.Drawing.Point(14, 104);
            this.txvPlnActivityType.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnActivityType.Name = "txvPlnActivityType";
            this.txvPlnActivityType.Size = new System.Drawing.Size(68, 13);
            this.txvPlnActivityType.TabIndex = 15;
            this.txvPlnActivityType.Text = "Activity Type";
            // 
            // txvPlnDate
            // 
            this.txvPlnDate.AutoSize = true;
            this.txvPlnDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnDate.Location = new System.Drawing.Point(14, 155);
            this.txvPlnDate.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnDate.Name = "txvPlnDate";
            this.txvPlnDate.Size = new System.Drawing.Size(30, 13);
            this.txvPlnDate.TabIndex = 18;
            this.txvPlnDate.Text = "Date";
            // 
            // txvPlnActivityCategory
            // 
            this.txvPlnActivityCategory.AutoSize = true;
            this.txvPlnActivityCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnActivityCategory.Location = new System.Drawing.Point(14, 121);
            this.txvPlnActivityCategory.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnActivityCategory.Name = "txvPlnActivityCategory";
            this.txvPlnActivityCategory.Size = new System.Drawing.Size(86, 13);
            this.txvPlnActivityCategory.TabIndex = 16;
            this.txvPlnActivityCategory.Text = "Activity Category";
            // 
            // txvPlnResponsibleOffice
            // 
            this.txvPlnResponsibleOffice.AutoSize = true;
            this.txvPlnResponsibleOffice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txvPlnResponsibleOffice.Location = new System.Drawing.Point(14, 138);
            this.txvPlnResponsibleOffice.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.txvPlnResponsibleOffice.Name = "txvPlnResponsibleOffice";
            this.txvPlnResponsibleOffice.Size = new System.Drawing.Size(96, 13);
            this.txvPlnResponsibleOffice.TabIndex = 17;
            this.txvPlnResponsibleOffice.Text = "Responsible Office";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(225, 601);
            this.panel2.TabIndex = 0;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label18.Location = new System.Drawing.Point(141, 411);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Activity Team";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label17.Location = new System.Drawing.Point(165, 329);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "Modules";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(171, 487);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Budget";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(162, 252);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "Outcome";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(130, 223);
            this.label14.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Funding Source";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label13.Location = new System.Drawing.Point(98, 206);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(114, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Number of Participants";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(129, 189);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Number of Days";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label11.Location = new System.Drawing.Point(144, 172);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Activity Days";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(182, 155);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Date";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(116, 138);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Responsible Office";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(126, 121);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Activity Category";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(144, 104);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Activity Type";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(132, 87);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Activity Subject";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(140, 70);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Activity Name";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(174, 53);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Owner";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.labelNavActivityName);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(1184, 695);
            this.panel1.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.btnOpenParticpantFeedbackSurvey);
            this.panel5.Controls.Add(this.btnParticipantRegistrastion);
            this.panel5.Location = new System.Drawing.Point(19, 30);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1149, 37);
            this.panel5.TabIndex = 2;
            // 
            // btnOpenParticpantFeedbackSurvey
            // 
            this.btnOpenParticpantFeedbackSurvey.Location = new System.Drawing.Point(152, 7);
            this.btnOpenParticpantFeedbackSurvey.Name = "btnOpenParticpantFeedbackSurvey";
            this.btnOpenParticpantFeedbackSurvey.Size = new System.Drawing.Size(165, 23);
            this.btnOpenParticpantFeedbackSurvey.TabIndex = 2;
            this.btnOpenParticpantFeedbackSurvey.Text = "Participant Feedback Survey";
            this.btnOpenParticpantFeedbackSurvey.UseVisualStyleBackColor = true;
            this.btnOpenParticpantFeedbackSurvey.Click += new System.EventHandler(this.btnOpenParticpantFeedbackSurvey_Click);
            // 
            // btnParticipantRegistrastion
            // 
            this.btnParticipantRegistrastion.Location = new System.Drawing.Point(4, 7);
            this.btnParticipantRegistrastion.Name = "btnParticipantRegistrastion";
            this.btnParticipantRegistrastion.Size = new System.Drawing.Size(142, 23);
            this.btnParticipantRegistrastion.TabIndex = 1;
            this.btnParticipantRegistrastion.Text = "Participant Registration";
            this.btnParticipantRegistrastion.UseVisualStyleBackColor = true;
            this.btnParticipantRegistrastion.Click += new System.EventHandler(this.btnParticipantRegistrastion_click);
            // 
            // labelNavActivityName
            // 
            this.labelNavActivityName.AutoSize = true;
            this.labelNavActivityName.Location = new System.Drawing.Point(20, 10);
            this.labelNavActivityName.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.labelNavActivityName.Name = "labelNavActivityName";
            this.labelNavActivityName.Size = new System.Drawing.Size(88, 13);
            this.labelNavActivityName.TabIndex = 1;
            this.labelNavActivityName.Text = "CBAS >> PLANS";
            // 
            // DialogViewCbActivity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 695);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DialogViewCbActivity";
            this.Text = "CAPACITY BUILDING ACTIVITY DETAILS";
            this.Load += new System.EventHandler(this.DialogViewCbActivity_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelNavActivityName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label txvActualFundingSource;
        private System.Windows.Forms.Label txvActualOwner;
        private System.Windows.Forms.Label txvActualResponsibleOffice;
        private System.Windows.Forms.Label txvActualNoParticipants;
        private System.Windows.Forms.Label txvActualActivityCategory;
        private System.Windows.Forms.Label txvActualActivityName;
        private System.Windows.Forms.Label txvActualDate;
        private System.Windows.Forms.Label txvActualNoOfDays;
        private System.Windows.Forms.Label txvActualActivityType;
        private System.Windows.Forms.Label txvActualSubject;
        private System.Windows.Forms.Label txvActualActivityDays;
        private System.Windows.Forms.Label txvPlnFundingSource;
        private System.Windows.Forms.Label txvPlnOwner;
        private System.Windows.Forms.Label txvPlnNoParticipants;
        private System.Windows.Forms.Label txvPlnActivityName;
        private System.Windows.Forms.Label txvPlnNoOfDays;
        private System.Windows.Forms.Label txvPlnSubject;
        private System.Windows.Forms.Label txvPlnActivityDays;
        private System.Windows.Forms.Label txvPlnActivityType;
        private System.Windows.Forms.Label txvPlnDate;
        private System.Windows.Forms.Label txvPlnActivityCategory;
        private System.Windows.Forms.Label txvPlnResponsibleOffice;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnOpenParticpantFeedbackSurvey;
        private System.Windows.Forms.Button btnParticipantRegistrastion;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listActModules;
        private System.Windows.Forms.ListView listPlnModules;
        private System.Windows.Forms.ListView listPlnTrainerExt;
        private System.Windows.Forms.ListView listPlnTrainerInt;
        private System.Windows.Forms.ListView listActTrainerExt;
        private System.Windows.Forms.ListView listActTrainerInt;
        private System.Windows.Forms.ListView listPlnBudget;
        private System.Windows.Forms.ListView listActBudget;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ListView listActOutcomes;
        private System.Windows.Forms.ListView listPlnOutcomes;
    }
}
﻿namespace EmisMasterDataTool
{
    partial class DialogViewPmActivity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogViewPmActivity));
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelActivityName = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnExitMeeting = new System.Windows.Forms.Button();
            this.btnDebriefs = new System.Windows.Forms.Button();
            this.btnAuditToolFiles = new System.Windows.Forms.Button();
            this.btnAuditLaunch = new System.Windows.Forms.Button();
            this.btnAuditSample = new System.Windows.Forms.Button();
            this.btnAuditDates = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txvPerformanceScoreCat = new System.Windows.Forms.Label();
            this.txvAcitivityId = new System.Windows.Forms.Label();
            this.txvPerformanceScore = new System.Windows.Forms.Label();
            this.txvEntity = new System.Windows.Forms.Label();
            this.txvLastAudited = new System.Windows.Forms.Label();
            this.txvAcuditType = new System.Windows.Forms.Label();
            this.txvOagYear = new System.Windows.Forms.Label();
            this.txvSourceOfFunding = new System.Windows.Forms.Label();
            this.txvOagOpinion = new System.Windows.Forms.Label();
            this.txvPpdaOffice = new System.Windows.Forms.Label();
            this.txvProcBudget = new System.Windows.Forms.Label();
            this.txvEntityCategory = new System.Windows.Forms.Label();
            this.txvEgpStatus = new System.Windows.Forms.Label();
            this.txvSector = new System.Windows.Forms.Label();
            this.txvGppStatus = new System.Windows.Forms.Label();
            this.txvSpendCategory = new System.Windows.Forms.Label();
            this.txvUsmidStatus = new System.Windows.Forms.Label();
            this.txvEntityType = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.listTeamMembers = new System.Windows.Forms.ListView();
            this.label8 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.listPmBudget = new System.Windows.Forms.ListView();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelPmEntryMeetingDate = new System.Windows.Forms.Label();
            this.txvStartDate = new System.Windows.Forms.Label();
            this.txvEndDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelActivityName);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(1127, 571);
            this.panel1.TabIndex = 0;
            // 
            // labelActivityName
            // 
            this.labelActivityName.AutoSize = true;
            this.labelActivityName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivityName.Location = new System.Drawing.Point(17, 14);
            this.labelActivityName.Name = "labelActivityName";
            this.labelActivityName.Size = new System.Drawing.Size(214, 13);
            this.labelActivityName.TabIndex = 3;
            this.labelActivityName.Text = "Performance Monitoring | FY 2010-2011";
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.btnExitMeeting);
            this.panel8.Controls.Add(this.btnDebriefs);
            this.panel8.Controls.Add(this.btnAuditToolFiles);
            this.panel8.Controls.Add(this.btnAuditLaunch);
            this.panel8.Controls.Add(this.btnAuditSample);
            this.panel8.Controls.Add(this.btnAuditDates);
            this.panel8.Location = new System.Drawing.Point(13, 41);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1102, 37);
            this.panel8.TabIndex = 2;
            // 
            // btnExitMeeting
            // 
            this.btnExitMeeting.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitMeeting.Location = new System.Drawing.Point(513, 7);
            this.btnExitMeeting.Name = "btnExitMeeting";
            this.btnExitMeeting.Size = new System.Drawing.Size(105, 23);
            this.btnExitMeeting.TabIndex = 9;
            this.btnExitMeeting.Text = "Exit Meeting";
            this.btnExitMeeting.UseVisualStyleBackColor = true;
            this.btnExitMeeting.Click += new System.EventHandler(this.btnExitMeeting_Click);
            // 
            // btnDebriefs
            // 
            this.btnDebriefs.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDebriefs.Location = new System.Drawing.Point(418, 7);
            this.btnDebriefs.Name = "btnDebriefs";
            this.btnDebriefs.Size = new System.Drawing.Size(89, 23);
            this.btnDebriefs.TabIndex = 7;
            this.btnDebriefs.Text = "Debriefs";
            this.btnDebriefs.UseVisualStyleBackColor = true;
            this.btnDebriefs.Click += new System.EventHandler(this.btnDebriefs_Click);
            // 
            // btnAuditToolFiles
            // 
            this.btnAuditToolFiles.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuditToolFiles.Location = new System.Drawing.Point(315, 7);
            this.btnAuditToolFiles.Name = "btnAuditToolFiles";
            this.btnAuditToolFiles.Size = new System.Drawing.Size(97, 23);
            this.btnAuditToolFiles.TabIndex = 5;
            this.btnAuditToolFiles.Text = "Audit Tool Files";
            this.btnAuditToolFiles.UseVisualStyleBackColor = true;
            this.btnAuditToolFiles.Click += new System.EventHandler(this.btnAuditToolFiles_Click);
            // 
            // btnAuditLaunch
            // 
            this.btnAuditLaunch.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuditLaunch.Location = new System.Drawing.Point(212, 7);
            this.btnAuditLaunch.Name = "btnAuditLaunch";
            this.btnAuditLaunch.Size = new System.Drawing.Size(97, 23);
            this.btnAuditLaunch.TabIndex = 4;
            this.btnAuditLaunch.Text = "Audit Launch";
            this.btnAuditLaunch.UseVisualStyleBackColor = true;
            this.btnAuditLaunch.Click += new System.EventHandler(this.btnAuditLaunch_Click);
            // 
            // btnAuditSample
            // 
            this.btnAuditSample.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuditSample.Location = new System.Drawing.Point(89, 7);
            this.btnAuditSample.Name = "btnAuditSample";
            this.btnAuditSample.Size = new System.Drawing.Size(117, 23);
            this.btnAuditSample.TabIndex = 3;
            this.btnAuditSample.Text = "Audit Sample";
            this.btnAuditSample.UseVisualStyleBackColor = true;
            this.btnAuditSample.Click += new System.EventHandler(this.btnAuditSample_Click);
            // 
            // btnAuditDates
            // 
            this.btnAuditDates.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuditDates.Location = new System.Drawing.Point(7, 7);
            this.btnAuditDates.Name = "btnAuditDates";
            this.btnAuditDates.Size = new System.Drawing.Size(75, 23);
            this.btnAuditDates.TabIndex = 2;
            this.btnAuditDates.Text = "Audit Dates";
            this.btnAuditDates.UseVisualStyleBackColor = true;
            this.btnAuditDates.Click += new System.EventHandler(this.btnAuditDates_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 127);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1101, 432);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(432, 424);
            this.panel3.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.panel6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel7, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(7, 24);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 397F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(422, 397);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(10);
            this.panel6.Size = new System.Drawing.Size(205, 391);
            this.panel6.TabIndex = 0;
            this.panel6.Paint += new System.Windows.Forms.PaintEventHandler(this.panel6_Paint);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(72, 299);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(120, 13);
            this.label27.TabIndex = 17;
            this.label27.Text = "Performance Category";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(90, 282);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(102, 13);
            this.label26.TabIndex = 16;
            this.label26.Text = "Performance Score";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(121, 265);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 13);
            this.label25.TabIndex = 15;
            this.label25.Text = "Last Audited";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(92, 248);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(100, 13);
            this.label24.TabIndex = 14;
            this.label24.Text = "OAG Opinion Year";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(115, 231);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "OAG Opinion\t";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(79, 214);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(113, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Procurement Budget";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(130, 197);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 13);
            this.label21.TabIndex = 11;
            this.label21.Text = "EGP Status";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(130, 180);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 13);
            this.label20.TabIndex = 10;
            this.label20.Text = "GPP Status";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(115, 163);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 9;
            this.label19.Text = "USMID Status";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(131, 146);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "Entity Type";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(103, 129);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 13);
            this.label17.TabIndex = 7;
            this.label17.Text = "Spend Category";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(153, 112);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "Sector";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(107, 95);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Entity Category";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(124, 78);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "PPDA Office";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(87, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Source Of Funding";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(132, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Audit Type";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(156, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Entity";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(135, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Activity ID";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel7.Controls.Add(this.txvPerformanceScoreCat);
            this.panel7.Controls.Add(this.txvAcitivityId);
            this.panel7.Controls.Add(this.txvPerformanceScore);
            this.panel7.Controls.Add(this.txvEntity);
            this.panel7.Controls.Add(this.txvLastAudited);
            this.panel7.Controls.Add(this.txvAcuditType);
            this.panel7.Controls.Add(this.txvOagYear);
            this.panel7.Controls.Add(this.txvSourceOfFunding);
            this.panel7.Controls.Add(this.txvOagOpinion);
            this.panel7.Controls.Add(this.txvPpdaOffice);
            this.panel7.Controls.Add(this.txvProcBudget);
            this.panel7.Controls.Add(this.txvEntityCategory);
            this.panel7.Controls.Add(this.txvEgpStatus);
            this.panel7.Controls.Add(this.txvSector);
            this.panel7.Controls.Add(this.txvGppStatus);
            this.panel7.Controls.Add(this.txvSpendCategory);
            this.panel7.Controls.Add(this.txvUsmidStatus);
            this.panel7.Controls.Add(this.txvEntityType);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(214, 3);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(10);
            this.panel7.Size = new System.Drawing.Size(205, 391);
            this.panel7.TabIndex = 1;
            // 
            // txvPerformanceScoreCat
            // 
            this.txvPerformanceScoreCat.AutoSize = true;
            this.txvPerformanceScoreCat.Location = new System.Drawing.Point(13, 299);
            this.txvPerformanceScoreCat.Name = "txvPerformanceScoreCat";
            this.txvPerformanceScoreCat.Size = new System.Drawing.Size(118, 13);
            this.txvPerformanceScoreCat.TabIndex = 35;
            this.txvPerformanceScoreCat.Text = "Performance category";
            this.txvPerformanceScoreCat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvAcitivityId
            // 
            this.txvAcitivityId.AutoSize = true;
            this.txvAcitivityId.Location = new System.Drawing.Point(13, 10);
            this.txvAcitivityId.Name = "txvAcitivityId";
            this.txvAcitivityId.Size = new System.Drawing.Size(57, 13);
            this.txvAcitivityId.TabIndex = 18;
            this.txvAcitivityId.Text = "Activity ID";
            this.txvAcitivityId.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvPerformanceScore
            // 
            this.txvPerformanceScore.AutoSize = true;
            this.txvPerformanceScore.Location = new System.Drawing.Point(13, 282);
            this.txvPerformanceScore.Name = "txvPerformanceScore";
            this.txvPerformanceScore.Size = new System.Drawing.Size(101, 13);
            this.txvPerformanceScore.TabIndex = 34;
            this.txvPerformanceScore.Text = "Performance score";
            this.txvPerformanceScore.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvEntity
            // 
            this.txvEntity.AutoSize = true;
            this.txvEntity.Location = new System.Drawing.Point(13, 27);
            this.txvEntity.Name = "txvEntity";
            this.txvEntity.Size = new System.Drawing.Size(36, 13);
            this.txvEntity.TabIndex = 19;
            this.txvEntity.Text = "Entity";
            this.txvEntity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvLastAudited
            // 
            this.txvLastAudited.AutoSize = true;
            this.txvLastAudited.Location = new System.Drawing.Point(13, 265);
            this.txvLastAudited.Name = "txvLastAudited";
            this.txvLastAudited.Size = new System.Drawing.Size(70, 13);
            this.txvLastAudited.TabIndex = 33;
            this.txvLastAudited.Text = "Last audited";
            this.txvLastAudited.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvAcuditType
            // 
            this.txvAcuditType.AutoSize = true;
            this.txvAcuditType.Location = new System.Drawing.Point(13, 44);
            this.txvAcuditType.Name = "txvAcuditType";
            this.txvAcuditType.Size = new System.Drawing.Size(60, 13);
            this.txvAcuditType.TabIndex = 20;
            this.txvAcuditType.Text = "Audit Type";
            this.txvAcuditType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvOagYear
            // 
            this.txvOagYear.AutoSize = true;
            this.txvOagYear.Location = new System.Drawing.Point(13, 248);
            this.txvOagYear.Name = "txvOagYear";
            this.txvOagYear.Size = new System.Drawing.Size(99, 13);
            this.txvOagYear.TabIndex = 32;
            this.txvOagYear.Text = "OAG opinion year";
            this.txvOagYear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvSourceOfFunding
            // 
            this.txvSourceOfFunding.AutoSize = true;
            this.txvSourceOfFunding.Location = new System.Drawing.Point(13, 61);
            this.txvSourceOfFunding.Name = "txvSourceOfFunding";
            this.txvSourceOfFunding.Size = new System.Drawing.Size(101, 13);
            this.txvSourceOfFunding.TabIndex = 21;
            this.txvSourceOfFunding.Text = "Source of funding";
            this.txvSourceOfFunding.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvOagOpinion
            // 
            this.txvOagOpinion.AutoSize = true;
            this.txvOagOpinion.Location = new System.Drawing.Point(13, 231);
            this.txvOagOpinion.Name = "txvOagOpinion";
            this.txvOagOpinion.Size = new System.Drawing.Size(75, 13);
            this.txvOagOpinion.TabIndex = 31;
            this.txvOagOpinion.Text = "OAG opinion\t";
            this.txvOagOpinion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txvOagOpinion.Click += new System.EventHandler(this.txvOagOpinion_Click);
            // 
            // txvPpdaOffice
            // 
            this.txvPpdaOffice.AutoSize = true;
            this.txvPpdaOffice.Location = new System.Drawing.Point(13, 78);
            this.txvPpdaOffice.Name = "txvPpdaOffice";
            this.txvPpdaOffice.Size = new System.Drawing.Size(66, 13);
            this.txvPpdaOffice.TabIndex = 22;
            this.txvPpdaOffice.Text = "PPDA office";
            this.txvPpdaOffice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvProcBudget
            // 
            this.txvProcBudget.AutoSize = true;
            this.txvProcBudget.Location = new System.Drawing.Point(13, 214);
            this.txvProcBudget.Name = "txvProcBudget";
            this.txvProcBudget.Size = new System.Drawing.Size(113, 13);
            this.txvProcBudget.TabIndex = 30;
            this.txvProcBudget.Text = "Procurement Budget";
            this.txvProcBudget.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvEntityCategory
            // 
            this.txvEntityCategory.AutoSize = true;
            this.txvEntityCategory.Location = new System.Drawing.Point(13, 95);
            this.txvEntityCategory.Name = "txvEntityCategory";
            this.txvEntityCategory.Size = new System.Drawing.Size(83, 13);
            this.txvEntityCategory.TabIndex = 23;
            this.txvEntityCategory.Text = "Entity category";
            this.txvEntityCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvEgpStatus
            // 
            this.txvEgpStatus.AutoSize = true;
            this.txvEgpStatus.Location = new System.Drawing.Point(13, 197);
            this.txvEgpStatus.Name = "txvEgpStatus";
            this.txvEgpStatus.Size = new System.Drawing.Size(61, 13);
            this.txvEgpStatus.TabIndex = 29;
            this.txvEgpStatus.Text = "EGP status";
            this.txvEgpStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvSector
            // 
            this.txvSector.AutoSize = true;
            this.txvSector.Location = new System.Drawing.Point(13, 112);
            this.txvSector.Name = "txvSector";
            this.txvSector.Size = new System.Drawing.Size(39, 13);
            this.txvSector.TabIndex = 24;
            this.txvSector.Text = "Sector";
            this.txvSector.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvGppStatus
            // 
            this.txvGppStatus.AutoSize = true;
            this.txvGppStatus.Location = new System.Drawing.Point(13, 180);
            this.txvGppStatus.Name = "txvGppStatus";
            this.txvGppStatus.Size = new System.Drawing.Size(61, 13);
            this.txvGppStatus.TabIndex = 28;
            this.txvGppStatus.Text = "GPP status";
            this.txvGppStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvSpendCategory
            // 
            this.txvSpendCategory.AutoSize = true;
            this.txvSpendCategory.Location = new System.Drawing.Point(13, 129);
            this.txvSpendCategory.Name = "txvSpendCategory";
            this.txvSpendCategory.Size = new System.Drawing.Size(87, 13);
            this.txvSpendCategory.TabIndex = 25;
            this.txvSpendCategory.Text = "Spend category";
            this.txvSpendCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvUsmidStatus
            // 
            this.txvUsmidStatus.AutoSize = true;
            this.txvUsmidStatus.Location = new System.Drawing.Point(13, 163);
            this.txvUsmidStatus.Name = "txvUsmidStatus";
            this.txvUsmidStatus.Size = new System.Drawing.Size(76, 13);
            this.txvUsmidStatus.TabIndex = 27;
            this.txvUsmidStatus.Text = "USMID status";
            this.txvUsmidStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txvEntityType
            // 
            this.txvEntityType.AutoSize = true;
            this.txvEntityType.Location = new System.Drawing.Point(13, 146);
            this.txvEntityType.Name = "txvEntityType";
            this.txvEntityType.Size = new System.Drawing.Size(61, 13);
            this.txvEntityType.TabIndex = 26;
            this.txvEntityType.Text = "Entity Type";
            this.txvEntityType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Entity Selection Details";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.listTeamMembers);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(443, 4);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(5);
            this.panel4.Size = new System.Drawing.Size(213, 424);
            this.panel4.TabIndex = 4;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // listTeamMembers
            // 
            this.listTeamMembers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listTeamMembers.GridLines = true;
            this.listTeamMembers.Location = new System.Drawing.Point(5, 27);
            this.listTeamMembers.Name = "listTeamMembers";
            this.listTeamMembers.Size = new System.Drawing.Size(200, 210);
            this.listTeamMembers.TabIndex = 1;
            this.listTeamMembers.UseCompatibleStateImageBehavior = false;
            this.listTeamMembers.SelectedIndexChanged += new System.EventHandler(this.listTeamMembers_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Team Details";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.listPmBudget);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(663, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(434, 424);
            this.panel5.TabIndex = 5;
            // 
            // listPmBudget
            // 
            this.listPmBudget.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listPmBudget.Location = new System.Drawing.Point(8, 27);
            this.listPmBudget.Name = "listPmBudget";
            this.listPmBudget.Size = new System.Drawing.Size(423, 210);
            this.listPmBudget.TabIndex = 1;
            this.listPmBudget.UseCompatibleStateImageBehavior = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Budget Details";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.labelPmEntryMeetingDate);
            this.panel2.Controls.Add(this.txvStartDate);
            this.panel2.Controls.Add(this.txvEndDate);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(13, 84);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1101, 37);
            this.panel2.TabIndex = 0;
            // 
            // labelPmEntryMeetingDate
            // 
            this.labelPmEntryMeetingDate.AutoSize = true;
            this.labelPmEntryMeetingDate.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPmEntryMeetingDate.Location = new System.Drawing.Point(138, 13);
            this.labelPmEntryMeetingDate.Name = "labelPmEntryMeetingDate";
            this.labelPmEntryMeetingDate.Size = new System.Drawing.Size(67, 13);
            this.labelPmEntryMeetingDate.TabIndex = 5;
            this.labelPmEntryMeetingDate.Text = "dd-mm-yyyy";
            // 
            // txvStartDate
            // 
            this.txvStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txvStartDate.AutoSize = true;
            this.txvStartDate.Location = new System.Drawing.Point(815, 13);
            this.txvStartDate.Name = "txvStartDate";
            this.txvStartDate.Size = new System.Drawing.Size(67, 13);
            this.txvStartDate.TabIndex = 4;
            this.txvStartDate.Text = "dd-mm-yyyy";
            // 
            // txvEndDate
            // 
            this.txvEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txvEndDate.AutoSize = true;
            this.txvEndDate.Location = new System.Drawing.Point(1023, 13);
            this.txvEndDate.Name = "txvEndDate";
            this.txvEndDate.Size = new System.Drawing.Size(67, 13);
            this.txvEndDate.TabIndex = 3;
            this.txvEndDate.Text = "dd-mm-yyyy";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(901, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Activity End Date:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(680, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Activity Start Date:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(4, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Entry Meeting Date:";
            // 
            // DialogViewPmActivity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 571);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogViewPmActivity";
            this.Text = "Performance Monitoring - PPDA Entity Management System";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label txvStartDate;
        private System.Windows.Forms.Label txvEndDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelPmEntryMeetingDate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label txvPerformanceScoreCat;
        private System.Windows.Forms.Label txvAcitivityId;
        private System.Windows.Forms.Label txvPerformanceScore;
        private System.Windows.Forms.Label txvEntity;
        private System.Windows.Forms.Label txvLastAudited;
        private System.Windows.Forms.Label txvAcuditType;
        private System.Windows.Forms.Label txvOagYear;
        private System.Windows.Forms.Label txvSourceOfFunding;
        private System.Windows.Forms.Label txvOagOpinion;
        private System.Windows.Forms.Label txvPpdaOffice;
        private System.Windows.Forms.Label txvProcBudget;
        private System.Windows.Forms.Label txvEntityCategory;
        private System.Windows.Forms.Label txvEgpStatus;
        private System.Windows.Forms.Label txvSector;
        private System.Windows.Forms.Label txvGppStatus;
        private System.Windows.Forms.Label txvSpendCategory;
        private System.Windows.Forms.Label txvUsmidStatus;
        private System.Windows.Forms.Label txvEntityType;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnExitMeeting;
        private System.Windows.Forms.Button btnDebriefs;
        private System.Windows.Forms.Button btnAuditToolFiles;
        private System.Windows.Forms.Button btnAuditLaunch;
        private System.Windows.Forms.Button btnAuditSample;
        private System.Windows.Forms.Button btnAuditDates;
        private System.Windows.Forms.Label labelActivityName;
        private System.Windows.Forms.ListView listPmBudget;
        private System.Windows.Forms.ListView listTeamMembers;
    }
}
﻿namespace EmisMasterDataTool
{
    partial class SubDialogPmAuditToolFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubDialogPmAuditToolFiles));
            this.panel1 = new System.Windows.Forms.Panel();
            this.gpBoxProcessTool = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gridViewSampleFiles = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcRefNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubjectOfProcurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Assignee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emisUploadStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Action = new System.Windows.Forms.DataGridViewImageColumn();
            this.gpBoxDisposalTool = new System.Windows.Forms.GroupBox();
            this.tableLayoutDisposalTool = new System.Windows.Forms.TableLayoutPanel();
            this.labelDisposalToolProgressStatus = new System.Windows.Forms.Label();
            this.labelDisposalProgressStatusHeader = new System.Windows.Forms.Label();
            this.labelDisposalToolEmisUploadStatus = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnDisposalToolOptions = new System.Windows.Forms.Button();
            this.labelDisposalEmisUploadStatusHeader = new System.Windows.Forms.Label();
            this.gpBoxSystemOrStructureTool = new System.Windows.Forms.GroupBox();
            this.tableLayoutSystemOrStructureTool = new System.Windows.Forms.TableLayoutPanel();
            this.labelSystemOrStructureToolProgressStatus = new System.Windows.Forms.Label();
            this.labelSystemOrStructureProgressStatusHeader = new System.Windows.Forms.Label();
            this.labelSystemToolEmisUploadStatus = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSystemOrStructureToolOptions = new System.Windows.Forms.Button();
            this.labeEmisUploadStatusHeader = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.panel1.SuspendLayout();
            this.gpBoxProcessTool.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSampleFiles)).BeginInit();
            this.gpBoxDisposalTool.SuspendLayout();
            this.tableLayoutDisposalTool.SuspendLayout();
            this.gpBoxSystemOrStructureTool.SuspendLayout();
            this.tableLayoutSystemOrStructureTool.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gpBoxProcessTool);
            this.panel1.Controls.Add(this.gpBoxDisposalTool);
            this.panel1.Controls.Add(this.gpBoxSystemOrStructureTool);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(1197, 561);
            this.panel1.TabIndex = 0;
            // 
            // gpBoxProcessTool
            // 
            this.gpBoxProcessTool.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpBoxProcessTool.Controls.Add(this.label1);
            this.gpBoxProcessTool.Controls.Add(this.gridViewSampleFiles);
            this.gpBoxProcessTool.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpBoxProcessTool.Location = new System.Drawing.Point(14, 179);
            this.gpBoxProcessTool.Name = "gpBoxProcessTool";
            this.gpBoxProcessTool.Padding = new System.Windows.Forms.Padding(10);
            this.gpBoxProcessTool.Size = new System.Drawing.Size(1171, 369);
            this.gpBoxProcessTool.TabIndex = 2;
            this.gpBoxProcessTool.TabStop = false;
            this.gpBoxProcessTool.Text = "Process Audit Tools";
            this.gpBoxProcessTool.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sample Files";
            // 
            // gridViewSampleFiles
            // 
            this.gridViewSampleFiles.AllowUserToAddRows = false;
            this.gridViewSampleFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridViewSampleFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewSampleFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.ProcRefNo,
            this.SubjectOfProcurement,
            this.Amount,
            this.Assignee,
            this.Status,
            this.emisUploadStatus,
            this.Action});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridViewSampleFiles.DefaultCellStyle = dataGridViewCellStyle1;
            this.gridViewSampleFiles.Location = new System.Drawing.Point(23, 49);
            this.gridViewSampleFiles.Name = "gridViewSampleFiles";
            this.gridViewSampleFiles.ReadOnly = true;
            this.gridViewSampleFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridViewSampleFiles.Size = new System.Drawing.Size(1135, 307);
            this.gridViewSampleFiles.TabIndex = 0;
            this.gridViewSampleFiles.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridViewSampleFiles_CellContentClick);
            this.gridViewSampleFiles.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.gridViewSampleFiles_CellContextMenuStripNeeded);
            this.gridViewSampleFiles.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.gridViewSampleFiles_DataBindingComplete);
            this.gridViewSampleFiles.MouseHover += new System.EventHandler(this.gridViewSampleFiles_MouseHover);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // ProcRefNo
            // 
            this.ProcRefNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProcRefNo.DataPropertyName = "procurement_reference";
            this.ProcRefNo.FillWeight = 2F;
            this.ProcRefNo.HeaderText = "Proc Ref No.";
            this.ProcRefNo.Name = "ProcRefNo";
            this.ProcRefNo.ReadOnly = true;
            // 
            // SubjectOfProcurement
            // 
            this.SubjectOfProcurement.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SubjectOfProcurement.DataPropertyName = "procurement_subject";
            this.SubjectOfProcurement.FillWeight = 5F;
            this.SubjectOfProcurement.HeaderText = "Subject of Procurement";
            this.SubjectOfProcurement.Name = "SubjectOfProcurement";
            this.SubjectOfProcurement.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Amount.DataPropertyName = "contract_value";
            this.Amount.FillWeight = 2F;
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // Assignee
            // 
            this.Assignee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Assignee.DataPropertyName = "assigneeName";
            this.Assignee.FillWeight = 4F;
            this.Assignee.HeaderText = "Assignee";
            this.Assignee.Name = "Assignee";
            this.Assignee.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Status.DataPropertyName = "status";
            this.Status.FillWeight = 4F;
            this.Status.HeaderText = "Progress Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // emisUploadStatus
            // 
            this.emisUploadStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.emisUploadStatus.FillWeight = 3F;
            this.emisUploadStatus.HeaderText = "EMIS Upload Status";
            this.emisUploadStatus.Name = "emisUploadStatus";
            this.emisUploadStatus.ReadOnly = true;
            // 
            // Action
            // 
            this.Action.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Action.FillWeight = 1F;
            this.Action.HeaderText = "Action";
            this.Action.Image = global::EmisMasterDataTool.Properties.Resources.edit_list_rtl;
            this.Action.Name = "Action";
            this.Action.ReadOnly = true;
            this.Action.ToolTipText = "Right click for options";
            // 
            // gpBoxDisposalTool
            // 
            this.gpBoxDisposalTool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpBoxDisposalTool.Controls.Add(this.tableLayoutDisposalTool);
            this.gpBoxDisposalTool.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpBoxDisposalTool.Location = new System.Drawing.Point(14, 99);
            this.gpBoxDisposalTool.Name = "gpBoxDisposalTool";
            this.gpBoxDisposalTool.Size = new System.Drawing.Size(1171, 74);
            this.gpBoxDisposalTool.TabIndex = 1;
            this.gpBoxDisposalTool.TabStop = false;
            this.gpBoxDisposalTool.Text = "Disposal Audit Tool";
            // 
            // tableLayoutDisposalTool
            // 
            this.tableLayoutDisposalTool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tableLayoutDisposalTool.ColumnCount = 3;
            this.tableLayoutDisposalTool.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutDisposalTool.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutDisposalTool.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutDisposalTool.Controls.Add(this.labelDisposalToolProgressStatus, 0, 1);
            this.tableLayoutDisposalTool.Controls.Add(this.labelDisposalProgressStatusHeader, 0, 0);
            this.tableLayoutDisposalTool.Controls.Add(this.labelDisposalToolEmisUploadStatus, 0, 1);
            this.tableLayoutDisposalTool.Controls.Add(this.label5, 2, 0);
            this.tableLayoutDisposalTool.Controls.Add(this.btnDisposalToolOptions, 2, 1);
            this.tableLayoutDisposalTool.Controls.Add(this.labelDisposalEmisUploadStatusHeader, 1, 0);
            this.tableLayoutDisposalTool.Location = new System.Drawing.Point(130, 13);
            this.tableLayoutDisposalTool.Name = "tableLayoutDisposalTool";
            this.tableLayoutDisposalTool.RowCount = 2;
            this.tableLayoutDisposalTool.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutDisposalTool.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutDisposalTool.Size = new System.Drawing.Size(539, 55);
            this.tableLayoutDisposalTool.TabIndex = 4;
            // 
            // labelDisposalToolProgressStatus
            // 
            this.labelDisposalToolProgressStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDisposalToolProgressStatus.AutoSize = true;
            this.labelDisposalToolProgressStatus.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDisposalToolProgressStatus.Location = new System.Drawing.Point(3, 27);
            this.labelDisposalToolProgressStatus.Name = "labelDisposalToolProgressStatus";
            this.labelDisposalToolProgressStatus.Size = new System.Drawing.Size(236, 28);
            this.labelDisposalToolProgressStatus.TabIndex = 5;
            this.labelDisposalToolProgressStatus.Text = "Not Yet Started";
            this.labelDisposalToolProgressStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDisposalProgressStatusHeader
            // 
            this.labelDisposalProgressStatusHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDisposalProgressStatusHeader.AutoSize = true;
            this.labelDisposalProgressStatusHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDisposalProgressStatusHeader.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.labelDisposalProgressStatusHeader.Location = new System.Drawing.Point(3, 0);
            this.labelDisposalProgressStatusHeader.Name = "labelDisposalProgressStatusHeader";
            this.labelDisposalProgressStatusHeader.Size = new System.Drawing.Size(236, 27);
            this.labelDisposalProgressStatusHeader.TabIndex = 0;
            this.labelDisposalProgressStatusHeader.Text = "Progress Status";
            this.labelDisposalProgressStatusHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDisposalToolEmisUploadStatus
            // 
            this.labelDisposalToolEmisUploadStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDisposalToolEmisUploadStatus.AutoSize = true;
            this.labelDisposalToolEmisUploadStatus.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDisposalToolEmisUploadStatus.Location = new System.Drawing.Point(245, 27);
            this.labelDisposalToolEmisUploadStatus.Name = "labelDisposalToolEmisUploadStatus";
            this.labelDisposalToolEmisUploadStatus.Size = new System.Drawing.Size(236, 28);
            this.labelDisposalToolEmisUploadStatus.TabIndex = 2;
            this.labelDisposalToolEmisUploadStatus.Text = "Pending";
            this.labelDisposalToolEmisUploadStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(487, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 27);
            this.label5.TabIndex = 1;
            this.label5.Text = "Action";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDisposalToolOptions
            // 
            this.btnDisposalToolOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDisposalToolOptions.FlatAppearance.BorderSize = 0;
            this.btnDisposalToolOptions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisposalToolOptions.Image = global::EmisMasterDataTool.Properties.Resources.edit_list_rtl;
            this.btnDisposalToolOptions.Location = new System.Drawing.Point(487, 30);
            this.btnDisposalToolOptions.Name = "btnDisposalToolOptions";
            this.btnDisposalToolOptions.Size = new System.Drawing.Size(49, 22);
            this.btnDisposalToolOptions.TabIndex = 3;
            this.btnDisposalToolOptions.UseVisualStyleBackColor = true;
            this.btnDisposalToolOptions.Click += new System.EventHandler(this.btnDisposalToolOptions_Click);
            this.btnDisposalToolOptions.MouseHover += new System.EventHandler(this.btnDisposalToolOptions_MouseHover);
            // 
            // labelDisposalEmisUploadStatusHeader
            // 
            this.labelDisposalEmisUploadStatusHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDisposalEmisUploadStatusHeader.AutoSize = true;
            this.labelDisposalEmisUploadStatusHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDisposalEmisUploadStatusHeader.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.labelDisposalEmisUploadStatusHeader.Location = new System.Drawing.Point(245, 0);
            this.labelDisposalEmisUploadStatusHeader.Name = "labelDisposalEmisUploadStatusHeader";
            this.labelDisposalEmisUploadStatusHeader.Size = new System.Drawing.Size(236, 27);
            this.labelDisposalEmisUploadStatusHeader.TabIndex = 4;
            this.labelDisposalEmisUploadStatusHeader.Text = "EMIS Upload Status";
            this.labelDisposalEmisUploadStatusHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gpBoxSystemOrStructureTool
            // 
            this.gpBoxSystemOrStructureTool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpBoxSystemOrStructureTool.Controls.Add(this.tableLayoutSystemOrStructureTool);
            this.gpBoxSystemOrStructureTool.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpBoxSystemOrStructureTool.Location = new System.Drawing.Point(13, 13);
            this.gpBoxSystemOrStructureTool.Name = "gpBoxSystemOrStructureTool";
            this.gpBoxSystemOrStructureTool.Size = new System.Drawing.Size(1171, 80);
            this.gpBoxSystemOrStructureTool.TabIndex = 0;
            this.gpBoxSystemOrStructureTool.TabStop = false;
            this.gpBoxSystemOrStructureTool.Text = "System Audit Tool";
            // 
            // tableLayoutSystemOrStructureTool
            // 
            this.tableLayoutSystemOrStructureTool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tableLayoutSystemOrStructureTool.ColumnCount = 3;
            this.tableLayoutSystemOrStructureTool.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutSystemOrStructureTool.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutSystemOrStructureTool.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutSystemOrStructureTool.Controls.Add(this.labelSystemOrStructureToolProgressStatus, 0, 1);
            this.tableLayoutSystemOrStructureTool.Controls.Add(this.labelSystemOrStructureProgressStatusHeader, 0, 0);
            this.tableLayoutSystemOrStructureTool.Controls.Add(this.labelSystemToolEmisUploadStatus, 0, 1);
            this.tableLayoutSystemOrStructureTool.Controls.Add(this.label3, 2, 0);
            this.tableLayoutSystemOrStructureTool.Controls.Add(this.btnSystemOrStructureToolOptions, 2, 1);
            this.tableLayoutSystemOrStructureTool.Controls.Add(this.labeEmisUploadStatusHeader, 1, 0);
            this.tableLayoutSystemOrStructureTool.Location = new System.Drawing.Point(131, 12);
            this.tableLayoutSystemOrStructureTool.Name = "tableLayoutSystemOrStructureTool";
            this.tableLayoutSystemOrStructureTool.RowCount = 2;
            this.tableLayoutSystemOrStructureTool.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutSystemOrStructureTool.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutSystemOrStructureTool.Size = new System.Drawing.Size(539, 62);
            this.tableLayoutSystemOrStructureTool.TabIndex = 0;
            // 
            // labelSystemOrStructureToolProgressStatus
            // 
            this.labelSystemOrStructureToolProgressStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSystemOrStructureToolProgressStatus.AutoSize = true;
            this.labelSystemOrStructureToolProgressStatus.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSystemOrStructureToolProgressStatus.Location = new System.Drawing.Point(3, 31);
            this.labelSystemOrStructureToolProgressStatus.Name = "labelSystemOrStructureToolProgressStatus";
            this.labelSystemOrStructureToolProgressStatus.Size = new System.Drawing.Size(236, 31);
            this.labelSystemOrStructureToolProgressStatus.TabIndex = 5;
            this.labelSystemOrStructureToolProgressStatus.Text = "Not Yet Started";
            this.labelSystemOrStructureToolProgressStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSystemOrStructureProgressStatusHeader
            // 
            this.labelSystemOrStructureProgressStatusHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSystemOrStructureProgressStatusHeader.AutoSize = true;
            this.labelSystemOrStructureProgressStatusHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSystemOrStructureProgressStatusHeader.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.labelSystemOrStructureProgressStatusHeader.Location = new System.Drawing.Point(3, 0);
            this.labelSystemOrStructureProgressStatusHeader.Name = "labelSystemOrStructureProgressStatusHeader";
            this.labelSystemOrStructureProgressStatusHeader.Size = new System.Drawing.Size(236, 31);
            this.labelSystemOrStructureProgressStatusHeader.TabIndex = 0;
            this.labelSystemOrStructureProgressStatusHeader.Text = "Progress Status";
            this.labelSystemOrStructureProgressStatusHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSystemToolEmisUploadStatus
            // 
            this.labelSystemToolEmisUploadStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSystemToolEmisUploadStatus.AutoSize = true;
            this.labelSystemToolEmisUploadStatus.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSystemToolEmisUploadStatus.Location = new System.Drawing.Point(245, 31);
            this.labelSystemToolEmisUploadStatus.Name = "labelSystemToolEmisUploadStatus";
            this.labelSystemToolEmisUploadStatus.Size = new System.Drawing.Size(236, 31);
            this.labelSystemToolEmisUploadStatus.TabIndex = 2;
            this.labelSystemToolEmisUploadStatus.Text = "Pending";
            this.labelSystemToolEmisUploadStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSystemToolEmisUploadStatus.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(487, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 31);
            this.label3.TabIndex = 1;
            this.label3.Text = "Action";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSystemOrStructureToolOptions
            // 
            this.btnSystemOrStructureToolOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSystemOrStructureToolOptions.FlatAppearance.BorderSize = 0;
            this.btnSystemOrStructureToolOptions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSystemOrStructureToolOptions.Image = global::EmisMasterDataTool.Properties.Resources.edit_list_rtl;
            this.btnSystemOrStructureToolOptions.Location = new System.Drawing.Point(487, 34);
            this.btnSystemOrStructureToolOptions.Name = "btnSystemOrStructureToolOptions";
            this.btnSystemOrStructureToolOptions.Size = new System.Drawing.Size(49, 25);
            this.btnSystemOrStructureToolOptions.TabIndex = 3;
            this.btnSystemOrStructureToolOptions.UseVisualStyleBackColor = true;
            this.btnSystemOrStructureToolOptions.MouseLeave += new System.EventHandler(this.btnSystemOrStructureToolOptions_MouseLeave);
            this.btnSystemOrStructureToolOptions.MouseHover += new System.EventHandler(this.btnSystemOrStructureToolOptions_MouseHover);
            // 
            // labeEmisUploadStatusHeader
            // 
            this.labeEmisUploadStatusHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labeEmisUploadStatusHeader.AutoSize = true;
            this.labeEmisUploadStatusHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeEmisUploadStatusHeader.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.labeEmisUploadStatusHeader.Location = new System.Drawing.Point(245, 0);
            this.labeEmisUploadStatusHeader.Name = "labeEmisUploadStatusHeader";
            this.labeEmisUploadStatusHeader.Size = new System.Drawing.Size(236, 31);
            this.labeEmisUploadStatusHeader.TabIndex = 4;
            this.labeEmisUploadStatusHeader.Text = "EMIS Upload Status";
            this.labeEmisUploadStatusHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewImageColumn1.FillWeight = 1F;
            this.dataGridViewImageColumn1.HeaderText = "Action";
            this.dataGridViewImageColumn1.Image = global::EmisMasterDataTool.Properties.Resources.edit_list_rtl;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ToolTipText = "Right click for options";
            // 
            // SubDialogPmAuditToolFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 561);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SubDialogPmAuditToolFiles";
            this.Text = "Audit Tool Files";
            this.panel1.ResumeLayout(false);
            this.gpBoxProcessTool.ResumeLayout(false);
            this.gpBoxProcessTool.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSampleFiles)).EndInit();
            this.gpBoxDisposalTool.ResumeLayout(false);
            this.tableLayoutDisposalTool.ResumeLayout(false);
            this.tableLayoutDisposalTool.PerformLayout();
            this.gpBoxSystemOrStructureTool.ResumeLayout(false);
            this.tableLayoutSystemOrStructureTool.ResumeLayout(false);
            this.tableLayoutSystemOrStructureTool.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox gpBoxProcessTool;
        private System.Windows.Forms.DataGridView gridViewSampleFiles;
        private System.Windows.Forms.GroupBox gpBoxDisposalTool;
        private System.Windows.Forms.GroupBox gpBoxSystemOrStructureTool;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutDisposalTool;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelDisposalProgressStatusHeader;
        private System.Windows.Forms.Label labelDisposalToolEmisUploadStatus;
        private System.Windows.Forms.Button btnDisposalToolOptions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutSystemOrStructureTool;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelSystemOrStructureProgressStatusHeader;
        private System.Windows.Forms.Label labelSystemToolEmisUploadStatus;
        private System.Windows.Forms.Button btnSystemOrStructureToolOptions;
        private System.Windows.Forms.Label labelSystemOrStructureToolProgressStatus;
        private System.Windows.Forms.Label labeEmisUploadStatusHeader;
        private System.Windows.Forms.Label labelDisposalToolProgressStatus;
        private System.Windows.Forms.Label labelDisposalEmisUploadStatusHeader;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcRefNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubjectOfProcurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Assignee;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn emisUploadStatus;
        private System.Windows.Forms.DataGridViewImageColumn Action;
    }
}
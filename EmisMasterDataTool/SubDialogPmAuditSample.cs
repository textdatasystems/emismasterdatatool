﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using Processor.Entities.custom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class SubDialogPmAuditSample : Form, IInnerDialogPmAddSampleFile
    {
        private int activityEmisId;
        public SubDialogPmAuditSample(int activityEmisId)
        {
            InitializeComponent();
            this.activityEmisId = activityEmisId;

            loadData();
        }

        private void loadData()
        {

            var activity = new DatabaseHandler().findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityEmisId);

            if(activity == null)
            {
                return;
            }

            //display the name
            labelActivityName.Text = activity.displayName();

            //load the sample files
            loadActivitySampleFiles();

        }

        private void loadActivitySampleFiles()
        {

            var samples = new DatabaseHandler().getCustomPmAuditSampleFiles(DatabaseHandler.dbConnection(), activityEmisId);

            DataTable dt = new SharedCommons().ToDataTable<CustomPmAuditSampleFile>(samples);

            gridViewSampleFiles.AutoGenerateColumns = false;
            gridViewSampleFiles.DataSource = dt;

        }

        private void SubDialogAuditSample_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            showDialogAddAuditSample();
        }

        private void showDialogAddAuditSample(bool open = true)
        {

            var modalAddSmapleFile = new InnerDialogPmAddSampleFile(activityEmisId);
            modalAddSmapleFile.iInnerDialogPmAddSampleFile = this;

            if (!open)
            {
                if (modalAddSmapleFile.Visible)
                {
                    modalAddSmapleFile.Close();
                }

                return;
            }


            modalAddSmapleFile.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalAddSmapleFile.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalAddSmapleFile.Dispose();

        }

        public void sampleFileSuccessfullyAdded()
        {

            //reload the sample list
            loadActivitySampleFiles();

        }

    }
}

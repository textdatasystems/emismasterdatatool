﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Processor.Entities.custom;

namespace EmisMasterDataTool
{
    public partial class DialogViewCbActivity : Form
    {
        private SubDialogCbParticipantRegistration subDialogCbParticipantReg;
        private SubDialogCbParticipantFeedbackSurvey subDialogCbParticipantFeedbackSurvey;

        private int activityEmisId { get; set; }
        public DialogViewCbActivity(int activityEmisId)
        {

            InitializeComponent();
            this.activityEmisId = activityEmisId;

            loadData();

        }

        private void loadData()
        {

            loadActivityDetails();

        }

        private void loadActivityDetails()
        {

            var db = DatabaseHandler.dbConnection();
            DatabaseHandler databaseHandler = new DatabaseHandler();
            var activity = databaseHandler.findCustomCbActivityByActivityId(db, activityEmisId);

            if(activity == null)
            {
                WidgetHandler.showMessage("Failed to get activity details for activity [" + activityEmisId + "]", true);
                return;
            }

            labelNavActivityName.Text = "CBAS >> PLANS >> ACTIVITIES >> " + activity.activityName.ToUpper();

            populatePlannedPanelFields(activity);
            populateActualPanelFields(activity.child);


        }

        private void populateActualPanelFields(CustomCbActivity childActivity)
        {
            if(childActivity == null)
            {
                //set null for actual fields
                txvActualOwner.Text = "";
                txvActualActivityName.Text = "";
                txvActualSubject.Text = "";
                txvActualActivityType.Text = "";
                txvActualActivityCategory.Text = "";
                txvActualResponsibleOffice.Text = "";
                txvActualDate.Text = "";
                txvActualActivityDays.Text = "";
                txvActualNoOfDays.Text = "";
                txvActualNoParticipants.Text = "";
                txvActualFundingSource.Text = "";

                return;
            }

            txvActualOwner.Text = childActivity.cordinatorName;
            txvActualActivityName.Text = childActivity.activityName;
            txvActualSubject.Text = childActivity.activitySubject;
            txvActualActivityType.Text = childActivity.activityTypeName;
            txvActualActivityCategory.Text = childActivity.ActivityCategory;
            txvActualResponsibleOffice.Text = childActivity.responsibleOffice;
            txvActualDate.Text = childActivity.startDate;
            txvActualActivityDays.Text = "";
            txvActualNoOfDays.Text = childActivity.noOfdays;
            txvActualNoParticipants.Text = childActivity.noOfPeople;
            txvActualFundingSource.Text = childActivity.fundingSourceName;

            //populate modules
            populateActualModules(childActivity.modules);
            //populate trainers
            populateTrainers(childActivity.trainersExternal, listActTrainerExt, childActivity.trainersInternal, listActTrainerInt);
            //budget items
            WidgetHandler.populateBudgetItemAmounts(childActivity.budgetItemAmounts, listActBudget);
            //outcomes
            WidgetHandler.populateExpectedOutcomes(childActivity.expectedOutcomes, listActOutcomes);

        }


        private void populateTrainers(List<CustomCbActivityTrainer> externalTrainers, ListView listViewInternal,  List<CustomCbActivityTrainer> internalTrainers, ListView listViewExternal)
        {

            listViewInternal.View = View.Details;

            listViewInternal.Columns.Add("#", 5);
            listViewInternal.Columns.Add("Internal Trainer");

            foreach (ColumnHeader column in listViewInternal.Columns) { column.Width = -2; }

            for (int count = 0; count < internalTrainers.Count; count++)
            {
                listViewInternal.Items.Add(new ListViewItem(new[] { (count + 1).ToString(), internalTrainers[count].trainerName }));
            }


            //external
            listViewExternal.View = View.Details;

            listViewExternal.Columns.Add("#", 5);
            listViewExternal.Columns.Add("External Trainer");

            foreach (ColumnHeader column in listViewExternal.Columns) { column.Width = -2; }

            for (int count = 0; count < externalTrainers.Count; count++)
            {
                listViewExternal.Items.Add(new ListViewItem(new[] { (count + 1).ToString(), externalTrainers[count].trainerName }));
            }
            
        }

        private void populatePlannedPanelFields(CustomCbActivity activity)
        {
            if(activity == null)
            {
                return;
            }

            txvPlnOwner.Text = activity.cordinatorName;
            txvPlnActivityName.Text = activity.activityName;
            txvPlnSubject.Text = activity.activitySubject;
            txvPlnActivityType.Text = activity.activityTypeName;
            txvPlnActivityCategory.Text = activity.ActivityCategory;
            txvPlnResponsibleOffice.Text =activity.responsibleOffice;
            txvPlnDate.Text = activity.startDate;
            txvPlnActivityDays.Text = "";
            txvPlnNoOfDays.Text = activity.noOfdays;
            txvPlnNoParticipants.Text = activity.noOfPeople;
            txvPlnFundingSource.Text = activity.fundingSourceName;
            
            //modules
            populatePlannedModules(activity.modules);
            //trainers
            populateTrainers(activity.trainersExternal, listPlnTrainerExt, activity.trainersInternal, listPlnTrainerInt);
            //budget items
            WidgetHandler.populateBudgetItemAmounts(activity.budgetItemAmounts, listPlnBudget);
            //outcomes
            WidgetHandler.populateExpectedOutcomes(activity.expectedOutcomes, listPlnOutcomes);
        }

        private void populatePlannedModules(List<CustomCbActivityModule> modules)
        {                      

            listPlnModules.View = View.Details;

            listPlnModules.Columns.Add("#",10);
            listPlnModules.Columns.Add("Module");

            foreach (ColumnHeader column in listPlnModules.Columns) { column.Width = -2; }
            
            for (int count = 0; count < modules.Count; count++)
            {
                listPlnModules.Items.Add(new ListViewItem(new[] {(count+1).ToString(), modules[count].moduleName }));
            }

        }
        

        private void populateActualModules(List<CustomCbActivityModule> modules)
        {

            listActModules.View = View.Details;

            listActModules.Columns.Add("#", 10);
            listActModules.Columns.Add("Module");

            foreach (ColumnHeader column in listActModules.Columns) { column.Width = -2; }

            for (int count = 0; count < modules.Count; count++)
            {
                listActModules.Items.Add(new ListViewItem(new[] { (count + 1).ToString(), modules[count].moduleName }));
            }

        }
             


        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void DialogViewCbActivity_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void showDialogParticipantRegistrationList(bool open)
        {

            if (!open)
            {
                if (subDialogCbParticipantReg.Visible)
                {
                    subDialogCbParticipantReg.Close();
                }
                return;
            }

            subDialogCbParticipantReg = new SubDialogCbParticipantRegistration(activityEmisId);
            subDialogCbParticipantReg.StartPosition = FormStartPosition.CenterParent;
            
            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (subDialogCbParticipantReg.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            subDialogCbParticipantReg.Dispose();

        }

        private void btnParticipantRegistrastion_click(object sender, EventArgs e)
        {
            showDialogParticipantRegistrationList(true);
        }

        private void btnOpenParticpantFeedbackSurvey_Click(object sender, EventArgs e)
        {
            showDialogParticpantFeedbackSurvey(true);
        }

        private void showDialogParticpantFeedbackSurvey(bool open)
        {

            if (!open)
            {
                if (subDialogCbParticipantFeedbackSurvey.Visible)
                {
                    subDialogCbParticipantFeedbackSurvey.Close();
                }
                return;
            }

            subDialogCbParticipantFeedbackSurvey = new SubDialogCbParticipantFeedbackSurvey(activityEmisId);
            subDialogCbParticipantFeedbackSurvey.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (subDialogCbParticipantFeedbackSurvey.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            subDialogCbParticipantFeedbackSurvey.Dispose();

        }


    }
}

﻿namespace EmisMasterDataTool
{
    partial class DialogEntityDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogEntityDetails));
            this.panel1 = new System.Windows.Forms.Panel();
            this.txvTown = new System.Windows.Forms.Label();
            this.txvDistrict = new System.Windows.Forms.Label();
            this.txvPpdaOffice = new System.Windows.Forms.Label();
            this.txvGppStatus = new System.Windows.Forms.Label();
            this.txvGapStatus = new System.Windows.Forms.Label();
            this.txvUsmidStatus = new System.Windows.Forms.Label();
            this.txvEntityType = new System.Windows.Forms.Label();
            this.txvSpendCategory = new System.Windows.Forms.Label();
            this.txvSector = new System.Windows.Forms.Label();
            this.txvCategory = new System.Windows.Forms.Label();
            this.txvEntityShortCode = new System.Windows.Forms.Label();
            this.txvEntityName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txvPhysicalAddress = new System.Windows.Forms.Label();
            this.txvWebsite = new System.Windows.Forms.Label();
            this.txvContactNo = new System.Windows.Forms.Label();
            this.txvEmail = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txvContactPersonPhone = new System.Windows.Forms.Label();
            this.txvContactPersonName = new System.Windows.Forms.Label();
            this.txvContactPersonNameEmail = new System.Windows.Forms.Label();
            this.txvContactPersonRoleInPP = new System.Windows.Forms.Label();
            this.txvContactPersonTitle = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txvTown);
            this.panel1.Controls.Add(this.txvDistrict);
            this.panel1.Controls.Add(this.txvPpdaOffice);
            this.panel1.Controls.Add(this.txvGppStatus);
            this.panel1.Controls.Add(this.txvGapStatus);
            this.panel1.Controls.Add(this.txvUsmidStatus);
            this.panel1.Controls.Add(this.txvEntityType);
            this.panel1.Controls.Add(this.txvSpendCategory);
            this.panel1.Controls.Add(this.txvSector);
            this.panel1.Controls.Add(this.txvCategory);
            this.panel1.Controls.Add(this.txvEntityShortCode);
            this.panel1.Controls.Add(this.txvEntityName);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(416, 401);
            this.panel1.TabIndex = 0;
            // 
            // txvTown
            // 
            this.txvTown.AutoSize = true;
            this.txvTown.Location = new System.Drawing.Point(218, 352);
            this.txvTown.Name = "txvTown";
            this.txvTown.Size = new System.Drawing.Size(37, 13);
            this.txvTown.TabIndex = 24;
            this.txvTown.Text = "Mpigi";
            // 
            // txvDistrict
            // 
            this.txvDistrict.AutoSize = true;
            this.txvDistrict.Location = new System.Drawing.Point(218, 323);
            this.txvDistrict.Name = "txvDistrict";
            this.txvDistrict.Size = new System.Drawing.Size(50, 13);
            this.txvDistrict.TabIndex = 23;
            this.txvDistrict.Text = "Kampala";
            // 
            // txvPpdaOffice
            // 
            this.txvPpdaOffice.AutoSize = true;
            this.txvPpdaOffice.Location = new System.Drawing.Point(218, 295);
            this.txvPpdaOffice.Name = "txvPpdaOffice";
            this.txvPpdaOffice.Size = new System.Drawing.Size(68, 13);
            this.txvPpdaOffice.TabIndex = 22;
            this.txvPpdaOffice.Text = "Head Office";
            // 
            // txvGppStatus
            // 
            this.txvGppStatus.AutoSize = true;
            this.txvGppStatus.Location = new System.Drawing.Point(218, 265);
            this.txvGppStatus.Name = "txvGppStatus";
            this.txvGppStatus.Size = new System.Drawing.Size(24, 13);
            this.txvGppStatus.TabIndex = 21;
            this.txvGppStatus.Text = "Off";
            // 
            // txvGapStatus
            // 
            this.txvGapStatus.AutoSize = true;
            this.txvGapStatus.Location = new System.Drawing.Point(218, 235);
            this.txvGapStatus.Name = "txvGapStatus";
            this.txvGapStatus.Size = new System.Drawing.Size(24, 13);
            this.txvGapStatus.TabIndex = 20;
            this.txvGapStatus.Text = "Off";
            // 
            // txvUsmidStatus
            // 
            this.txvUsmidStatus.AutoSize = true;
            this.txvUsmidStatus.Location = new System.Drawing.Point(218, 212);
            this.txvUsmidStatus.Name = "txvUsmidStatus";
            this.txvUsmidStatus.Size = new System.Drawing.Size(24, 13);
            this.txvUsmidStatus.TabIndex = 19;
            this.txvUsmidStatus.Text = "Off";
            // 
            // txvEntityType
            // 
            this.txvEntityType.AutoSize = true;
            this.txvEntityType.Location = new System.Drawing.Point(218, 180);
            this.txvEntityType.Name = "txvEntityType";
            this.txvEntityType.Size = new System.Drawing.Size(61, 13);
            this.txvEntityType.TabIndex = 18;
            this.txvEntityType.Text = "Entity Type";
            // 
            // txvSpendCategory
            // 
            this.txvSpendCategory.AutoSize = true;
            this.txvSpendCategory.Location = new System.Drawing.Point(218, 157);
            this.txvSpendCategory.Name = "txvSpendCategory";
            this.txvSpendCategory.Size = new System.Drawing.Size(28, 13);
            this.txvSpendCategory.TabIndex = 17;
            this.txvSpendCategory.Text = "Low";
            // 
            // txvSector
            // 
            this.txvSector.AutoSize = true;
            this.txvSector.Location = new System.Drawing.Point(218, 125);
            this.txvSector.Name = "txvSector";
            this.txvSector.Size = new System.Drawing.Size(121, 13);
            this.txvSector.TabIndex = 16;
            this.txvSector.Text = "Justice Law And Order";
            // 
            // txvCategory
            // 
            this.txvCategory.AutoSize = true;
            this.txvCategory.Location = new System.Drawing.Point(218, 96);
            this.txvCategory.Name = "txvCategory";
            this.txvCategory.Size = new System.Drawing.Size(99, 13);
            this.txvCategory.TabIndex = 15;
            this.txvCategory.Text = "Local Government";
            // 
            // txvEntityShortCode
            // 
            this.txvEntityShortCode.AutoSize = true;
            this.txvEntityShortCode.Location = new System.Drawing.Point(218, 67);
            this.txvEntityShortCode.Name = "txvEntityShortCode";
            this.txvEntityShortCode.Size = new System.Drawing.Size(38, 13);
            this.txvEntityShortCode.TabIndex = 14;
            this.txvEntityShortCode.Text = "MDLG";
            // 
            // txvEntityName
            // 
            this.txvEntityName.AutoSize = true;
            this.txvEntityName.Location = new System.Drawing.Point(218, 42);
            this.txvEntityName.Name = "txvEntityName";
            this.txvEntityName.Size = new System.Drawing.Size(61, 13);
            this.txvEntityName.TabIndex = 13;
            this.txvEntityName.Text = "Mpigi DLG";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(-1, -1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 401);
            this.panel2.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(111, 296);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "PPDA Office";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(142, 324);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "District";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(151, 353);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Town";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(124, 266);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "GPP Status";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(117, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "EGP Status";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(102, 213);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "USMID Status";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(118, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Entity Type";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(96, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Spend Category";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(145, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Sector";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(107, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Entity Category";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(99, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Entity Short Code";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(124, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Entity Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Location = new System.Drawing.Point(435, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(402, 197);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Contact Details";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.txvPhysicalAddress, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txvWebsite, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txvContactNo, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.txvEmail, 0, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(214, 23);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(182, 100);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // txvPhysicalAddress
            // 
            this.txvPhysicalAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvPhysicalAddress.AutoSize = true;
            this.txvPhysicalAddress.Location = new System.Drawing.Point(3, 0);
            this.txvPhysicalAddress.Name = "txvPhysicalAddress";
            this.txvPhysicalAddress.Size = new System.Drawing.Size(61, 25);
            this.txvPhysicalAddress.TabIndex = 0;
            this.txvPhysicalAddress.Text = "Mpigi DLG";
            this.txvPhysicalAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txvWebsite
            // 
            this.txvWebsite.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvWebsite.AutoSize = true;
            this.txvWebsite.Location = new System.Drawing.Point(3, 25);
            this.txvWebsite.Name = "txvWebsite";
            this.txvWebsite.Size = new System.Drawing.Size(60, 25);
            this.txvWebsite.TabIndex = 1;
            this.txvWebsite.Text = "mpigi.com";
            this.txvWebsite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txvContactNo
            // 
            this.txvContactNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvContactNo.AutoSize = true;
            this.txvContactNo.Location = new System.Drawing.Point(3, 50);
            this.txvContactNo.Name = "txvContactNo";
            this.txvContactNo.Size = new System.Drawing.Size(87, 25);
            this.txvContactNo.TabIndex = 2;
            this.txvContactNo.Text = "+256 701 08199";
            this.txvContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txvEmail
            // 
            this.txvEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvEmail.AutoSize = true;
            this.txvEmail.Location = new System.Drawing.Point(3, 75);
            this.txvEmail.Name = "txvEmail";
            this.txvEmail.Size = new System.Drawing.Size(103, 25);
            this.txvEmail.TabIndex = 3;
            this.txvEmail.Text = "admin@mpigi.com";
            this.txvEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.Location = new System.Drawing.Point(7, 20);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 119);
            this.panel3.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label25, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label27, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label28, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(193, 104);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(99, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(91, 25);
            this.label25.TabIndex = 0;
            this.label25.Text = "Physical Address";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(141, 25);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 25);
            this.label26.TabIndex = 1;
            this.label26.Text = "Website";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(99, 50);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(91, 25);
            this.label27.TabIndex = 2;
            this.label27.Text = "Contact Number";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(156, 75);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(34, 29);
            this.label28.TabIndex = 3;
            this.label28.Text = "Email";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel4);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Location = new System.Drawing.Point(435, 217);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(402, 197);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Contact Person";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.txvContactPersonPhone, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.txvContactPersonName, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.txvContactPersonNameEmail, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.txvContactPersonRoleInPP, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.txvContactPersonTitle, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(214, 32);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(182, 127);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // txvContactPersonPhone
            // 
            this.txvContactPersonPhone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvContactPersonPhone.AutoSize = true;
            this.txvContactPersonPhone.Location = new System.Drawing.Point(3, 100);
            this.txvContactPersonPhone.Name = "txvContactPersonPhone";
            this.txvContactPersonPhone.Size = new System.Drawing.Size(176, 27);
            this.txvContactPersonPhone.TabIndex = 4;
            this.txvContactPersonPhone.Text = "+256 789 0988";
            this.txvContactPersonPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txvContactPersonName
            // 
            this.txvContactPersonName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvContactPersonName.AutoSize = true;
            this.txvContactPersonName.Location = new System.Drawing.Point(3, 0);
            this.txvContactPersonName.Name = "txvContactPersonName";
            this.txvContactPersonName.Size = new System.Drawing.Size(176, 25);
            this.txvContactPersonName.TabIndex = 0;
            this.txvContactPersonName.Text = "Timothy Kasaga";
            this.txvContactPersonName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txvContactPersonNameEmail
            // 
            this.txvContactPersonNameEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvContactPersonNameEmail.AutoSize = true;
            this.txvContactPersonNameEmail.Location = new System.Drawing.Point(3, 75);
            this.txvContactPersonNameEmail.Name = "txvContactPersonNameEmail";
            this.txvContactPersonNameEmail.Size = new System.Drawing.Size(176, 25);
            this.txvContactPersonNameEmail.TabIndex = 3;
            this.txvContactPersonNameEmail.Text = "timothy@gmail.com";
            this.txvContactPersonNameEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txvContactPersonRoleInPP
            // 
            this.txvContactPersonRoleInPP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvContactPersonRoleInPP.AutoSize = true;
            this.txvContactPersonRoleInPP.Location = new System.Drawing.Point(3, 50);
            this.txvContactPersonRoleInPP.Name = "txvContactPersonRoleInPP";
            this.txvContactPersonRoleInPP.Size = new System.Drawing.Size(176, 25);
            this.txvContactPersonRoleInPP.TabIndex = 2;
            this.txvContactPersonRoleInPP.Text = "AO";
            this.txvContactPersonRoleInPP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txvContactPersonTitle
            // 
            this.txvContactPersonTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvContactPersonTitle.AutoSize = true;
            this.txvContactPersonTitle.Location = new System.Drawing.Point(3, 25);
            this.txvContactPersonTitle.Name = "txvContactPersonTitle";
            this.txvContactPersonTitle.Size = new System.Drawing.Size(176, 25);
            this.txvContactPersonTitle.TabIndex = 1;
            this.txvContactPersonTitle.Text = "Mr";
            this.txvContactPersonTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.txvContactPersonTitle.Click += new System.EventHandler(this.label39_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel4.Controls.Add(this.tableLayoutPanel3);
            this.panel4.Location = new System.Drawing.Point(7, 20);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 142);
            this.panel4.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.label37, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label36, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label35, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label34, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 11);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(193, 128);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label37
            // 
            this.label37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(130, 100);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(60, 28);
            this.label37.TabIndex = 4;
            this.label37.Text = "Telephone";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(156, 75);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(34, 25);
            this.label36.TabIndex = 3;
            this.label36.Text = "Email";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(38, 50);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(152, 25);
            this.label35.TabIndex = 2;
            this.label35.Text = "Role In Procurement Process";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(162, 25);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 25);
            this.label34.TabIndex = 1;
            this.label34.Text = "Title";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(149, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 25);
            this.label33.TabIndex = 0;
            this.label33.Text = "Names";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label33.Click += new System.EventHandler(this.label33_Click);
            // 
            // DialogEntityDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 426);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DialogEntityDetails";
            this.Text = "Entity Details";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label txvTown;
        private System.Windows.Forms.Label txvDistrict;
        private System.Windows.Forms.Label txvPpdaOffice;
        private System.Windows.Forms.Label txvGppStatus;
        private System.Windows.Forms.Label txvGapStatus;
        private System.Windows.Forms.Label txvUsmidStatus;
        private System.Windows.Forms.Label txvEntityType;
        private System.Windows.Forms.Label txvSpendCategory;
        private System.Windows.Forms.Label txvSector;
        private System.Windows.Forms.Label txvCategory;
        private System.Windows.Forms.Label txvEntityShortCode;
        private System.Windows.Forms.Label txvEntityName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label txvPhysicalAddress;
        private System.Windows.Forms.Label txvWebsite;
        private System.Windows.Forms.Label txvContactNo;
        private System.Windows.Forms.Label txvEmail;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label txvContactPersonName;
        private System.Windows.Forms.Label txvContactPersonNameEmail;
        private System.Windows.Forms.Label txvContactPersonRoleInPP;
        private System.Windows.Forms.Label txvContactPersonTitle;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label txvContactPersonPhone;
    }
}
﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using Processor.Entities.custom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class SubDialogCbParticipantRegistration : Form, IInnerDialogCbRegisterParticipant
    {
        private int activityEmisId;
        private InnerDialogCbRegisterParticipant modalRegForm;

        public SubDialogCbParticipantRegistration(int activityEmisId)
        {
            InitializeComponent();
            this.activityEmisId = activityEmisId;

            loadData();

        }

        private void loadData()
        {

            loadParticipants();

        }

        private void loadParticipants()
        {

            var people = new DatabaseHandler().getCustomCbActivityPeople(DatabaseHandler.dbConnection(), activityEmisId);

            DataTable dt = new SharedCommons().ToDataTable<CustomCbActivityPeople>(people);

            gridViewParticipants.AutoGenerateColumns = false;
            gridViewParticipants.DataSource = dt;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            showDialogParticipantRegistrationForm(true);
        }

        private void showDialogParticipantRegistrationForm(bool open)
        {

            if (!open)
            {
                if (modalRegForm.Visible)
                {
                    modalRegForm.Close();
                }
                return;
            }


            modalRegForm = new InnerDialogCbRegisterParticipant(activityEmisId);
            modalRegForm.iInnerDialogCbRegisterParticipant = this;
            modalRegForm.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalRegForm.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalRegForm.Dispose();

        }

        public void participantSuccessfullyRegistered()
        {

            loadParticipants();

        }
    }
}

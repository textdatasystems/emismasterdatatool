﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using Processor.Entities.custom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class SubDialogPmDebriefs : Form, IInnerDialogCustomRegisterMeetingAttendee
    {
        private int activityEmisId;
        public SubDialogPmDebriefs(int activityEmisId)
        {
            InitializeComponent();
            this.activityEmisId = activityEmisId;

            loadData();

        }

        private void loadData()
        {

            //show activity name
            var activity = new DatabaseHandler().findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityEmisId);
            if (activity == null)
            {
                return;
            }
            else
            {
                labelActivityName.Text = activity.displayName();
            }

            //get activity participants
            loadParticipants();
            
        }

        private void loadParticipants()
        {

            String auditActivityType = Globals.PM_AUDIT_DATE_TAG_DEBRIEF;
            var attendees = new DatabaseHandler().getCustomPmMeetingAttendances(DatabaseHandler.dbConnection(), activityEmisId, auditActivityType);
            var dt = new SharedCommons().ToDataTable<CustomPmMeetingAttendances>(attendees);

            gridViewAttendees.AutoGenerateColumns = false;
            gridViewAttendees.DataSource = dt;


        }

      
        private void showDialogRegisterAttendee(bool open = true)
        {

            var registerAttendee = new InnerDialogCustomRegisterMeetingAttendee(activityEmisId, Globals.PM_AUDIT_DATE_TAG_DEBRIEF);
            registerAttendee.iInnerDialogCustomRegisterMeetingAttendee = this;

            if (!open)
            {
                if (registerAttendee.Visible)
                {
                    registerAttendee.Close();
                }

                return;
            }


            registerAttendee.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (registerAttendee.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            registerAttendee.Dispose();

        }

        public void meetingAttendeeRegistrationSuccessful()
        {
            //reload attendee list
            loadParticipants();
        }

        private void btnRegisterAttendee_Click_1(object sender, EventArgs e)
        {
            showDialogRegisterAttendee();
        }
    }
}

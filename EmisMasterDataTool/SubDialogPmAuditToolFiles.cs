﻿using EmisMasterDataTool.Code;
using EmisMasterDataTool.Code.ui.views;
using Processor.ControlObjects;
using Processor.Entities.custom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class SubDialogPmAuditToolFiles : Form
    {
        private int activityEmisId;
        private static String AUDIT_TYPE = "";

        public SubDialogPmAuditToolFiles(int activityEmisId)
        {

            InitializeComponent();
            this.activityEmisId = activityEmisId;

            loadData();

        }

        private void loadData()
        {

            //get the activity
            var activity = new DatabaseHandler().findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityEmisId);

            if(activity == null)
            {
                WidgetHandler.showMessage("Failed to get PM Activity with EMIS Id ["+activityEmisId+"]");
            }

            //customize the general UI
            customizeInterfaceBasedOnAuditType(activity.auditType);

            //show sample files for the activity
            loadActivityAuditSampleFiles();

        }

        private void customizeInterfaceBasedOnAuditType(string auditType)
        {

            //set the audit type for global use
            AUDIT_TYPE = auditType;
           
            if(AUDIT_TYPE == AppConstants.AUDIT_TYPES_PERFORMANCE_AUDIT)
            {

                String textGpBoxSystemOrStructureTool = "Performance Audit System Tool";
                String textGpBoxDisposalTool = "Performance Audit Disposal Tool";
                String textGpBoxProcessTool = "Performance Audit Process Tools";

                updateUI(textGpBoxSystemOrStructureTool, textGpBoxDisposalTool, textGpBoxProcessTool);

                //get the system tool progress status
                //get the disposal tool progress status
                var auditSystemTool = new DatabaseHandler().getPmAuditSystemsToolByIdOrToolReference(DatabaseHandler.dbConnection(), activityEmisId);
                String systemToolProgressStatus = auditSystemTool == null ? AppConstants.TOOL_PROGRESS_STATUS_NOT_STARTED : "Last edited on " + auditSystemTool.updatedOn;
                labelSystemOrStructureToolProgressStatus.Text = systemToolProgressStatus;

                var auditDisposalTool = new DatabaseHandler().getPmAuditDisposalToolByIdOrToolReference(DatabaseHandler.dbConnection(), activityEmisId);
                String disposalToolProgressStatus = auditDisposalTool == null ? AppConstants.TOOL_PROGRESS_STATUS_NOT_STARTED : "Last edited on " + auditDisposalTool.updatedOn;
                labelDisposalToolProgressStatus.Text = disposalToolProgressStatus;

            }
            else if(AUDIT_TYPE == AppConstants.AUDIT_TYPES_COMPLIANCE)
            {
                String textGpBoxSystemOrStructureTool = "Compliance Structure Tool";
                String textGpBoxDisposalTool = "Disposal Tool";
                String textGpBoxProcessTool = "Compliance Process Tools";

                updateUI(textGpBoxSystemOrStructureTool, textGpBoxDisposalTool, textGpBoxProcessTool);

                //get the structure tool progress status
                var compStructTool = new DatabaseHandler().getPmCompStructToolByIdOrActivityId(DatabaseHandler.dbConnection(), activityEmisId);
                String compStructToolProgressStatus = compStructTool == null ? AppConstants.TOOL_PROGRESS_STATUS_NOT_STARTED : "Last edited on " + compStructTool.updatedOn;
                labelSystemOrStructureToolProgressStatus.Text = compStructToolProgressStatus;

                labelDisposalToolProgressStatus.Text = "N/A";
                labelDisposalToolEmisUploadStatus.Text = "N/A";

            }
                        

        }

        private void updateUI(string textGpBoxSystemOrStructureTool, string textGpBoxDisposalTool, string textGpBoxProcessTool)
        {
            gpBoxSystemOrStructureTool.Text = textGpBoxSystemOrStructureTool;
            gpBoxDisposalTool.Text = textGpBoxDisposalTool;
            gpBoxDisposalTool.Enabled = AUDIT_TYPE == AppConstants.AUDIT_TYPES_COMPLIANCE ? false : true;
            gpBoxProcessTool.Text = textGpBoxProcessTool;
        }

        private void loadActivityAuditSampleFiles()
        {

            var samples = new DatabaseHandler().getCustomPmAuditSampleFiles(DatabaseHandler.dbConnection(), activityEmisId);

            DataTable dt = new SharedCommons().ToDataTable<CustomPmAuditSampleFile>(samples);

            gridViewSampleFiles.AutoGenerateColumns = false;
            gridViewSampleFiles.DataSource = dt;

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void gridViewSampleFiles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDisposalToolOptions_Click(object sender, EventArgs e)
        {

        }

        private void btnSystemOrStructureToolOptions_MouseHover(object sender, EventArgs e)
        {

            //options menu is customized based on whether we are editing or it's a new tool

            bool isNewToolInstance = true;

            if(AUDIT_TYPE == AppConstants.AUDIT_TYPES_PERFORMANCE_AUDIT)
            {
                var tool = new DatabaseHandler().getPmAuditSystemsToolByIdOrToolReference(DatabaseHandler.dbConnection(), activityEmisId);
                isNewToolInstance = tool == null;
            }
            else if (AUDIT_TYPE == AppConstants.AUDIT_TYPES_COMPLIANCE)
            {
                var compStructTool = new DatabaseHandler().getPmCompStructToolByIdOrActivityId(DatabaseHandler.dbConnection(), activityEmisId);
                isNewToolInstance = compStructTool == null;
            }        
                       

            ContextMenuStrip menuStrip = buildAuditToolsContextMenu(contextMenuHandleBtnSystemOrStructureToolOptions, isNewToolInstance);
            menuStrip.MouseLeave += menuMouseLeaveHandler;

            Control control = (Control)sender;
            control.ContextMenuStrip = menuStrip;
            control.ContextMenuStrip.Show(control, 0, 0);

        }

        private void menuMouseLeaveHandler(object sender, EventArgs e)
        {
            var menuStrip = (ContextMenuStrip)sender;
            menuStrip.Dispose();
        }

        private void contextMenuHandleBtnSystemOrStructureToolOptions(object sender, EventArgs e)
        {

            var menuItem = (ToolStripMenuItem)sender;
            String option = menuItem.Text;
            
            switch (option)
            {

                case AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_NEW:
                {
                    launchSystemOrStructureTool();
                    break;
                }
                case AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_EDIT:
                {
                    launchSystemOrStructureTool(false);
                    break;
                }
                case AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_JSON:
                {
                    downloadSystemOrStructureToolJSON();
                    break;
                }
                default:
                {
                    WidgetHandler.showMessage("Unknown menu option selected[" + option + "]", true);
                    break;
                }

            }

        }

        private void downloadSystemOrStructureToolJSON()
        {

            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if(folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {

                String dir = folderBrowserDialog.SelectedPath;
                String result = "";

                var jsonGenerator = new JsonGenerator();

                bool isGenerated = AUDIT_TYPE == AppConstants.AUDIT_TYPES_PERFORMANCE_AUDIT ?
                                    jsonGenerator.generateAuditSystemToolJson(dir, activityEmisId, out result) :
                                    jsonGenerator.generateCompStructureToolJson(dir, activityEmisId, out result);
                
                if (!isGenerated){

                    WidgetHandler.showMessage(result);

                }
                else
                {
                    WidgetHandler.showMessage("Json successfully created, file path " + result, false);
                }

            }

        }


        private void downloadDisposalToolJSON()
        {

            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {

                String dir = folderBrowserDialog.SelectedPath;
                String result = "";

                var jsonGenerator = new JsonGenerator();
                bool isGenerated = jsonGenerator.generateAuditDisposalToolJson(dir, activityEmisId, out result);

                if (!isGenerated)
                {

                    WidgetHandler.showMessage(result);

                }
                else
                {
                    WidgetHandler.showMessage("Json successfully created, file path " + result, false);
                }

            }

        }


        private void downloadProcessToolJSON(int sampleFileLocalId)
        {

            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {

                String dir = folderBrowserDialog.SelectedPath;
                String result = "";

                var jsonGenerator = new JsonGenerator();
                bool isGenerated = AUDIT_TYPE == AppConstants.AUDIT_TYPES_PERFORMANCE_AUDIT ?
                    jsonGenerator.generateAuditProcessToolJson(dir, activityEmisId, sampleFileLocalId, out result) : 
                    jsonGenerator.generateCompProcessToolJson(dir, activityEmisId, sampleFileLocalId, out result) ;

                if (!isGenerated)
                {

                    WidgetHandler.showMessage(result);

                }
                else
                {
                    WidgetHandler.showMessage("Json successfully created, file path " + result, false);
                }

            }

        }


        private void launchSystemOrStructureTool(bool isNew = true)
        {

            if (AUDIT_TYPE == AppConstants.AUDIT_TYPES_PERFORMANCE_AUDIT)
            {

                //check if it has an existing PM tool
                var toolToUpdate = new DatabaseHandler().getPmAuditSystemsToolByIdOrToolReference(DatabaseHandler.dbConnection(), activityEmisId);
                WidgetHandler.showModal(new DialogPmAuditSystemTool(activityEmisId, toolToUpdate), this);

            }
            else if (AUDIT_TYPE == AppConstants.AUDIT_TYPES_COMPLIANCE)
            {
                //check if it has an existing PM tool
                var toolToUpdate = new DatabaseHandler().getPmCompStructToolByIdOrActivityId(DatabaseHandler.dbConnection(), activityEmisId);
                WidgetHandler.showModal(new DialogPmComplianceStructureTool(activityEmisId, toolToUpdate), this);
            }
            else
            {
                WidgetHandler.showMessage("Unsupported Audit Type [" + AUDIT_TYPE + "]");
            }

        }

        private void launchProcessTool(int sampleFileLocalId,bool isNew = true)
        {

            if (AUDIT_TYPE == AppConstants.AUDIT_TYPES_PERFORMANCE_AUDIT)
            {

                DatabaseHandler dbHandler = new DatabaseHandler();
                var tool = dbHandler.getPmAuditProcessToolByIdOrActivityIdAndSampleFileLocalId(DatabaseHandler.dbConnection(), activityEmisId, sampleFileLocalId);
                WidgetHandler.showModal(new DialogPmAuditProcessTool(activityEmisId, sampleFileLocalId, tool), this);

            }
            else if (AUDIT_TYPE == AppConstants.AUDIT_TYPES_COMPLIANCE)
            {
                DatabaseHandler dbHandler = new DatabaseHandler();
                var tool = dbHandler.getPmCompProcessToolByIdOrActivityIdAndSampleFileLocalId(DatabaseHandler.dbConnection(), activityEmisId, sampleFileLocalId);
                WidgetHandler.showModal(new DialogPmComplianceProcessTool(activityEmisId, sampleFileLocalId, tool), this);
            }
            else
            {
                WidgetHandler.showMessage("Unsupported Audit Type [" + AUDIT_TYPE + "]");
            }

        }

        private void gridViewSampleFiles_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            gridViewSampleFiles.ClearSelection();
        }

        private void contextMenuHandleBtnDisposalToolOptions(object sender, EventArgs e)
        {

            var menuItem = (ToolStripMenuItem)sender;
            String option = menuItem.Text;
            
            switch (option)
            {

                case AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_NEW:
                {
                    var tool = new DatabaseHandler().getPmAuditDisposalToolByIdOrToolReference(DatabaseHandler.dbConnection(), activityEmisId);
                    WidgetHandler.showModal(new DialogPmAuditDisposalTool(activityEmisId, tool), this);
                    break;
                }
                case AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_EDIT:
                {
                    var tool = new DatabaseHandler().getPmAuditDisposalToolByIdOrToolReference(DatabaseHandler.dbConnection(), activityEmisId);
                    WidgetHandler.showModal(new DialogPmAuditDisposalTool(activityEmisId, tool), this);
                    break;
                }
                case AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_JSON:
                {
                    downloadDisposalToolJSON();
                    break;
                }
                default:
                {
                    WidgetHandler.showMessage("Unknown menu option selected[" + option + "]", true);
                    break;
                }

            }

        }
        
        private void btnSystemOrStructureToolOptions_MouseLeave(object sender, EventArgs e)
        {           
        }

        private void btnDisposalToolOptions_MouseHover(object sender, EventArgs e)
        {

            //options menu is customized based on whether we are editing or it's a new tool
            var tool = new DatabaseHandler().getPmAuditDisposalToolByIdOrToolReference(DatabaseHandler.dbConnection(), activityEmisId);
            bool isNewToolInstance = tool == null;

            ContextMenuStrip menuStrip = buildAuditToolsContextMenu(contextMenuHandleBtnDisposalToolOptions, isNewToolInstance);
            menuStrip.MouseLeave += menuMouseLeaveHandler;

            Control control = (Control)sender;
            control.ContextMenuStrip = menuStrip;

            control.ContextMenuStrip.Show(control, 0, 0);

        }

        private ContextMenuStrip buildAuditToolsContextMenu(EventHandler menuItemEventHandler, bool isNew = true, String processToolSampleFileLocalId = "NA")
        {

            ToolStripMenuItem menuItem1 = new ToolStripMenuItem(AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_NEW, Properties.Resources.chain);
            menuItem1.Tag = processToolSampleFileLocalId;
            menuItem1.Click += menuItemEventHandler;
            menuItem1.Enabled = isNew;

            ToolStripMenuItem menuItem2 = new ToolStripMenuItem(AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_EDIT, Properties.Resources.chain);
            menuItem2.Tag = processToolSampleFileLocalId;
            menuItem2.Click += menuItemEventHandler;
            menuItem2.Enabled = !isNew ? true : false;

            ToolStripMenuItem menuItem3 = new ToolStripMenuItem(AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_JSON, Properties.Resources.chain);
            menuItem3.Tag = processToolSampleFileLocalId;
            menuItem3.Click += menuItemEventHandler;
            menuItem3.Enabled = !isNew ? true : false;

            ContextMenuStrip menuStrip = new ContextMenuStrip();
            menuStrip.Items.Add(menuItem1);
            menuStrip.Items.Add(menuItem2);
            menuStrip.Items.Add(menuItem3);
            return menuStrip;

        }

        private void gridViewSampleFiles_MouseHover(object sender, EventArgs e)
        {

        }

        private void gridViewSampleFiles_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {

            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
                return;

            var cell = dgv.Rows[e.RowIndex].Cells[0];
            String processToolSampleFileLocalId = (String)cell.Value;


            //check a tool exists for the process sample file
            bool toolExists = checkIfProcessToolExistsForTheSampleFile(processToolSampleFileLocalId, AUDIT_TYPE);

            ContextMenuStrip menuStrip =  buildAuditToolsContextMenu(gridViewSampleFilesContextMenuHandler,!toolExists, processToolSampleFileLocalId);
            e.ContextMenuStrip = menuStrip;

        }

        private bool checkIfProcessToolExistsForTheSampleFile(string processToolSampleFileLocalId, string auditType)
        {

            var dbConn = DatabaseHandler.dbConnection();
            var dbHandler = new DatabaseHandler();

            if(auditType == AppConstants.AUDIT_TYPES_PERFORMANCE_AUDIT)
            {

                int sampleFileId = 0;
                int.TryParse(processToolSampleFileLocalId, out sampleFileId);
                var sampleFile = dbHandler.getPmAuditProcessToolByIdOrActivityIdAndSampleFileLocalId(dbConn, activityEmisId, sampleFileId);
                return sampleFile != null;

            }
            else if(auditType == AppConstants.AUDIT_TYPES_COMPLIANCE)
            {

                int sampleFileId = 0;
                int.TryParse(processToolSampleFileLocalId, out sampleFileId);
                var sampleFile = dbHandler.getPmCompProcessToolByIdOrActivityIdAndSampleFileLocalId(dbConn, activityEmisId, sampleFileId);
                return sampleFile != null;

            }
            
            return false;

        }

        private void gridViewSampleFilesContextMenuHandler(object sender, EventArgs e)
        {

            var menuItem = (ToolStripMenuItem)sender;
            String option = menuItem.Text;

            String tag = (String)menuItem.Tag;
            int sampleFileLocalId = 0;

            if (!int.TryParse(tag, out sampleFileLocalId) || sampleFileLocalId == 0)
            {
                WidgetHandler.showMessage("Invalid Sample File local ID [" + tag + "]", true);
                return;
            }

            switch (option)
            {

                case AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_NEW:
                {
                    launchProcessTool(sampleFileLocalId);
                    break;
                }
                case AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_EDIT:
                {
                    launchProcessTool(sampleFileLocalId);
                    break;
                }
                case AppConstants.MENU_STRIP_ITEM_AUDIT_TOOL_JSON:
                {
                    downloadProcessToolJSON(sampleFileLocalId);
                    break;
                }
                default:
                {
                    WidgetHandler.showMessage("Unknown menu option selected[" + option + "]", true);
                    break;
                }

            }


        }
    }

}

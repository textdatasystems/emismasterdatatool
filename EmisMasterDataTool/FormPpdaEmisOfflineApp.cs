﻿using EmisMasterDataTool.Code;
using EmisMasterDataTool.Code.ui.views;
using Processor.ControlObjects;
using Processor.Entities;
using Processor.Entities.custom;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class FormPpdaEmisOfflineApp : Form
    {

        //modals used
        DialogEntityBranches dialogEntityBranches;
        DialogEntityDetails dialogEntityDetails;
        DialogEntityPeople dialogEntityPeople;

        DialogViewCbActivity dialogViewCbActivity;
        DialogViewPmActivity dialogViewPmActivity;

        public FormPpdaEmisOfflineApp()
        {
            InitializeComponent();

            loadData();
        }

        private void loadData()
        {

            //get the financial years
            List<FinacialYear> financialYears = loadFinancialYearsData();

            //initialize entities tab
            initializeEntitiesTabData();

            //initialize PM tab
            initializePmTabData(financialYears);

            //initialize CB tab
            initializeCbTabData(financialYears);

        }

        private void initializeCbTabData(List<FinacialYear> financialYears)
        {
            WidgetHandler.populateFinancialYearsDropdown(cbxCbFinancialYears, financialYears);
        }

        private void initializePmTabData(List<FinacialYear> financialYears)
        {
            WidgetHandler.populateFinancialYearsDropdown(cbxPmFinancialYears, financialYears);
        }

        private List<FinacialYear> loadFinancialYearsData()
        {
            //get list of finacial years as these maybe used in morethan one place
            var db = new DatabaseHandler();
            var financialYears = db.allFinancialYears(DatabaseHandler.dbConnection());
            return financialYears;
        }

        private void initializeEntitiesTabData()
        {

            var dbHandler = new DatabaseHandler();
            var entityList = dbHandler.allEntities(DatabaseHandler.dbConnection());

            DataTable dtEntities = new SharedCommons().ToDataTable<Entity>(entityList);

            gridViewEntities.AutoGenerateColumns = false;
            gridViewEntities.DataSource = dtEntities;
            
        }
              
        private void gridViewEntities_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {

            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
                return;

            var cell = dgv.Rows[e.RowIndex].Cells[0];
            String emisId = (String)cell.Value;

            ContextMenuStrip menuStrip = new ContextMenuStrip();
           
          
            ToolStripMenuItem menuItem1 = new ToolStripMenuItem("Details",Properties.Resources.chain);
            menuItem1.Tag = emisId;// e.RowIndex;
            menuItem1.Click += contextMenuHandler;

            ToolStripMenuItem menuItem2 = new ToolStripMenuItem("Branches", Properties.Resources.chain);
            menuItem2.Tag = emisId; // e.RowIndex;
            menuItem2.Click += contextMenuHandler;

            ToolStripMenuItem menuItem3 = new ToolStripMenuItem("People", Properties.Resources.chain);
            menuItem3.Tag = emisId; // e.RowIndex;
            menuItem3.Click += contextMenuHandler;

            menuStrip.Items.Add(menuItem1);
            menuStrip.Items.Add(menuItem2);
            menuStrip.Items.Add(menuItem3);

            e.ContextMenuStrip = menuStrip;

        }

        private void contextMenuHandler(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;
            String option = menuItem.Text;
            option = option.ToUpper();

            String tag = (String)menuItem.Tag;
            int emisId = 0;

            if(!int.TryParse(tag, out emisId) || emisId == 0)
            {
                showMessage("Failed to get EMIS Id for the entity [" + tag + "]", true);
                return;
            }

            switch (option)
            {

                case "BRANCHES":
                    {
                        showDialogEntityBranches(true, emisId);
                        break;
                    }
                case "DETAILS":
                    {
                        showDialogEntityDetails(true, emisId);
                        break;
                    }
                case "PEOPLE":
                    {
                        showDialogEntityPeople(true, emisId);
                        break;
                    }
                default:
                    {
                        showMessage("Unknown menu option selected["+option+"]", true);
                        break;
                    }

            }            
            
        }


        private void contextMenuHandlerCbActivitiesGridView(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;
            String option = menuItem.Text;
            option = option.ToUpper();

            String tag = (String)menuItem.Tag;
            int activityEmisId = 0;

            if (!int.TryParse(tag, out activityEmisId) || activityEmisId == 0)
            {
                showMessage("Failed to get activity EMIS Id for the activity [" + tag + "]", true);
                return;
            }

            switch (option.ToLower())
            {

                case "view activity details":
                    {
                        showDialogCbActivityDetails(true, activityEmisId);
                        break;
                    }                
                default:
                    {
                        showMessage("Unknown menu option selected[" + option + "]", true);
                        break;
                    }

            }

        }


        private void contextMenuHandlerPmActivitiesGridView(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;
            String option = menuItem.Text;
            option = option.ToUpper();

            String tag = (String)menuItem.Tag;
            int activityEmisId = 0;

            if (!int.TryParse(tag, out activityEmisId) || activityEmisId == 0)
            {
                showMessage("Failed to get activity EMIS Id for the activity [" + tag + "]", true);
                return;
            }

            switch (option.ToLower())
            {

                case "view activity details":
                    {
                        showDialogPmActivityDetails(true, activityEmisId);
                        break;
                    }
                default:
                    {
                        showMessage("Unknown menu option selected[" + option + "]", true);
                        break;
                    }

            }

        }


        private void showMessage(string message, bool isError)
        {

            if (isError)
            {
                DialogResult res = MessageBox.Show(message, "Operation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult res = MessageBox.Show(message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void showDialogEntityBranches(bool open, int emisId)
        {

            if (!open)
            {
                if (dialogEntityBranches.Visible)
                {
                    dialogEntityBranches.Close();
                }
                return;
            }


            dialogEntityBranches = new DialogEntityBranches(emisId);
            dialogEntityBranches.StartPosition = FormStartPosition.CenterParent;

            //dialogEntityBranches.iDialogAddParticipant = this;


            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (dialogEntityBranches.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
                //this.txtResult.Text = dialogAddParticipant.TextBox1.Text;
                
            }
            else
            {
                //this.txtResult.Text = "Cancelled";
            }

            dialogEntityBranches.Dispose();

        }
        
        private void showDialogEntityPeople(bool open, int emisId)
        {

            if (!open)
            {
                if (dialogEntityPeople.Visible)
                {
                    dialogEntityPeople.Close();
                }
                return;
            }


            dialogEntityPeople = new DialogEntityPeople(emisId);
            dialogEntityPeople.StartPosition = FormStartPosition.CenterParent;

            //dialogEntityPeople.iDialogAddParticipant = this;


            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (dialogEntityPeople.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
                //this.txtResult.Text = dialogAddParticipant.TextBox1.Text;

            }
            else
            {
                //this.txtResult.Text = "Cancelled";
            }

            dialogEntityPeople.Dispose();

        }

        private void showDialogEntityDetails(bool open, int emisId = 0)
        {

            if (!open)
            {
                if (dialogEntityDetails.Visible)
                {
                    dialogEntityDetails.Close();
                }
                return;
            }


            dialogEntityDetails = new DialogEntityDetails(emisId);
            //dialogEntityDetails.entityEmisId = emisId;
            dialogEntityDetails.StartPosition = FormStartPosition.CenterParent;

            //dialogEntityDetails.iDialogAddParticipant = this;


            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (dialogEntityDetails.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
                //this.txtResult.Text = dialogAddParticipant.TextBox1.Text;

            }
            else
            {
                //this.txtResult.Text = "Cancelled";
            }

            dialogEntityDetails.Dispose();

        }

        private void cbxPmFinancialYears_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbxPmFinancialYears.SelectedValue == null || cbxPmFinancialYears.SelectedIndex == 0)
            {
                return;
            }

            var fyEmisId = (int)cbxPmFinancialYears.SelectedValue;

            var dbHandler = new DatabaseHandler();
            PmPlan plan = dbHandler.getPmPlanForFinancialYear(DatabaseHandler.dbConnection(), fyEmisId);
            
            if(plan == null)
            {
                gridViewPmAcitivities.DataSource = null;
                gridViewPmAcitivities.Rows.Clear();
                WidgetHandler.showMessage("No plan found for the selected FY", true);
                return;
            }

            loadPmActivities(plan.emisId);
            
        }

        private void cbxCbFinancialYears_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbxCbFinancialYears.SelectedValue == null || cbxCbFinancialYears.SelectedIndex == 0)
            {
                return;
            }

            var fyEmisId =(int)cbxCbFinancialYears.SelectedValue;

            var dbHandler = new DatabaseHandler();
            CbPlan plan = dbHandler.getCbPlanForFinancialYear(DatabaseHandler.dbConnection(), fyEmisId);

            if (plan == null)
            {
                gridViewCbActivities.DataSource = null;
                gridViewCbActivities.Rows.Clear();
                WidgetHandler.showMessage("No plan found for the selected FY", true);
                return;
            }

            loadCbActivities(plan.emisId);


        }

        private void loadCbActivities(int cbPlanId)
        {

            var dbHandler = new DatabaseHandler();
            List<CustomCbActivity> activities = dbHandler.getCustomCbActivities(DatabaseHandler.dbConnection(), cbPlanId);

            DataTable dt = new SharedCommons().ToDataTable<CustomCbActivity>(activities);

            gridViewCbActivities.AutoGenerateColumns = false;
            gridViewCbActivities.DataSource = dt;

        }

        private void gridViewCbActivities_RowContextMenuStripNeeded(object sender, DataGridViewRowContextMenuStripNeededEventArgs e)
        {

        }

        private void gridViewCbActivities_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {

            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
                return;

            var cell = dgv.Rows[e.RowIndex].Cells[0];
            String activityEmisId = (String)cell.Value;

            ContextMenuStrip menuStrip = new ContextMenuStrip();


            ToolStripMenuItem menuItem1 = new ToolStripMenuItem("View Activity Details", Properties.Resources.chain);
            menuItem1.Tag = activityEmisId;
            menuItem1.Click += contextMenuHandlerCbActivitiesGridView;
            
            menuStrip.Items.Add(menuItem1);

            e.ContextMenuStrip = menuStrip;

        }


        private void showDialogCbActivityDetails(bool open, int emisId)
        {

            if (!open)
            {
                if (dialogViewCbActivity.Visible)
                {
                    dialogViewCbActivity.Close();
                }
                return;
            }


            dialogViewCbActivity = new DialogViewCbActivity(emisId);
            dialogViewCbActivity.StartPosition = FormStartPosition.CenterParent;
            
            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (dialogViewCbActivity.ShowDialog(this) == DialogResult.OK)
            {

            }
            else
            {
            }

            dialogViewCbActivity.Dispose();

        }


        private void loadPmActivities(int pmPlanId)
        {

            var dbHandler = new DatabaseHandler();
            List<CustomPmActivity> activities = dbHandler.getCustomPmActivities(DatabaseHandler.dbConnection(), pmPlanId);

            DataTable dt = new SharedCommons().ToDataTable<CustomPmActivity>(activities);

            gridViewPmAcitivities.AutoGenerateColumns = false;
            gridViewPmAcitivities.DataSource = dt;

        }

        private void gridViewPmAcitivities_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {

            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
                return;

            var cell = dgv.Rows[e.RowIndex].Cells[0];
            String activityEmisId = (String)cell.Value;

            ContextMenuStrip menuStrip = new ContextMenuStrip();


            ToolStripMenuItem menuItem1 = new ToolStripMenuItem("View Activity Details", Properties.Resources.chain);
            menuItem1.Tag = activityEmisId;
            menuItem1.Click += contextMenuHandlerPmActivitiesGridView;

            menuStrip.Items.Add(menuItem1);

            e.ContextMenuStrip = menuStrip;

        }


        private void showDialogPmActivityDetails(bool open, int emisId)
        {

            if (!open)
            {
                if (dialogViewPmActivity.Visible)
                {
                    dialogViewPmActivity.Close();
                }
                return;
            }


            dialogViewPmActivity = new DialogViewPmActivity(emisId);
            dialogViewPmActivity.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (dialogViewPmActivity.ShowDialog(this) == DialogResult.OK)
            {

            }
            else
            {
            }

            dialogViewPmActivity.Dispose();

        }

        private void gridViewEntities_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            gridViewEntities.ClearSelection();
        }

        private void gridViewPmAcitivities_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            gridViewPmAcitivities.ClearSelection();
        }

        private void gridViewCbActivities_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            gridViewCbActivities.ClearSelection();
        }

        private void masterDataToolStripMenuItem_Click(object sender, EventArgs e)
        {

            WidgetHandler.showModal(new DialogMasterData(),this);
        }

    }
}

﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using Processor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class DialogEntityBranches : Form, ISubDialogAddBranch {

        private SubDialogAddBranch subDialogAddBranch;
        private int entityEmisId { get; set; }
        public DialogEntityBranches(int entityEmisId)
        {

            this.entityEmisId = entityEmisId;
            InitializeComponent();

            //load entity braches
            loadData();

        }

        private void loadData()
        {

            List<Branch> branches = new DatabaseHandler().allBranches(DatabaseHandler.dbConnection(), entityEmisId);
            var dt = new SharedCommons().ToDataTable<Branch>(branches);

            gridViewBranches.AutoGenerateColumns = false;
            gridViewBranches.DataSource = dt;

        }

        private void btnAddBranch_Click(object sender, EventArgs e)
        {
            showDialogAddBranch(true);
        }


        private void showDialogAddBranch(bool open)
        {

            if (!open)
            {
                if (subDialogAddBranch.Visible)
                {
                    subDialogAddBranch.Close();
                }
                return;
            }


            subDialogAddBranch = new SubDialogAddBranch(entityEmisId);
            subDialogAddBranch.iSubDialogAddBranch = this;

            subDialogAddBranch.StartPosition = FormStartPosition.CenterParent;
            

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (subDialogAddBranch.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
                //this.txtResult.Text = dialogAddParticipant.TextBox1.Text;

            }
            else
            {
                //this.txtResult.Text = "Cancelled";
            }

            subDialogAddBranch.Dispose();

        }

        public void branchSuccessfullyAdded()
        {
            loadData();
        }

    }
}

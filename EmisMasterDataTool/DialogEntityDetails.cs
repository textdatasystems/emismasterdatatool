﻿using Processor.ControlObjects;
using Processor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class DialogEntityDetails : Form
    {
        public int entityEmisId { get; set; }

        public DialogEntityDetails(int emisId)
        {

            InitializeComponent();

            entityEmisId = emisId;

            loadEntityDetails();

        }
                   

        private void loadEntityDetails()
        {
            
            if(entityEmisId == 0)
            {
                //invalid emis ID
                showMessage("Invalid EMIS Id [" + entityEmisId + "]", true);
                return;
            }

            Entity entity = new DatabaseHandler().findEntityByEmisId(DatabaseHandler.dbConnection(),entityEmisId);

            if(entity == null)
            {
                showMessage("Failed to get Entity Details for EMIS Id [" + entityEmisId + "]", true);
                return;
            }

            populateEntityDetails(entity);

        }

        private void populateEntityDetails(Entity entity)
        {

            txvEntityName.Text = entity.entityName;
            txvEntityShortCode.Text = entity.procCode;
            txvContactNo.Text = entity.contactNumber;
            txvGppStatus.Text = entity.egpStatus ? "ON" : "OFF";
            txvEmail.Text = entity.email;
            txvGppStatus.Text = entity.gppStatus ? "ON" : "OFF";
            txvPhysicalAddress.Text = entity.physicalAddress;
            txvWebsite.Text = entity.website;
            txvUsmidStatus.Text = entity.usmidStatus ? "ON" : "OFF";

        }

        private void label33_Click(object sender, EventArgs e)
        {

        }

        private void label39_Click(object sender, EventArgs e)
        {

        }

        private void showMessage(string message, bool isError)
        {

            if (isError)
            {
                DialogResult res = MessageBox.Show(message, "Operation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult res = MessageBox.Show(message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

    }
}

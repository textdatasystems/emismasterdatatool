﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class InnerDialogCbSurveyUploadForm : Form
    {
        public InnerDialogCbSurveyUploadForm()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnChooseFile_Click(object sender, EventArgs e)
        {

            // Show the dialog that allows user to select a file, the 
            // call will result a value from the DialogResult enum
            // when the dialog is dismissed.
            DialogResult result = this.openFileDialogsSurvey.ShowDialog();
            // if a file is selected
            if (result == DialogResult.OK)
            {
                // Set the selected file URL to the textbox
                this.txvFileUrl.Text = this.openFileDialogsSurvey.FileName;
            }

        }

        private void openFileDialogsSurvey_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}

﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using Processor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class SubDialogAddPerson : Form
    {
        private int entityEmisId { get; set; }
        public ISubDialogAddPerson iSubDialogAddPerson { get; set; }

        public SubDialogAddPerson(int entityEmisId)
        {
            InitializeComponent();
            this.entityEmisId = entityEmisId;

            loadData();
        }

        private void loadData()
        {
            List<ProcurementRole> roles = DataGenerator.mockProcurementRoles();
            WidgetHandler.populateProcurementRolesDropdown(cbxProcurementRole, roles);
        }

        

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!validInput())
            {
                return;
            }

            //input is valid
            String firstName = txvFirstName.Text;
            String lastName = txvLastName.Text;
            String email = txvEmail.Text;
            String note = txvNote.Text;
            String phoneNo = txvPhoneNo.Text;
            String title = txvTitle.Text;
            String gender = rbtnGenderMale.Checked ? "male" : "female";
            String roleInProc = cbxProcurementRole.Text;

            People person = new People();
            person.firstName = firstName;
            person.lastName = lastName;
            person.email = email;
            person.entityEmisId = entityEmisId;
            person.gender = gender;
            person.note = note;
            person.phoneNo = phoneNo;
            person.title = title;
            person.roleInProcurement = roleInProc;

            DatabaseHandler.addPerson(DatabaseHandler.dbConnection(), person);

            //clear the fields
            clearFields();

            //show success message
            showMessage("Person successfully added", false);

            //send message to parent form to reload people list
            iSubDialogAddPerson.personSuccessfullyAdded();
           

        }

        private bool validInput()
        {
            String firstName = txvFirstName.Text;
            String lastName = txvLastName.Text;
            String email = txvEmail.Text;
            String note = txvNote.Text;
            String phoneNo = txvPhoneNo.Text;
            String title = txvTitle.Text;
            String gender = rbtnGenderMale.Checked ? "male" : "female";
            int selectProcRoleIndex = cbxProcurementRole.SelectedIndex;

            if (String.IsNullOrEmpty(firstName))
            {
                String msg = "Please enter a valid first name";
                showMessage(msg, true);
                return false;
            }

            if (String.IsNullOrEmpty(lastName))
            {
                String msg = "Please enter a valid last name";
                showMessage(msg, true);
                return false;
            }

            if (!String.IsNullOrEmpty(email) && !SharedCommons.IsValidEmail(email))
            {
                String msg = "Please enter a valid email address";
                showMessage(msg, true);
                return false;
            }

            if (selectProcRoleIndex == 0)
            {
                String msg = "Please select the role of the person in the procurement process";
                showMessage(msg, true);
                return false;
            }

            return true;

        }

        private void clearFields()
        {

            txvFirstName.Text = "";
            txvLastName.Text = "";
            txvEmail.Text = "";
            txvNote.Text = "";
            txvPhoneNo.Text = "";
            txvTitle.Text = "";
            cbxProcurementRole.SelectedIndex = 0;
            rbtnGenderMale.Checked = true;

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void showMessage(string message, bool isError)
        {

            if (isError)
            {
                DialogResult res = MessageBox.Show(message, "Operation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult res = MessageBox.Show(message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
    }
}

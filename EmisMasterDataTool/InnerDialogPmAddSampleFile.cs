﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using Processor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class InnerDialogPmAddSampleFile : Form
    {
        private int activityEmisId;
        public IInnerDialogPmAddSampleFile iInnerDialogPmAddSampleFile { get; set; }

        public InnerDialogPmAddSampleFile(int activityEmisId)
        {
            InitializeComponent();
            this.activityEmisId = activityEmisId;

            initializeCustom();

            loadData();

        }

        private void initializeCustom()
        {
            dateTimePickerDateOfAward.Format = DateTimePickerFormat.Custom;
            dateTimePickerDateOfAward.CustomFormat = "yyyy-MM-dd";
        }

        private void loadData()
        {

            //procurement methods
            WidgetHandler.populateDropdownWithStringData(cbxProcMethd, DataGenerator.procurementMethods());
            //providers
            WidgetHandler.populateDropdownWithStringData(cbxProvider, DataGenerator.mockProviders());
            //users
            List<User> users = new DatabaseHandler().allUsers(DatabaseHandler.dbConnection());
            WidgetHandler.populateUsersCombo(cbxAssignee, users);


        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSaveSampleFile_Click(object sender, EventArgs e)
        {
            //save the sampel file
            if (!validData())
            {
                return;
            }

            PmAuditSampleFile sample = new PmAuditSampleFile();
            sample.pm_activity_id = activityEmisId;
            sample.procurement_reference = txvProcRefNo.Text;
            sample.procurement_method = cbxProcMethd.Text;
            sample.procurement_subject = txvSubject.Text;

            double value = 0;
            sample.contract_value =  double.TryParse(txvContractValue.Text, out value) ? value : 0.0 ;

            sample.user_id = (int)cbxAssignee.SelectedValue;
            sample.provider = cbxProvider.Text;
            sample.date_of_award = dateTimePickerDateOfAward.Text;

            DatabaseHandler.addPmAuditSampleFile(DatabaseHandler.dbConnection(), sample);
            
            //alert caller to update their UI
            iInnerDialogPmAddSampleFile.sampleFileSuccessfullyAdded();

            //show message
            WidgetHandler.showMessage("Sample file successfully added", false);

            //clear fields
            clearFields();         

        }

        private void clearFields()
        {

            txvProcRefNo.Text = "";
            txvContractValue.Text = "";
            txvSubject.Text = "";
            cbxAssignee.SelectedIndex = 0;
            cbxProcMethd.SelectedIndex = 0;
            cbxProvider.SelectedIndex = 0;

        }

        private bool validData()
        {

            if (String.IsNullOrEmpty(txvProcRefNo.Text))
            {
                WidgetHandler.showMessage("Please enter the procurement reference no", true);
                return false;
            }

            if (String.IsNullOrEmpty(txvSubject.Text))
            {
                WidgetHandler.showMessage("Please enter the procurement subject", true);
                return false;
            }

            return true;

        }
    }
}

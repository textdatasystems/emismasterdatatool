﻿namespace EmisMasterDataTool
{
    partial class SubDialogPmAuditDates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubDialogPmAuditDates));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txvTimeExitMeeting = new System.Windows.Forms.TextBox();
            this.txvDateExitMeeting = new System.Windows.Forms.TextBox();
            this.txvTimeReceipt = new System.Windows.Forms.TextBox();
            this.txvDateReceipt = new System.Windows.Forms.TextBox();
            this.txvTimeSubmissionOfManagementLetter = new System.Windows.Forms.TextBox();
            this.txvDateSubmissionOfManagementLetter = new System.Windows.Forms.TextBox();
            this.txvTimeEndDate = new System.Windows.Forms.TextBox();
            this.txvDateEndDate = new System.Windows.Forms.TextBox();
            this.txvTimeDebrief = new System.Windows.Forms.TextBox();
            this.txvDateDebrief = new System.Windows.Forms.TextBox();
            this.txvTimeCuttoff = new System.Windows.Forms.TextBox();
            this.txvDateCuttoff = new System.Windows.Forms.TextBox();
            this.txvTimeStartDate = new System.Windows.Forms.TextBox();
            this.txvDateStartDate = new System.Windows.Forms.TextBox();
            this.txvTimeEntryMeeting = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txvDateEntryMeeting = new System.Windows.Forms.TextBox();
            this.labelActivityName = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.labelActivityName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(20);
            this.panel1.Size = new System.Drawing.Size(908, 439);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(807, 392);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.txvTimeExitMeeting, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.txvDateExitMeeting, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txvTimeReceipt, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.txvDateReceipt, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txvTimeSubmissionOfManagementLetter, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.txvDateSubmissionOfManagementLetter, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txvTimeEndDate, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.txvDateEndDate, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txvTimeDebrief, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.txvDateDebrief, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txvTimeCuttoff, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.txvDateCuttoff, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txvTimeStartDate, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.txvDateStartDate, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txvTimeEntryMeeting, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.txvDateEntryMeeting, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(23, 55);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(859, 331);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // txvTimeExitMeeting
            // 
            this.txvTimeExitMeeting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvTimeExitMeeting.Location = new System.Drawing.Point(646, 292);
            this.txvTimeExitMeeting.Name = "txvTimeExitMeeting";
            this.txvTimeExitMeeting.ReadOnly = true;
            this.txvTimeExitMeeting.Size = new System.Drawing.Size(209, 22);
            this.txvTimeExitMeeting.TabIndex = 26;
            // 
            // txvDateExitMeeting
            // 
            this.txvDateExitMeeting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvDateExitMeeting.Location = new System.Drawing.Point(432, 292);
            this.txvDateExitMeeting.Name = "txvDateExitMeeting";
            this.txvDateExitMeeting.ReadOnly = true;
            this.txvDateExitMeeting.Size = new System.Drawing.Size(207, 22);
            this.txvDateExitMeeting.TabIndex = 25;
            // 
            // txvTimeReceipt
            // 
            this.txvTimeReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvTimeReceipt.Location = new System.Drawing.Point(646, 256);
            this.txvTimeReceipt.Name = "txvTimeReceipt";
            this.txvTimeReceipt.ReadOnly = true;
            this.txvTimeReceipt.Size = new System.Drawing.Size(209, 22);
            this.txvTimeReceipt.TabIndex = 24;
            // 
            // txvDateReceipt
            // 
            this.txvDateReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvDateReceipt.Location = new System.Drawing.Point(432, 256);
            this.txvDateReceipt.Name = "txvDateReceipt";
            this.txvDateReceipt.ReadOnly = true;
            this.txvDateReceipt.Size = new System.Drawing.Size(207, 22);
            this.txvDateReceipt.TabIndex = 23;
            // 
            // txvTimeSubmissionOfManagementLetter
            // 
            this.txvTimeSubmissionOfManagementLetter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvTimeSubmissionOfManagementLetter.Location = new System.Drawing.Point(646, 220);
            this.txvTimeSubmissionOfManagementLetter.Name = "txvTimeSubmissionOfManagementLetter";
            this.txvTimeSubmissionOfManagementLetter.ReadOnly = true;
            this.txvTimeSubmissionOfManagementLetter.Size = new System.Drawing.Size(209, 22);
            this.txvTimeSubmissionOfManagementLetter.TabIndex = 22;
            // 
            // txvDateSubmissionOfManagementLetter
            // 
            this.txvDateSubmissionOfManagementLetter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvDateSubmissionOfManagementLetter.Location = new System.Drawing.Point(432, 220);
            this.txvDateSubmissionOfManagementLetter.Name = "txvDateSubmissionOfManagementLetter";
            this.txvDateSubmissionOfManagementLetter.ReadOnly = true;
            this.txvDateSubmissionOfManagementLetter.Size = new System.Drawing.Size(207, 22);
            this.txvDateSubmissionOfManagementLetter.TabIndex = 21;
            this.txvDateSubmissionOfManagementLetter.TextChanged += new System.EventHandler(this.textBox11_TextChanged);
            // 
            // txvTimeEndDate
            // 
            this.txvTimeEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvTimeEndDate.Location = new System.Drawing.Point(646, 184);
            this.txvTimeEndDate.Name = "txvTimeEndDate";
            this.txvTimeEndDate.ReadOnly = true;
            this.txvTimeEndDate.Size = new System.Drawing.Size(209, 22);
            this.txvTimeEndDate.TabIndex = 20;
            // 
            // txvDateEndDate
            // 
            this.txvDateEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvDateEndDate.Location = new System.Drawing.Point(432, 184);
            this.txvDateEndDate.Name = "txvDateEndDate";
            this.txvDateEndDate.ReadOnly = true;
            this.txvDateEndDate.Size = new System.Drawing.Size(207, 22);
            this.txvDateEndDate.TabIndex = 19;
            // 
            // txvTimeDebrief
            // 
            this.txvTimeDebrief.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvTimeDebrief.Location = new System.Drawing.Point(646, 148);
            this.txvTimeDebrief.Name = "txvTimeDebrief";
            this.txvTimeDebrief.ReadOnly = true;
            this.txvTimeDebrief.Size = new System.Drawing.Size(209, 22);
            this.txvTimeDebrief.TabIndex = 18;
            this.txvTimeDebrief.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // txvDateDebrief
            // 
            this.txvDateDebrief.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvDateDebrief.Location = new System.Drawing.Point(432, 148);
            this.txvDateDebrief.Name = "txvDateDebrief";
            this.txvDateDebrief.ReadOnly = true;
            this.txvDateDebrief.Size = new System.Drawing.Size(207, 22);
            this.txvDateDebrief.TabIndex = 17;
            // 
            // txvTimeCuttoff
            // 
            this.txvTimeCuttoff.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvTimeCuttoff.Location = new System.Drawing.Point(646, 112);
            this.txvTimeCuttoff.Name = "txvTimeCuttoff";
            this.txvTimeCuttoff.ReadOnly = true;
            this.txvTimeCuttoff.Size = new System.Drawing.Size(209, 22);
            this.txvTimeCuttoff.TabIndex = 16;
            // 
            // txvDateCuttoff
            // 
            this.txvDateCuttoff.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvDateCuttoff.Location = new System.Drawing.Point(432, 112);
            this.txvDateCuttoff.Name = "txvDateCuttoff";
            this.txvDateCuttoff.ReadOnly = true;
            this.txvDateCuttoff.Size = new System.Drawing.Size(207, 22);
            this.txvDateCuttoff.TabIndex = 15;
            // 
            // txvTimeStartDate
            // 
            this.txvTimeStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvTimeStartDate.Location = new System.Drawing.Point(646, 76);
            this.txvTimeStartDate.Name = "txvTimeStartDate";
            this.txvTimeStartDate.ReadOnly = true;
            this.txvTimeStartDate.Size = new System.Drawing.Size(209, 22);
            this.txvTimeStartDate.TabIndex = 14;
            // 
            // txvDateStartDate
            // 
            this.txvDateStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvDateStartDate.Location = new System.Drawing.Point(432, 76);
            this.txvDateStartDate.Name = "txvDateStartDate";
            this.txvDateStartDate.ReadOnly = true;
            this.txvDateStartDate.Size = new System.Drawing.Size(207, 22);
            this.txvDateStartDate.TabIndex = 13;
            // 
            // txvTimeEntryMeeting
            // 
            this.txvTimeEntryMeeting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvTimeEntryMeeting.Location = new System.Drawing.Point(646, 40);
            this.txvTimeEntryMeeting.Name = "txvTimeEntryMeeting";
            this.txvTimeEntryMeeting.ReadOnly = true;
            this.txvTimeEntryMeeting.Size = new System.Drawing.Size(209, 22);
            this.txvTimeEntryMeeting.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(4, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "Activity";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(432, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 35);
            this.label3.TabIndex = 1;
            this.label3.Text = "Date";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(646, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 35);
            this.label4.TabIndex = 2;
            this.label4.Text = "Time";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 35);
            this.label5.TabIndex = 3;
            this.label5.Text = "Entry meeting";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 35);
            this.label6.TabIndex = 4;
            this.label6.Text = "Activity Start date";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 35);
            this.label7.TabIndex = 5;
            this.label7.Text = "Cut-off date for receiving files";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(165, 35);
            this.label8.TabIndex = 6;
            this.label8.Text = "Debrief the Accounting Officer";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 181);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 35);
            this.label9.TabIndex = 7;
            this.label9.Text = "Activity End date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 217);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(393, 35);
            this.label10.TabIndex = 8;
            this.label10.Text = "Submission of a management letter to the Entity for management response";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 253);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(360, 35);
            this.label11.TabIndex = 9;
            this.label11.Text = "Deadline for receipt of official management response from the Entity";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 289);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 41);
            this.label12.TabIndex = 10;
            this.label12.Text = "Exit meeting";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txvDateEntryMeeting
            // 
            this.txvDateEntryMeeting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txvDateEntryMeeting.Location = new System.Drawing.Point(432, 40);
            this.txvDateEntryMeeting.Name = "txvDateEntryMeeting";
            this.txvDateEntryMeeting.ReadOnly = true;
            this.txvDateEntryMeeting.Size = new System.Drawing.Size(207, 22);
            this.txvDateEntryMeeting.TabIndex = 11;
            this.txvDateEntryMeeting.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // labelActivityName
            // 
            this.labelActivityName.AutoSize = true;
            this.labelActivityName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivityName.Location = new System.Drawing.Point(23, 23);
            this.labelActivityName.Name = "labelActivityName";
            this.labelActivityName.Size = new System.Drawing.Size(393, 13);
            this.labelActivityName.TabIndex = 0;
            this.labelActivityName.Text = "Uganda Police Force - Performance Audits - FY 2019-2020 - Audit Schedule";
            // 
            // SubDialogPmAuditDates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 439);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SubDialogPmAuditDates";
            this.Text = "Audit Schedule";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelActivityName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txvDateEntryMeeting;
        private System.Windows.Forms.TextBox txvTimeExitMeeting;
        private System.Windows.Forms.TextBox txvDateExitMeeting;
        private System.Windows.Forms.TextBox txvTimeReceipt;
        private System.Windows.Forms.TextBox txvDateReceipt;
        private System.Windows.Forms.TextBox txvTimeSubmissionOfManagementLetter;
        private System.Windows.Forms.TextBox txvDateSubmissionOfManagementLetter;
        private System.Windows.Forms.TextBox txvTimeEndDate;
        private System.Windows.Forms.TextBox txvDateEndDate;
        private System.Windows.Forms.TextBox txvTimeDebrief;
        private System.Windows.Forms.TextBox txvDateDebrief;
        private System.Windows.Forms.TextBox txvTimeCuttoff;
        private System.Windows.Forms.TextBox txvDateCuttoff;
        private System.Windows.Forms.TextBox txvTimeStartDate;
        private System.Windows.Forms.TextBox txvDateStartDate;
        private System.Windows.Forms.TextBox txvTimeEntryMeeting;
    }
}
﻿namespace EmisMasterDataTool
{
    partial class InnerDialogPmAddSampleFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InnerDialogPmAddSampleFile));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSaveSampleFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbxProcMethd = new System.Windows.Forms.ComboBox();
            this.cbxProvider = new System.Windows.Forms.ComboBox();
            this.cbxAssignee = new System.Windows.Forms.ComboBox();
            this.txvSubject = new System.Windows.Forms.TextBox();
            this.txvContractValue = new System.Windows.Forms.TextBox();
            this.txvProcRefNo = new System.Windows.Forms.TextBox();
            this.dateTimePickerDateOfAward = new System.Windows.Forms.DateTimePicker();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSaveSampleFile);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(754, 441);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnSaveSampleFile
            // 
            this.btnSaveSampleFile.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSaveSampleFile.Location = new System.Drawing.Point(309, 326);
            this.btnSaveSampleFile.Margin = new System.Windows.Forms.Padding(3, 25, 3, 3);
            this.btnSaveSampleFile.Name = "btnSaveSampleFile";
            this.btnSaveSampleFile.Size = new System.Drawing.Size(185, 23);
            this.btnSaveSampleFile.TabIndex = 2;
            this.btnSaveSampleFile.Text = "Save Sample File";
            this.btnSaveSampleFile.UseVisualStyleBackColor = true;
            this.btnSaveSampleFile.Click += new System.EventHandler(this.btnSaveSampleFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "SAMPLE DETAILS";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.cbxProcMethd, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbxProvider, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbxAssignee, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txvSubject, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txvContractValue, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txvProcRefNo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePickerDateOfAward, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 49);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(726, 249);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(127, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "Procurement Refenrece Number";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(169, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 35);
            this.label3.TabIndex = 1;
            this.label3.Text = "Subject of Procurement";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(181, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 35);
            this.label4.TabIndex = 2;
            this.label4.Text = "Procurement Method";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(241, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 35);
            this.label5.TabIndex = 3;
            this.label5.Text = "Provider";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(169, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 35);
            this.label6.TabIndex = 4;
            this.label6.Text = "Date of Contract Award";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(210, 175);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 35);
            this.label7.TabIndex = 5;
            this.label7.Text = "Contract Value";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(237, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 39);
            this.label8.TabIndex = 6;
            this.label8.Text = "Assignee";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbxProcMethd
            // 
            this.cbxProcMethd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.cbxProcMethd.FormattingEnabled = true;
            this.cbxProcMethd.Location = new System.Drawing.Point(293, 73);
            this.cbxProcMethd.Name = "cbxProcMethd";
            this.cbxProcMethd.Size = new System.Drawing.Size(185, 21);
            this.cbxProcMethd.TabIndex = 7;
            // 
            // cbxProvider
            // 
            this.cbxProvider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.cbxProvider.FormattingEnabled = true;
            this.cbxProvider.Location = new System.Drawing.Point(293, 108);
            this.cbxProvider.Name = "cbxProvider";
            this.cbxProvider.Size = new System.Drawing.Size(185, 21);
            this.cbxProvider.TabIndex = 8;
            // 
            // cbxAssignee
            // 
            this.cbxAssignee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.cbxAssignee.FormattingEnabled = true;
            this.cbxAssignee.Location = new System.Drawing.Point(293, 213);
            this.cbxAssignee.Name = "cbxAssignee";
            this.cbxAssignee.Size = new System.Drawing.Size(185, 21);
            this.cbxAssignee.TabIndex = 9;
            // 
            // txvSubject
            // 
            this.txvSubject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvSubject.Location = new System.Drawing.Point(293, 38);
            this.txvSubject.Name = "txvSubject";
            this.txvSubject.Size = new System.Drawing.Size(359, 20);
            this.txvSubject.TabIndex = 10;
            // 
            // txvContractValue
            // 
            this.txvContractValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvContractValue.Location = new System.Drawing.Point(293, 178);
            this.txvContractValue.Name = "txvContractValue";
            this.txvContractValue.Size = new System.Drawing.Size(185, 20);
            this.txvContractValue.TabIndex = 11;
            // 
            // txvProcRefNo
            // 
            this.txvProcRefNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txvProcRefNo.Location = new System.Drawing.Point(293, 3);
            this.txvProcRefNo.Name = "txvProcRefNo";
            this.txvProcRefNo.Size = new System.Drawing.Size(185, 20);
            this.txvProcRefNo.TabIndex = 12;
            // 
            // dateTimePickerDateOfAward
            // 
            this.dateTimePickerDateOfAward.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dateTimePickerDateOfAward.Location = new System.Drawing.Point(293, 143);
            this.dateTimePickerDateOfAward.Name = "dateTimePickerDateOfAward";
            this.dateTimePickerDateOfAward.Size = new System.Drawing.Size(185, 20);
            this.dateTimePickerDateOfAward.TabIndex = 13;
            // 
            // InnerDialogPmAddSampleFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 441);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InnerDialogPmAddSampleFile";
            this.Text = "ADD SAMPLE FILE";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbxProcMethd;
        private System.Windows.Forms.ComboBox cbxProvider;
        private System.Windows.Forms.ComboBox cbxAssignee;
        private System.Windows.Forms.TextBox txvSubject;
        private System.Windows.Forms.TextBox txvContractValue;
        private System.Windows.Forms.TextBox txvProcRefNo;
        private System.Windows.Forms.DateTimePicker dateTimePickerDateOfAward;
        private System.Windows.Forms.Button btnSaveSampleFile;
    }
}
﻿using Processor.ControlObjects;
using Processor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class DialogEntityPeople : Form, ISubDialogAddPerson
    {
        SubDialogAddPerson subDialogAddPerson;
        private int entityEmisId { get; set; }

        public DialogEntityPeople(int entityEmisId)
        {
            InitializeComponent();
            this.entityEmisId = entityEmisId;

            loadData();

        }

        private void loadData()
        {

            List<People> people = new DatabaseHandler().allPeople(DatabaseHandler.dbConnection(), entityEmisId);
            var dt = new SharedCommons().ToDataTable<People>(people);

            gridViewPeople.AutoGenerateColumns = false;
            gridViewPeople.DataSource = dt;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            showDialogAddPerson(true);
        }

        private void showDialogAddPerson(bool open)
        {

            if (!open)
            {
                if (subDialogAddPerson.Visible)
                {
                    subDialogAddPerson.Close();
                }
                return;
            }


            subDialogAddPerson = new SubDialogAddPerson(entityEmisId);
            subDialogAddPerson.iSubDialogAddPerson = this;

            subDialogAddPerson.StartPosition = FormStartPosition.CenterParent;

            //subDialogAddPerson.iDialogAddParticipant = this;


            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (subDialogAddPerson.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
                //this.txtResult.Text = dialogAddParticipant.TextBox1.Text;

            }
            else
            {
                //this.txtResult.Text = "Cancelled";
            }

            subDialogAddPerson.Dispose();

        }

        public void personSuccessfullyAdded()
        {

            loadData();

        }
    }
}

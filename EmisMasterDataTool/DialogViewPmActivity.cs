﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Processor.Entities.custom;

namespace EmisMasterDataTool
{
    public partial class DialogViewPmActivity : Form
    {

        private int activityEmisId;
        private SubDialogPmAuditDates modalAuditDates;
        private SubDialogPmAuditLaunch modalAuditLauch;
        private SubDialogPmDebriefs modalDebriefs;
        private SubDialogPmExitMeeting modalExitMeeting;

        public DialogViewPmActivity(int activityEmisId)
        {
            InitializeComponent();
            this.activityEmisId = activityEmisId;

            loadData();

        }

        private void loadData()
        {            
            loadActivityDetails();
        }

        private void loadActivityDetails()
        {

            var db = DatabaseHandler.dbConnection();
            DatabaseHandler databaseHandler = new DatabaseHandler();
            var activity = databaseHandler.findCustomPmActivityByActivityId(db, activityEmisId);

            if (activity == null)
            {
                WidgetHandler.showMessage("Failed to get activity details for activity [" + activityEmisId + "]", true);
                return;
            }

            //show activity name
            labelActivityName.Text = activity.displayName();

            //show start date and end
            txvStartDate.Text = activity.startDate;
            txvEndDate.Text = activity.endDate;

            //show general details
            populateActivityDetailsPanelFields(activity);

            //show entry date
            showEntryMeetingDate(db, databaseHandler);

        }

        private void showEntryMeetingDate(SQLite.SQLiteConnection db, DatabaseHandler databaseHandler)
        {

            var auditDates = databaseHandler.getPmActivityAuditDates(db, activityEmisId);
            if (auditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_ENTRY_MEETING))
            {
                var entryMeetingDates = auditDates[Globals.PM_AUDIT_DATE_TAG_ENTRY_MEETING];
                labelPmEntryMeetingDate.Text = entryMeetingDates.activity_date;
            }

        }

        private void populateActivityDetailsPanelFields(CustomPmActivity activity)
        {

            txvAcitivityId.Text = activity.emisId.ToString();
            txvEntity.Text = activity.entityName;
            txvAcuditType.Text = activity.AuditType;
            txvSourceOfFunding.Text = activity.fundingSourceName;
            txvPpdaOffice.Text = activity.responsibleOffice;
            txvEntityCategory.Text = "";
            txvSector.Text = "";
            txvSpendCategory.Text = "";
            txvEntityType.Text = "";
            txvUsmidStatus.Text = "";
            txvGppStatus.Text = "";
            txvEgpStatus.Text = "";
            txvProcBudget.Text = activity.procurementBudget;
            txvOagOpinion.Text = activity.oagOpinion;
            txvOagYear.Text = "";
            txvLastAudited.Text = activity.lastAudited;
            txvPerformanceScore.Text = activity.previousPerformanceScore;
            txvPerformanceScoreCat.Text = activity.previousPerformanceCategory;

            //budget items
            WidgetHandler.populateBudgetItemAmounts(activity.budgetItemAmounts, listPmBudget);
            //team members
            if(activity.pmTeam != null)
            {
                populatePmTeamMembers(activity.pmTeam.teamMembers, listTeamMembers);
            }            

        }


        private void populatePmTeamMembers(List<CustomPmTeamMember> teamMembers, ListView listView)
        {

            listView.View = View.Details;

            listView.Columns.Add("#", 5);
            listView.Columns.Add("Team Member");
            listView.Columns[0].ListView.Font = new System.Drawing.Font(listView.Columns[0].ListView.Font, System.Drawing.FontStyle.Bold);


            foreach (ColumnHeader column in listView.Columns) { column.Width = -2; }

            for (int count = 0; count < teamMembers.Count; count++)
            {
                ListViewItem listViewItem = new ListViewItem(new[] { (count + 1).ToString(), teamMembers[count].fullName });
                listViewItem.Font = new System.Drawing.Font(listViewItem.Font, System.Drawing.FontStyle.Regular);
                listView.Items.Add(listViewItem);
            }

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void txvOagOpinion_Click(object sender, EventArgs e)
        {

        }

        private void listTeamMembers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAuditDates_Click(object sender, EventArgs e)
        {
            showDialogAuditDates(true);
        }


        private void showDialogAuditDates(bool open)
        {

            if (!open)
            {
                if (modalAuditDates.Visible)
                {
                    modalAuditDates.Close();
                }
                return;
            }

            modalAuditDates = new SubDialogPmAuditDates(activityEmisId);
            modalAuditDates.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalAuditDates.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalAuditDates.Dispose();

        }

        private void showDialogAuditLaunch(bool open)
        {

            if (!open)
            {
                if (modalAuditLauch.Visible)
                {
                    modalAuditLauch.Close();
                }
                return;
            }

            modalAuditLauch = new SubDialogPmAuditLaunch(activityEmisId);
            modalAuditLauch.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalAuditLauch.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalAuditLauch.Dispose();

        }


        private void showDialogDebriefs(bool open)
        {

            if (!open)
            {
                if (modalDebriefs.Visible)
                {
                    modalDebriefs.Close();
                }
                return;
            }

            modalDebriefs = new SubDialogPmDebriefs(activityEmisId);
            modalDebriefs.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalDebriefs.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalDebriefs.Dispose();

        }

        private void showDialogExitMeetings(bool open)
        {

            if (!open)
            {
                if (modalExitMeeting.Visible)
                {
                    modalExitMeeting.Close();
                }
                return;
            }

            modalExitMeeting = new SubDialogPmExitMeeting(activityEmisId);
            modalExitMeeting.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalExitMeeting.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalExitMeeting.Dispose();

        }

        private void btnAuditLaunch_Click(object sender, EventArgs e)
        {
            showDialogAuditLaunch(true);
        }

        private void btnDebriefs_Click(object sender, EventArgs e)
        {
            showDialogDebriefs(true);
        }

        private void btnExitMeeting_Click(object sender, EventArgs e)
        {
            showDialogExitMeetings(true);
        }

        private void btnAuditSample_Click(object sender, EventArgs e)
        {
            showDialogAuditSamples();
        }


        private void showDialogAuditSamples(bool open = true)
        {
            var modalAuditSample = new SubDialogPmAuditSample(activityEmisId);

            if (!open)
            {
                if (modalAuditSample.Visible)
                {
                    modalAuditSample.Close();
                }
                return;
            }

            
            modalAuditSample.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalAuditSample.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalAuditSample.Dispose();

        }


        private void showDialogAuditToolFiles(bool open = true)
        {
            var modalAuditToolFiles = new SubDialogPmAuditToolFiles(activityEmisId);

            if (!open)
            {
                if (modalAuditToolFiles.Visible)
                {
                    modalAuditToolFiles.Close();
                }
                return;
            }


            modalAuditToolFiles.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalAuditToolFiles.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalAuditToolFiles.Dispose();

        }

        private void btnAuditToolFiles_Click(object sender, EventArgs e)
        {
            showDialogAuditToolFiles();
        }
    }

}

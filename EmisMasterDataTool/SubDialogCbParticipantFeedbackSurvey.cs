﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class SubDialogCbParticipantFeedbackSurvey : Form
    {
        private int activityEmisId;
        private InnerDialogCbSurveyUploadForm modalBrowseForSurveyFile;

        public SubDialogCbParticipantFeedbackSurvey(int activityEmisId)
        {
            InitializeComponent();
            this.activityEmisId = activityEmisId;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnOpenFileUploadForm_Click(object sender, EventArgs e)
        {
            showDialogUploadSurveyForm(true);
        }

        private void showDialogUploadSurveyForm(bool open)
        {

            if (!open)
            {
                if (modalBrowseForSurveyFile.Visible)
                {
                    modalBrowseForSurveyFile.Close();
                }
                return;
            }


            modalBrowseForSurveyFile = new InnerDialogCbSurveyUploadForm();

            //a listener is required to receive the file received
            modalBrowseForSurveyFile.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalBrowseForSurveyFile.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalBrowseForSurveyFile.Dispose();

        }

    }
}

﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using Processor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class InnerDialogCbRegisterParticipant : Form
    {
        private int activityEmisId;
        public IInnerDialogCbRegisterParticipant iInnerDialogCbRegisterParticipant { get; set; }
        public InnerDialogCbRegisterParticipant(int activityEmisId)
        {
            InitializeComponent();
            this.activityEmisId = activityEmisId;

            loadData();

        }

        private void loadData()
        {

            //populate procurement roles
            List<ProcurementRole> roles = DataGenerator.mockProcurementRoles();
            WidgetHandler.populateProcurementRolesDropdown(cbxProcurementRole, roles);

            List<Entity> entities = new DatabaseHandler().allEntities(DatabaseHandler.dbConnection());
            WidgetHandler.populateEntities(cbxEntities, entities);

        }

        private void txvNote_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txvPhoneNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSaveParticipant_Click(object sender, EventArgs e)
        {

            int personLocalId = 0;

            //attempt to save the guy
            saveParticipantInPeopleTable(out personLocalId);

            //failed to save person
            if (personLocalId == 0)
            {
                return;
            }

            //save person under a cb activity person
            saveCbActivityPerson(personLocalId);

        }

        private void saveCbActivityPerson(int personLocalId)
        {

            //save the person in CbActivityPeople table
            CbActivityPeople activityPeople = new CbActivityPeople();
            activityPeople.cb_activity_id = activityEmisId;
            activityPeople.person_local_id = personLocalId;

            //what happens if we fail to save, ideally you are supposed to delete the person from people
            try
            {

                DatabaseHandler.addCbActivityPeople(DatabaseHandler.dbConnection(), activityPeople);

                //clear fields
                clearFields();

                //person saved
                WidgetHandler.showMessage("Participant successfully saved", false);

                //alert caller
                iInnerDialogCbRegisterParticipant.participantSuccessfullyRegistered();

            }
            catch (Exception exception)
            {

                WidgetHandler.showMessage("Error occurred on saving participant [" + exception.Message + "]", true);
                DatabaseHandler.deletePerson(DatabaseHandler.dbConnection(), personLocalId);

            }

        }

        private void clearFields()
        {

            txvFirstName.Text = "";
            txvLastName.Text = "";
            txvEmail.Text = "";
            txvNote.Text = "";
            txvPhoneNo.Text = "";
            txvTitle.Text = "";
            cbxProcurementRole.SelectedIndex = 0;
            cbxEntities.SelectedIndex = 0;
            rbtnGenderMale.Checked = true;

        }

        private void saveParticipantInPeopleTable(out int personLocalId)
        {           

            if (!validInput())
            {
                personLocalId = 0;
                return;
            }

            //input is valid
            String firstName = txvFirstName.Text;
            String lastName = txvLastName.Text;
            String email = txvEmail.Text;
            String note = txvNote.Text;
            String phoneNo = txvPhoneNo.Text;
            String title = txvTitle.Text;
            String gender = rbtnGenderMale.Checked ? "male" : "female";
            String roleInProc = cbxProcurementRole.Text;
            int entityEmisId = (int)cbxEntities.SelectedValue;

            People person = new People();
            person.firstName = firstName;
            person.lastName = lastName;
            person.email = email;
            person.entityEmisId = entityEmisId;
            person.gender = gender;
            person.note = note;
            person.phoneNo = phoneNo;
            person.title = title;
            person.roleInProcurement = roleInProc;

            int id = DatabaseHandler.addPerson(DatabaseHandler.dbConnection(), person);
            personLocalId = id;
            
        }

        private bool validInput()
        {
            String firstName = txvFirstName.Text;
            String lastName = txvLastName.Text;
            String email = txvEmail.Text;
            String note = txvNote.Text;
            String phoneNo = txvPhoneNo.Text;
            String title = txvTitle.Text;
            String gender = rbtnGenderMale.Checked ? "male" : "female";
            int selectProcRoleIndex = cbxProcurementRole.SelectedIndex;

            if (String.IsNullOrEmpty(firstName))
            {
                String msg = "Please enter a valid first name";
                WidgetHandler.showMessage(msg, true);
                return false;
            }

            if (String.IsNullOrEmpty(lastName))
            {
                String msg = "Please enter a valid last name";
                WidgetHandler.showMessage(msg, true);
                return false;
            }

            if(cbxEntities.SelectedIndex == 0)
            {
                String msg = "Please select entity";
                WidgetHandler.showMessage(msg, true);
                return false;
            }

            if (!String.IsNullOrEmpty(email) && !SharedCommons.IsValidEmail(email))
            {
                String msg = "Please enter a valid email address";
                WidgetHandler.showMessage(msg, true);
                return false;
            }

            if (selectProcRoleIndex == 0)
            {
                String msg = "Please select the role of the person in the procurement process";
                WidgetHandler.showMessage(msg, true);
                return false;
            }

            return true;

        }

    }
}

﻿using Processor.ControlObjects;
using Processor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class SubDialogPmAuditDates : Form
    {
        private int activtyEmisId;
        public SubDialogPmAuditDates(int activtyEmisId)
        {
            InitializeComponent();
            this.activtyEmisId = activtyEmisId;

            loadData();

        }

        private void loadData()
        {

            DatabaseHandler db = new DatabaseHandler();

            var activity = db.findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activtyEmisId);
            if(activity == null)
            {
                return;
            }

            labelActivityName.Text = activity.displayName();

            Dictionary<String, PmAuditDate> auditDates = db.getPmActivityAuditDates(DatabaseHandler.dbConnection(), activtyEmisId);

            if (auditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_ENTRY_MEETING))
            {
                var data = auditDates[Globals.PM_AUDIT_DATE_TAG_ENTRY_MEETING];
                txvDateEntryMeeting.Text = data.activity_date;
                txvTimeEntryMeeting.Text = data.activity_time;
            }


            if (auditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_START_DATE))
            {
                var data = auditDates[Globals.PM_AUDIT_DATE_TAG_START_DATE];
                txvDateStartDate.Text = data.activity_date;
                txvTimeStartDate.Text = data.activity_time;
            }

            if (auditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_CUTT_OFF))
            {
                var data = auditDates[Globals.PM_AUDIT_DATE_TAG_CUTT_OFF];
                txvDateCuttoff.Text = data.activity_date;
                txvTimeCuttoff.Text = data.activity_time;
            }

            if (auditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_DEBRIEF))
            {
                var data = auditDates[Globals.PM_AUDIT_DATE_TAG_DEBRIEF];
                txvDateDebrief.Text = data.activity_date;
                txvTimeDebrief.Text = data.activity_time;
            }

            if (auditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_END_DATE))
            {
                var data = auditDates[Globals.PM_AUDIT_DATE_TAG_END_DATE];
                txvDateEndDate.Text = data.activity_date;
                txvTimeEndDate.Text = data.activity_time;
            }

            if (auditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_LETTER_SUBMISSION))
            {
                var data = auditDates[Globals.PM_AUDIT_DATE_TAG_LETTER_SUBMISSION];
                txvDateSubmissionOfManagementLetter.Text = data.activity_date;
                txvTimeSubmissionOfManagementLetter.Text = data.activity_time;
            }

            if (auditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_RECEIPT_DATE))
            {
                var data = auditDates[Globals.PM_AUDIT_DATE_TAG_RECEIPT_DATE];
                txvDateReceipt.Text = data.activity_date;
                txvTimeReceipt.Text = data.activity_time;
            }

            if (auditDates.ContainsKey(Globals.PM_AUDIT_DATE_TAG_EXIT_MEETING))
            {
                var data = auditDates[Globals.PM_AUDIT_DATE_TAG_EXIT_MEETING];
                txvDateExitMeeting.Text = data.activity_date;
                txvTimeExitMeeting.Text = data.activity_time;
            }
            
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

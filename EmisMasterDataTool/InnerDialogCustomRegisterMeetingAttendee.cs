﻿using EmisMasterDataTool.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Processor.Entities;
using Processor.ControlObjects;

namespace EmisMasterDataTool
{
    public partial class InnerDialogCustomRegisterMeetingAttendee : Form, IInnerDialogCustomAddPerson
    {

        private int activityEmisId;
        private String auditActivity;
        public IInnerDialogCustomRegisterMeetingAttendee iInnerDialogCustomRegisterMeetingAttendee { get; set; }

        public InnerDialogCustomRegisterMeetingAttendee(int activityEmisId, String auditActivity)
        {
            InitializeComponent();
            this.activityEmisId = activityEmisId;
            this.auditActivity = auditActivity;

            loadData();

        }

        private void loadData()
        {

            //load attendee type
            loadAttendeeType();

            //load users
            loadUsers();

            //load people
            loadPeople();

            //load meeting roles
            loadMeetingRoles();

            //hide some fields
            hideAllParticipantSelectionOptions();

            //show activity name
            var activity = new DatabaseHandler().findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityEmisId);
            if(activity == null)
            {
                return;
            }
            else
            {
                labelActivityName.Text = activity.entityName;
            }
            
        }

        private void loadMeetingRoles()
        {
            DatabaseHandler db = new DatabaseHandler();
            var roles = db.allPmAuditMeetingRoles(DatabaseHandler.dbConnection());
            WidgetHandler.populatePmAuditMeetingRoleCombo(cbxMeetingRole, roles);
        }

        private void loadPeople()
        {
            DatabaseHandler db = new DatabaseHandler();
            var people = db.allPeople(DatabaseHandler.dbConnection());
            WidgetHandler.populatePeoplesCombo(cbxPeople, people);
        }

        private void loadUsers()
        {
            DatabaseHandler db = new DatabaseHandler();
            var users = db.allUsers(DatabaseHandler.dbConnection());
            WidgetHandler.populateUsersCombo(cbxUsers, users);
        }

        private void loadAttendeeType()
        {

            List<AttendeeType> attendeTypes = DataGenerator.attendeeTypes();
            WidgetHandler.populateAttendeTypeCombo(cbxCommentableType, attendeTypes);

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void btnAddEntityPerson_Click(object sender, EventArgs e)
        {
            showDialogAddPerson(true);
        }

        private void showDialogAddPerson(bool open)
        {

            var modalAddPerson = new InnerDialogCustomAddPerson(activityEmisId);
            modalAddPerson.iInnerDialogCustomAddPerson = this;

            if (!open)
            {
                if (modalAddPerson.Visible)
                {
                    modalAddPerson.Close();
                }

                return;
            }

            
            modalAddPerson.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (modalAddPerson.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            modalAddPerson.Dispose();

        }

        public void personSuccessfullyCreated(People person)
        {
            //add the person to the people combox and preselect that person
            loadPeople();
            cbxPeople.SelectedText = person.FullName;

        }

        private void btnSaveAttendee_Click(object sender, EventArgs e)
        {

            //save the attendee
            if (!validData())
            {
                return;
            }

            //proceed with saving the guy
            PmMeetingAttendances attendence = new PmMeetingAttendances();
            attendence.pm_activity_id = activityEmisId;
            attendence.commentable_type = (String)cbxCommentableType.SelectedValue;
            attendence.commentable_id = (String)cbxCommentableType.SelectedValue == Globals.ATTENDEE_TYPE_PPDA_USER ?
                (int)cbxUsers.SelectedValue : (int)cbxPeople.SelectedValue;
            attendence.audit_meeting_role_id = (int)cbxMeetingRole.SelectedValue;
            attendence.audit_activity = auditActivity;

            DatabaseHandler.addPmMeetingAttendance(DatabaseHandler.dbConnection(),attendence);

            //show message
            WidgetHandler.showMessage("Attendance successfully captured", false);

            //clear fields
            clearFields();

            //notify caller
            iInnerDialogCustomRegisterMeetingAttendee.meetingAttendeeRegistrationSuccessful();

        }

        private void clearFields()
        {

            cbxCommentableType.SelectedIndex = 0;
            cbxUsers.SelectedIndex = 0;
            cbxPeople.SelectedIndex = 0;
            cbxMeetingRole.SelectedIndex = 0;
            
        }

        private bool validData()
        {
            
            if(cbxCommentableType.SelectedIndex == 0)
            {
                WidgetHandler.showMessage("Please select attendee type", true);
                return false;
            }

            String attendeeType =(String)cbxCommentableType.SelectedValue;

            if(attendeeType == Globals.ATTENDEE_TYPE_ENTITY_PERSON && cbxPeople.SelectedIndex == 0)
            {
                WidgetHandler.showMessage("Please select entity person", true);
                return false;
            }

            if (attendeeType == Globals.ATTENDEE_TYPE_PPDA_USER && cbxUsers.SelectedIndex == 0)
            {
                WidgetHandler.showMessage("Please select PPDA user", true);
                return false;
            }

            if (cbxMeetingRole.SelectedIndex == 0)
            {
                WidgetHandler.showMessage("Please select role in procurement", true);
                return false;
            }

            return true;

        }

        private void cbxCommentableType_SelectedIndexChanged(object sender, EventArgs e)
        {

            if(cbxCommentableType.SelectedIndex == 0)
            {

                hideAllParticipantSelectionOptions();
                return;
            }

            String selectedType = (String)cbxCommentableType.SelectedValue;
            if(selectedType == Globals.ATTENDEE_TYPE_PPDA_USER)
            {
                //hide options for entity

                labelPpdaUser.Enabled = true;
                cbxUsers.Enabled = true;

                labelEntityPerson.Enabled = false;
                cbxPeople.Enabled = false;
                btnAddEntityPerson.Enabled = false;

            }
            else if (selectedType == Globals.ATTENDEE_TYPE_ENTITY_PERSON)
            {
                //hide options for ppda user

                labelPpdaUser.Enabled = false;
                cbxUsers.Enabled = false;

                labelEntityPerson.Enabled = true;
                cbxPeople.Enabled = true;
                btnAddEntityPerson.Enabled = true;

            }

        }

        private void hideAllParticipantSelectionOptions()
        {

            labelPpdaUser.Enabled = false;
            cbxUsers.Enabled = false;

            labelEntityPerson.Enabled = false;
            cbxPeople.Enabled = false;
            btnAddEntityPerson.Enabled = false;

        }
    }
}

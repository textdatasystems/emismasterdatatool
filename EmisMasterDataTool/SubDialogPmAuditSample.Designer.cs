﻿namespace EmisMasterDataTool
{
    partial class SubDialogPmAuditSample
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubDialogPmAuditSample));
            this.panel1 = new System.Windows.Forms.Panel();
            this.gridViewSampleFiles = new System.Windows.Forms.DataGridView();
            this.ProcRefNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubjectOfProcurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcMethod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Provider = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateOfAward = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContractValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Assignee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.labelActivityName = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSampleFiles)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gridViewSampleFiles);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.labelActivityName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(1036, 490);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // gridViewSampleFiles
            // 
            this.gridViewSampleFiles.AllowUserToAddRows = false;
            this.gridViewSampleFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridViewSampleFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewSampleFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProcRefNo,
            this.SubjectOfProcurement,
            this.ProcMethod,
            this.Provider,
            this.DateOfAward,
            this.ContractValue,
            this.Assignee});
            this.gridViewSampleFiles.Location = new System.Drawing.Point(17, 84);
            this.gridViewSampleFiles.Name = "gridViewSampleFiles";
            this.gridViewSampleFiles.ReadOnly = true;
            this.gridViewSampleFiles.Size = new System.Drawing.Size(1006, 393);
            this.gridViewSampleFiles.TabIndex = 2;
            // 
            // ProcRefNo
            // 
            this.ProcRefNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProcRefNo.DataPropertyName = "procurement_reference";
            this.ProcRefNo.FillWeight = 2F;
            this.ProcRefNo.HeaderText = "Proc Ref No.";
            this.ProcRefNo.Name = "ProcRefNo";
            this.ProcRefNo.ReadOnly = true;
            // 
            // SubjectOfProcurement
            // 
            this.SubjectOfProcurement.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SubjectOfProcurement.DataPropertyName = "procurement_subject";
            this.SubjectOfProcurement.FillWeight = 5F;
            this.SubjectOfProcurement.HeaderText = "Subject of Procurement";
            this.SubjectOfProcurement.Name = "SubjectOfProcurement";
            this.SubjectOfProcurement.ReadOnly = true;
            // 
            // ProcMethod
            // 
            this.ProcMethod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProcMethod.DataPropertyName = "procurement_method";
            this.ProcMethod.FillWeight = 3F;
            this.ProcMethod.HeaderText = "Proc Method";
            this.ProcMethod.Name = "ProcMethod";
            this.ProcMethod.ReadOnly = true;
            // 
            // Provider
            // 
            this.Provider.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Provider.DataPropertyName = "provider";
            this.Provider.FillWeight = 4F;
            this.Provider.HeaderText = "Provider";
            this.Provider.Name = "Provider";
            this.Provider.ReadOnly = true;
            // 
            // DateOfAward
            // 
            this.DateOfAward.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DateOfAward.DataPropertyName = "date_of_award";
            this.DateOfAward.FillWeight = 2F;
            this.DateOfAward.HeaderText = "Date of Award";
            this.DateOfAward.Name = "DateOfAward";
            this.DateOfAward.ReadOnly = true;
            // 
            // ContractValue
            // 
            this.ContractValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ContractValue.DataPropertyName = "contract_value";
            this.ContractValue.FillWeight = 2F;
            this.ContractValue.HeaderText = "Contract Value";
            this.ContractValue.Name = "ContractValue";
            this.ContractValue.ReadOnly = true;
            // 
            // Assignee
            // 
            this.Assignee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Assignee.DataPropertyName = "assigneeName";
            this.Assignee.FillWeight = 3F;
            this.Assignee.HeaderText = "Assignee";
            this.Assignee.Name = "Assignee";
            this.Assignee.ReadOnly = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(17, 54);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Add Sample File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelActivityName
            // 
            this.labelActivityName.AutoSize = true;
            this.labelActivityName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivityName.Location = new System.Drawing.Point(14, 22);
            this.labelActivityName.Margin = new System.Windows.Forms.Padding(3, 0, 3, 20);
            this.labelActivityName.Name = "labelActivityName";
            this.labelActivityName.Size = new System.Drawing.Size(384, 13);
            this.labelActivityName.TabIndex = 0;
            this.labelActivityName.Text = "Uganda Police Force - Performance Audits - FY 2019-2020 - Audit Sample";
            // 
            // SubDialogPmAuditSample
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 490);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SubDialogPmAuditSample";
            this.Text = "Audit Samples";
            this.Load += new System.EventHandler(this.SubDialogAuditSample_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSampleFiles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelActivityName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView gridViewSampleFiles;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcRefNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubjectOfProcurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcMethod;
        private System.Windows.Forms.DataGridViewTextBoxColumn Provider;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateOfAward;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContractValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Assignee;
    }
}
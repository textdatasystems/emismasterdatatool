﻿namespace EmisMasterDataTool
{
    partial class FormPpdaEmisOfflineApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPpdaEmisOfflineApp));
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gridViewEntities = new System.Windows.Forms.DataGridView();
            this.emisId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EntityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhysicalAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sector = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Branches = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.People = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Actions = new System.Windows.Forms.DataGridViewImageColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gridViewPmAcitivities = new System.Windows.Forms.DataGridView();
            this.ActivityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PmEntityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pmAuditType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PmTeam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FundingSource = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResponsibleOffice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PmAction = new System.Windows.Forms.DataGridViewImageColumn();
            this.cbxPmFinancialYears = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gridViewCbActivities = new System.Windows.Forms.DataGridView();
            this.CbActivityID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CbActivityName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CbDistrict = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CbCoordinator = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CbStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CbEndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CbAction = new System.Windows.Forms.DataGridViewImageColumn();
            this.cbxCbFinancialYears = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.entityContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tabPageFollowups = new System.Windows.Forms.TabPage();
            this.menuStrip2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEntities)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPmAcitivities)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCbActivities)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.masterDataToolStripMenuItem,
            this.loginUserToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(815, 24);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // masterDataToolStripMenuItem
            // 
            this.masterDataToolStripMenuItem.Name = "masterDataToolStripMenuItem";
            this.masterDataToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.masterDataToolStripMenuItem.Text = "Master Data";
            this.masterDataToolStripMenuItem.Click += new System.EventHandler(this.masterDataToolStripMenuItem_Click);
            // 
            // loginUserToolStripMenuItem
            // 
            this.loginUserToolStripMenuItem.Name = "loginUserToolStripMenuItem";
            this.loginUserToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.loginUserToolStripMenuItem.Text = "Login User";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPageFollowups);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(10);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(815, 388);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gridViewEntities);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(807, 362);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Entities";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gridViewEntities
            // 
            this.gridViewEntities.AllowUserToAddRows = false;
            this.gridViewEntities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridViewEntities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewEntities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.emisId,
            this.EntityName,
            this.ProcCode,
            this.PhysicalAddress,
            this.ContactNumber,
            this.Sector,
            this.Branches,
            this.People,
            this.Actions});
            this.gridViewEntities.Location = new System.Drawing.Point(10, 10);
            this.gridViewEntities.Margin = new System.Windows.Forms.Padding(10);
            this.gridViewEntities.Name = "gridViewEntities";
            this.gridViewEntities.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridViewEntities.Size = new System.Drawing.Size(787, 342);
            this.gridViewEntities.TabIndex = 0;
            this.gridViewEntities.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.gridViewEntities_CellContextMenuStripNeeded);
            this.gridViewEntities.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.gridViewEntities_DataBindingComplete);
            // 
            // emisId
            // 
            this.emisId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.emisId.DataPropertyName = "emisId";
            this.emisId.FillWeight = 2F;
            this.emisId.HeaderText = "EmisId";
            this.emisId.Name = "emisId";
            this.emisId.Visible = false;
            // 
            // EntityName
            // 
            this.EntityName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EntityName.DataPropertyName = "entityName";
            this.EntityName.FillWeight = 5F;
            this.EntityName.HeaderText = "Entity Name";
            this.EntityName.Name = "EntityName";
            // 
            // ProcCode
            // 
            this.ProcCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProcCode.DataPropertyName = "procCode";
            this.ProcCode.FillWeight = 3F;
            this.ProcCode.HeaderText = "Proc Code";
            this.ProcCode.Name = "ProcCode";
            // 
            // PhysicalAddress
            // 
            this.PhysicalAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PhysicalAddress.DataPropertyName = "physicalAddress";
            this.PhysicalAddress.FillWeight = 4F;
            this.PhysicalAddress.HeaderText = "Physical Address";
            this.PhysicalAddress.Name = "PhysicalAddress";
            // 
            // ContactNumber
            // 
            this.ContactNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ContactNumber.DataPropertyName = "contactNumber";
            this.ContactNumber.FillWeight = 3F;
            this.ContactNumber.HeaderText = "Contact Number";
            this.ContactNumber.Name = "ContactNumber";
            // 
            // Sector
            // 
            this.Sector.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Sector.FillWeight = 3F;
            this.Sector.HeaderText = "Sector";
            this.Sector.Name = "Sector";
            // 
            // Branches
            // 
            this.Branches.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Branches.FillWeight = 2F;
            this.Branches.HeaderText = "No of Branches";
            this.Branches.Name = "Branches";
            // 
            // People
            // 
            this.People.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.People.FillWeight = 2F;
            this.People.HeaderText = "No of People";
            this.People.Name = "People";
            // 
            // Actions
            // 
            this.Actions.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Actions.FillWeight = 2F;
            this.Actions.HeaderText = "Actions";
            this.Actions.Image = global::EmisMasterDataTool.Properties.Resources.edit_list_rtl;
            this.Actions.Name = "Actions";
            this.Actions.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(807, 362);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Performance Monitoring";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gridViewPmAcitivities);
            this.panel1.Controls.Add(this.cbxPmFinancialYears);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(801, 356);
            this.panel1.TabIndex = 0;
            // 
            // gridViewPmAcitivities
            // 
            this.gridViewPmAcitivities.AllowUserToAddRows = false;
            this.gridViewPmAcitivities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridViewPmAcitivities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewPmAcitivities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ActivityID,
            this.PmEntityName,
            this.pmAuditType,
            this.PmTeam,
            this.FundingSource,
            this.ResponsibleOffice,
            this.StartDate,
            this.EndDate,
            this.PmAction});
            this.gridViewPmAcitivities.Location = new System.Drawing.Point(9, 36);
            this.gridViewPmAcitivities.Name = "gridViewPmAcitivities";
            this.gridViewPmAcitivities.ReadOnly = true;
            this.gridViewPmAcitivities.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridViewPmAcitivities.Size = new System.Drawing.Size(779, 307);
            this.gridViewPmAcitivities.TabIndex = 2;
            this.gridViewPmAcitivities.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.gridViewPmAcitivities_CellContextMenuStripNeeded);
            this.gridViewPmAcitivities.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.gridViewPmAcitivities_DataBindingComplete);
            // 
            // ActivityID
            // 
            this.ActivityID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ActivityID.DataPropertyName = "emisId";
            this.ActivityID.FillWeight = 3F;
            this.ActivityID.HeaderText = "Activity ID";
            this.ActivityID.Name = "ActivityID";
            this.ActivityID.ReadOnly = true;
            // 
            // PmEntityName
            // 
            this.PmEntityName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PmEntityName.DataPropertyName = "entityName";
            this.PmEntityName.FillWeight = 4F;
            this.PmEntityName.HeaderText = "Entity Name";
            this.PmEntityName.Name = "PmEntityName";
            this.PmEntityName.ReadOnly = true;
            // 
            // pmAuditType
            // 
            this.pmAuditType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.pmAuditType.DataPropertyName = "AuditType";
            this.pmAuditType.FillWeight = 3F;
            this.pmAuditType.HeaderText = "Audit Type";
            this.pmAuditType.Name = "pmAuditType";
            this.pmAuditType.ReadOnly = true;
            // 
            // PmTeam
            // 
            this.PmTeam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PmTeam.FillWeight = 3F;
            this.PmTeam.HeaderText = "Team";
            this.PmTeam.Name = "PmTeam";
            this.PmTeam.ReadOnly = true;
            // 
            // FundingSource
            // 
            this.FundingSource.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FundingSource.DataPropertyName = "fundingSourceName";
            this.FundingSource.FillWeight = 3F;
            this.FundingSource.HeaderText = "Funding Source";
            this.FundingSource.Name = "FundingSource";
            this.FundingSource.ReadOnly = true;
            // 
            // ResponsibleOffice
            // 
            this.ResponsibleOffice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ResponsibleOffice.DataPropertyName = "responsibleOffice";
            this.ResponsibleOffice.FillWeight = 3F;
            this.ResponsibleOffice.HeaderText = "PPDA Office";
            this.ResponsibleOffice.Name = "ResponsibleOffice";
            this.ResponsibleOffice.ReadOnly = true;
            // 
            // StartDate
            // 
            this.StartDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StartDate.DataPropertyName = "startDate";
            this.StartDate.FillWeight = 3F;
            this.StartDate.HeaderText = "Start Date";
            this.StartDate.Name = "StartDate";
            this.StartDate.ReadOnly = true;
            // 
            // EndDate
            // 
            this.EndDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EndDate.DataPropertyName = "endDate";
            this.EndDate.FillWeight = 3F;
            this.EndDate.HeaderText = "End Date";
            this.EndDate.Name = "EndDate";
            this.EndDate.ReadOnly = true;
            // 
            // PmAction
            // 
            this.PmAction.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PmAction.FillWeight = 2F;
            this.PmAction.HeaderText = "Action";
            this.PmAction.Image = global::EmisMasterDataTool.Properties.Resources.edit_list_rtl;
            this.PmAction.Name = "PmAction";
            this.PmAction.ReadOnly = true;
            // 
            // cbxPmFinancialYears
            // 
            this.cbxPmFinancialYears.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPmFinancialYears.FormattingEnabled = true;
            this.cbxPmFinancialYears.Location = new System.Drawing.Point(99, 9);
            this.cbxPmFinancialYears.Name = "cbxPmFinancialYears";
            this.cbxPmFinancialYears.Size = new System.Drawing.Size(165, 21);
            this.cbxPmFinancialYears.TabIndex = 1;
            this.cbxPmFinancialYears.SelectedIndexChanged += new System.EventHandler(this.cbxPmFinancialYears_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 10, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Financial Year:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(807, 362);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Capacity Building";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gridViewCbActivities);
            this.panel2.Controls.Add(this.cbxCbFinancialYears);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(801, 356);
            this.panel2.TabIndex = 0;
            // 
            // gridViewCbActivities
            // 
            this.gridViewCbActivities.AllowUserToAddRows = false;
            this.gridViewCbActivities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridViewCbActivities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewCbActivities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CbActivityID,
            this.CbActivityName,
            this.CbDistrict,
            this.CbCoordinator,
            this.CbStartDate,
            this.CbEndDate,
            this.CbAction});
            this.gridViewCbActivities.Location = new System.Drawing.Point(17, 43);
            this.gridViewCbActivities.Name = "gridViewCbActivities";
            this.gridViewCbActivities.ReadOnly = true;
            this.gridViewCbActivities.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridViewCbActivities.Size = new System.Drawing.Size(771, 300);
            this.gridViewCbActivities.TabIndex = 2;
            this.gridViewCbActivities.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.gridViewCbActivities_CellContextMenuStripNeeded);
            this.gridViewCbActivities.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.gridViewCbActivities_DataBindingComplete);
            this.gridViewCbActivities.RowContextMenuStripNeeded += new System.Windows.Forms.DataGridViewRowContextMenuStripNeededEventHandler(this.gridViewCbActivities_RowContextMenuStripNeeded);
            // 
            // CbActivityID
            // 
            this.CbActivityID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CbActivityID.DataPropertyName = "activityId";
            this.CbActivityID.FillWeight = 3F;
            this.CbActivityID.HeaderText = "Activity ID";
            this.CbActivityID.Name = "CbActivityID";
            this.CbActivityID.ReadOnly = true;
            // 
            // CbActivityName
            // 
            this.CbActivityName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CbActivityName.DataPropertyName = "activityName";
            this.CbActivityName.FillWeight = 5F;
            this.CbActivityName.HeaderText = "Activity Name";
            this.CbActivityName.Name = "CbActivityName";
            this.CbActivityName.ReadOnly = true;
            // 
            // CbDistrict
            // 
            this.CbDistrict.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CbDistrict.DataPropertyName = "districtName";
            this.CbDistrict.FillWeight = 3F;
            this.CbDistrict.HeaderText = "District";
            this.CbDistrict.Name = "CbDistrict";
            this.CbDistrict.ReadOnly = true;
            // 
            // CbCoordinator
            // 
            this.CbCoordinator.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CbCoordinator.DataPropertyName = "cordinatorName";
            this.CbCoordinator.FillWeight = 3F;
            this.CbCoordinator.HeaderText = "Coordinator";
            this.CbCoordinator.Name = "CbCoordinator";
            this.CbCoordinator.ReadOnly = true;
            // 
            // CbStartDate
            // 
            this.CbStartDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CbStartDate.DataPropertyName = "startDate";
            this.CbStartDate.FillWeight = 3F;
            this.CbStartDate.HeaderText = "Start Date";
            this.CbStartDate.Name = "CbStartDate";
            this.CbStartDate.ReadOnly = true;
            // 
            // CbEndDate
            // 
            this.CbEndDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CbEndDate.DataPropertyName = "endDate";
            this.CbEndDate.FillWeight = 3F;
            this.CbEndDate.HeaderText = "End Date";
            this.CbEndDate.Name = "CbEndDate";
            this.CbEndDate.ReadOnly = true;
            // 
            // CbAction
            // 
            this.CbAction.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CbAction.FillWeight = 2F;
            this.CbAction.HeaderText = "Actions";
            this.CbAction.Image = global::EmisMasterDataTool.Properties.Resources.edit_list_rtl;
            this.CbAction.Name = "CbAction";
            this.CbAction.ReadOnly = true;
            this.CbAction.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CbAction.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // cbxCbFinancialYears
            // 
            this.cbxCbFinancialYears.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCbFinancialYears.FormattingEnabled = true;
            this.cbxCbFinancialYears.Location = new System.Drawing.Point(97, 11);
            this.cbxCbFinancialYears.Name = "cbxCbFinancialYears";
            this.cbxCbFinancialYears.Size = new System.Drawing.Size(163, 21);
            this.cbxCbFinancialYears.TabIndex = 1;
            this.cbxCbFinancialYears.SelectedIndexChanged += new System.EventHandler(this.cbxCbFinancialYears_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Financial Year:";
            // 
            // entityContextMenuStrip
            // 
            this.entityContextMenuStrip.Name = "entityContextMenuStrip";
            this.entityContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            // 
            // tabPageFollowups
            // 
            this.tabPageFollowups.Location = new System.Drawing.Point(4, 22);
            this.tabPageFollowups.Name = "tabPageFollowups";
            this.tabPageFollowups.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFollowups.Size = new System.Drawing.Size(807, 362);
            this.tabPageFollowups.TabIndex = 3;
            this.tabPageFollowups.Text = "Follow Ups";
            this.tabPageFollowups.UseVisualStyleBackColor = true;
            // 
            // FormPpdaEmisOfflineApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 412);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip2);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPpdaEmisOfflineApp";
            this.Text = "PPDA Entity Management System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEntities)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPmAcitivities)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCbActivities)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem masterDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginUserToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ContextMenuStrip entityContextMenuStrip;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxPmFinancialYears;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView gridViewCbActivities;
        private System.Windows.Forms.ComboBox cbxCbFinancialYears;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView gridViewEntities;
        private System.Windows.Forms.DataGridView gridViewPmAcitivities;
        private System.Windows.Forms.DataGridViewTextBoxColumn emisId;
        private System.Windows.Forms.DataGridViewTextBoxColumn EntityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhysicalAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sector;
        private System.Windows.Forms.DataGridViewTextBoxColumn Branches;
        private System.Windows.Forms.DataGridViewTextBoxColumn People;
        private System.Windows.Forms.DataGridViewImageColumn Actions;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActivityID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PmEntityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn pmAuditType;
        private System.Windows.Forms.DataGridViewTextBoxColumn PmTeam;
        private System.Windows.Forms.DataGridViewTextBoxColumn FundingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResponsibleOffice;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndDate;
        private System.Windows.Forms.DataGridViewImageColumn PmAction;
        private System.Windows.Forms.DataGridViewTextBoxColumn CbActivityID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CbActivityName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CbDistrict;
        private System.Windows.Forms.DataGridViewTextBoxColumn CbCoordinator;
        private System.Windows.Forms.DataGridViewTextBoxColumn CbStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CbEndDate;
        private System.Windows.Forms.DataGridViewImageColumn CbAction;
        private System.Windows.Forms.TabPage tabPageFollowups;
    }
}


﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using Processor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class InnerDialogCustomAddPerson : Form
    {
        public IInnerDialogCustomAddPerson iInnerDialogCustomAddPerson { get; set; }
        private int entityId { get; set; }

        private int activityEmisId { get; set; }
        public InnerDialogCustomAddPerson(int activityEmisId)
        {
            InitializeComponent();
            this.activityEmisId = activityEmisId;

            loadData();

        }

        private void loadData()
        {

            var activity = new DatabaseHandler().findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityEmisId);
            if(activity == null)
            {
                WidgetHandler.showMessage("Failed to find activity with Id [" + activityEmisId + "]", true);
                return;
            }

            entityId = activity.entityId;

            //populate other fields
            var roles = DataGenerator.mockProcurementRoles();
            WidgetHandler.populateProcurementRolesDropdown(cbxProcurementRole, roles);

        }

        private void btnSavePerson_clicked(object sender, EventArgs e)
        {
            //saved the person

            if (!validInput())
            {
                return;
            }

            //input is valid
            String firstName = txvFirstName.Text;
            String lastName = txvLastName.Text;
            String email = txvEmail.Text;
            String note = txvNote.Text;
            String phoneNo = txvPhoneNo.Text;
            String title = txvTitle.Text;
            String gender = rbtnGenderMale.Checked ? "male" : "female";
            String roleInProc = cbxProcurementRole.Text;

            People person = new People();
            person.firstName = firstName;
            person.lastName = lastName;
            person.email = email;
            person.entityEmisId = entityId;
            person.gender = gender;
            person.note = note;
            person.phoneNo = phoneNo;
            person.title = title;
            person.roleInProcurement = roleInProc;

            DatabaseHandler.addPerson(DatabaseHandler.dbConnection(), person);

            //clear the fields
            clearFields();

            //show success message
            WidgetHandler.showMessage("Person successfully added", false);

           
            iInnerDialogCustomAddPerson.personSuccessfullyCreated(person);

        }

        private void clearFields()
        {
            txvFirstName.Text = "";
            txvLastName.Text = "";
            txvEmail.Text = "";
            txvNote.Text = "";
            txvPhoneNo.Text = "";
            txvTitle.Text = "";
            cbxProcurementRole.SelectedIndex = 0;
            rbtnGenderMale.Checked = true;
        }

        private bool validInput()
        {
            String firstName = txvFirstName.Text;
            String lastName = txvLastName.Text;
            String email = txvEmail.Text;
            String note = txvNote.Text;
            String phoneNo = txvPhoneNo.Text;
            String title = txvTitle.Text;
            String gender = rbtnGenderMale.Checked ? "male" : "female";
            int selectProcRoleIndex = cbxProcurementRole.SelectedIndex;

            if (String.IsNullOrEmpty(firstName))
            {
                String msg = "Please enter a valid first name";
                WidgetHandler.showMessage(msg, true);
                return false;
            }

            if (String.IsNullOrEmpty(lastName))
            {
                String msg = "Please enter a valid last name";
                WidgetHandler.showMessage(msg, true);
                return false;
            }

            if (!String.IsNullOrEmpty(email) && !SharedCommons.IsValidEmail(email))
            {
                String msg = "Please enter a valid email address";
                WidgetHandler.showMessage(msg, true);
                return false;
            }

            if (selectProcRoleIndex == 0)
            {
                String msg = "Please select the role of the person in the procurement process";
                WidgetHandler.showMessage(msg, true);
                return false;
            }

            return true;
        }

        private void InnerDialogCustomAddPerson_Load(object sender, EventArgs e)
        {

        }
    }
}

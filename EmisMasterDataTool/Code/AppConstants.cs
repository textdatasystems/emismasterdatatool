﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmisMasterDataTool.Code
{
    public class AppConstants
    {

        public const String MENU_STRIP_ITEM_AUDIT_TOOL_NEW = "Start New Tool";
        public const String MENU_STRIP_ITEM_AUDIT_TOOL_EDIT = "Edit Tool";
        public const String MENU_STRIP_ITEM_AUDIT_TOOL_JSON = "Download JSON";
        internal static string AUDIT_TYPES_PERFORMANCE_AUDIT = "Performance Audits";
        internal static string AUDIT_TYPES_COMPLIANCE = "Compliance Inspection";
        internal static string PREFIX_AST = "AST";
        internal static string PREFIX_ADT = "ADT";
        internal static string PREFIX_APT = "APT";
        internal static string PREFIX_CST = "CST";
        internal static string PREFIX_CPT = "CPT";

        internal static string TOOL_PROGRESS_STATUS_NOT_STARTED = "Not Yet Started";
        internal static string PLACE_HOLDER_DATE = "yyyy-MM-dd HH:mm:ss";
    }

}

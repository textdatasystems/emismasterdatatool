﻿using EmisTool.Code.ui.custom;
using Processor.Entities.PM.compliance.process;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace EmisTool.Code
{
    public class UiHelperCpt
    {
        public static int FieldCountPmAuditProcessTool = 0;

        public static TableLayoutPanel GenerateTableV1(ComplianceProcessToolSection toolSection, out SectionEvaluationCpt sectionEval, AppStateCompliance appState)
        {

            String sectionTableFieldName = "ComplianceProcessToolSec" + toolSection.Id;

            //initialize the out variable
            sectionEval = new SectionEvaluationCpt();
            sectionEval.sectionEmisId = toolSection.Id;
            sectionEval.name = toolSection.name;
            sectionEval.sectionTableFieldName = sectionTableFieldName;
            var sectionItemsEvalList = sectionEval.sectionItemEvaluations;


            //get the list of items in this section
            List<ComplianceProcessToolSectionItem> sectionItems = toolSection.items;


            int COLUMN_COUNT = 12;
            int ROW_COUNT = sectionItems.Count + 2; // add the header row and the footer row

            TableLayoutPanel tableLayoutPanel = new TableLayoutPanel();

            //Now we will generate the table, setting up the row and column counts first
            tableLayoutPanel.ColumnCount = COLUMN_COUNT;
            tableLayoutPanel.RowCount = ROW_COUNT;


            for (int columnIndex = 0; columnIndex < COLUMN_COUNT; columnIndex++)
            {

                //First add a column

                float columnWidth = getColumnWidth(columnIndex);
                tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, columnWidth));

                for (int rowIndex = 0; rowIndex < ROW_COUNT; rowIndex++)
                {

                    bool isFooterRow = rowIndex == ROW_COUNT - 1;
                    bool isHeaderRow = rowIndex == 0;

                    //Next, add a row.  Only do this when once, when creating the first column
                    if (columnIndex == 0 && isHeaderRow || columnIndex == 0 && isFooterRow)
                    {
                        tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));
                    }
                    else if (columnIndex == 0)
                    {
                        tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));
                    }


                    //generate the cell control
                    Control control;
                    if (isHeaderRow)
                    {
                        control = buildHeaderCellControlV1(columnIndex, toolSection);
                    }
                    else if (isFooterRow)
                    {

                        control = buildFoooterCellControl(columnIndex);

                        //we need to keep track of the controls that will hold the average scores for the different cases
                        if (columnIndex >= 2 || columnIndex <= 11)
                        {
                            updateCaseAverageScoreContainer(toolSection.Id, columnIndex, control);

                            //handles autofilling of the scores in the average scores tab
                            control.TextChanged += appState.iCptAnalyisUpdater.autoFillAverageScoresTab;
                        }

                    }
                    else
                    {

                        ComplianceProcessToolSectionItem currentItem = sectionItems[rowIndex - 1];
                        control = buildBodyCellControlV1(columnIndex, currentItem);


                        //begin logic for updating app state  

                        //ensure that the item exists in the dictionary if not add
                        if (!sectionItemsEvalList.ContainsKey(currentItem.emisId))
                        {

                            var sectionEvalItem = new SectionItemEvaluationCpt();
                            sectionEvalItem.sectionEmisId = toolSection.Id;
                            sectionEvalItem.sectionItemEmisId = currentItem.emisId;
                            sectionEvalItem.yesRank = currentItem.yesRank;
                            sectionEvalItem.noRank = currentItem.noRank;
                            sectionEvalItem.naRank = currentItem.naRank;

                            sectionItemsEvalList.Add(currentItem.emisId, sectionEvalItem);

                        }


                        var oldSectionItemEval = sectionItemsEvalList[currentItem.emisId];
                        oldSectionItemEval.evaluationFieldRole = currentItem.fieldRole;
                        oldSectionItemEval.evaluationFieldRole = currentItem.description;

                        //we now update the fields of the sectionEvaItem
                        var sectionEvalItemUpdated = updateSectionEvalItemCpt(oldSectionItemEval, columnIndex, currentItem, control);

                        //update the item in the list
                        sectionItemsEvalList[sectionEvalItemUpdated.sectionItemEmisId] = sectionEvalItemUpdated;

                        //end logic for updating app state

                    }


                    if (control is ComboBox)
                    {
                        ((ComboBox)control).SelectedIndexChanged += new EventHandler(appState.updatePmComplianceProcessToolState);
                    }
                    else
                    {
                        control.Leave += new EventHandler(appState.updatePmComplianceProcessToolState);
                    }


                    tableLayoutPanel.Controls.Add(control, columnIndex, rowIndex);

                }

            }


            tableLayoutPanel.Dock = DockStyle.Fill;
            tableLayoutPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanel.AutoScroll = false;
            tableLayoutPanel.AutoSize = true;
            tableLayoutPanel.Name = sectionTableFieldName;

            return tableLayoutPanel;

        }

        internal static Panel GenerateAverageScoresTable(IEventHandlersCpt eventHandlersCpt)
        {

            var panelAverageScores = new Panel();

            // 
            // tableLayoutPanelAverageScores
            // 
            Label label19 = new Label();
            Label label18 = new Label();
            Label label17 = new Label();
            Label label16 = new Label();
            Label label15 = new Label();
            Label label14 = new Label();
            Label label13 = new Label();
            Label label12 = new Label();
            Label label11 = new Label();
            Label label10 = new Label();
            Label label9 = new Label();
            Label label8 = new Label();
            Label label7 = new Label();
            Label label20 = new Label();
            Label label21 = new Label();
            Label label22 = new Label();
            Label label23 = new Label();
            Label label24 = new Label();
            Label label25 = new Label();
            Label label26 = new Label();
            Label label27 = new Label();
            Label label29 = new Label();
            Label label30 = new Label();
            Label label31 = new Label();
            Label label32 = new Label();
            Label label33 = new Label();
            Label label34 = new Label();
            Label label35 = new Label();
            Label label36 = new Label();
            Label label37 = new Label();
                        
            Label label39 = new Label();
            Label label38 = new Label();
            Label label28 = new Label();
            Label label40 = new Label();
            Label label41 = new Label();
            Label label42 = new Label();
            Label label43 = new Label();
            Label label44 = new Label();
            Label label45 = new Label();
            Label label46 = new Label();
            Label label47 = new Label();
            Label label48 = new Label();
            Label label50 = new Label();
            Label label51 = new Label();
            Label label52 = new Label();
            Label label53 = new Label();
            Label label54 = new Label();
            Label label55 = new Label();
            Label label56 = new Label();
            
            // txvRAvgScore
            TextBox txvRAvgScore = new TextBox() { Dock = DockStyle.Fill, Name = "txvRAvgScore" };
            txvRAvgScore.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAvgOfSectionAvgScores);
            
            // txvRCase10 
            TextBox txvRCase10 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase10" };
            txvRCase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase10.TextChanged += eventHandlersCpt.calculateAverageCase10;

            // txvRCase9
            TextBox txvRCase9 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase9" };
            txvRCase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase9.TextChanged += eventHandlersCpt.calculateAverageCase9;

            // txvRCase8
            TextBox txvRCase8 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase8" };
            txvRCase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase8.TextChanged += eventHandlersCpt.calculateAverageCase8;

            // txvRCase7
            TextBox txvRCase7 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase7" };
            txvRCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase7);

            // txvRCase6
            TextBox txvRCase6 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase6" };
            txvRCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase6);

            // txvRCase5
            TextBox txvRCase5 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase5" };
            txvRCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase5);

            // txvRCase4
            TextBox txvRCase4 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase4" };
            txvRCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase4);

            // txvRCase3 
            TextBox txvRCase3 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase3" };
            txvRCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase3);

            // txvRCase2 
            TextBox txvRCase2 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase2" };
            txvRCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase2);

            // txvRCase1
            TextBox txvRCase1 = new TextBox() { Dock = DockStyle.Fill, Name = "txvRCase1" };
            txvRCase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageRecords);
            txvRCase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase1);

            // txvCMAvgScore
            TextBox txvCMAvgScore = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMAvgScore" };
            txvCMAvgScore.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAvgOfSectionAvgScores);
            
            // txvCMCase10
            TextBox txvCMCase10 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase10" };
            txvCMCase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase10);

            // txvCMCase9
            TextBox txvCMCase9 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase9" };
            txvCMCase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase9);

            // txvCMCase8
            TextBox txvCMCase8 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase8" };
            txvCMCase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase8);

            // txvCMCase7
            TextBox txvCMCase7 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase7" };
            txvCMCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase7);

            // txvCMCase6
            TextBox txvCMCase6 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase6" };
            txvCMCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase6);

            // txvCMCase5
            TextBox txvCMCase5 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase5" };
            txvCMCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase5);

            // txvCMCase4
            TextBox txvCMCase4 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase4" };
            txvCMCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase4);

            // txvCMCase3 
            TextBox txvCMCase3 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase3" };
            txvCMCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase3);

            // txvCMCase2 
            TextBox txvCMCase2 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase2" };
            txvCMCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase2);

            // txvCMCase1
            TextBox txvCMCase1 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCMCase1" };
            txvCMCase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContractMangt);
            txvCMCase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase1);

            // txvCAvgScore 
            TextBox txvCAvgScore = new TextBox() { Dock = DockStyle.Fill, Name = "txvCAvgScore" };
            txvCAvgScore.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAvgOfSectionAvgScores);
             
            // txvCCase10
            TextBox txvCCase10 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase10" };
            txvCCase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase10);

            // txvCCase9
            TextBox txvCCase9 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase9" };
            txvCCase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase9);

            // txvCCase8
            TextBox txvCCase8 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase8" };
            txvCCase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase8);

            // txvCCase7txvCCase7
            TextBox txvCCase7 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase7" };
            txvCCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase7);
            // 
            // txvCCase6
            TextBox txvCCase6 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase6" };
            txvCCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase6);
            // 
            // txvCCase5
            TextBox txvCCase5 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase5" };
            txvCCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase5);
            // 
            // txvCCase4
            TextBox txvCCase4 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase4" };
            txvCCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase4);
            // 
            // txvCCase3
            TextBox txvCCase3 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase3" };
            txvCCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase3);
            // 
            // txvCCase2
            TextBox txvCCase2 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase2" };
            txvCCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase2);
            // 
            // txvCCase1
            TextBox txvCCase1 = new TextBox() { Dock = DockStyle.Fill, Name = "txvCCase1" };
            txvCCase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageContracting);
            txvCCase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase1);
            // 
            // txvEAvgScore
            TextBox txvEAvgScore = new TextBox() { Dock = DockStyle.Fill, Name = "txvEAvgScore" };
            txvEAvgScore.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAvgOfSectionAvgScores);
            // 
            // txvECase10
            TextBox txvECase10 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase10" };
            txvECase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase10);
            // 
            // txvECase9
            TextBox txvECase9 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase9" };
            txvECase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase9);
            // 
            // txvECase8
            TextBox txvECase8 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase8" };
            txvECase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase8);
            // 
            // txvECase7
            TextBox txvECase7 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase7" };
            txvECase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase7);
            // 
            // txvECase6
            TextBox txvECase6 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase6" };
            txvECase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase6);
            // 
            // txvECase5
            TextBox txvECase5 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase5" };
            txvECase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase5);
            // 
            // txvECase4
            TextBox txvECase4 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase4" };
            txvECase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase4);
            // 
            // txvECase3
            TextBox txvECase3 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase3" };
            txvECase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase3);
            // 
            // txvECase2
            TextBox txvECase2 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase2" };
            txvECase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase2);
            // 
            // txvECase1
            TextBox txvECase1 = new TextBox() { Dock = DockStyle.Fill, Name = "txvECase1" };
            txvECase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageEvaluation);
            txvECase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase1);
            // 
            // txvBAvgScore
            TextBox txvBAvgScore = new TextBox() { Dock = DockStyle.Fill, Name = "txvBAvgScore" };
            txvBAvgScore.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAvgOfSectionAvgScores);
            // 
            // txvBCase10
            TextBox txvBCase10 =  new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase10" };
            txvBCase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase10);
            // 
            // txvBCase9
            TextBox txvBCase9 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase9" };
            txvBCase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase9);
            // 
            // txvBCase8
            TextBox txvBCase8 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase8" };
            txvBCase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase8);
            // 
            // txvBCase7
            TextBox txvBCase7 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase7" };
            txvBCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase7);
            // 
            // txvBCase6
            TextBox txvBCase6 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase6" };
            txvBCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase6);
            // 
            // txvBCase5
            TextBox txvBCase5 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase5" };
            txvBCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase5);
            // 
            // txvBCase4
            TextBox txvBCase4 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase4" };
            txvBCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase4);
            // 
            // txvBCase3
            TextBox txvBCase3 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase3" };
            txvBCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase3);
            // 
            // txvBCase2
            TextBox txvBCase2 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase2" };
            txvBCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase2);
            // 
            // txvBCase1
            TextBox txvBCase1 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBCase1" };
            txvBCase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBidding);
            txvBCase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase1);
            // 
            // txvBDAvgScore
            TextBox txvBDAvgScore = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDAvgScore" }; 
            txvBDAvgScore.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAvgOfSectionAvgScores);
            // 
            // txvBDCase10
            TextBox txvBDCase10 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase10" };
            txvBDCase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase10);
            // 
            // txvBDCase9
            TextBox txvBDCase9 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase9" };
            txvBDCase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase9);
            // 
            // txvBDCase8
            TextBox txvBDCase8 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase8" };
            txvBDCase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase8);
            // 
            // txvBDCase7
            TextBox txvBDCase7 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase7" };
            txvBDCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase7);
            // 
            // txvBDCase6
            TextBox txvBDCase6 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase6" };
            txvBDCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase6);
            // 
            // txvBDCase5
            TextBox txvBDCase5 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase5" };
            txvBDCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase5);
            // 
            // txvBDCase4
            TextBox txvBDCase4 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase4" };
            txvBDCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase4);
            // 
            // txvBDCase3
            TextBox txvBDCase3 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase3" };
            txvBDCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase3);
            // 
            // txvBDCase2
            TextBox txvBDCase2 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase2" };
            txvBDCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase2);
            // 
            // txvBDCase1
            TextBox txvBDCase1 = new TextBox() { Dock = DockStyle.Fill, Name = "txvBDCase1" };
            txvBDCase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageBiddingDocument);
            txvBDCase1.TextChanged += eventHandlersCpt.calculateAverageCase1;
            // 
            // txvPIAvgScore
            TextBox txvPIAvgScore = new TextBox() { Dock = DockStyle.Fill, Name = "txvPIAvgScore" };
            txvPIAvgScore.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAvgOfSectionAvgScores);
            // 
            // txvPICase10
            TextBox txvPICase10 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase10" };
            txvPICase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase10.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase10);
            // 
            // txvPICase9
            TextBox txvPICase9 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase9" };
            txvPICase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase9.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase9);
            // 
            // txvPICase8
            TextBox txvPICase8 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase8" };
            txvPICase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase8.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase8);
            // 
            // txvPICase7 
            TextBox txvPICase7 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase7" };
            txvPICase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase7.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase7);
            // 
            // txvPICase6
            TextBox txvPICase6 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase6" };
            txvPICase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase6.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase6);
            // 
            // txvPICase5
            TextBox txvPICase5 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase5" };
            txvPICase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase5.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase5);
            // 
            // txvPICase4
            TextBox txvPICase4 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase4" };
            txvPICase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase4.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase4);
            // 
            // txvPICase3
            TextBox txvPICase3 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase3" };
            txvPICase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase3.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase3);
            // 
            // txvPICase2
            TextBox txvPICase2 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase2" };
            txvPICase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase2.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAverageCase2);

            // txvPICase1
            TextBox txvPICase1 = new TextBox() { Dock = DockStyle.Fill, Name = "txvPICase1" };
            txvPICase1.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAveragePlanningAndInitiation);
            txvPICase1.TextChanged += eventHandlersCpt.calculateAverageCase1;

            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Dock = DockStyle.Fill;
            label19.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label19.Location = new Point(933, 1);
            label19.Name = "label19";
            label19.Size = new Size(70, 25);
            label19.TabIndex = 12;
            label19.Text = "Avg. Score";
            label19.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Dock = DockStyle.Fill;
            label18.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label18.Location = new Point(863, 1);
            label18.Name = "label18";
            label18.Size = new Size(63, 25);
            label18.TabIndex = 11;
            label18.Text = "Case 10";
            label18.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Dock = DockStyle.Fill;
            label17.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label17.Location = new Point(793, 1);
            label17.Name = "label17";
            label17.Size = new Size(63, 25);
            label17.TabIndex = 10;
            label17.Text = "Case 9";
            label17.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Dock = DockStyle.Fill;
            label16.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label16.Location = new Point(723, 1);
            label16.Name = "label16";
            label16.Size = new Size(63, 25);
            label16.TabIndex = 9;
            label16.Text = "Case 8";
            label16.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Dock = DockStyle.Fill;
            label15.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label15.Location = new Point(653, 1);
            label15.Name = "label15";
            label15.Size = new Size(63, 25);
            label15.TabIndex = 8;
            label15.Text = "Case 7";
            label15.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Dock = DockStyle.Fill;
            label14.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label14.Location = new Point(583, 1);
            label14.Name = "label14";
            label14.Size = new Size(63, 25);
            label14.TabIndex = 7;
            label14.Text = "Case 6";
            label14.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Dock = DockStyle.Fill;
            label13.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label13.Location = new Point(513, 1);
            label13.Name = "label13";
            label13.Size = new Size(63, 25);
            label13.TabIndex = 6;
            label13.Text = "Case 5";
            label13.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Dock = DockStyle.Fill;
            label12.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label12.Location = new Point(443, 1);
            label12.Name = "label12";
            label12.Size = new Size(63, 25);
            label12.TabIndex = 5;
            label12.Text = "Case 4";
            label12.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Dock = DockStyle.Fill;
            label11.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label11.Location = new Point(373, 1);
            label11.Name = "label11";
            label11.Size = new Size(63, 25);
            label11.TabIndex = 4;
            label11.Text = "Case 3";
            label11.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Dock = DockStyle.Fill;
            label10.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label10.Location = new Point(303, 1);
            label10.Name = "label10";
            label10.Size = new Size(63, 25);
            label10.TabIndex = 3;
            label10.Text = "Case 2";
            label10.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Dock = DockStyle.Fill;
            label9.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label9.Location = new Point(233, 1);
            label9.Name = "label9";
            label9.Size = new Size(63, 25);
            label9.TabIndex = 2;
            label9.Text = "Case 1";
            label9.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Dock = DockStyle.Fill;
            label8.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label8.Location = new Point(54, 1);
            label8.Name = "label8";
            label8.Size = new Size(172, 25);
            label8.TabIndex = 1;
            label8.Text = "Pariculars";
            label8.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Dock = DockStyle.Fill;
            label7.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label7.Location = new Point(4, 1);
            label7.Name = "label7";
            label7.Size = new Size(43, 25);
            label7.TabIndex = 0;
            label7.Text = "Sn";
            label7.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Dock = DockStyle.Fill;
            label20.Location = new Point(4, 27);
            label20.Name = "label20";
            label20.Size = new Size(43, 25);
            label20.TabIndex = 13;
            label20.Text = "1";
            label20.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Dock = DockStyle.Fill;
            label21.Location = new Point(4, 53);
            label21.Name = "label21";
            label21.Size = new Size(43, 25);
            label21.TabIndex = 14;
            label21.Text = "2";
            label21.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Dock = DockStyle.Fill;
            label22.Location = new Point(4, 79);
            label22.Name = "label22";
            label22.Size = new Size(43, 25);
            label22.TabIndex = 15;
            label22.Text = "3";
            label22.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Dock = DockStyle.Fill;
            label23.Location = new Point(4, 105);
            label23.Name = "label23";
            label23.Size = new Size(43, 25);
            label23.TabIndex = 16;
            label23.Text = "4";
            label23.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.Dock = DockStyle.Fill;
            label24.Location = new Point(4, 157);
            label24.Name = "label24";
            label24.Size = new Size(43, 25);
            label24.TabIndex = 17;
            label24.Text = "6";
            label24.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Dock = DockStyle.Fill;
            label25.Location = new Point(4, 131);
            label25.Name = "label25";
            label25.Size = new Size(43, 25);
            label25.TabIndex = 18;
            label25.Text = "5";
            label25.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Dock = DockStyle.Fill;
            label26.Location = new Point(4, 183);
            label26.Name = "label26";
            label26.Size = new Size(43, 25);
            label26.TabIndex = 19;
            label26.Text = "7";
            label26.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            label27.AutoSize = true;
            label27.Dock = DockStyle.Fill;
            label27.Location = new Point(4, 209);
            label27.Name = "label27";
            label27.Size = new Size(43, 25);
            label27.TabIndex = 20;
            label27.Text = "8";
            label27.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            label29.AutoSize = true;
            label29.Dock = DockStyle.Fill;
            label29.Location = new Point(54, 27);
            label29.Name = "label29";
            label29.Size = new Size(172, 25);
            label29.TabIndex = 22;
            label29.Text = "Planning & Initiation";
            label29.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label30
            // 
            label30.AutoSize = true;
            label30.Dock = DockStyle.Fill;
            label30.Location = new Point(54, 53);
            label30.Name = "label30";
            label30.Size = new Size(172, 25);
            label30.TabIndex = 23;
            label30.Text = "Bidding Document";
            label30.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label31
            // 
            label31.AutoSize = true;
            label31.Dock = DockStyle.Fill;
            label31.Location = new Point(54, 79);
            label31.Name = "label31";
            label31.Size = new Size(172, 25);
            label31.TabIndex = 24;
            label31.Text = "Bidding";
            label31.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            label32.AutoSize = true;
            label32.Dock = DockStyle.Fill;
            label32.Location = new Point(54, 105);
            label32.Name = "label32";
            label32.Size = new Size(172, 25);
            label32.TabIndex = 25;
            label32.Text = "Evaluation";
            label32.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            label33.AutoSize = true;
            label33.Dock = DockStyle.Fill;
            label33.Location = new Point(54, 157);
            label33.Name = "label33";
            label33.Size = new Size(172, 25);
            label33.TabIndex = 26;
            label33.Text = "Contract Management";
            label33.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            label34.AutoSize = true;
            label34.Dock = DockStyle.Fill;
            label34.Location = new Point(54, 131);
            label34.Name = "label34";
            label34.Size = new Size(172, 25);
            label34.TabIndex = 27;
            label34.Text = "Contracting";
            label34.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            label35.AutoSize = true;
            label35.Dock = DockStyle.Fill;
            label35.Location = new Point(54, 183);
            label35.Name = "label35";
            label35.Size = new Size(172, 25);
            label35.TabIndex = 28;
            label35.Text = "Records";
            label35.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label36
            // 
            label36.AutoSize = true;
            label36.Dock = DockStyle.Fill;
            label36.Location = new Point(54, 209);
            label36.Name = "label36";
            label36.Size = new Size(172, 25);
            label36.TabIndex = 29;
            label36.Text = "Procurement structure";
            label36.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label37
            // 
            label37.AutoSize = true;
            label37.Dock = DockStyle.Fill;
            label37.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label37.Location = new Point(54, 235);
            label37.Name = "label37";
            label37.Size = new Size(172, 33);
            label37.TabIndex = 30;
            label37.Text = "Avg. Score per Case";
            label37.TextAlign = ContentAlignment.MiddleLeft;
                      

            // txvPSAvgScore
            TextBox txvPSAvgScore = new TextBox();
            txvPSAvgScore.Dock = DockStyle.Fill;
            txvPSAvgScore.Location = new Point(933, 212);
            txvPSAvgScore.Name = "txvPSAvgScore";
            txvPSAvgScore.Size = new Size(70, 22);
            txvPSAvgScore.TabIndex = 108;
            txvPSAvgScore.TextChanged += new System.EventHandler(eventHandlersCpt.calculateAvgOfSectionAvgScores);
            
            // txvAvgAvgScore 
            TextBox txvAvgAvgScore = new TextBox();
            txvAvgAvgScore.Dock = DockStyle.Top;
            txvAvgAvgScore.Location = new Point(933, 238);
            txvAvgAvgScore.Name = "txvAvgAvgScore";
            txvAvgAvgScore.Size = new Size(70, 22);
            txvAvgAvgScore.TabIndex = 109;
            
            // txvAvgCase1
            TextBox txvAvgCase1 = new TextBox();
            txvAvgCase1.Dock = DockStyle.Top;
            txvAvgCase1.Location = new Point(233, 238);
            txvAvgCase1.Name = "txvAvgCase1";
            txvAvgCase1.Size = new Size(63, 22);
            txvAvgCase1.TabIndex = 110;
            
            // txvAvgCase2
            TextBox txvAvgCase2 = new TextBox();
            txvAvgCase2.Dock = DockStyle.Top;
            txvAvgCase2.Location = new Point(303, 238);
            txvAvgCase2.Name = "txvAvgCase2";
            txvAvgCase2.Size = new Size(63, 22);
            txvAvgCase2.TabIndex = 111;
            
            // txvAvgCase3            
            TextBox txvAvgCase3 = new TextBox();
            txvAvgCase3.Dock = DockStyle.Top;
            txvAvgCase3.Location = new Point(373, 238);
            txvAvgCase3.Name = "txvAvgCase3";
            txvAvgCase3.Size = new Size(63, 22);
            txvAvgCase3.TabIndex = 112;
            
            // txvAvgCase4            
            TextBox txvAvgCase4 = new TextBox();
            txvAvgCase4.Dock = DockStyle.Top;
            txvAvgCase4.Location = new Point(443, 238);
            txvAvgCase4.Name = "txvAvgCase4";
            txvAvgCase4.Size = new Size(63, 22);
            txvAvgCase4.TabIndex = 113;
            
            // txvAvgCase5 
            TextBox txvAvgCase5 = new TextBox();
            txvAvgCase5.Dock = DockStyle.Top;
            txvAvgCase5.Location = new Point(513, 238);
            txvAvgCase5.Name = "txvAvgCase5";
            txvAvgCase5.Size = new Size(63, 22);
            txvAvgCase5.TabIndex = 114;
            
            // txvAvgCase6 
            TextBox txvAvgCase6 = new TextBox();
            txvAvgCase6.Dock = DockStyle.Top;
            txvAvgCase6.Location = new Point(583, 238);
            txvAvgCase6.Name = "txvAvgCase6";
            txvAvgCase6.Size = new Size(63, 22);
            txvAvgCase6.TabIndex = 115;
            
            // txvAvgCase7
            TextBox txvAvgCase7 = new TextBox();
            txvAvgCase7.Dock = DockStyle.Top;
            txvAvgCase7.Location = new Point(653, 238);
            txvAvgCase7.Name = "txvAvgCase7";
            txvAvgCase7.Size = new Size(63, 22);
            txvAvgCase7.TabIndex = 116;
            
            // txvAvgCase8
            TextBox txvAvgCase8 = new TextBox();
            txvAvgCase8.Dock = DockStyle.Top;
            txvAvgCase8.Location = new Point(723, 238);
            txvAvgCase8.Name = "txvAvgCase8";
            txvAvgCase8.Size = new Size(63, 22);
            txvAvgCase8.TabIndex = 117;
            
            // txvAvgCase9            
            TextBox txvAvgCase9 = new TextBox();
            txvAvgCase9.Dock = DockStyle.Top;
            txvAvgCase9.Location = new Point(793, 238);
            txvAvgCase9.Name = "txvAvgCase9";
            txvAvgCase9.Size = new Size(63, 22);
            txvAvgCase9.TabIndex = 118;
            
            // txvAvgCase10
            TextBox txvAvgCase10 = new TextBox();
            txvAvgCase10.Dock = DockStyle.Top;
            txvAvgCase10.Location = new Point(863, 238);
            txvAvgCase10.Name = "txvAvgCase10";
            txvAvgCase10.Size = new Size(63, 22);
            txvAvgCase10.TabIndex = 119;
                                    
            // txvMirrorPIAvgScore
            TextBox txvMirrorPIAvgScore = new TextBox();
            txvMirrorPIAvgScore.Dock = DockStyle.Fill;
            txvMirrorPIAvgScore.Location = new Point(306, 212);
            txvMirrorPIAvgScore.Name = "txvMirrorPIAvgScore";
            txvMirrorPIAvgScore.Size = new Size(70, 22);
            txvMirrorPIAvgScore.TabIndex = 27;
            
            // txvMirrorBDAvgScore            
            TextBox txvMirrorBDAvgScore = new TextBox();
            txvMirrorBDAvgScore.Dock = DockStyle.Fill;
            txvMirrorBDAvgScore.Location = new Point(306, 186);
            txvMirrorBDAvgScore.Name = "txvMirrorBDAvgScore";
            txvMirrorBDAvgScore.Size = new Size(70, 22);
            txvMirrorBDAvgScore.TabIndex = 26;
            
            // txvMirrorBAvgScore
            TextBox txvMirrorBAvgScore = new TextBox();
            txvMirrorBAvgScore.Dock = DockStyle.Fill;
            txvMirrorBAvgScore.Location = new Point(306, 160);
            txvMirrorBAvgScore.Name = "txvMirrorBAvgScore";
            txvMirrorBAvgScore.Size = new Size(70, 22);
            txvMirrorBAvgScore.TabIndex = 25;
            
            // txvMirrorEAvgScore            
            TextBox txvMirrorEAvgScore = new TextBox();
            txvMirrorEAvgScore.Dock = DockStyle.Fill;
            txvMirrorEAvgScore.Location = new Point(306, 134);
            txvMirrorEAvgScore.Name = "txvMirrorEAvgScore";
            txvMirrorEAvgScore.Size = new Size(70, 22);
            txvMirrorEAvgScore.TabIndex = 24;
            
            // txvMirrorCAvgScore
            TextBox txvMirrorCAvgScore = new TextBox();
            txvMirrorCAvgScore.Dock = DockStyle.Fill;
            txvMirrorCAvgScore.Location = new Point(306, 108);
            txvMirrorCAvgScore.Name = "txvMirrorCAvgScore";
            txvMirrorCAvgScore.Size = new Size(70, 22);
            txvMirrorCAvgScore.TabIndex = 23;
            
            // txvMirrorCMAvgScore            
            TextBox txvMirrorCMAvgScore = new TextBox();
            txvMirrorCMAvgScore.Dock = DockStyle.Fill;
            txvMirrorCMAvgScore.Location = new Point(306, 82);
            txvMirrorCMAvgScore.Name = "txvMirrorCMAvgScore";
            txvMirrorCMAvgScore.Size = new Size(70, 22);
            txvMirrorCMAvgScore.TabIndex = 22;
            
            // txvMirrorRAvgScore            
            TextBox txvMirrorRAvgScore = new TextBox();
            txvMirrorRAvgScore.Dock = DockStyle.Fill;
            txvMirrorRAvgScore.Location = new Point(306, 56);
            txvMirrorRAvgScore.Name = "txvMirrorRAvgScore";
            txvMirrorRAvgScore.Size = new Size(70, 22);
            txvMirrorRAvgScore.TabIndex = 21;
            // 
            // label39
            // 
            label39.AutoSize = true;
            label39.Dock = DockStyle.Fill;
            label39.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label39.Location = new Point(306, 1);
            label39.Name = "label39";
            label39.Size = new Size(70, 25);
            label39.TabIndex = 2;
            label39.Text = "Score";
            label39.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            label38.AutoSize = true;
            label38.Dock = DockStyle.Fill;
            label38.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label38.Location = new Point(61, 1);
            label38.Name = "label38";
            label38.Size = new Size(238, 25);
            label38.TabIndex = 1;
            label38.Text = "Compliance area";
            label38.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            label28.AutoSize = true;
            label28.Dock = DockStyle.Fill;
            label28.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            label28.Location = new Point(4, 1);
            label28.Name = "label28";
            label28.Size = new Size(50, 25);
            label28.TabIndex = 0;
            label28.Text = "No.";
            label28.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            label40.AutoSize = true;
            label40.Dock = DockStyle.Fill;
            label40.Location = new Point(4, 53);
            label40.Name = "label40";
            label40.Size = new Size(50, 25);
            label40.TabIndex = 3;
            label40.Text = "2";
            label40.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            label41.AutoSize = true;
            label41.Dock = DockStyle.Fill;
            label41.Location = new Point(4, 27);
            label41.Name = "label41";
            label41.Size = new Size(50, 25);
            label41.TabIndex = 4;
            label41.Text = "1";
            label41.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            label42.AutoSize = true;
            label42.Dock = DockStyle.Fill;
            label42.Location = new Point(4, 79);
            label42.Name = "label42";
            label42.Size = new Size(50, 25);
            label42.TabIndex = 5;
            label42.Text = "3";
            label42.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            label43.AutoSize = true;
            label43.Dock = DockStyle.Fill;
            label43.Location = new Point(4, 105);
            label43.Name = "label43";
            label43.Size = new Size(50, 25);
            label43.TabIndex = 6;
            label43.Text = "4";
            label43.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label44
            // 
            label44.AutoSize = true;
            label44.Dock = DockStyle.Fill;
            label44.Location = new Point(4, 131);
            label44.Name = "label44";
            label44.Size = new Size(50, 25);
            label44.TabIndex = 7;
            label44.Text = "5";
            label44.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label45
            // 
            label45.AutoSize = true;
            label45.Dock = DockStyle.Fill;
            label45.Location = new Point(4, 157);
            label45.Name = "label45";
            label45.Size = new Size(50, 25);
            label45.TabIndex = 8;
            label45.Text = "6";
            label45.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label46
            // 
            label46.AutoSize = true;
            label46.Dock = DockStyle.Fill;
            label46.Location = new Point(4, 183);
            label46.Name = "label46";
            label46.Size = new Size(50, 25);
            label46.TabIndex = 9;
            label46.Text = "7";
            label46.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label47
            // 
            label47.AutoSize = true;
            label47.Dock = DockStyle.Fill;
            label47.Location = new Point(4, 209);
            label47.Name = "label47";
            label47.Size = new Size(50, 29);
            label47.TabIndex = 10;
            label47.Text = "8";
            label47.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            label48.AutoSize = true;
            label48.Dock = DockStyle.Fill;
            label48.Location = new Point(61, 27);
            label48.Name = "label48";
            label48.Size = new Size(238, 25);
            label48.TabIndex = 11;
            label48.Text = "Procurement structure";
            label48.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label50
            // 
            label50.AutoSize = true;
            label50.Dock = DockStyle.Fill;
            label50.Location = new Point(61, 53);
            label50.Name = "label50";
            label50.Size = new Size(238, 25);
            label50.TabIndex = 13;
            label50.Text = "Records";
            label50.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            label51.AutoSize = true;
            label51.Dock = DockStyle.Fill;
            label51.Location = new Point(61, 79);
            label51.Name = "label51";
            label51.Size = new Size(238, 25);
            label51.TabIndex = 14;
            label51.Text = "Contract Management";
            label51.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            label52.AutoSize = true;
            label52.Dock = DockStyle.Fill;
            label52.Location = new Point(61, 105);
            label52.Name = "label52";
            label52.Size = new Size(238, 25);
            label52.TabIndex = 15;
            label52.Text = "Contracting";
            label52.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label53
            // 
            label53.AutoSize = true;
            label53.Dock = DockStyle.Fill;
            label53.Location = new Point(61, 131);
            label53.Name = "label53";
            label53.Size = new Size(238, 25);
            label53.TabIndex = 16;
            label53.Text = "Evaluation";
            label53.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label54
            // 
            label54.AutoSize = true;
            label54.Dock = DockStyle.Fill;
            label54.Location = new Point(61, 157);
            label54.Name = "label54";
            label54.Size = new Size(238, 25);
            label54.TabIndex = 17;
            label54.Text = "Bidding";
            label54.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label55
            // 
            label55.AutoSize = true;
            label55.Dock = DockStyle.Fill;
            label55.Location = new Point(61, 209);
            label55.Name = "label55";
            label55.Size = new Size(238, 29);
            label55.TabIndex = 18;
            label55.Text = "Planning & Initiation";
            label55.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // label56
            // 
            label56.AutoSize = true;
            label56.Dock = DockStyle.Fill;
            label56.Location = new Point(61, 183);
            label56.Name = "label56";
            label56.Size = new Size(238, 25);
            label56.TabIndex = 19;
            label56.Text = "Bidding Document";
            label56.TextAlign = ContentAlignment.MiddleLeft;          

            // txvMirrorPSAvgScore            
            TextBox txvMirrorPSAvgScore = new TextBox();
            txvMirrorPSAvgScore.Dock = DockStyle.Fill;
            txvMirrorPSAvgScore.Location = new Point(306, 30);
            txvMirrorPSAvgScore.Name = "txvMirrorPSAvgScore";
            txvMirrorPSAvgScore.Size = new Size(70, 22);
            txvMirrorPSAvgScore.TabIndex = 20;


            TableLayoutPanel tableLayoutPanelAverageScores = new TableLayoutPanel();
            tableLayoutPanelAverageScores.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Left)
            | AnchorStyles.Right)));
            tableLayoutPanelAverageScores.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanelAverageScores.ColumnCount = 13;
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 180F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanelAverageScores.Controls.Add(txvRAvgScore, 12, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase10, 11, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase9, 10, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase8, 9, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase7, 8, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase6, 7, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase5, 6, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase4, 5, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase3, 4, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase2, 3, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvRCase1, 2, 7);
            tableLayoutPanelAverageScores.Controls.Add(txvCMAvgScore, 12, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase10, 11, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase9, 10, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase8, 9, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase7, 8, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase6, 7, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase5, 6, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase4, 5, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase3, 4, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase2, 3, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCMCase1, 2, 6);
            tableLayoutPanelAverageScores.Controls.Add(txvCAvgScore, 12, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase10, 11, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase9, 10, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase8, 9, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase7, 8, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase6, 7, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase5, 6, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase4, 5, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase3, 4, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase2, 3, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvCCase1, 2, 5);
            tableLayoutPanelAverageScores.Controls.Add(txvEAvgScore, 12, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase10, 11, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase9, 10, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase8, 9, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase7, 8, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase6, 7, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase5, 6, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase4, 5, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase3, 4, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase2, 3, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvECase1, 2, 4);
            tableLayoutPanelAverageScores.Controls.Add(txvBAvgScore, 12, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase10, 11, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase9, 10, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase8, 9, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase7, 8, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase6, 7, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase5, 6, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase4, 5, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase3, 4, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase2, 3, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBCase1, 2, 3);
            tableLayoutPanelAverageScores.Controls.Add(txvBDAvgScore, 12, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase10, 11, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase9, 10, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase8, 9, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase7, 8, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase6, 7, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase5, 6, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase4, 5, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase3, 4, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase2, 3, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvBDCase1, 2, 2);
            tableLayoutPanelAverageScores.Controls.Add(txvPIAvgScore, 12, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase10, 11, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase9, 10, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase8, 9, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase7, 8, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase6, 7, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase5, 6, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase4, 5, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase3, 4, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase2, 3, 1);
            tableLayoutPanelAverageScores.Controls.Add(label19, 12, 0);
            tableLayoutPanelAverageScores.Controls.Add(label18, 11, 0);
            tableLayoutPanelAverageScores.Controls.Add(label17, 10, 0);
            tableLayoutPanelAverageScores.Controls.Add(label16, 9, 0);
            tableLayoutPanelAverageScores.Controls.Add(label15, 8, 0);
            tableLayoutPanelAverageScores.Controls.Add(label14, 7, 0);
            tableLayoutPanelAverageScores.Controls.Add(label13, 6, 0);
            tableLayoutPanelAverageScores.Controls.Add(label12, 5, 0);
            tableLayoutPanelAverageScores.Controls.Add(label11, 4, 0);
            tableLayoutPanelAverageScores.Controls.Add(label10, 3, 0);
            tableLayoutPanelAverageScores.Controls.Add(label9, 2, 0);
            tableLayoutPanelAverageScores.Controls.Add(label8, 1, 0);
            tableLayoutPanelAverageScores.Controls.Add(label7, 0, 0);
            tableLayoutPanelAverageScores.Controls.Add(label20, 0, 1);
            tableLayoutPanelAverageScores.Controls.Add(label21, 0, 2);
            tableLayoutPanelAverageScores.Controls.Add(label22, 0, 3);
            tableLayoutPanelAverageScores.Controls.Add(label23, 0, 4);
            tableLayoutPanelAverageScores.Controls.Add(label24, 0, 6);
            tableLayoutPanelAverageScores.Controls.Add(label25, 0, 5);
            tableLayoutPanelAverageScores.Controls.Add(label26, 0, 7);
            tableLayoutPanelAverageScores.Controls.Add(label27, 0, 8);
            tableLayoutPanelAverageScores.Controls.Add(label29, 1, 1);
            tableLayoutPanelAverageScores.Controls.Add(label30, 1, 2);
            tableLayoutPanelAverageScores.Controls.Add(label31, 1, 3);
            tableLayoutPanelAverageScores.Controls.Add(label32, 1, 4);
            tableLayoutPanelAverageScores.Controls.Add(label33, 1, 6);
            tableLayoutPanelAverageScores.Controls.Add(label34, 1, 5);
            tableLayoutPanelAverageScores.Controls.Add(label35, 1, 7);
            tableLayoutPanelAverageScores.Controls.Add(label36, 1, 8);
            tableLayoutPanelAverageScores.Controls.Add(label37, 1, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvPICase1, 2, 1);
            tableLayoutPanelAverageScores.Controls.Add(txvPSAvgScore, 12, 8);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgAvgScore, 12, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase1, 2, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase2, 3, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase3, 4, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase4, 5, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase5, 6, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase6, 7, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase7, 8, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase8, 9, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase9, 10, 9);
            tableLayoutPanelAverageScores.Controls.Add(txvAvgCase10, 11, 9);
            tableLayoutPanelAverageScores.Location = new Point(7, 4);
            tableLayoutPanelAverageScores.Name = "tableLayoutPanelAverageScores";
            tableLayoutPanelAverageScores.RowCount = 10;
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanelAverageScores.Height = 269;
           // tableLayoutPanelAverageScores.Size = new Size(900, 269);
            tableLayoutPanelAverageScores.TabIndex = 0;


            //score summaries
            TableLayoutPanel tableLayoutPanelScoreSummaries = new TableLayoutPanel();

            // 
            // tableLayoutPanelScoreSummaries
            // 
            tableLayoutPanelScoreSummaries.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanelScoreSummaries.ColumnCount = 3;
            tableLayoutPanelScoreSummaries.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15F));
            tableLayoutPanelScoreSummaries.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 65F));
            tableLayoutPanelScoreSummaries.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanelScoreSummaries.Controls.Add(txvMirrorPIAvgScore, 2, 8);
            tableLayoutPanelScoreSummaries.Controls.Add(txvMirrorBDAvgScore, 2, 7);
            tableLayoutPanelScoreSummaries.Controls.Add(txvMirrorBAvgScore, 2, 6);
            tableLayoutPanelScoreSummaries.Controls.Add(txvMirrorEAvgScore, 2, 5);
            tableLayoutPanelScoreSummaries.Controls.Add(txvMirrorCAvgScore, 2, 4);
            tableLayoutPanelScoreSummaries.Controls.Add(txvMirrorCMAvgScore, 2, 3);
            tableLayoutPanelScoreSummaries.Controls.Add(txvMirrorRAvgScore, 2, 2);
            tableLayoutPanelScoreSummaries.Controls.Add(label39, 2, 0);
            tableLayoutPanelScoreSummaries.Controls.Add(label38, 1, 0);
            tableLayoutPanelScoreSummaries.Controls.Add(label28, 0, 0);
            tableLayoutPanelScoreSummaries.Controls.Add(label40, 0, 2);
            tableLayoutPanelScoreSummaries.Controls.Add(label41, 0, 1);
            tableLayoutPanelScoreSummaries.Controls.Add(label42, 0, 3);
            tableLayoutPanelScoreSummaries.Controls.Add(label43, 0, 4);
            tableLayoutPanelScoreSummaries.Controls.Add(label44, 0, 5);
            tableLayoutPanelScoreSummaries.Controls.Add(label45, 0, 6);
            tableLayoutPanelScoreSummaries.Controls.Add(label46, 0, 7);
            tableLayoutPanelScoreSummaries.Controls.Add(label47, 0, 8);
            tableLayoutPanelScoreSummaries.Controls.Add(label48, 1, 1);
            tableLayoutPanelScoreSummaries.Controls.Add(label50, 1, 2);
            tableLayoutPanelScoreSummaries.Controls.Add(label51, 1, 3);
            tableLayoutPanelScoreSummaries.Controls.Add(label52, 1, 4);
            tableLayoutPanelScoreSummaries.Controls.Add(label53, 1, 5);
            tableLayoutPanelScoreSummaries.Controls.Add(label54, 1, 6);
            tableLayoutPanelScoreSummaries.Controls.Add(label55, 1, 8);
            tableLayoutPanelScoreSummaries.Controls.Add(label56, 1, 7);
            tableLayoutPanelScoreSummaries.Controls.Add(txvMirrorPSAvgScore, 2, 1);
            tableLayoutPanelScoreSummaries.Location = new Point(18, 290);
            tableLayoutPanelScoreSummaries.Name = "tableLayoutPanelScoreSummaries";
            tableLayoutPanelScoreSummaries.RowCount = 9;
            tableLayoutPanelScoreSummaries.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelScoreSummaries.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelScoreSummaries.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelScoreSummaries.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelScoreSummaries.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelScoreSummaries.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelScoreSummaries.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelScoreSummaries.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelScoreSummaries.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelScoreSummaries.Size = new Size(380, 239);
            tableLayoutPanelScoreSummaries.TabIndex = 1;


            // 
            // panelAverageScores
            // 
            panelAverageScores.Controls.Add(tableLayoutPanelAverageScores);
            panelAverageScores.Controls.Add(tableLayoutPanelScoreSummaries);
            panelAverageScores.Dock = DockStyle.Fill;
            panelAverageScores.Name = "panelAverageScores";
            panelAverageScores.TabIndex = 2;

            return panelAverageScores;

        }

      
        

        
        private static void updateCaseAverageScoreContainer(int sectionLocalId, int columnIndex, Control control)
        {

            //get the scoreControls for the section we dealing with
            CaseScoresControls scoreControls = PanelPmComplianceProcessTool.CPT_SECTION_TOTAL_TXVS.ContainsKey(sectionLocalId) ? PanelPmComplianceProcessTool.CPT_SECTION_TOTAL_TXVS[sectionLocalId] : new CaseScoresControls();

            //case column indexes
            int case1 = 2;
            int case2 = 3;
            int case3 = 4;
            int case4 = 5;
            int case5 = 6;
            int case6 = 7;
            int case7 = 8;
            int case8 = 9;
            int case9 = 10;
            int case10 = 11;

            //based on the index, set it's control
            if (columnIndex == case1)
            {
                scoreControls.case1 = control;
            }
            else if (columnIndex == case2)
            {
                scoreControls.case2 = control;
            }
            else if (columnIndex == case3)
            {
                scoreControls.case3 = control;
            }
            else if (columnIndex == case4)
            {
                scoreControls.case4 = control;
            }
            else if (columnIndex == case5)
            {
                scoreControls.case5 = control;
            }
            else if (columnIndex == case6)
            {
                scoreControls.case6 = control;
            }
            else if (columnIndex == case7)
            {
                scoreControls.case7 = control;
            }
            else if (columnIndex == case8)
            {
                scoreControls.case8 = control;
            }
            else if (columnIndex == case9)
            {
                scoreControls.case9 = control;
            }
            else if (columnIndex == case10)
            {
                scoreControls.case10 = control;
            }

            //update the section controls for that section in the dictionary
            PanelPmComplianceProcessTool.CPT_SECTION_TOTAL_TXVS[sectionLocalId] = scoreControls;

        }

        private static SectionItemEvaluationCpt updateSectionEvalItemCpt(SectionItemEvaluationCpt sectionEvalItemToUpdate, int columnIndex, ComplianceProcessToolSectionItem sectionItem, Control control)
        {

            int case1 = 2;
            int case2 = 3;
            int case3 = 4;
            int case4 = 5;
            int case5 = 6;
            int case6 = 7;
            int case7 = 8;
            int case8 = 9;
            int case9 = 10;
            int case10 = 11;

            if (columnIndex == case1)
            {
                sectionEvalItemToUpdate.case1FieldName = control.Name;
                sectionEvalItemToUpdate.evaluationFieldType = sectionItem.responseType;
            }
            else if (columnIndex == case2)
            {
                sectionEvalItemToUpdate.case2FieldName = control.Name;
            }
            else if (columnIndex == case3)
            {
                sectionEvalItemToUpdate.case3FieldName = control.Name;
            }
            else if (columnIndex == case3)
            {
                sectionEvalItemToUpdate.case3FieldName = control.Name;
            }
            else if (columnIndex == case4)
            {
                sectionEvalItemToUpdate.case4FieldName = control.Name;
            }
            else if (columnIndex == case5)
            {
                sectionEvalItemToUpdate.case5FieldName = control.Name;
            }
            else if (columnIndex == case6)
            {
                sectionEvalItemToUpdate.case6FieldName = control.Name;
            }
            else if (columnIndex == case7)
            {
                sectionEvalItemToUpdate.case7FieldName = control.Name;
            }
            else if (columnIndex == case8)
            {
                sectionEvalItemToUpdate.case8FieldName = control.Name;
            }
            else if (columnIndex == case9)
            {
                sectionEvalItemToUpdate.case9FieldName = control.Name;
            }
            else if (columnIndex == case10)
            {
                sectionEvalItemToUpdate.case10FieldName = control.Name;
            }

            return sectionEvalItemToUpdate;

        }

        private static float getColumnWidth(int columnIndex)
        {

            if (columnIndex == 0)
            {
                return 50F;
            }
            else if (columnIndex == 1)
            {
                return 400F;
            }
            else if (columnIndex >= 2 && columnIndex <= 12)
            {
                return 80F;
            }
            else
            {
                return 0;
            }

        }

        private static Control buildBodyCellControlV1(int columnIndex, ComplianceProcessToolSectionItem sectionItem)
        {

            Control control = new Control();

            FieldCountPmAuditProcessTool++;
            String fieldName = "ComplianceProcessToolField" + FieldCountPmAuditProcessTool;


            if (columnIndex == 0)
            {
                Label labelRank = new Label() { Text = sectionItem.rank.ToString() };
                labelRank.Name = fieldName;
                return labelRank;
            }
            else if (columnIndex == 1)
            {
                //here we return a table layout 
                String text = sectionItem.description;
                Label labelDesc = new Label() { Text = text, AutoSize = true, Dock = DockStyle.Fill };
                labelDesc.Name = fieldName;

                //if it has important text show the text
                new ToolTip().SetToolTip(labelDesc, "More description");

                return labelDesc;
            }
            else
            {
                ComboBox cbx = new ComboBox();
                cbx.Items.Add("Select score");
                cbx.Items.Add("Yes");
                cbx.Items.Add("No");
                cbx.Items.Add("Na");
                cbx.DropDownStyle = ComboBoxStyle.DropDownList;
                cbx.Dock = DockStyle.Fill;
                cbx.Name = fieldName;
                cbx.SelectedIndex = 0;
                return cbx;

            }

        }

        private static Control buildBodyCellControl(int columnIndex, string fieldType, String textContent = "Rank")
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                Label labelRank = new Label() { Text = textContent };
                return labelRank;
            }
            else if (columnIndex == 1)
            {
                //here we return a table layout 
                String text = textContent == "Rank" ? "Item Description" : textContent;
                Label labelRank = new Label() { Text = text };

                //if it has important text show the text
                new ToolTip().SetToolTip(labelRank, "More description");

                return labelRank;
            }
            else if (columnIndex == 2)
            {
                //here we return a table layout                 
                return buildResponseFieldControl(fieldType);
            }
            else
            {

                RichTextBox textbox = new RichTextBox();
                textbox.Height = 50;
                textbox.Width = 400;
                return textbox;

            }


        }

        private static Control buildFoooterCellControl(int columnIndex)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                return new Label();
            }
            else if (columnIndex == 1)
            {
                Label labelRank = new Label() { Text = "TOTAL", Anchor = AnchorStyles.Right | AnchorStyles.Top, TextAlign = ContentAlignment.TopRight, ForeColor = Color.DarkRed };
                return labelRank;
            }
            else if (columnIndex >= 2 || columnIndex <= 11)
            {
                TextBox textbox = new TextBox();
                textbox.Height = 25;
                textbox.Dock = DockStyle.Fill;

                return textbox;
            }

            else
            {
                return new Label();
            }


        }



        private static Control buildHeaderCellControlV1(int columnIndex, ComplianceProcessToolSection toolSection)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                Label sectionRankLabel = new Label() { Text = toolSection.rank.ToString(), ForeColor = Color.DarkBlue };
                return sectionRankLabel;
            }
            else if (columnIndex == 1)
            {
                Label sectionNameLabel = new Label() { Text = toolSection.name.ToUpper(), ForeColor = Color.DarkBlue, Dock = DockStyle.Fill };
                return sectionNameLabel;
            }
            else if (columnIndex >= 2 || columnIndex <= 11)
            {

                int caseNo = columnIndex - 1;

                Label caseColumnLabel = new Label() { Text = "Case " + caseNo, Dock = DockStyle.Fill, ForeColor = Color.DarkBlue };
                return caseColumnLabel;
            }
            else
            {
                return new Label();
            }

        }

        private static Control buildHeaderCellControl(int columnIndex)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                Label sectionRankLabel = new Label() { Text = "1", ForeColor = Color.DarkBlue };
                return sectionRankLabel;
            }
            else if (columnIndex == 1)
            {
                Label sectionNameLabel = new Label() { Text = "SECTION A", ForeColor = Color.DarkBlue };
                return sectionNameLabel;
            }
            else if (columnIndex == 2)
            {
                return new Label();
            }
            else if (columnIndex == 3)
            {
                Label sectionColumn4Label = new Label() { Text = "AUDIT FINDINGS AND EVIDENCE", AutoSize = false, Width = 400, ForeColor = Color.DarkBlue };
                return sectionColumn4Label;
            }
            else if (columnIndex == 4)
            {
                Label sectionColumn5Label = new Label() { Text = "EXCEPTIONS", ForeColor = Color.DarkBlue };
                return sectionColumn5Label;
            }
            else
            {
                return new Label();
            }

        }

        private static Control buildResponseFieldControl(string fieldType)
        {

            if (fieldType == "yesnona")
            {

                TableLayoutPanel panel = new TableLayoutPanel();
                panel.ColumnCount = 3;
                panel.RowCount = 1;
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 34));
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33));
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33));

                panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));

                panel.Controls.Add(new RadioButton() { Text = "Yes" }, 0, 0);
                panel.Controls.Add(new RadioButton() { Text = "No" }, 1, 0);
                panel.Controls.Add(new RadioButton() { Text = "Na", Checked = true }, 2, 0);

                panel.Dock = DockStyle.Fill;
                return panel;

            }
            else if (fieldType == "date")
            {
                DateTimePicker datePicker = new DateTimePicker();
                datePicker.Height = 25;
                datePicker.Format = DateTimePickerFormat.Short;
                datePicker.Dock = DockStyle.Fill;
                return datePicker;
            }
            else
            {
                TextBox textbox = new TextBox();
                textbox.Height = 25;
                textbox.Dock = DockStyle.Fill;
                return textbox;
            }

        }


        public static TableLayoutPanel GenerateIdentificationSectionTable()
        {

            int COLUMN_COUNT = 4;
            int ROW_COUNT = 12;

            TableLayoutPanel panel = new TableLayoutPanel();

            panel.ColumnCount = COLUMN_COUNT;
            panel.RowCount = ROW_COUNT;

            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 5F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 35F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));

            //section 1

            Label label = new Label() { Text = "ASSETS DISPOSAL PROCESS AUDIT CRITERIA", Dock = DockStyle.Fill };
            panel.Controls.Add(label, 1, 0);

            panel.Controls.Add(new Label() { Text = "1.0", Dock = DockStyle.Fill }, 0, 1);
            panel.Controls.Add(new Label() { Text = "IDENTIFICATION", Dock = DockStyle.Fill }, 1, 1);

            panel.Controls.Add(new Label() { Text = "1.1", Dock = DockStyle.Fill }, 0, 2);
            panel.Controls.Add(new Label() { Text = "Name of Entity", Dock = DockStyle.Fill }, 1, 2);
            panel.Controls.Add(new TextBox() { Name = "txvCptNameOfEntity", Dock = DockStyle.Fill }, 2, 2);

            panel.Controls.Add(new Label() { Text = "1.2", Dock = DockStyle.Fill }, 0, 3);
            panel.Controls.Add(new Label() { Text = "Disposal Reference No.", Dock = DockStyle.Fill }, 1, 3);
            panel.Controls.Add(new TextBox() { Name = "txvCptReferenceNo", Dock = DockStyle.Fill }, 2, 3);

            panel.Controls.Add(new Label() { Text = "1.3", Dock = DockStyle.Fill }, 0, 4);
            panel.Controls.Add(new Label() { Text = "Audit Period", Dock = DockStyle.Fill }, 1, 4);
            panel.Controls.Add(new TextBox() { Name = "txvCptAuditPeriod", Dock = DockStyle.Fill }, 2, 4);

            panel.Controls.Add(new Label() { Text = "1.4", Dock = DockStyle.Fill }, 0, 5);
            panel.Controls.Add(new Label() { Text = "Asset Description", Dock = DockStyle.Fill }, 1, 5);
            panel.Controls.Add(new TextBox() { Name = "txvCptAssetDescription", Dock = DockStyle.Fill }, 2, 5);

            panel.Controls.Add(new Label() { Text = "1.5", Dock = DockStyle.Fill }, 0, 6);
            panel.Controls.Add(new Label() { Text = "Name of Buyer", Dock = DockStyle.Fill }, 1, 6);
            panel.Controls.Add(new TextBox() { Name = "txvCptNameOfBuyer", Dock = DockStyle.Fill }, 2, 6);

            panel.Controls.Add(new Label() { Text = "1.6", Dock = DockStyle.Fill }, 0, 7);
            panel.Controls.Add(new Label() { Text = "Disposal Value", Dock = DockStyle.Fill }, 1, 7);
            panel.Controls.Add(new TextBox() { Name = "txvCptDisposalValue", Dock = DockStyle.Fill }, 2, 7);

            panel.Controls.Add(new Label() { Text = "1.7", Dock = DockStyle.Fill }, 0, 8);
            panel.Controls.Add(new Label() { Text = "Valuation Amount", Dock = DockStyle.Fill }, 1, 8);
            panel.Controls.Add(new TextBox() { Name = "txvCptValuationAmount", Dock = DockStyle.Fill }, 2, 8);

            panel.Controls.Add(new Label() { Text = "1.8", Dock = DockStyle.Fill }, 0, 9);
            panel.Controls.Add(new Label() { Text = "Disposal Method", Dock = DockStyle.Fill }, 1, 9);
            ComboBox cbx1 = new ComboBox() { Name = "cbxCptDisposalMethod", Dock = DockStyle.Fill };
            cbx1.Items.Add("Select Option");
            cbx1.Items.Add("Bidding");
            cbx1.Items.Add("Auction");
            cbx1.Items.Add("Direct");
            cbx1.Items.Add("Officers");
            cbx1.Items.Add("Destruction");
            cbx1.Items.Add("Conversion");
            cbx1.Items.Add("Trade-In");
            cbx1.Items.Add("Transfer");
            cbx1.Items.Add("Donation");
            cbx1.SelectedIndex = 0;
            panel.Controls.Add(cbx1, 2, 9);


            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 10);

            panel.Dock = DockStyle.Fill;
            panel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            panel.AutoScroll = true;
            panel.AutoSize = true;

            return panel;

        }


    }

}

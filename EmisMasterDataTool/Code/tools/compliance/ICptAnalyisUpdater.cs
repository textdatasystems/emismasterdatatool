﻿using Processor.Entities.PM.compliance.process;
using Processor.Entities.PM.disposal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmisTool.Code
{
    public interface ICptAnalyisUpdater
    {

        void updateCptSectionScoresInAnalyisTab(SectionEvaluationCpt section);
        void autofillCptAnalysisTabTextFieldBasedOnEvalItemValue(SectionItemEvaluationCpt sectionItem);
        void autoFillAverageScoresTab(object sender, System.EventArgs e);

    }
}

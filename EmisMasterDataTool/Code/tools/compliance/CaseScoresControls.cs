﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code
{
    public class CaseScoresControls
    {
        public Control case1 { get; set; }
        public Control case2 { get; set; }
        public Control case3 { get; set; }
        public Control case4 { get; set; }
        public Control case5 { get; set; }
        public Control case6 { get; set; }
        public Control case7 { get; set; }
        public Control case8 { get; set; }
        public Control case9 { get; set; }
        public Control case10 { get; set; }

    }
}

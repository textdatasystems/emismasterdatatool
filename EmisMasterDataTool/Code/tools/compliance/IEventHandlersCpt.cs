﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmisTool.Code
{
    public interface IEventHandlersCpt
    {

        void calculateAveragePlanningAndInitiation(object sender, EventArgs e);
        void calculateAverageBiddingDocument(object sender, EventArgs e);
        void calculateAverageEvaluation(object sender, EventArgs e);
        void calculateAverageContracting(object sender, EventArgs e);
        void calculateAverageContractMangt(object sender, EventArgs e);
        void calculateAverageBidding(object sender, EventArgs e);
        void calculateAverageRecords(object sender, EventArgs e);

        void calculateAvgOfSectionAvgScores(object sender, EventArgs e);

        void calculateAverageCase1(object sender, EventArgs e);
        void calculateAverageCase2(object sender, EventArgs e);
        void calculateAverageCase3(object sender, EventArgs e);
        void calculateAverageCase4(object sender, EventArgs e);
        void calculateAverageCase5(object sender, EventArgs e);
        void calculateAverageCase6(object sender, EventArgs e);
        void calculateAverageCase7(object sender, EventArgs e);
        void calculateAverageCase8(object sender, EventArgs e);
        void calculateAverageCase9(object sender, EventArgs e);
        void calculateAverageCase10(object sender, EventArgs e);


    }

}

﻿using EmisTool.Code.ui.custom;
using Processor.Entities.PM;
using Processor.Entities.PM.compliance.process;
using Processor.Entities.PM.disposal;
using Processor.Entities.PM.systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code
{

    public class AppStateCompliance
    {
        
        public ICptAnalyisUpdater iCptAnalyisUpdater { get; set; }

        public static string TAB_CBAS_CURRENT_ACTIVITY = "0";

        public static List<SectionEvaluationCpt> complianceProcessToolSectionEvaluations = new List<SectionEvaluationCpt>();
         
       
        internal static bool sectionEvaluationCptExists(long sectionEmisId)
        {

            foreach (var item in complianceProcessToolSectionEvaluations)
            {
                if (item.sectionEmisId == sectionEmisId)
                {
                    return true;
                }
            }

            return false;

        }

       
        public void updatePmComplianceProcessToolState(object sender, System.EventArgs e)
        {

            long affectedEmisSectionID = 0;

            Control control = (Control)sender;
            String controlName = control.Name;

            if (String.IsNullOrEmpty(controlName) || control.Text.Contains("Select"))
            {
                return;
            }

            //iterate through the and find a control with this name, and then set its value

            List<SectionEvaluationCpt> updatedList = new List<SectionEvaluationCpt>();

            foreach (var section in complianceProcessToolSectionEvaluations)
            {

                //update dictionary
                Dictionary<long, SectionItemEvaluationCpt> updatedDictSectionItems = new Dictionary<long, SectionItemEvaluationCpt>();

                foreach (KeyValuePair<long, SectionItemEvaluationCpt> entry in section.sectionItemEvaluations)
                {

                    // do something with entry.Value or entry.Key
                    var item = entry.Value;
                   
                    if (item.case1FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case1FieldValue = value;
                    }
                    else if (item.case2FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case2FieldValue = value;
                    }
                    else if (item.case3FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case3FieldValue = value;
                    }
                    else if (item.case4FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case4FieldValue = value;
                    }
                    else if (item.case5FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case5FieldValue = value;
                    }
                    else if (item.case6FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case6FieldValue = value;
                    }
                    else if (item.case7FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case7FieldValue = value;
                    }
                    else if (item.case8FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case8FieldValue = value;
                    }
                    else if (item.case9FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case9FieldValue = value;
                    }
                    else if (item.case10FieldName == controlName)
                    {
                        affectedEmisSectionID = section.sectionEmisId;
                        String value = control.Text;
                        item.case10FieldValue = value;
                    }                  
                    
                    //insert into the updated dictonary
                    updatedDictSectionItems.Add(entry.Key, item);                                       

                }
                
                //update the section details
                section.sectionItemEvaluations = updatedDictSectionItems;
                updatedList.Add(section);

               // if(stopCheckingOtherSections) { break; };

            }

            //update app state
            AppStateCompliance.complianceProcessToolSectionEvaluations = updatedList;

            //update section data
            if(affectedEmisSectionID != 0)
            {
                sumCptSection(affectedEmisSectionID);
            }
            
        }
               

        public long sumCptSection(long sectionEmisId)
        {

            long totalYes = 0;
            long totalNo = 0;

            CaseScores caseScores = new CaseScores();

            foreach (SectionEvaluationCpt section in complianceProcessToolSectionEvaluations)
            {

                //this is the section we are working with               
                if (section.sectionEmisId == sectionEmisId)
                {

                    var items = section.sectionItemEvaluations;

                    foreach (KeyValuePair<long, SectionItemEvaluationCpt> entry in section.sectionItemEvaluations)
                    {

                        var item = entry.Value;

                        //we found the matching colum
                        if (item.evaluationFieldType == "yesnona")
                        {

                            //logic that confuses like Gloria, but I will try to explain
                            /*
                             Each of the items has 10 cases, so when I am on the item I need to pick it's value
                             for each case and then based on that value update the case score for the section
                             e.g if I am on Item 1, it has case1, case2 ... case10 so I have to pick the score of
                             case1 and update case1ScoreYes and case1ScoreNo for the section
                             */
                            String case1Val = item.case1FieldValue;
                            String case2Val = item.case2FieldValue;
                            String case3Val = item.case3FieldValue;
                            String case4Val = item.case4FieldValue;
                            String case5Val = item.case5FieldValue;
                            String case6Val = item.case6FieldValue;
                            String case7Val = item.case7FieldValue;
                            String case8Val = item.case8FieldValue;
                            String case9Val = item.case9FieldValue;
                            String case10Val = item.case10FieldValue;

                            //handle case 1
                            if (item.case1FieldValue == "Yes")
                            {
                                caseScores.case1Yes = caseScores.case1Yes + 1;

                            }
                            else if (item.case1FieldValue == "No")
                            {
                                caseScores.case1No = caseScores.case1No + 1;
                            }

                            //handle case 2
                            if (item.case2FieldValue == "Yes")
                            {
                                caseScores.case2Yes = caseScores.case2Yes + 1;

                            }
                            else if (item.case2FieldValue == "No")
                            {
                                caseScores.case2No = caseScores.case2No + 1;
                            }

                            //handle case 3
                            if (item.case3FieldValue == "Yes")
                            {
                                caseScores.case3Yes = caseScores.case3Yes + 1;

                            }
                            else if (item.case3FieldValue == "No")
                            {
                                caseScores.case3No = caseScores.case3No + 1;
                            }

                            //handle case 4
                            if (item.case4FieldValue == "Yes")
                            {
                                caseScores.case4Yes = caseScores.case4Yes + 1;

                            }
                            else if (item.case4FieldValue == "No")
                            {
                                caseScores.case4No = caseScores.case4No + 1;
                            }

                            //handle case 5
                            if (item.case5FieldValue == "Yes")
                            {
                                caseScores.case5Yes = caseScores.case5Yes + 1;

                            }
                            else if (item.case5FieldValue == "No")
                            {
                                caseScores.case5No = caseScores.case5No + 1;
                            }

                            //handle case 6
                            if (item.case6FieldValue == "Yes")
                            {
                                caseScores.case6Yes = caseScores.case6Yes + 1;

                            }
                            else if (item.case6FieldValue == "No")
                            {
                                caseScores.case6No = caseScores.case6No + 1;
                            }

                            //handle case 7
                            if (item.case7FieldValue == "Yes")
                            {
                                caseScores.case7Yes = caseScores.case7Yes + 1;

                            }
                            else if (item.case7FieldValue == "No")
                            {
                                caseScores.case7No = caseScores.case7No + 1;
                            }

                            //handle case 8
                            if (item.case8FieldValue == "Yes")
                            {
                                caseScores.case8Yes = caseScores.case8Yes + 1;

                            }
                            else if (item.case8FieldValue == "No")
                            {
                                caseScores.case8No = caseScores.case8No + 1;
                            }

                            //handle case 9
                            if (item.case9FieldValue == "Yes")
                            {
                                caseScores.case9Yes = caseScores.case9Yes + 1;

                            }
                            else if (item.case9FieldValue == "No")
                            {
                                caseScores.case9No = caseScores.case9No + 1;
                            }

                            //handle case 10
                            if (item.case10FieldValue == "Yes")
                            {
                                caseScores.case10Yes = caseScores.case10Yes + 1;

                            }
                            else if (item.case10FieldValue == "No")
                            {
                                caseScores.case10No = caseScores.case10No + 1;
                            }

                        }

                    }

                    //update the case scores in this section
                    section.caseScores = caseScores;

                    //update the App State
                    updateCptSectionInAppState(section);

                }

            }

            //show the scores
            displaySectionCaseAverageScores(sectionEmisId, caseScores);

            return totalYes;

        }

        private static void displaySectionCaseAverageScores(long sectionEmisId, CaseScores caseScores)
        {

            var caseScoreControls = PanelPmComplianceProcessTool.CPT_SECTION_TOTAL_TXVS[sectionEmisId];

            //case 1
            var calcHandler = new CalculationHelper();
            calcHandler.calculateCaseAverageScore(caseScores.case1Yes, caseScores.case1No, caseScoreControls.case1);
            calcHandler.calculateCaseAverageScore(caseScores.case2Yes, caseScores.case2No, caseScoreControls.case2);
            calcHandler.calculateCaseAverageScore(caseScores.case3Yes, caseScores.case3No, caseScoreControls.case3);
            calcHandler.calculateCaseAverageScore(caseScores.case4Yes, caseScores.case4No, caseScoreControls.case4);
            calcHandler.calculateCaseAverageScore(caseScores.case5Yes, caseScores.case5No, caseScoreControls.case5);
            calcHandler.calculateCaseAverageScore(caseScores.case6Yes, caseScores.case6No, caseScoreControls.case6);
            calcHandler.calculateCaseAverageScore(caseScores.case7Yes, caseScores.case7No, caseScoreControls.case7);
            calcHandler.calculateCaseAverageScore(caseScores.case8Yes, caseScores.case8No, caseScoreControls.case8);
            calcHandler.calculateCaseAverageScore(caseScores.case9Yes, caseScores.case9No, caseScoreControls.case9);
            calcHandler.calculateCaseAverageScore(caseScores.case10Yes, caseScores.case10No, caseScoreControls.case10);


        }

        public void updateCptSectionInAppState(SectionEvaluationCpt section)
        {

            List<SectionEvaluationCpt> sectionsUpdated = new List<SectionEvaluationCpt>();

            foreach (var oldSec in complianceProcessToolSectionEvaluations)
            {
                if (section.sectionEmisId == oldSec.sectionEmisId)
                {
                    sectionsUpdated.Add(section);
                }
                else
                {
                    sectionsUpdated.Add(oldSec);
                }
            }

            AppStateCompliance.complianceProcessToolSectionEvaluations = sectionsUpdated;

            //call the main UI to update the section score
            iCptAnalyisUpdater.updateCptSectionScoresInAnalyisTab(section);

        }

        public static SectionEvaluationCpt findSectionEvaluationCptById(long sectionEmisId)
        {

            foreach (var section in complianceProcessToolSectionEvaluations)
            {
                if (section.sectionEmisId == sectionEmisId)
                {
                    return section;
                }                
            }

            return null;

        }        

    }


}

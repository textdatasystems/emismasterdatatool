﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace EmisTool.Code
{
    public class UiHelperCst
    {       

        public static TableLayoutPanel GenerateAboutEntitySection(EventHandler eventHandlerCalcAvgScore)
        {

            int COLUMN_COUNT = 6;
            int ROW_COUNT = 58;

            TableLayoutPanel panel = new TableLayoutPanel();
            
            panel.ColumnCount = COLUMN_COUNT;
            panel.RowCount = ROW_COUNT;

            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
                     
            //row 0
            Label label = new Label() { Text = "Entity Identification", Dock = DockStyle.Fill };            
            panel.Controls.Add(label, 0, 0);
            panel.SetRowSpan(label, 3);

            Label label1 = new Label() { Text = "Name of Procuring and Disposing Entity", Dock = DockStyle.Fill };
            panel.Controls.Add(label1, 1, 0);

            TextBox txv1 = new TextBox() { Name = "txvNameOfEntity", Dock = DockStyle.Fill };
            panel.Controls.Add(txv1, 2, 0);
            panel.SetColumnSpan(txv1, 3);

            //row 1
            Label label2 = new Label() { Text = "Sector", Dock = DockStyle.Fill };
            panel.Controls.Add(label2, 1, 1);

            TextBox txv2 = new TextBox() { Name = "txvSector", Dock = DockStyle.Fill };
            panel.Controls.Add(txv2, 2, 1);
            panel.SetColumnSpan(txv2, 3);

            //row 2
            Label label3 = new Label() { Text = "Date of operationalization of the PDE where applicable", Dock = DockStyle.Fill };
            panel.Controls.Add(label3, 1, 2);

            TextBox txv3 = new TextBox() { Name = "txvDateOfOperationalization", Dock = DockStyle.Fill };
            panel.Controls.Add(txv3, 2, 2);
            panel.SetColumnSpan(txv3, 3);

            //row 3
            Label label4 = new Label() { Text = "Size of PDE", Dock = DockStyle.Fill };
            panel.Controls.Add(label4, 0, 3);
            panel.SetRowSpan(label4, 6);

            Label label5 = new Label() { Text = "Number of Staff members", Dock = DockStyle.Fill };
            panel.Controls.Add(label5, 1, 3);

            TextBox txv4 = new TextBox() { Name = "txvNoOfStaff", Dock = DockStyle.Fill };
            panel.Controls.Add(txv4, 2, 3);
            panel.SetColumnSpan(txv4, 3);

            Label label6 = new Label() { Text = "Departments", Dock = DockStyle.Fill };
            panel.Controls.Add(label6, 1, 4);
            panel.SetRowSpan(label6, 5);


            TextBox txv5 = new TextBox() { Name = "txvDept1", Dock = DockStyle.Fill };
            panel.Controls.Add(txv5, 2, 4);

            TextBox txv6 = new TextBox() { Name = "txvDept2", Dock = DockStyle.Fill };
            panel.Controls.Add(txv6, 3, 4);

            TextBox txv7 = new TextBox() { Name = "txvDept3", Dock = DockStyle.Fill };
            panel.Controls.Add(txv7, 4, 4);


            //
            TextBox txv8 = new TextBox() { Name = "txvDept4", Dock = DockStyle.Fill };
            panel.Controls.Add(txv8, 2, 5);

            TextBox txv9 = new TextBox() { Name = "txvDept5", Dock = DockStyle.Fill };
            panel.Controls.Add(txv9, 3, 5);

            TextBox txv10 = new TextBox() { Name = "txvDept6", Dock = DockStyle.Fill };
            panel.Controls.Add(txv10, 4, 5);


            //
            TextBox txv11 = new TextBox() { Name = "txvDept7", Dock = DockStyle.Fill };
            panel.Controls.Add(txv11, 2, 6);

            TextBox txv12 = new TextBox() { Name = "txvDept8", Dock = DockStyle.Fill };
            panel.Controls.Add(txv12, 3, 6);

            TextBox txv13 = new TextBox() { Name = "txvDept9", Dock = DockStyle.Fill };
            panel.Controls.Add(txv13, 4, 6);

            //
            TextBox txv14 = new TextBox() { Name = "txvDept10", Dock = DockStyle.Fill };
            panel.Controls.Add(txv14, 2, 7);

            TextBox txv15 = new TextBox() { Name = "txvDept11", Dock = DockStyle.Fill };
            panel.Controls.Add(txv15, 3, 7);

            TextBox txv16 = new TextBox() { Name = "txvDept12", Dock = DockStyle.Fill };
            panel.Controls.Add(txv16, 4, 7);

            //
            TextBox txv17 = new TextBox() { Name = "txvDept13", Dock = DockStyle.Fill };
            panel.Controls.Add(txv17, 2, 8);

            TextBox txv18 = new TextBox() { Name = "txvDept14", Dock = DockStyle.Fill };
            panel.Controls.Add(txv18, 3, 8);

            TextBox txv19 = new TextBox() { Name = "txvDept15", Dock = DockStyle.Fill };
            panel.Controls.Add(txv19, 4, 8);


            Label label20 = new Label() { Text = "Geographical spread if any", Dock = DockStyle.Fill };
            panel.Controls.Add(label20, 1, 9);

            TextBox txv21 = new TextBox() { Name = "txvGeoSpread", Dock = DockStyle.Fill };
            panel.Controls.Add(txv21, 2, 9);
            panel.SetColumnSpan(txv21, 3);


            //
            Label label22 = new Label() { Text = "Budget", Dock = DockStyle.Fill };
            panel.Controls.Add(label22, 0, 10);
            panel.SetRowSpan(label22, 5);

            Label label23 = new Label() { Text = "Total Entity Budget", Dock = DockStyle.Fill };
            panel.Controls.Add(label23, 1, 10);

            TextBox txv24 = new TextBox() { Name="txvTotalEntityBudget", Dock = DockStyle.Fill };
            panel.Controls.Add(txv24, 2, 10);
            panel.SetColumnSpan(txv24, 3);
            
            //
            Label label27 = new Label() { Text = "Total Procurement  Budget for the Financial Year", Dock = DockStyle.Fill };
            panel.Controls.Add(label27, 1, 11);

            TextBox txv28 = new TextBox() { Name = "txvTotalProcBudget", Dock = DockStyle.Fill };
            panel.Controls.Add(txv28, 2, 11);
            panel.SetColumnSpan(txv28, 3);

            //
            Label label29 = new Label() { Text = "Estimated Volume of Procurements as % of total annual budget of PDE", Dock = DockStyle.Fill };
            panel.Controls.Add(label29, 1, 12);

            TextBox txv30 = new TextBox() { Name = "txvVolProcVsAnnualBudget", Dock = DockStyle.Fill };
            panel.Controls.Add(txv30, 2, 12);
            panel.SetColumnSpan(txv30, 3);

            //
            Label label31 = new Label() { Text = "Source of funding (GOU or Donor funded- internally generated revenue)", Dock = DockStyle.Fill };
            panel.Controls.Add(label31, 1, 13);

            TextBox txv32 = new TextBox() { Name = "txvSourceOfFunding", Dock = DockStyle.Fill };
            panel.Controls.Add(txv32, 2, 13);
            panel.SetColumnSpan(txv32, 3);

            //
            Label label33 = new Label() { Text = "Previous Compliance Ratings of PDE and dates of these ratings", Dock = DockStyle.Fill };
            panel.Controls.Add(label33, 1, 14);

            TextBox txv34 = new TextBox() { Name = "txvPreviousRatingsAndDate", Dock = DockStyle.Fill };
            panel.Controls.Add(txv34, 2, 14);
            panel.SetColumnSpan(txv34, 3);


            //
            Label label34 = new Label() { Text = "Accounting Officer", Dock = DockStyle.Fill };
            panel.Controls.Add(label34, 0, 15);
            panel.SetRowSpan(label34, 2);

            Label label35 = new Label() { Text = "Name of the Accounting Officer and substantive position in held in the Entity", Dock = DockStyle.Fill };
            panel.Controls.Add(label35, 1, 15);

            TextBox txv36 = new TextBox() { Name = "txvNameOfAO", Dock = DockStyle.Fill };
            panel.Controls.Add(txv36, 2, 15);
            panel.SetColumnSpan(txv36, 3);

            //
            Label label37 = new Label() { Text = "Is the PDU well faciliteted in terms of space and internet connectivity?", Dock = DockStyle.Fill };
            panel.Controls.Add(label37, 1, 16);

            ComboBox cbx38 = new ComboBox() { Name = "cbxFacilitatedWithInternet"};
            cbx38.Items.Add("Select score");
            cbx38.Items.Add("Yes");
            cbx38.Items.Add("Yes");
            cbx38.Items.Add("Na");
            cbx38.SelectedIndex = 0;
            cbx38.SelectedIndexChanged += eventHandlerCalcAvgScore;
            panel.Controls.Add(cbx38, 2, 16);
            panel.SetColumnSpan(cbx38, 3);


            //
            Label label39 = new Label() { Text = "Contracts Committee", Dock = DockStyle.Fill };
            panel.Controls.Add(label39, 0, 17);
            panel.SetRowSpan(label39, 7);

            Label label40 = new Label() { Text = "Names of members", Dock = DockStyle.Fill };
            panel.Controls.Add(label40, 1, 17);

            Label label41 = new Label() { Text = "Position on the Contracts Committee", Dock = DockStyle.Fill };
            panel.Controls.Add(label41, 2, 17);

            Label label42 = new Label() { Text = "Substantive Position in the PDE", Dock = DockStyle.Fill };
            panel.Controls.Add(label42, 3, 17);

            Label label43 = new Label() { Text = "Date of PS/ST approval for each member", Dock = DockStyle.Fill };
            panel.Controls.Add(label43, 4, 17);

            //
            TextBox txv44 = new TextBox() { Name = "txvMemberName2", Dock = DockStyle.Fill };
            panel.Controls.Add(txv44, 1, 18);

            TextBox txv45 = new TextBox() { Name = "txvMemberPosition2", Dock = DockStyle.Fill };
            panel.Controls.Add(txv45, 2, 18);

            TextBox txv46 = new TextBox() { Name = "txvMemberSubstantivePos2", Dock = DockStyle.Fill };
            panel.Controls.Add(txv46, 3, 18);

            TextBox txv47 = new TextBox() { Name = "txvMemberDateOfApproval2", Dock = DockStyle.Fill };
            panel.Controls.Add(txv47, 4, 18);

            //
            TextBox txv48 = new TextBox() { Name = "txvMemberName3", Dock = DockStyle.Fill };
            panel.Controls.Add(txv48, 1, 19);

            TextBox txv49 = new TextBox() { Name = "txvMemberPosition3", Dock = DockStyle.Fill };
            panel.Controls.Add(txv49, 2, 19);

            TextBox txv50 = new TextBox() { Name = "txvMemberSubstantivePos3", Dock = DockStyle.Fill };
            panel.Controls.Add(txv50, 3, 19);

            TextBox txv51 = new TextBox() { Name = "txvMemberDateOfApproval3", Dock = DockStyle.Fill };
            panel.Controls.Add(txv51, 4, 19);

            //
            TextBox txv52 = new TextBox() { Name = "txvMemberName4", Dock = DockStyle.Fill };
            panel.Controls.Add(txv52, 1, 20);

            TextBox txv53 = new TextBox() { Name = "txvMemberPosition4", Dock = DockStyle.Fill };
            panel.Controls.Add(txv53, 2, 20);

            TextBox txv54 = new TextBox() { Name = "txvMemberSubstantivePos4", Dock = DockStyle.Fill };
            panel.Controls.Add(txv54, 3, 20);

            TextBox txv55 = new TextBox() { Name = "txvMemberDateOfApproval4", Dock = DockStyle.Fill };
            panel.Controls.Add(txv55, 4, 20);

            //
            TextBox txv56 = new TextBox() { Name = "txvMemberName5", Dock = DockStyle.Fill };
            panel.Controls.Add(txv56, 1, 21);

            TextBox txv57 = new TextBox() { Name = "txvMemberPosition5", Dock = DockStyle.Fill };
            panel.Controls.Add(txv57, 2, 21);

            TextBox txv58 = new TextBox() { Name = "txvMemberSubstantivePos5", Dock = DockStyle.Fill };
            panel.Controls.Add(txv58, 3, 21);

            TextBox txv59 = new TextBox() { Name = "txvMemberDateOfApproval5", Dock = DockStyle.Fill };
            panel.Controls.Add(txv59, 4, 21);

            //
            TextBox txv60 = new TextBox() { Name = "txvMemberName6", Dock = DockStyle.Fill };
            panel.Controls.Add(txv60, 1, 22);

            TextBox txv61 = new TextBox() { Name = "txvMemberPosition6", Dock = DockStyle.Fill };
            panel.Controls.Add(txv61, 2, 22);

            TextBox txv62 = new TextBox() { Name = "txvMemberSubstantivePos6", Dock = DockStyle.Fill };
            panel.Controls.Add(txv62, 3, 22);

            TextBox txv63 = new TextBox() { Name = "txvMemberDateOfApproval6", Dock = DockStyle.Fill };
            panel.Controls.Add(txv63, 4, 22);
            
            //
            Label label64 = new Label() { Text = "Are the members of the Contracts Committee approved by ST and PP Form 220 filed with PPDA", Dock = DockStyle.Fill };
            panel.Controls.Add(label64, 1, 23);

            ComboBox cbx65 = new ComboBox() { Name = "cbxAreCCMembersApproved" };
            cbx65.Items.Add("Select score");
            cbx65.Items.Add("Yes");
            cbx65.Items.Add("No");
            cbx65.Items.Add("Na");
            cbx65.SelectedIndex = 0;
            panel.Controls.Add(cbx65, 2, 23);
            panel.SetColumnSpan(cbx65, 3);



            //
            Label label66 = new Label() { Text = "Procurement and Disposal Unit", Dock = DockStyle.Fill };
            panel.Controls.Add(label66, 0, 24);
            panel.SetRowSpan(label66, 12);

            Label label67 = new Label() { Text = "Names of staff", Dock = DockStyle.Fill };
            panel.Controls.Add(label67, 1, 24);

            Label label68 = new Label() { Text = "Qualification", Dock = DockStyle.Fill };
            panel.Controls.Add(label68, 2, 24);

            Label label69 = new Label() { Text = "Position on the Unit ", Dock = DockStyle.Fill };
            panel.Controls.Add(label69, 3, 24);

            Label label70 = new Label() { Text = "Date of Appointment", Dock = DockStyle.Fill };
            panel.Controls.Add(label70, 4, 24);

            //
            TextBox txv71 = new TextBox() { Name = "txvPduMemberName1", Dock = DockStyle.Fill };
            panel.Controls.Add(txv71, 1, 25);

            TextBox txv72 = new TextBox() { Name = "txvPduMemberQualif1", Dock = DockStyle.Fill };
            panel.Controls.Add(txv72, 2, 25);

            TextBox txv73 = new TextBox() { Name = "txvPduMemberPosition1", Dock = DockStyle.Fill };
            panel.Controls.Add(txv73, 3, 25);

            TextBox txv74 = new TextBox() { Name = "txvPduMemberApprovalDate1", Dock = DockStyle.Fill };
            panel.Controls.Add(txv74, 4, 25);

            //
            TextBox txv75 = new TextBox() { Name = "txvPduMemberName2", Dock = DockStyle.Fill };
            panel.Controls.Add(txv75, 1, 26);

            TextBox txv76 = new TextBox() { Name = "txvPduMemberQualif2", Dock = DockStyle.Fill };
            panel.Controls.Add(txv76, 2, 26);

            TextBox txv77 = new TextBox() { Name = "txvPduMemberPosition2", Dock = DockStyle.Fill };
            panel.Controls.Add(txv77, 3, 26);

            TextBox txv78 = new TextBox() { Name = "txvPduMemberApprovalDate2", Dock = DockStyle.Fill };
            panel.Controls.Add(txv78, 4, 26);

            //
            TextBox txv79 = new TextBox() { Name = "txvPduMemberName3", Dock = DockStyle.Fill };
            panel.Controls.Add(txv79, 1, 27);

            TextBox txv80 = new TextBox() { Name = "txvPduMemberQualif3", Dock = DockStyle.Fill };
            panel.Controls.Add(txv80, 2, 27);

            TextBox txv81 = new TextBox() { Name = "txvPduMemberPosition3", Dock = DockStyle.Fill };
            panel.Controls.Add(txv81, 3, 27);

            TextBox txv82 = new TextBox() { Name = "txvPduMemberApprovalDate3", Dock = DockStyle.Fill };
            panel.Controls.Add(txv82, 4, 27);

            //
            TextBox txv83 = new TextBox() { Name = "txvPduMemberName4", Dock = DockStyle.Fill };
            panel.Controls.Add(txv83, 1, 28);

            TextBox txv84 = new TextBox() { Name = "txvPduMemberQualif4", Dock = DockStyle.Fill };
            panel.Controls.Add(txv84, 2, 28);

            TextBox txv85 = new TextBox() { Name = "txvPduMemberPosition4", Dock = DockStyle.Fill };
            panel.Controls.Add(txv85, 3, 28);

            TextBox txv86 = new TextBox() { Name = "txvPduMemberApprovalDate4", Dock = DockStyle.Fill };
            panel.Controls.Add(txv86, 4, 28);

            //
            TextBox txv87 = new TextBox() { Name = "txvPduMemberName5", Dock = DockStyle.Fill };
            panel.Controls.Add(txv87, 1, 29);

            TextBox txv88 = new TextBox() { Name = "txvPduMemberQualif5", Dock = DockStyle.Fill };
            panel.Controls.Add(txv88, 2, 29);

            TextBox txv89 = new TextBox() { Name = "txvPduMemberPosition5", Dock = DockStyle.Fill };
            panel.Controls.Add(txv89, 3, 29);

            TextBox txv90 = new TextBox() { Name = "txvPduMemberApprovalDate5", Dock = DockStyle.Fill };
            panel.Controls.Add(txv90, 4, 29);

            //
            TextBox txv91 = new TextBox() { Name = "txvPduMemberName6", Dock = DockStyle.Fill };
            panel.Controls.Add(txv91, 1, 30);

            TextBox txv92 = new TextBox() { Name = "txvPduMemberQualif6", Dock = DockStyle.Fill };
            panel.Controls.Add(txv92, 2, 30);

            TextBox txv93 = new TextBox() { Name = "txvPduMemberPosition6", Dock = DockStyle.Fill };
            panel.Controls.Add(txv93, 3, 30);

            TextBox txv94 = new TextBox() { Name = "txvPduMemberApprovalDate6", Dock = DockStyle.Fill };
            panel.Controls.Add(txv94, 4, 30);

            //
            Label labela1 = new Label() { Text = "Does the PDU have a list of Pre-Qualified Providers?", Dock = DockStyle.Fill };
            panel.Controls.Add(labela1, 1, 31);

            ComboBox cbxa2 = new ComboBox() { Name = "cbxHasListOfProviders" };
            cbxa2.Items.Add("Select score");
            cbxa2.Items.Add("Yes");
            cbxa2.Items.Add("No");
            cbxa2.Items.Add("Na");
            cbxa2.SelectedIndex = 0;
            cbxa2.SelectedIndexChanged += eventHandlerCalcAvgScore;
            panel.Controls.Add(cbxa2, 2, 31);
            panel.SetColumnSpan(cbxa2, 3);

            
            Label labela3 = new Label() { Text = "Does the PDU have an approved consolidated  Procurement Plan?", Dock = DockStyle.Fill };
            panel.Controls.Add(labela3, 1, 32);

            ComboBox cbxa4 = new ComboBox() { Name = "cbxHasProcPlan" };
            cbxa4.Items.Add("Select score");
            cbxa4.Items.Add("Yes");
            cbxa4.Items.Add("No");
            cbxa4.Items.Add("Na");
            cbxa4.SelectedIndex = 0;
            panel.Controls.Add(cbxa4, 2, 32);
            panel.SetColumnSpan(cbxa4, 3);

            //
            Label labela5 = new Label() { Text = "Does the PDU use framework contracts for repetitively procured items?", Dock = DockStyle.Fill };
            panel.Controls.Add(labela5, 1, 33);

            ComboBox cbxa6 = new ComboBox() { Name = "cbxUsesFrameworkContracts" };
            cbxa6.Items.Add("Select score");
            cbxa6.Items.Add("Yes");
            cbxa6.Items.Add("No");
            cbxa6.Items.Add("Na");
            cbxa6.SelectedIndex = 0;
            panel.Controls.Add(cbxa6, 2, 33);
            panel.SetColumnSpan(cbxa6, 3);

            //
            Label labela7 = new Label() { Text = "Do individual cases have the standard procurement reference numbering?", Dock = DockStyle.Fill };
            panel.Controls.Add(labela7, 1, 34);

            ComboBox cbxa8 = new ComboBox() { Name = "cbxIndCaseHaveProcRefNo" };
            cbxa8.Items.Add("Select score");
            cbxa8.Items.Add("Yes");
            cbxa8.Items.Add("No");
            cbxa8.Items.Add("Na");
            cbxa8.SelectedIndex = 0;
            panel.Controls.Add(cbxa8, 2, 34);
            panel.SetColumnSpan(cbxa8, 3);

            //
            Label labela9 = new Label() { Text = "Does PDU use the abridged version of the procurement adverts?", Dock = DockStyle.Fill };
            panel.Controls.Add(labela9, 1, 35);

            ComboBox cbxa10 = new ComboBox() { Name = "cbxUsesBridgedVersionOfProcAds" };
            cbxa10.Items.Add("Select score");
            cbxa10.Items.Add("Yes");
            cbxa10.Items.Add("No");
            cbxa10.Items.Add("Na");
            cbxa10.SelectedIndex = 0;
            cbxa10.SelectedIndexChanged += eventHandlerCalcAvgScore;
            panel.Controls.Add(cbxa10, 2, 35);
            panel.SetColumnSpan(cbxa10, 3);

            //
            Label labela11 = new Label() { Text = "Internal Audit", Dock = DockStyle.Fill };
            panel.Controls.Add(labela11, 0, 36);
            panel.SetRowSpan(labela11, 2);

            Label labela12 = new Label() { Text = "Name of the Internal Auditors", Dock = DockStyle.Fill };
            panel.Controls.Add(labela12, 1, 36);

            TextBox txva13 = new TextBox() { Name = "txvNameInternalAuditors", Dock = DockStyle.Fill };
            panel.Controls.Add(txva13, 2, 36);
            panel.SetColumnSpan(txva13, 3);

            //
            Label labela14 = new Label() { Text = "Does the Internal Auditor have a clear understanding of his/her role in the procurement and disposal process?", Dock = DockStyle.Fill };
            panel.Controls.Add(labela14, 1, 37);

            ComboBox cbxa15 = new ComboBox() { Name = "cbxAuditorUnderstandsProcRole" };
            cbxa15.Items.Add("Select score");
            cbxa15.Items.Add("Yes");
            cbxa15.Items.Add("No");
            cbxa15.Items.Add("Na");
            cbxa15.SelectedIndex = 0;
            cbxa15.SelectedIndexChanged += eventHandlerCalcAvgScore;
            panel.Controls.Add(cbxa15, 2, 37);
            panel.SetColumnSpan(cbxa15, 3);


            //
            Label labela16 = new Label() { Text = "Reporting to PPDA", Dock = DockStyle.Fill };
            panel.Controls.Add(labela16, 0, 38);
            panel.SetRowSpan(labela16, 16);

            Label labela17 = new Label() { Text = "Were the Procurement reports submitted to the Authority in time", Dock = DockStyle.Fill };
            panel.Controls.Add(labela17, 1, 38);

            ComboBox cbxa18 = new ComboBox() { Name = "cbxWereReportsSubmittedOnTime" };
            cbxa18.Items.Add("Select score");
            cbxa18.Items.Add("Yes");
            cbxa18.Items.Add("No");
            cbxa18.Items.Add("Na");
            cbxa18.SelectedIndex = 0;
            cbxa18.SelectedIndexChanged += eventHandlerCalcAvgScore;
            panel.Controls.Add(cbxa18, 2, 38);

            Label labela19 = new Label() { Text = " Date submitted to the Authority", Dock = DockStyle.Fill };
            panel.Controls.Add(labela19, 3, 38);
            panel.SetColumnSpan(labela19, 2);

            //
            TextBox txva20 = new TextBox() { Name = "txvReportName1", Dock = DockStyle.Fill };
            panel.Controls.Add(txva20, 2, 39);

            TextBox txva21 = new TextBox() { Name = "txvReportDate1", Dock = DockStyle.Fill };
            panel.Controls.Add(txva21, 3, 39);
            panel.SetColumnSpan(txva21, 2);

            //
            TextBox txva22 = new TextBox() { Name = "txvReportName2", Dock = DockStyle.Fill };
            panel.Controls.Add(txva22, 2, 40);

            TextBox txva23 = new TextBox() { Name = "txvReportDate2", Dock = DockStyle.Fill };
            panel.Controls.Add(txva23, 3, 40);
            panel.SetColumnSpan(txva23, 2);

            //
            TextBox txva24 = new TextBox() { Name = "txvReportName3", Dock = DockStyle.Fill };
            panel.Controls.Add(txva24, 2, 41);

            TextBox txva25 = new TextBox() { Name = "txvReportDate3", Dock = DockStyle.Fill };
            panel.Controls.Add(txva25, 3, 41);
            panel.SetColumnSpan(txva25, 2);

            //
            TextBox txva26 = new TextBox() { Name = "txvReportName4", Dock = DockStyle.Fill };
            panel.Controls.Add(txva26, 2, 42);

            TextBox txva27 = new TextBox() { Name = "txvReportDate4", Dock = DockStyle.Fill };
            panel.Controls.Add(txva27, 3, 42);
            panel.SetColumnSpan(txva27, 2);

            //
            TextBox txva28 = new TextBox() { Name = "txvReportName5", Dock = DockStyle.Fill };
            panel.Controls.Add(txva28, 2, 43);

            TextBox txva29 = new TextBox() { Name = "txvReportDate5", Dock = DockStyle.Fill };
            panel.Controls.Add(txva29, 3, 43);
            panel.SetColumnSpan(txva29, 2);

            //
            TextBox txva30 = new TextBox() { Name = "txvReportName6", Dock = DockStyle.Fill };
            panel.Controls.Add(txva30, 2, 44);

            TextBox txva31 = new TextBox() { Name = "txvReportDate6", Dock = DockStyle.Fill };
            panel.Controls.Add(txva31, 3, 44);
            panel.SetColumnSpan(txva31, 2);

            //
            TextBox txva32 = new TextBox() { Name = "txvReportName7", Dock = DockStyle.Fill };
            panel.Controls.Add(txva32, 2, 45);

            TextBox txva33 = new TextBox() { Name = "txvReportDate7", Dock = DockStyle.Fill };
            panel.Controls.Add(txva33, 3, 45);
            panel.SetColumnSpan(txva33, 2);

            //
            TextBox txva34 = new TextBox() { Name = "txvReportName8", Dock = DockStyle.Fill };
            panel.Controls.Add(txva34, 2, 46);

            TextBox txva35 = new TextBox() { Name = "txvReportDate8", Dock = DockStyle.Fill };
            panel.Controls.Add(txva35, 3, 46);
            panel.SetColumnSpan(txva35, 2);

            //
            TextBox txva36 = new TextBox() { Name = "txvReportName9", Dock = DockStyle.Fill };
            panel.Controls.Add(txva36, 2, 47);

            TextBox txva37 = new TextBox() { Name = "txvReportDate9", Dock = DockStyle.Fill };
            panel.Controls.Add(txva37, 3, 47);
            panel.SetColumnSpan(txva37, 2);

            //
            TextBox txva38 = new TextBox() { Name = "txvReportName10", Dock = DockStyle.Fill };
            panel.Controls.Add(txva38, 2, 48);

            TextBox txva39 = new TextBox() { Name = "txvReportDate10", Dock = DockStyle.Fill };
            panel.Controls.Add(txva39, 3, 48);
            panel.SetColumnSpan(txva39, 2);

            //
            TextBox txva40 = new TextBox() { Name = "txvReportName11", Dock = DockStyle.Fill };
            panel.Controls.Add(txva40, 2, 49);

            TextBox txva41 = new TextBox() { Name = "txvReportDate11", Dock = DockStyle.Fill };
            panel.Controls.Add(txva41, 3, 49);
            panel.SetColumnSpan(txva41, 2);

            //
            TextBox txva42 = new TextBox() { Name = "txvReportName12", Dock = DockStyle.Fill };
            panel.Controls.Add(txva42, 2, 50);

            TextBox txva43 = new TextBox() { Name = "txvReportDate12", Dock = DockStyle.Fill };
            panel.Controls.Add(txva43, 3, 50);
            panel.SetColumnSpan(txva43, 2);


            //
            Label labela44 = new Label() { Text = "Does the Entity upload data onto the GPP", Dock = DockStyle.Fill };
            panel.Controls.Add(labela44, 1, 51);

            ComboBox cbxa45 = new ComboBox() { Name = "cbxDoesEntityUploadDataOnGpp" };
            cbxa45.Items.Add("Select score");
            cbxa45.Items.Add("Yes");
            cbxa45.Items.Add("No");
            cbxa45.Items.Add("Na");
            cbxa45.SelectedIndex = 0;
            cbxa45.SelectedIndexChanged += eventHandlerCalcAvgScore;
            panel.Controls.Add(cbxa45, 2, 51);
            panel.SetColumnSpan(cbxa45, 3);

            //
            Label labela46 = new Label() { Text = "Are reports submitted in the  Standard formats?", Dock = DockStyle.Fill };
            panel.Controls.Add(labela46, 1, 52);

            ComboBox cbxa47 = new ComboBox() { Name = "cbxReportsInStandardFormat" };
            cbxa47.Items.Add("Select score");
            cbxa47.Items.Add("Yes");
            cbxa47.Items.Add("No");
            cbxa47.Items.Add("Na");
            cbxa47.SelectedIndex = 0;
            panel.Controls.Add(cbxa47, 2, 52);
            panel.SetColumnSpan(cbxa47, 3);

            //
            Label labela48 = new Label() { Text = "Are queries by PPDA on reports submitted satisfactorily resolved?", Dock = DockStyle.Fill };
            panel.Controls.Add(labela48, 1, 53);

            ComboBox cbxa49 = new ComboBox() { Name = "cbxPpdaQueriesResolved"};
            cbxa49.Items.Add("Select score");
            cbxa49.Items.Add("Yes");
            cbxa49.Items.Add("No");
            cbxa49.Items.Add("Na");
            cbxa49.SelectedIndex = 0;
            panel.Controls.Add(cbxa49, 2, 53);
            panel.SetColumnSpan(cbxa49, 3);


            //
            Label labela50 = new Label() { Text = "Disposal", Dock = DockStyle.Fill };
            panel.Controls.Add(labela50, 0, 54);

            Label labela51 = new Label() { Text = "Has the Entity disposed off its obsolete items?", Dock = DockStyle.Fill };
            panel.Controls.Add(labela51, 1, 54);

            ComboBox cbxa52 = new ComboBox() { Name = "cbxHasEntityDisposedOff" };
            cbxa52.Items.Add("Select score");
            cbxa52.Items.Add("Yes");
            cbxa52.Items.Add("No");
            cbxa52.Items.Add("Na");
            cbxa52.SelectedIndex = 0;
            cbxa52.SelectedIndexChanged += eventHandlerCalcAvgScore;
            panel.Controls.Add(cbxa52, 2, 54);
            panel.SetColumnSpan(cbxa52, 3);


            //
            Label labela53 = new Label() { Text = "Average Procurement Structure Score", Dock = DockStyle.Fill };
            panel.Controls.Add(labela53, 0, 55);
            panel.SetColumnSpan(labela53, 4);

            TextBox txva54 = new TextBox() { Dock = DockStyle.Fill , Name = "txvAvgProcStructureScore"};
            panel.Controls.Add(txva54, 1, 55);


            //
            Label labela55 = new Label() { Text = "Reviewed By", Dock = DockStyle.Fill };
            panel.Controls.Add(labela55, 0, 56);

            TextBox txva56 = new TextBox() { Dock = DockStyle.Fill, Name = "txvReviewedBy" };
            panel.Controls.Add(txva56, 1, 56);

            //
            Label labela57 = new Label() { Text = "Date of Review", Dock = DockStyle.Fill };
            panel.Controls.Add(labela57, 2, 56);
            panel.SetColumnSpan(labela57, 2);

            TextBox txva58 = new TextBox() { Dock = DockStyle.Fill, Name = "txvDateOfReview" };
            panel.Controls.Add(txva58, 4, 56);


            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 57);

            panel.Dock = DockStyle.Fill;
            panel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            panel.AutoScroll = true;
            panel.AutoSize = true;

            return panel;

        }

        internal static TableLayoutPanel GenerateSpendTable(EventHandler evtCalcCstSpendMacro, EventHandler evtCalcCstSpendMicro)
        {
           
            Label labelHeaderMicro = new Label() { Dock = DockStyle.Fill, Name = "labelHeaderMicro", Text = "Micro", TextAlign = ContentAlignment.MiddleLeft };

            Label labelHeaderMacro = new Label() { Dock = DockStyle.Fill, Name = "labelHeaderMacro", Text = "Macro", TextAlign = ContentAlignment.MiddleLeft };
            
            Label labelHeaderMonth = new Label() { Dock = DockStyle.Fill, Name = "labelHeaderMonth", Text = "Month", TextAlign = ContentAlignment.MiddleLeft };

            Label labelJan = new Label() { Dock = DockStyle.Fill, Name = "labelJan", Text = "January", TextAlign = ContentAlignment.MiddleLeft };
 
            Label labelFeb = new Label() { Dock = DockStyle.Fill, Name = "labelFeb", Text = "February", TextAlign = ContentAlignment.MiddleLeft };
            
            Label labelMar = new Label() { Dock = DockStyle.Fill, Name = "labelMar", Text = "March", TextAlign = ContentAlignment.MiddleLeft };
 
            Label labelApr = new Label() { Dock = DockStyle.Fill, Name = "labelApr", Text = "April", TextAlign = ContentAlignment.MiddleLeft };

            Label labelMay = new Label() { Dock = DockStyle.Fill, Name = "labelMay", Text = "May", TextAlign = ContentAlignment.MiddleLeft };

            Label labelJun = new Label() { Dock = DockStyle.Fill, Name = "labelJun", Text = "June", TextAlign = ContentAlignment.MiddleLeft };
 
            Label labelJul = new Label() { Dock = DockStyle.Fill, Name = "labelJul", Text = "July", TextAlign = ContentAlignment.MiddleLeft };
 
            Label labelAug = new Label() { Dock = DockStyle.Fill, Name = "labelAug", Text = "August", TextAlign = ContentAlignment.MiddleLeft };

            Label labelSept = new Label() { Dock = DockStyle.Fill, Name = "labelSept", Text = "September", TextAlign = ContentAlignment.MiddleLeft };
 
            Label labelOct = new Label() { Dock = DockStyle.Fill, Name = "labelOct", Text = "October", TextAlign = ContentAlignment.MiddleLeft };

            Label labelNov = new Label() { Dock = DockStyle.Fill, Name = "labelNov", Text = "November", TextAlign = ContentAlignment.MiddleLeft };

            Label labelDec = new Label() { Name = "labelDec", Text = "December", Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft };


            Label labelTotalExpenditure = new Label() { Dock = DockStyle.Fill, Name = "labelTotalExpenditure", Text = "Total Expenditure", TextAlign = ContentAlignment.MiddleLeft };
            
            Label labelLabelEra = new Label() { Dock = DockStyle.Fill, Name = "labelLabelEra", Text = "ERA house", TextAlign = ContentAlignment.MiddleLeft };

             
            TextBox txvMacroFeb = new TextBox(){ Name = "txvMacroFeb", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };          
            txvMacroFeb.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
            
            TextBox txvMacroJan = new TextBox(){ Name = "txvMacroJan", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroJan.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
            
            TextBox txvMacroMar = new TextBox() { Name = "txvMacroMar", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroMar.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
            
            TextBox txvMacroApr = new TextBox() { Name = "txvMacroApr", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroApr.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
             
            TextBox txvMacroMay = new TextBox() { Name = "txvMacroMay", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroMay.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
             
            TextBox txvMacroJun = new TextBox() { Name = "txvMacroJun", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroJun.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
            
            TextBox txvMacroJul = new TextBox() { Name = "txvMacroJul", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroJul.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
            
            TextBox txvMacroAug = new TextBox() { Name = "txvMacroAug", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroAug.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
            
            TextBox txvMacroSept = new TextBox() { Name = "txvMacroSept", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroSept.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
            
            TextBox txvMacroOct = new TextBox() { Name = "txvMacroOct", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroOct.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);
            
            TextBox txvMacroNov = new TextBox() { Name = "txvMacroNov", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroNov.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);

            TextBox txvMacroDec = new TextBox() { Name = "txvMacroDec", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMacroDec.TextChanged += new System.EventHandler(evtCalcCstSpendMacro);


            TextBox txvMicroJan = new TextBox() { Name = "txvMicroJan", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroJan.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
           
            TextBox txvMicroFeb = new TextBox() { Name = "txvMicroFeb", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroFeb.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
            
            TextBox txvMicroMar = new TextBox() { Name = "txvMicroMar", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroMar.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
        
            TextBox txvMicroApr = new TextBox() { Name = "txvMicroApr", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroApr.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
           
            TextBox txvMicroMay = new TextBox() { Name = "txvMicroMay", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroMay.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
           
            TextBox txvMicroJun = new TextBox() { Name = "txvMicroJun", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroJun.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
           
            TextBox txvMicroJul = new TextBox() { Name = "txvMicroJul", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroJul.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);

            TextBox txvMicroAug = new TextBox() { Name = "txvMicroAug", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroAug.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
             
            TextBox txvMicroSept = new TextBox() { Name = "txvMicroSept", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroSept.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
            
            TextBox txvMicroOct = new TextBox() { Name = "txvMicroOct", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroOct.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
            
            TextBox txvMicroNov = new TextBox() { Name = "txvMicroNov", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroNov.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);
            
            TextBox txvMicroDec = new TextBox() { Name = "txvMicroDec", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            txvMicroDec.TextChanged += new System.EventHandler(evtCalcCstSpendMicro);


            TextBox txvMacroTotal = new TextBox() { Name = "txvMacroTotal", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
            
            TextBox txvMicroTotal = new TextBox() { Name = "txvMicroTotal", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
                      
            TextBox txvTotalMacroAndMicro = new TextBox() { Name = "txvTotalMacroAndMicro", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
 

            TextBox txvEra = new TextBox() { Name = "txvEra", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
 
            Label labelPlan = new Label() { Name = "labelPlan", Text = "Plan", Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft };
                     
            TextBox txvPlanMacro = new TextBox() { Name = "txvPlanMacro", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
           
            TextBox txvPlanMicro = new TextBox() { Name = "txvPlanMicro", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };
           
            TextBox txvPlanImplementation = new TextBox() { Name = "txvPlanImplementation", TextAlign = HorizontalAlignment.Right, Anchor = AnchorStyles.Top | AnchorStyles.Left };

            Label labelPlanImplementation = new Label() { Name = "labelPlanImplementation", Text = "Plan implementation", Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft };

          

            // 
            // tableLayoutSpend
            // 
            TableLayoutPanel tableLayoutSpend = new TableLayoutPanel();
            
            tableLayoutSpend.Anchor = ((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
            tableLayoutSpend.AutoScroll = true;
            tableLayoutSpend.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutSpend.ColumnCount = 5;
            tableLayoutSpend.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tableLayoutSpend.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tableLayoutSpend.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tableLayoutSpend.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tableLayoutSpend.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tableLayoutSpend.Controls.Add(labelHeaderMicro, 2, 0);
            tableLayoutSpend.Controls.Add(labelHeaderMacro, 1, 0);
            tableLayoutSpend.Controls.Add(labelHeaderMonth, 0, 0);
            tableLayoutSpend.Controls.Add(labelJan, 0, 1);
            tableLayoutSpend.Controls.Add(labelFeb, 0, 2);
            tableLayoutSpend.Controls.Add(labelMar, 0, 3);
            tableLayoutSpend.Controls.Add(labelApr, 0, 4);
            tableLayoutSpend.Controls.Add(labelMay, 0, 5);
            tableLayoutSpend.Controls.Add(labelJun, 0, 6);
            tableLayoutSpend.Controls.Add(labelJul, 0, 7);
            tableLayoutSpend.Controls.Add(labelAug, 0, 8);
            tableLayoutSpend.Controls.Add(labelSept, 0, 9);
            tableLayoutSpend.Controls.Add(labelOct, 0, 10);
            tableLayoutSpend.Controls.Add(labelNov, 0, 11);
            tableLayoutSpend.Controls.Add(labelTotalExpenditure, 0, 13);
            tableLayoutSpend.Controls.Add(labelLabelEra, 0, 15);
            tableLayoutSpend.Controls.Add(txvMacroFeb, 1, 2);
            tableLayoutSpend.Controls.Add(txvMacroJan, 1, 1);
            tableLayoutSpend.Controls.Add(txvMacroMar, 1, 3);
            tableLayoutSpend.Controls.Add(txvMacroApr, 1, 4);
            tableLayoutSpend.Controls.Add(txvMacroMay, 1, 5);
            tableLayoutSpend.Controls.Add(txvMacroJun, 1, 6);
            tableLayoutSpend.Controls.Add(txvMacroJul, 1, 7);
            tableLayoutSpend.Controls.Add(txvMacroAug, 1, 8);
            tableLayoutSpend.Controls.Add(txvMacroSept, 1, 9);
            tableLayoutSpend.Controls.Add(txvMacroOct, 1, 10);
            tableLayoutSpend.Controls.Add(txvMacroNov, 1, 11);
            tableLayoutSpend.Controls.Add(txvMicroJan, 2, 1);
            tableLayoutSpend.Controls.Add(txvMicroFeb, 2, 2);
            tableLayoutSpend.Controls.Add(txvMicroMar, 2, 3);
            tableLayoutSpend.Controls.Add(txvMicroApr, 2, 4);
            tableLayoutSpend.Controls.Add(txvMicroMay, 2, 5);
            tableLayoutSpend.Controls.Add(txvMicroJun, 2, 6);
            tableLayoutSpend.Controls.Add(txvMicroJul, 2, 7);
            tableLayoutSpend.Controls.Add(txvMicroAug, 2, 8);
            tableLayoutSpend.Controls.Add(txvMicroSept, 2, 9);
            tableLayoutSpend.Controls.Add(txvMicroOct, 2, 10);
            tableLayoutSpend.Controls.Add(txvMicroNov, 2, 11);
            tableLayoutSpend.Controls.Add(labelDec, 0, 12);
            tableLayoutSpend.Controls.Add(txvMacroDec, 1, 12);
            tableLayoutSpend.Controls.Add(txvMicroDec, 2, 12);
            tableLayoutSpend.Controls.Add(txvMacroTotal, 1, 13);
            tableLayoutSpend.Controls.Add(txvMicroTotal, 2, 13);
            tableLayoutSpend.Controls.Add(txvTotalMacroAndMicro, 3, 13);
            tableLayoutSpend.Controls.Add(txvEra, 1, 15);
            tableLayoutSpend.Controls.Add(labelPlan, 0, 16);
            tableLayoutSpend.Controls.Add(txvPlanMacro, 1, 16);
            tableLayoutSpend.Controls.Add(txvPlanMicro, 2, 16);
            tableLayoutSpend.Controls.Add(txvPlanImplementation, 3, 16);
            tableLayoutSpend.Controls.Add(labelPlanImplementation, 4, 16);
            tableLayoutSpend.Location = new System.Drawing.Point(13, 13);
            tableLayoutSpend.Name = "tableLayoutSpend";
            tableLayoutSpend.RowCount = 17;
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutSpend.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutSpend.Size = new System.Drawing.Size(1090, 500);

            return tableLayoutSpend;

        }
        
    }

}

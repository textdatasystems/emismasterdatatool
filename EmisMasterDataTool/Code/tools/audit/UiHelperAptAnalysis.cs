﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code
{
    public class UiHelperAptAnalysis
    {

        public IAptAnalyisUpdater iAptAnalyisUpdater { get; set; }

        public  TableLayoutPanel GenerateProcessAnalyisTable()
        {
            
            int COLUMN_COUNT = 6;
            int ROW_COUNT = 80;
                        
            TableLayoutPanel panel = new TableLayoutPanel();

            //Now we will generate the table, setting up the row and column counts first
            panel.ColumnCount = COLUMN_COUNT;
            panel.RowCount = ROW_COUNT;

            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 5F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15F));
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));

            //section 1

            Label label = new Label() { Text = "PROCUREMENT PROCESS AUDIT ANALYSIS", Dock = DockStyle.Fill };
            panel.Controls.Add(label, 1, 0);


            panel.Controls.Add(new Label() { Text = "9" }, 0, 1);
            panel.Controls.Add(new Label() { Text = "Compliance Analysis", Dock = DockStyle.Fill }, 1, 1);
            panel.Controls.Add(new Label() { Text = "Yes" }, 2, 1);
            panel.Controls.Add(new Label() { Text = "No" }, 3, 1);
            panel.Controls.Add(new Label() { Text = "Total" }, 4, 1);
            panel.Controls.Add(new Label() { Text = "Compliance Score (%)" }, 5, 1);

            
            panel.Controls.Add(new Label() { Text = "9.1" }, 0, 2);
            panel.Controls.Add(new Label() { Text = "Procurement planning and initiation", Dock = DockStyle.Fill }, 1, 2);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect91YesScore", Dock = DockStyle.Fill }, 2, 2);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect91NoScore", Dock = DockStyle.Fill }, 3, 2);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect91TotalScore", Dock = DockStyle.Fill }, 4, 2);
            TextBox txvAnalysisScore1 = new TextBox() { Text = "0", Name = "Sect91PercentScore", Dock = DockStyle.Fill };
            txvAnalysisScore1.TextChanged += iAptAnalyisUpdater.calculateProcurementComplianceLevel;
            panel.Controls.Add(txvAnalysisScore1, 5, 2);

            panel.Controls.Add(new Label() { Text = "9.2" }, 0, 3);
            panel.Controls.Add(new Label() { Text = "Bidding Document", Dock = DockStyle.Fill }, 1, 3);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect92YesScore", Dock = DockStyle.Fill }, 2, 3);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect92NoScore", Dock = DockStyle.Fill }, 3, 3);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect92TotalScore", Dock = DockStyle.Fill }, 4, 3);
            TextBox txvAnalysisScore2 = new TextBox() { Text = "0", Name = "Sect92PercentScore", Dock = DockStyle.Fill };
            txvAnalysisScore2.TextChanged += iAptAnalyisUpdater.calculateProcurementComplianceLevel;
            panel.Controls.Add(txvAnalysisScore2, 5, 3);

            panel.Controls.Add(new Label() { Text = "9.3" }, 0, 4);
            panel.Controls.Add(new Label() { Text = "Bidding", Dock = DockStyle.Fill }, 1, 4);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect93YesScore", Dock = DockStyle.Fill }, 2, 4);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect93NoScore", Dock = DockStyle.Fill }, 3, 4);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect93TotalScore", Dock = DockStyle.Fill }, 4, 4);
            TextBox txvAnalysisScore3 = new TextBox() { Text = "0", Name = "Sect93PercentScore", Dock = DockStyle.Fill };
            txvAnalysisScore3.TextChanged += iAptAnalyisUpdater.calculateProcurementComplianceLevel;
            panel.Controls.Add(txvAnalysisScore3, 5, 4);

            panel.Controls.Add(new Label() { Text = "9.4" }, 0, 5);
            panel.Controls.Add(new Label() { Text = "Bid Evaluation", Dock = DockStyle.Fill }, 1, 5);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect94YesScore", Dock = DockStyle.Fill }, 2, 5);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect94NoScore", Dock = DockStyle.Fill }, 3, 5);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect94TotalScore", Dock = DockStyle.Fill }, 4, 5);
            TextBox txvAnalysisScore4 = new TextBox() { Text = "0", Name = "Sect94PercentScore", Dock = DockStyle.Fill };
            txvAnalysisScore4.TextChanged += iAptAnalyisUpdater.calculateProcurementComplianceLevel;
            panel.Controls.Add(txvAnalysisScore4, 5, 5);

            panel.Controls.Add(new Label() { Text = "9.5" }, 0, 6);
            panel.Controls.Add(new Label() { Text = "Procurement Contracting", Dock = DockStyle.Fill }, 1, 6);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect95YesScore", Dock = DockStyle.Fill }, 2, 6);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect95NoScore", Dock = DockStyle.Fill }, 3, 6);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect95TotalScore", Dock = DockStyle.Fill }, 4, 6);
            TextBox txvAnalysisScore5 = new TextBox() { Text = "0", Name = "Sect95PercentScore", Dock = DockStyle.Fill };
            txvAnalysisScore5.TextChanged += iAptAnalyisUpdater.calculateProcurementComplianceLevel;
            panel.Controls.Add(txvAnalysisScore5, 5, 6);

            panel.Controls.Add(new Label() { Text = "9.6" }, 0, 7);
            panel.Controls.Add(new Label() { Text = "Contract Management", Dock = DockStyle.Fill }, 1, 7);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect96YesScore", Dock = DockStyle.Fill }, 2, 7);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect96NoScore", Dock = DockStyle.Fill }, 3, 7);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect96TotalScore", Dock = DockStyle.Fill }, 4, 7);
            TextBox txvAnalysisScore6 = new TextBox() { Text = "0", Name = "Sect96PercentScore", Dock = DockStyle.Fill };
            txvAnalysisScore6.TextChanged += iAptAnalyisUpdater.calculateProcurementComplianceLevel;
            panel.Controls.Add(txvAnalysisScore6, 5, 7);

            panel.Controls.Add(new Label() { Text = "9.7" }, 0, 8);
            panel.Controls.Add(new Label() { Text = "Records Keeping", Dock = DockStyle.Fill }, 1, 8);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect97YesScore", Dock = DockStyle.Fill }, 2, 8);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect97NoScore", Dock = DockStyle.Fill }, 3, 8);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect97TotalScore", Dock = DockStyle.Fill }, 4, 8);
            TextBox txvAnalysisScore7 = new TextBox() { Text = "0", Name = "Sect97PercentScore", Dock = DockStyle.Fill };
            txvAnalysisScore7.TextChanged += iAptAnalyisUpdater.calculateProcurementComplianceLevel;
            panel.Controls.Add(txvAnalysisScore7, 5, 8);
            
            panel.Controls.Add(new Label() { Text = "9.8" }, 0, 9);
            panel.Controls.Add(new Label() { Text = "Procurement Process Compliance Level", Dock = DockStyle.Fill }, 1, 9);
            TextBox txva = new TextBox() { Text = "0", Name = "Sect98PercentScore", Dock = DockStyle.Fill };
            txva.TextChanged += iAptAnalyisUpdater.autoFillCompAndPerfTabScores;
            panel.Controls.Add(txva, 5, 9);


            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 10);

            //section 2

            Label label1 = new Label() { Text = "PART B: PERFORMANCE ANALYSIS", Dock = DockStyle.Fill };
            panel.Controls.Add(label1, 1, 11); panel.SetColumnSpan(label1, 5);

            panel.Controls.Add(new Label() { Text = "10" }, 0, 12);
            panel.Controls.Add(new Label() { Text = "PROCESS DATES", Dock = DockStyle.Fill }, 1, 12);
            panel.Controls.Add(new Label() { Text = "Unit" }, 4, 12);
            panel.Controls.Add(new Label() { Text = "DATE (MM DD YYYY)", Dock = DockStyle.Fill }, 5, 12);

            panel.Controls.Add(new Label() { Text = "10.1" }, 0, 13);
            panel.Controls.Add(new Label() { Text = "Start date per procurement plan", Dock = DockStyle.Fill }, 1, 13);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect101TotalScore", Dock = DockStyle.Fill }, 4, 13);
            TextBox txv1 = new TextBox() { Text = "0", Name = "Sect101PercentScore", Dock = DockStyle.Fill };
            txv1.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv1, 5, 13);

            panel.Controls.Add(new Label() { Text = "10.2" }, 0, 14);
            panel.Controls.Add(new Label() { Text = "Date of initiation request", Dock = DockStyle.Fill }, 1, 14);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect102TotalScore", Dock = DockStyle.Fill }, 4, 14);
            TextBox txv2 = new TextBox() { Text = "0", Name = "Sect102PercentScore", Dock = DockStyle.Fill };
            txv2.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv2, 5, 14);

            panel.Controls.Add(new Label() { Text = "10.3" }, 0, 15);
            panel.Controls.Add(new Label() { Text = "Date of bid invitation notice", Dock = DockStyle.Fill }, 1, 15);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect103TotalScore", Dock = DockStyle.Fill }, 4, 15);
            TextBox txv3 = new TextBox() { Text = "0", Name = "Sect103PercentScore", Dock = DockStyle.Fill };
            txv3.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv3, 5, 15);

            panel.Controls.Add(new Label() { Text = "10.4" }, 0, 16);
            panel.Controls.Add(new Label() { Text = "Date of bid submission", Dock = DockStyle.Fill }, 1, 16);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect104TotalScore", Dock = DockStyle.Fill }, 4, 16);
            TextBox txv4 = new TextBox() { Text = "0", Name = "Sect104PercentScore", Dock = DockStyle.Fill };
            txv4.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv4, 5, 16);

            panel.Controls.Add(new Label() { Text = "10.5" }, 0, 17);
            panel.Controls.Add(new Label() { Text = "Date of bid evaluation report", Dock = DockStyle.Fill }, 1, 17);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect105TotalScore", Dock = DockStyle.Fill }, 4, 17);
            TextBox txv5 = new TextBox() { Text = "0", Name = "Sect105PercentScore", Dock = DockStyle.Fill };
            txv5.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv5, 5, 17);

            panel.Controls.Add(new Label() { Text = "10.6" }, 0, 18);
            panel.Controls.Add(new Label() { Text = "Date of contract award", Dock = DockStyle.Fill }, 1, 18);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect106TotalScore", Dock = DockStyle.Fill }, 4, 18);
            TextBox txv6 = new TextBox() { Text = "0", Name = "Sect106PercentScore", Dock = DockStyle.Fill };
            txv6.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv6, 5, 18);

            panel.Controls.Add(new Label() { Text = "10.7" }, 0, 19);
            panel.Controls.Add(new Label() { Text = "Date of notice of best evaluated bidder", Dock = DockStyle.Fill }, 1, 19);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect107TotalScore", Dock = DockStyle.Fill }, 4, 19);
            TextBox txv7 = new TextBox() { Text = "0", Name = "Sect107PercentScore", Dock = DockStyle.Fill };
            txv7.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv7, 5, 19);

            panel.Controls.Add(new Label() { Text = "10.8" }, 0, 20);
            panel.Controls.Add(new Label() { Text = "Date of expiry of NoBEB", Dock = DockStyle.Fill }, 1, 20);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect108TotalScore", Dock = DockStyle.Fill }, 4, 20);
            TextBox txv8 = new TextBox() { Text = "0", Name = "Sect108PercentScore", Dock = DockStyle.Fill };
            txv8.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv8, 5, 20);

            panel.Controls.Add(new Label() { Text = "10.9" }, 0, 21);
            panel.Controls.Add(new Label() { Text = "Date of contract signature", Dock = DockStyle.Fill }, 1, 21);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect109TotalScore", Dock = DockStyle.Fill }, 4, 21);
            TextBox txv9 = new TextBox() { Text = "0", Name = "Sect109PercentScore", Dock = DockStyle.Fill };
            txv9.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv9, 5, 21);

            panel.Controls.Add(new Label() { Text = "10.10" }, 0, 22);
            panel.Controls.Add(new Label() { Text = "Date or contract signature per procurement plan", Dock = DockStyle.Fill }, 1, 22);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect1010TotalScore", Dock = DockStyle.Fill }, 4, 22);
            TextBox txv10 = new TextBox() { Text = "0", Name = "Sect1010PercentScore", Dock = DockStyle.Fill };
            txv10.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv10, 5, 22);

            panel.Controls.Add(new Label() { Text = "10.11" }, 0, 23);
            panel.Controls.Add(new Label() { Text = "Contractual completion date", Dock = DockStyle.Fill }, 1, 23);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect1011TotalScore", Dock = DockStyle.Fill }, 4, 23);
            TextBox txv11 = new TextBox() { Text = "0", Name = "Sect1011PercentScore", Dock = DockStyle.Fill };
            txv11.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv11, 5, 23);

            panel.Controls.Add(new Label() { Text = "10.12" }, 0, 24);
            panel.Controls.Add(new Label() { Text = "Actual completion date", Dock = DockStyle.Fill }, 1, 24);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect1012TotalScore", Dock = DockStyle.Fill }, 4, 24);
            TextBox txv12 = new TextBox() { Text = "0", Name = "Sect1012PercentScore", Dock = DockStyle.Fill };
            txv12.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv12, 5, 24);

            panel.Controls.Add(new Label() { Text = "10.13" }, 0, 25);
            panel.Controls.Add(new Label() { Text = "Date of certification of invoice", Dock = DockStyle.Fill }, 1, 25);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect1013TotalScore", Dock = DockStyle.Fill }, 4, 25);
            TextBox txv13 = new TextBox() { Text = "0", Name = "Sect1013PercentScore", Dock = DockStyle.Fill };
            txv13.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv13, 5, 25);

            panel.Controls.Add(new Label() { Text = "10.14" }, 0, 26);
            panel.Controls.Add(new Label() { Text = "Date invoice paid", Dock = DockStyle.Fill }, 1, 26);
            panel.Controls.Add(new Label() { Text = "Date", Name = "Sect1014TotalScore", Dock = DockStyle.Fill }, 4, 26);
            TextBox txv14 = new TextBox() { Text = "0", Name = "Sect1014PercentScore", Dock = DockStyle.Fill };
            txv14.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txv14, 5, 26);


            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 27);

            panel.Controls.Add(new Label() { Text = "11" }, 0, 28);
            panel.Controls.Add(new Label() { Text = "PROCESS TIME ANALYSIS", Dock = DockStyle.Fill }, 1, 28);
            panel.Controls.Add(new Label() { Text = "Formula" }, 2, 28);
            panel.Controls.Add(new Label() { Text = "Unit" }, 4, 28);
            panel.Controls.Add(new Label() { Text = "Indicator" }, 5, 28);

            panel.Controls.Add(new Label() { Text = "11.1" }, 0, 29);
            panel.Controls.Add(new Label() { Text = "Initiation start delay time", Dock = DockStyle.Fill }, 1, 29);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 29);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect111TotalScore", Dock = DockStyle.Fill }, 4, 29);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect111PercentScore", Dock = DockStyle.Fill }, 5, 29);

            panel.Controls.Add(new Label() { Text = "11.2" }, 0, 30);
            panel.Controls.Add(new Label() { Text = "Requisition Time", Dock = DockStyle.Fill }, 1, 30);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 30);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect112TotalScore", Dock = DockStyle.Fill }, 4, 30);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect112PercentScore", Dock = DockStyle.Fill }, 5, 30);


            panel.Controls.Add(new Label() { Text = "11.3" }, 0, 31);
            panel.Controls.Add(new Label() { Text = "Bid Submission Period", Dock = DockStyle.Fill }, 1, 31);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 31);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect113TotalScore", Dock = DockStyle.Fill }, 4, 31);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect113PercentScore", Dock = DockStyle.Fill }, 5, 31);


            panel.Controls.Add(new Label() { Text = "11.4" }, 0, 32);
            panel.Controls.Add(new Label() { Text = "Bid Evaluation Time", Dock = DockStyle.Fill }, 1, 32);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 32);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect114TotalScore", Dock = DockStyle.Fill }, 4, 32);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect114PercentScore", Dock = DockStyle.Fill }, 5, 32);


            panel.Controls.Add(new Label() { Text = "11.5" }, 0, 33);
            panel.Controls.Add(new Label() { Text = "Contract Award Period", Dock = DockStyle.Fill }, 1, 33);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 33);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect115TotalScore", Dock = DockStyle.Fill }, 4, 33);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect115PercentScore", Dock = DockStyle.Fill }, 5, 33);

            panel.Controls.Add(new Label() { Text = "11.6" }, 0, 34);
            panel.Controls.Add(new Label() { Text = "NoBEB display period", Dock = DockStyle.Fill }, 1, 34);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 34);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect116TotalScore", Dock = DockStyle.Fill }, 4, 34);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect116PercentScore", Dock = DockStyle.Fill }, 5, 34);

            panel.Controls.Add(new Label() { Text = "11.7" }, 0, 35);
            panel.Controls.Add(new Label() { Text = "Contracting Time", Dock = DockStyle.Fill }, 1, 35);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 35);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect117TotalScore", Dock = DockStyle.Fill }, 4, 35);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect117PercentScore", Dock = DockStyle.Fill }, 5, 35);

            panel.Controls.Add(new Label() { Text = "11.8" }, 0, 36);
            panel.Controls.Add(new Label() { Text = "Actual Procurement Time", Dock = DockStyle.Fill }, 1, 36);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 36);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect118TotalScore", Dock = DockStyle.Fill }, 4, 36);
            TextBox txvTimo1 = new TextBox() { Text = "0", Name = "Sect118PercentScore", Dock = DockStyle.Fill };
           // txvTimo1.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txvTimo1, 5, 36);

            panel.Controls.Add(new Label() { Text = "11.9" }, 0, 37);
            panel.Controls.Add(new Label() { Text = "Planned Procurement Time", Dock = DockStyle.Fill }, 1, 37);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 37);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect119TotalScore", Dock = DockStyle.Fill }, 4, 37);
            TextBox txvTimo2 = new TextBox() { Text = "0", Name = "Sect119PercentScore", Dock = DockStyle.Fill };
           // txvTimo2.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txvTimo2, 5, 37);

            panel.Controls.Add(new Label() { Text = "11.10" }, 0, 38);
            panel.Controls.Add(new Label() { Text = "Procurement Time Overrun", Dock = DockStyle.Fill }, 1, 38);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 38);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect1110TotalScore", Dock = DockStyle.Fill }, 4, 38);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect1110PercentScore", Dock = DockStyle.Fill }, 5, 38);

            panel.Controls.Add(new Label() { Text = "11.11" }, 0, 39);
            panel.Controls.Add(new Label() { Text = "Contractual Completion Time", Dock = DockStyle.Fill }, 1, 39);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 39);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect1111TotalScore", Dock = DockStyle.Fill }, 4, 39);
            TextBox txvTimoA = new TextBox() { Text = "0", Name = "Sect1111PercentScore", Dock = DockStyle.Fill };
            //txvTimoA.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txvTimoA, 5, 39);

            panel.Controls.Add(new Label() { Text = "11.12" }, 0, 40);
            panel.Controls.Add(new Label() { Text = "Actual Completion Time", Dock = DockStyle.Fill }, 1, 40);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 40);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect1112TotalScore", Dock = DockStyle.Fill }, 4, 40);
            TextBox txvTimoB = new TextBox() { Text = "0", Name = "Sect1112PercentScore", Dock = DockStyle.Fill };
           // txvTimoB.TextChanged += iAptAnalyisUpdater.calculateAnaysisTabFieldValues;
            panel.Controls.Add(txvTimoB, 5, 40);

            panel.Controls.Add(new Label() { Text = "11.13" }, 0, 41);
            panel.Controls.Add(new Label() { Text = "Completion Time Overrun", Dock = DockStyle.Fill }, 1, 41);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 41);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect1113TotalScore", Dock = DockStyle.Fill }, 4, 41);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect1113PercentScore", Dock = DockStyle.Fill }, 5, 41);

            panel.Controls.Add(new Label() { Text = "11.14" }, 0, 42);
            panel.Controls.Add(new Label() { Text = "Contractual payment period in days", Dock = DockStyle.Fill }, 1, 42);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 42);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect1114TotalScore", Dock = DockStyle.Fill }, 4, 42);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect1114PercentScore", Dock = DockStyle.Fill }, 5, 42);

            panel.Controls.Add(new Label() { Text = "11.15" }, 0, 43);
            panel.Controls.Add(new Label() { Text = "Actual Payment Period in Days", Dock = DockStyle.Fill }, 1, 43);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 43);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect1115TotalScore", Dock = DockStyle.Fill }, 4, 43);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect1115PercentScore", Dock = DockStyle.Fill }, 5, 43);

            panel.Controls.Add(new Label() { Text = "11.16" }, 0, 44);
            panel.Controls.Add(new Label() { Text = "Payment Period Overrun", Dock = DockStyle.Fill }, 1, 44);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 44);
            panel.Controls.Add(new Label() { Text = "Days", Name = "Sect1116TotalScore", Dock = DockStyle.Fill }, 4, 44);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect1116PercentScore", Dock = DockStyle.Fill }, 5, 44);

            panel.Controls.Add(new Label() { Text = "11.17" }, 0, 45);
            panel.Controls.Add(new Label() { Text = "Procurement Time Overrun (%)", Dock = DockStyle.Fill }, 1, 45);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 45);
            panel.Controls.Add(new Label() { Text = "%", Name = "Sect1117TotalScore", Dock = DockStyle.Fill }, 4, 45);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect1117PercentScore", Dock = DockStyle.Fill }, 5, 45);

            panel.Controls.Add(new Label() { Text = "11.18" }, 0, 46);
            panel.Controls.Add(new Label() { Text = "Completion Time Overrun (%)", Dock = DockStyle.Fill }, 1, 46);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 46);
            panel.Controls.Add(new Label() { Text = "%", Name = "Sect1118TotalScore", Dock = DockStyle.Fill }, 4, 46);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect1118PercentScore", Dock = DockStyle.Fill }, 5, 46);

            panel.Controls.Add(new Label() { Text = "11.19" }, 0, 47);
            panel.Controls.Add(new Label() { Text = "Payment Period Overrun (%)", Dock = DockStyle.Fill }, 1, 47);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 47);
            panel.Controls.Add(new Label() { Text = "%", Name = "Sect1119TotalScore", Dock = DockStyle.Fill }, 4, 47);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect1119PercentScore", Dock = DockStyle.Fill }, 5, 47);

            panel.Controls.Add(new Label() { Text = "11.20" }, 0, 48);
            panel.Controls.Add(new Label() { Text = "Procure Ratio", Dock = DockStyle.Fill }, 1, 48);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 48);
            panel.Controls.Add(new Label() { Text = "Ratio", Name = "Sect1120TotalScore", Dock = DockStyle.Fill }, 4, 48);
            TextBox txvb = new TextBox() { Text = "0", Name = "Sect1120PercentScore", Dock = DockStyle.Fill };
            txvb.TextChanged += iAptAnalyisUpdater.autoFillCompAndPerfTabScores;
            panel.Controls.Add(txvb, 5, 48);

            panel.Controls.Add(new Label() { Text = "11.21" }, 0, 49);
            panel.Controls.Add(new Label() { Text = "Completion Ratio", Dock = DockStyle.Fill }, 1, 49);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 49);
            panel.Controls.Add(new Label() { Text = "Ratio", Name = "Sect1121TotalScore", Dock = DockStyle.Fill }, 4, 49);
            TextBox txvc = new TextBox() { Text = "0", Name = "Sect1121PercentScore", Dock = DockStyle.Fill };
            txvc.TextChanged += iAptAnalyisUpdater.autoFillCompAndPerfTabScores;
            panel.Controls.Add(txvc, 5, 49);

            panel.Controls.Add(new Label() { Text = "11.22" }, 0, 50);
            panel.Controls.Add(new Label() { Text = "Payment Ratio", Dock = DockStyle.Fill }, 1, 50);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 50);
            panel.Controls.Add(new Label() { Text = "Ratio", Name = "Sect1122TotalScore", Dock = DockStyle.Fill }, 4, 50);
            TextBox txvd = new TextBox() { Text = "0", Name = "Sect1122PercentScore", Dock = DockStyle.Fill };
            txvd.TextChanged += iAptAnalyisUpdater.autoFillCompAndPerfTabScores;
            panel.Controls.Add(txvd, 5, 50);



            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 51);

            panel.Controls.Add(new Label() { Text = "12" }, 0, 52);
            panel.Controls.Add(new Label() { Text = "COMPETITION ANALYSIS", Dock = DockStyle.Fill }, 1, 52);
            panel.Controls.Add(new Label() { Text = "Formula" }, 2, 52);
            panel.Controls.Add(new Label() { Text = "Unit" }, 4, 52);
            panel.Controls.Add(new Label() { Text = "Indicator" }, 5, 52);

            panel.Controls.Add(new Label() { Text = "12.1" }, 0, 53);
            panel.Controls.Add(new Label() { Text = "Number of bidders invited to bid", Dock = DockStyle.Fill }, 1, 53);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 53);
            panel.Controls.Add(new Label() { Text = "No.", Name = "Sect121TotalScore", Dock = DockStyle.Fill }, 4, 53);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect121PercentScore", Dock = DockStyle.Fill }, 5, 53);

            panel.Controls.Add(new Label() { Text = "12.2" }, 0, 54);
            panel.Controls.Add(new Label() { Text = "Number of received bids", Dock = DockStyle.Fill }, 1, 54);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 54);
            panel.Controls.Add(new Label() { Text = "No.", Name = "Sect122TotalScore", Dock = DockStyle.Fill }, 4, 54);
            TextBox txve = new TextBox() { Text = "0", Name = "Sect122PercentScore", Dock = DockStyle.Fill };
            txve.TextChanged += iAptAnalyisUpdater.autoFillCompAndPerfTabScores;
            panel.Controls.Add(txve, 5, 54);

            panel.Controls.Add(new Label() { Text = "12.3" }, 0, 55);
            panel.Controls.Add(new Label() { Text = "Number of bids passed preliminary examination", Dock = DockStyle.Fill }, 1, 55);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 55);
            panel.Controls.Add(new Label() { Text = "No.", Name = "Sect123TotalScore", Dock = DockStyle.Fill }, 4, 55);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect123PercentScore", Dock = DockStyle.Fill }, 5, 55);

            panel.Controls.Add(new Label() { Text = "12.4" }, 0, 56);
            panel.Controls.Add(new Label() { Text = "Number of technically responsive bids", Dock = DockStyle.Fill }, 1, 56);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 56);
            panel.Controls.Add(new Label() { Text = "No.", Name = "Sect124TotalScore", Dock = DockStyle.Fill }, 4, 56);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect124PercentScore", Dock = DockStyle.Fill }, 5, 56);

            panel.Controls.Add(new Label() { Text = "12.5" }, 0, 57);
            panel.Controls.Add(new Label() { Text = "Bid Submission Rate", Dock = DockStyle.Fill }, 1, 57);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 57);
            panel.Controls.Add(new Label() { Text = "%", Name = "Sect125TotalScore", Dock = DockStyle.Fill }, 4, 57);
            TextBox txvf = new TextBox() { Text = "0", Name = "Sect125PercentScore", Dock = DockStyle.Fill };
            txvf.TextChanged += iAptAnalyisUpdater.autoFillCompAndPerfTabScores;
            panel.Controls.Add(txvf, 5, 57);

            panel.Controls.Add(new Label() { Text = "12.6" }, 0, 58);
            panel.Controls.Add(new Label() { Text = "Bid Responsive Rate", Dock = DockStyle.Fill }, 1, 58);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 58);
            panel.Controls.Add(new Label() { Text = "%", Name = "Sect126TotalScore", Dock = DockStyle.Fill }, 4, 58);
            TextBox txvg = new TextBox() { Text = "0", Name = "Sect126PercentScore", Dock = DockStyle.Fill };
            txvg.TextChanged += iAptAnalyisUpdater.autoFillCompAndPerfTabScores;
            panel.Controls.Add(txvg, 5, 58);


            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 59);

            panel.Controls.Add(new Label() { Text = "13" }, 0, 60);
            panel.Controls.Add(new Label() { Text = "COST ANALYSIS", Dock = DockStyle.Fill }, 1, 60);
            panel.Controls.Add(new Label() { Text = "Formula" }, 2, 60);
            panel.Controls.Add(new Label() { Text = "Unit" }, 4, 60);
            panel.Controls.Add(new Label() { Text = "" }, 5, 60);

            panel.Controls.Add(new Label() { Text = "13.1" }, 0, 61);
            panel.Controls.Add(new Label() { Text = "Procurement plan total cost inclusive VAT", Dock = DockStyle.Fill }, 1, 61);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 61);
            panel.Controls.Add(new Label() { Text = "UGX", Name = "Sect131TotalScore", Dock = DockStyle.Fill }, 4, 61);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect131PercentScore", Dock = DockStyle.Fill }, 5, 61);


            panel.Controls.Add(new Label() { Text = "13.2" }, 0, 62);
            panel.Controls.Add(new Label() { Text = "Accounting officer market price inclusive VAT", Dock = DockStyle.Fill }, 1, 62);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 62);
            panel.Controls.Add(new Label() { Text = "UGX", Name = "Sect132TotalScore", Dock = DockStyle.Fill }, 4, 62);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect132PercentScore", Dock = DockStyle.Fill }, 5, 62);

            panel.Controls.Add(new Label() { Text = "13.3" }, 0, 63);
            panel.Controls.Add(new Label() { Text = "Evaluated bid prices ranked lowest to highest:", Dock = DockStyle.Fill }, 1, 63);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 63);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect133TotalScore", Dock = DockStyle.Fill }, 4, 63);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect133PercentScore", Dock = DockStyle.Fill }, 5, 63);

            panel.Controls.Add(new Label() { Text = "13.3.1" }, 0, 64);
            panel.Controls.Add(new Label() { Text = "Name of Bidder", Dock = DockStyle.Fill }, 1, 64);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 64);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect1331TotalScore", Dock = DockStyle.Fill }, 4, 64);
            panel.Controls.Add(new Label() { Text = "Bid Price inclusive VAT", Name = "Sect1331PercentScore", Dock = DockStyle.Fill }, 5, 64);

            panel.Controls.Add(new Label() { Text = "13.3.2" }, 0, 65);
            panel.Controls.Add(new TextBox() { Text = "", Name = "txvBiddder1Name", Dock = DockStyle.Fill }, 1, 65);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 65);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect1332TotalScore", Dock = DockStyle.Fill }, 4, 65);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "txvBiddder1Price", Dock = DockStyle.Fill }, 5, 65);

            panel.Controls.Add(new Label() { Text = "13.3.3" }, 0, 66);
            panel.Controls.Add(new TextBox() { Text = "", Name = "txvBiddder2Name", Dock = DockStyle.Fill }, 1, 66);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 66);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect1333TotalScore", Dock = DockStyle.Fill }, 4, 66);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "txvBiddder2Price", Dock = DockStyle.Fill }, 5, 66);

            panel.Controls.Add(new Label() { Text = "13.3.4" }, 0, 67);
            panel.Controls.Add(new TextBox() { Text = "", Name = "txvBiddder3Name", Dock = DockStyle.Fill }, 1, 67);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 67);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect1334TotalScore", Dock = DockStyle.Fill }, 4, 67);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "txvBiddder3Price", Dock = DockStyle.Fill }, 5, 67);

            panel.Controls.Add(new Label() { Text = "13.3.5" }, 0, 68);
            panel.Controls.Add(new TextBox() { Text = "", Name = "txvBiddder4Name", Dock = DockStyle.Fill }, 1, 68);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 68);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect1335TotalScore", Dock = DockStyle.Fill }, 4, 68);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "txvBiddder4Price", Dock = DockStyle.Fill }, 5, 68);

            panel.Controls.Add(new Label() { Text = "13.3.6" }, 0, 69);
            panel.Controls.Add(new TextBox() { Text = "", Name = "txvBiddder5Name", Dock = DockStyle.Fill }, 1, 69);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 69);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect1336TotalScore", Dock = DockStyle.Fill }, 4, 69);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "txvBiddder5Price", Dock = DockStyle.Fill }, 5, 69);

            panel.Controls.Add(new Label() { Text = "13.3.7" }, 0, 70);
            panel.Controls.Add(new TextBox() { Text = "", Name = "txvBiddder6Name", Dock = DockStyle.Fill }, 1, 70);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 70);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect1337TotalScore", Dock = DockStyle.Fill }, 4, 70);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "txvBiddder6Price", Dock = DockStyle.Fill }, 5, 70);

            panel.Controls.Add(new Label() { Text = "13.3.8" }, 0, 71);
            panel.Controls.Add(new TextBox() { Text = "", Name = "txvBiddder7Name", Dock = DockStyle.Fill }, 1, 71);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 71);
            panel.Controls.Add(new Label() { Text = "", Name = "Sect1338TotalScore", Dock = DockStyle.Fill }, 4, 71);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "txvBiddder7Price", Dock = DockStyle.Fill }, 5, 71);


            panel.Controls.Add(new Label() { Text = "13.4" }, 0, 72);
            panel.Controls.Add(new Label() { Text = "Total contract award price inclusive VAT", Dock = DockStyle.Fill }, 1, 72);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 72);
            panel.Controls.Add(new Label() { Text = "UGX", Name = "Sect134TotalScore", Dock = DockStyle.Fill }, 4, 72);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect134PercentScore", Dock = DockStyle.Fill }, 5, 72);

            panel.Controls.Add(new Label() { Text = "13.5" }, 0, 73);
            panel.Controls.Add(new Label() { Text = "Final contract cost on completion", Dock = DockStyle.Fill }, 1, 73);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 73);
            panel.Controls.Add(new Label() { Text = "UGX", Name = "Sect135TotalScore", Dock = DockStyle.Fill }, 4, 73);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect135PercentScore", Dock = DockStyle.Fill }, 5, 73);

            panel.Controls.Add(new Label() { Text = "13.6" }, 0, 74);
            panel.Controls.Add(new Label() { Text = "Cost overrun amount", Dock = DockStyle.Fill }, 1, 74);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 74);
            panel.Controls.Add(new Label() { Text = "UGX", Name = "Sect136TotalScore", Dock = DockStyle.Fill }, 4, 74);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect136PercentScore", Dock = DockStyle.Fill }, 5, 74);

            panel.Controls.Add(new Label() { Text = "13.7" }, 0, 75);
            panel.Controls.Add(new Label() { Text = "Budget variance amount", Dock = DockStyle.Fill }, 1, 75);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 75);
            panel.Controls.Add(new Label() { Text = "UGX", Name = "Sect137TotalScore", Dock = DockStyle.Fill }, 4, 75);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect137PercentScore", Dock = DockStyle.Fill }, 5, 75);

            panel.Controls.Add(new Label() { Text = "13.8" }, 0, 76);
            panel.Controls.Add(new Label() { Text = "Budget variance", Dock = DockStyle.Fill }, 1, 76);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 76);
            panel.Controls.Add(new Label() { Text = "%", Name = "Sect138TotalScore", Dock = DockStyle.Fill }, 4, 76);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect138PercentScore", Dock = DockStyle.Fill }, 5, 76);

            panel.Controls.Add(new Label() { Text = "13.9" }, 0, 77);
            panel.Controls.Add(new Label() { Text = "Cost overrun", Dock = DockStyle.Fill }, 1, 77);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 77);
            panel.Controls.Add(new Label() { Text = "%", Name = "Sect139TotalScore", Dock = DockStyle.Fill }, 4, 77);
            panel.Controls.Add(new TextBox() { Text = "0", Name = "Sect139PercentScore", Dock = DockStyle.Fill }, 5, 77);

            panel.Controls.Add(new Label() { Text = "13.10" }, 0, 78);
            panel.Controls.Add(new Label() { Text = "Plan Ratio", Dock = DockStyle.Fill }, 1, 78);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 78);
            panel.Controls.Add(new Label() { Text = "Ratio", Name = "Sect1310TotalScore", Dock = DockStyle.Fill }, 4, 78);
            TextBox txvh = new TextBox() { Text = "0", Name = "Sect1310PercentScore", Dock = DockStyle.Fill };
            txvh.TextChanged += iAptAnalyisUpdater.autoFillCompAndPerfTabScores;
            panel.Controls.Add(txvh, 5, 78);

            panel.Controls.Add(new Label() { Text = "13.11" }, 0, 79);
            panel.Controls.Add(new Label() { Text = "Cost Ratio", Dock = DockStyle.Fill }, 1, 79);
            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 2, 79);
            panel.Controls.Add(new Label() { Text = "Ratio", Name = "Sect1311TotalScore", Dock = DockStyle.Fill }, 4, 79);
            TextBox txvi = new TextBox() { Text = "0", Name = "Sect1311PercentScore", Dock = DockStyle.Fill };
            txvi.TextChanged += iAptAnalyisUpdater.autoFillCompAndPerfTabScores;
            panel.Controls.Add(txvi, 5, 79);


            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 80);

            panel.Dock = DockStyle.Fill;
            panel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            panel.AutoScroll = true;
            panel.AutoSize = true;
            
            return panel;

        }

    }

}

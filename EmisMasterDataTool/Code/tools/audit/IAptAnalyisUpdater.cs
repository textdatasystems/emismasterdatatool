﻿using Processor.Entities.PM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmisTool.Code
{
    public interface IAptAnalyisUpdater
    {

        void updateSectionScoresInAnalyisTab(SectionEvaluation section);
        void autofillAnalysisTabTextFieldBasedOnEvalItemValue(SectionItemEvaluation sectionItem);
        void calculateAnaysisTabFieldValues(object sender, System.EventArgs e);
        void autoFillCompAndPerfTabScores(object sender, System.EventArgs e);
        void calculateProcurementComplianceLevel(object sender, System.EventArgs e);
        void loadExceptionsBasedOnExceptionSectionSelected(object sender, System.EventArgs e);

    }

}

﻿using Processor.Entities.PM.systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmisTool.Code
{
    public interface IAstAnalyisUpdater
    {
        void updateAstSectionScoresInAnalyisTab(SectionEvaluationAst section);
        void autofillAstAnalysisTabTextFieldBasedOnEvalItemValue(SectionItemEvaluationAst sectionItem);
        void loadExceptionsBasedOnExceptionSectionSelected(object sender, System.EventArgs e);

    }

}

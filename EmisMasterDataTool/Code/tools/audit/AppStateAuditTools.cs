﻿using EmisTool.Code.ui.custom;
using Processor.Entities.PM;
using Processor.Entities.PM.disposal;
using Processor.Entities.PM.systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code
{

    public class AppStateAuditTools
    {

        public IAptAnalyisUpdater iAptAnalyisUpdater { get; set; }
        public IAstAnalyisUpdater iAstAnalyisUpdater { get; set; }
        public IAdtAnalyisUpdater iAdtAnalyisUpdater { get; set; }

        public static string TAB_CBAS_CURRENT_ACTIVITY = "0";

        public static List<SectionEvaluation> auditProcessToolSectionEvaluations = new List<SectionEvaluation>();

        public static List<SectionEvaluationAst> auditSystemsToolSectionEvaluations = new List<SectionEvaluationAst>();

        public static List<SectionEvaluationAdt> auditDisposalToolSectionEvaluations = new List<SectionEvaluationAdt>();


        internal static bool sectionEvaluationAptExists(long sectionEmisId)
        {

            foreach(SectionEvaluation item in auditProcessToolSectionEvaluations)
            {
                if(item.sectionEmisId == sectionEmisId)
                {
                    return true;
                }
            }

            return false;

        }

        internal static bool sectionEvaluationAstExists(long sectionEmisId)
        {

            foreach (var item in auditSystemsToolSectionEvaluations)
            {
                if (item.sectionEmisId == sectionEmisId)
                {
                    return true;
                }
            }

            return false;

        }
        
        internal static bool sectionEvaluationAdtExists(long sectionEmisId)
        {

            foreach (var item in auditDisposalToolSectionEvaluations)
            {
                if (item.sectionEmisId == sectionEmisId)
                {
                    return true;
                }
            }

            return false;

        }

        public void updatePmAuditProcessToolState(object sender, System.EventArgs e)
        {
            
            Control control = (Control)sender;
            String controlName = control.Name;

            if(String.IsNullOrEmpty(controlName))
            {
                return;
            }

            //iterate through the and a control with this name, the set its value

            List<SectionEvaluation> updatedList = new List<SectionEvaluation>();

            foreach(SectionEvaluation section in auditProcessToolSectionEvaluations)
            {

                //update dictionary
                Dictionary<long, SectionItemEvaluation> updatedDictSectionItems = new Dictionary<long, SectionItemEvaluation>();

                foreach (KeyValuePair<long, SectionItemEvaluation> entry in section.sectionItemEvaluations)
                {

                    // do something with entry.Value or entry.Key

                    var item = entry.Value;

                    if (item.evaluationFieldName == controlName)
                    {

                        if(item.evaluationFieldType.ToLower() == "yesnona")
                        {
                            var checkedButton = control.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
                            item.evaluationFieldValue = checkedButton == null ? "" : checkedButton.Text;


                            //update the total of the section
                            sumAPTSection(section.sectionEmisId);

                        }
                        else
                        {
                            String value = control.Text;
                            item.evaluationFieldValue = value;

                            //it's either a date or text field value that might be used in the process analysis
                            iAptAnalyisUpdater.autofillAnalysisTabTextFieldBasedOnEvalItemValue(item);
                        }
                        
                    }
                    else if (item.exceptionFieldName == controlName)
                    {
                        String value = control.Text;
                        item.exceptionFieldValue = value;
                    }
                    else if (item.findingFieldName == controlName)
                    {
                        String value = control.Text;
                        item.findingFieldValue = value;
                    }
                    else if (item.managementLetterSectionFieldName == controlName)
                    {
                        if(control is ComboBox)
                        {
                            ComboBox cbx = (ComboBox)control;
                            int value = cbx.SelectedValue == null ? 0 : (int)cbx.SelectedValue;
                            item.managementLetterSectionFieldValue = value;
                        }                        
                    }
                    else if (item.managementLetterSectionItemFieldName == controlName)
                    {
                        if (control is ComboBox)
                        {
                            ComboBox cbx = (ComboBox)control;
                            int value = cbx.SelectedValue == null ? 0  : (int)cbx.SelectedValue;
                            item.managementLetterSectionItemFieldValue = value;
                        }
                    }

                    //insert into the updated dictonary
                    updatedDictSectionItems.Add(entry.Key, item);
                                      
                }

                //update the section details
                section.sectionItemEvaluations = updatedDictSectionItems;
                updatedList.Add(section);

            }
            
            //update app state
            AppStateAuditTools.auditProcessToolSectionEvaluations = updatedList;

        }

        public long sumAPTSection(long sectionEmisId)
        {
            long totalYes = 0;
            long totalNo = 0;

            foreach (SectionEvaluation section in auditProcessToolSectionEvaluations)
            {

                if(section.sectionEmisId == sectionEmisId)
                {

                    var items = section.sectionItemEvaluations;

                    foreach (KeyValuePair<long, SectionItemEvaluation> entry in section.sectionItemEvaluations)
                    {

                        var item = entry.Value;

                        //we found the matching colum
                        if (item.evaluationFieldType == "yesnona")
                        {
                            
                           
                            long itemValue = 0;

                            if(item.evaluationFieldValue == "Yes")
                            {
                                itemValue = item.yesRank;
                                totalYes += itemValue;
                            }
                            else if (item.evaluationFieldValue == "No")
                            {
                                itemValue = item.noRank;
                                totalNo += itemValue;
                            }
                            else if (item.evaluationFieldValue == "Na")
                            {
                                itemValue = item.naRank;
                            }
                            
                            
                        }

                    }

                    section.sectionYesTotalValue = totalYes;
                    section.sectionNoTotalValue = totalNo;
                    //update the App State

                    updateAptSectionInAppState(section);

                }

            }
            
            var totalField = PanelPmAuditProcessTool.APT_SECTION_TOTAL_TXVS[sectionEmisId];
            totalField.Text = "YES SCORE " + totalYes.ToString() + ", NO SCORE " + totalNo.ToString();

            return totalYes;
        }

        public void updateAptSectionInAppState(SectionEvaluation section)
        {

            List<SectionEvaluation> sectionsUpdated = new List<SectionEvaluation>();

            foreach(var oldSec in auditProcessToolSectionEvaluations)
            {
                if(section.sectionEmisId == oldSec.sectionEmisId)
                {
                    sectionsUpdated.Add(section);
                }
                else
                {
                    sectionsUpdated.Add(oldSec);
                }
            }


            AppStateAuditTools.auditProcessToolSectionEvaluations = sectionsUpdated;

            //call the main UI to update the section score
            iAptAnalyisUpdater.updateSectionScoresInAnalyisTab(section);

        }

        public void updatePmAuditSystemsToolState(object sender, System.EventArgs e)
        {

            Control control = (Control)sender;
            String controlName = control.Name;

            if (String.IsNullOrEmpty(controlName))
            {
                return;
            }

            //iterate through the and a control with this name, the set its value

            List<SectionEvaluationAst> updatedList = new List<SectionEvaluationAst>();

            foreach (var section in auditSystemsToolSectionEvaluations)
            {

                //update dictionary
                Dictionary<long, SectionItemEvaluationAst> updatedDictSectionItems = new Dictionary<long, SectionItemEvaluationAst>();

                foreach (KeyValuePair<long, SectionItemEvaluationAst> entry in section.sectionItemEvaluations)
                {

                    // do something with entry.Value or entry.Key

                    var item = entry.Value;

                    if (item.evaluationFieldName == controlName)
                    {

                        if (item.evaluationFieldType.ToLower() == "yesnona")
                        {
                            var checkedButton = control.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
                            item.evaluationFieldValue = checkedButton == null ? "" : checkedButton.Text;


                            //update the total of the section
                            sumASTSection(section.sectionEmisId);

                        }
                        else
                        {
                            String value = control.Text;
                            item.evaluationFieldValue = value;

                            //it's either a date or text field value that might be used in the process analysis
                            iAstAnalyisUpdater.autofillAstAnalysisTabTextFieldBasedOnEvalItemValue(item);
                        }

                    }
                    else if (item.exceptionFieldName == controlName)
                    {
                        String value = control.Text;
                        item.exceptionFieldValue = value;
                    }
                    else if (item.findingFieldName == controlName)
                    {
                        String value = control.Text;
                        item.findingFieldValue = value;
                    }
                    else if (item.managementLetterSectionFieldName == controlName)
                    {
                        if (control is ComboBox)
                        {
                            ComboBox cbx = (ComboBox)control;
                            int value = cbx.SelectedValue == null ? 0 : (int)cbx.SelectedValue;
                            item.managementLetterSectionFieldValue = value;
                        }
                    }
                    else if (item.managementLetterSectionItemFieldName == controlName)
                    {
                        if (control is ComboBox)
                        {
                            ComboBox cbx = (ComboBox)control;
                            int value = cbx.SelectedValue == null ? 0 : (int)cbx.SelectedValue;
                            item.managementLetterSectionItemFieldValue = value;
                        }
                    }

                    //insert into the updated dictonary
                    updatedDictSectionItems.Add(entry.Key, item);

                }


                //update the section details
                section.sectionItemEvaluations = updatedDictSectionItems;
                updatedList.Add(section);

            }

            //update app state
            AppStateAuditTools.auditSystemsToolSectionEvaluations = updatedList;

        }

        public void updatePmAuditDisposalToolState(object sender, System.EventArgs e)
        {

            long affectedEmisSectionID = 0;

            Control control = (Control)sender;
            String controlName = control.Name;

            if (String.IsNullOrEmpty(controlName))
            {
                return;
            }

            //iterate through the and find a control with this name, and then set its value

            List<SectionEvaluationAdt> updatedList = new List<SectionEvaluationAdt>();

            foreach (var section in auditDisposalToolSectionEvaluations)
            {

                //update dictionary
                Dictionary<long, SectionItemEvaluationAdt> updatedDictSectionItems = new Dictionary<long, SectionItemEvaluationAdt>();

                foreach (KeyValuePair<long, SectionItemEvaluationAdt> entry in section.sectionItemEvaluations)
                {

                    // do something with entry.Value or entry.Key

                    var item = entry.Value;

                    if (item.evaluationFieldName == controlName)
                    {

                        affectedEmisSectionID = section.sectionEmisId;

                        if (item.evaluationFieldType.ToLower() == "yesnona")
                        {

                            var checkedButton = control.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
                            item.evaluationFieldValue = checkedButton == null ? "" : checkedButton.Text;                                                     

                        }
                        else
                        {
                            String value = control.Text;
                            item.evaluationFieldValue = value;
                            
                        }

                    }
                    else if (item.exceptionFieldName == controlName)
                    {
                        String value = control.Text;
                        item.exceptionFieldValue = value;
                    }
                    else if (item.findingFieldName == controlName)
                    {
                        String value = control.Text;
                        item.findingFieldValue = value;
                    }
                    else if (item.managementLetterSectionFieldName == controlName)
                    {
                        if (control is ComboBox)
                        {
                            ComboBox cbx = (ComboBox)control;
                            int value = cbx.SelectedValue == null ? 0 : (int)cbx.SelectedValue;
                            item.managementLetterSectionFieldValue = value;
                        }
                    }
                    else if (item.managementLetterSectionItemFieldName == controlName)
                    {
                        if (control is ComboBox)
                        {
                            ComboBox cbx = (ComboBox)control;
                            int value = cbx.SelectedValue == null ? 0 : (int)cbx.SelectedValue;
                            item.managementLetterSectionItemFieldValue = value;
                        }
                    }

                    //insert into the updated dictonary
                    updatedDictSectionItems.Add(entry.Key, item);                                       

                }


                //update the section details
                section.sectionItemEvaluations = updatedDictSectionItems;
                updatedList.Add(section);

            }

            //update app state
            AppStateAuditTools.auditDisposalToolSectionEvaluations = updatedList;

            //update section data
            if(affectedEmisSectionID != 0)
            {
                sumADTSection(affectedEmisSectionID);
            }            

        }
        
        public long sumASTSection(long sectionEmisId)
        {
            long totalYes = 0;
            long totalNo = 0;

            foreach (SectionEvaluationAst section in auditSystemsToolSectionEvaluations)
            {

                if (section.sectionEmisId == sectionEmisId)
                {

                    var items = section.sectionItemEvaluations;

                    foreach (KeyValuePair<long, SectionItemEvaluationAst> entry in section.sectionItemEvaluations)
                    {

                        var item = entry.Value;

                        //we found the matching colum
                        if (item.evaluationFieldType == "yesnona")
                        {


                            long itemValue = 0;

                            if (item.evaluationFieldValue == "Yes")
                            {
                                itemValue = item.yesRank;
                                totalYes += itemValue;
                            }
                            else if (item.evaluationFieldValue == "No")
                            {
                                itemValue = item.noRank;
                                totalNo += itemValue;
                            }
                            else if (item.evaluationFieldValue == "Na")
                            {
                                itemValue = item.naRank;
                            }


                        }

                    }

                    section.sectionYesTotalValue = totalYes;
                    section.sectionNoTotalValue = totalNo;
                    //update the App State

                    updateAstSectionInAppState(section);

                }

            }

            var totalField = PanelPmAuditSystemsTool.AST_SECTION_TOTAL_TXVS[sectionEmisId];
            totalField.Text = "YES SCORE " + totalYes.ToString() + ", NO SCORE " + totalNo.ToString();

            return totalYes;
        }

        public long sumADTSection(long sectionEmisId)
        {

            long totalYes = 0;
            long totalNo = 0;

            foreach (SectionEvaluationAdt section in auditDisposalToolSectionEvaluations)
            {

                if (section.sectionEmisId == sectionEmisId)
                {

                    var items = section.sectionItemEvaluations;

                    foreach (KeyValuePair<long, SectionItemEvaluationAdt> entry in section.sectionItemEvaluations)
                    {

                        var item = entry.Value;

                        //we found the matching colum
                        if (item.evaluationFieldType == "yesnona")
                        {

                            long itemValue = 0;

                            if (item.evaluationFieldValue == "Yes")
                            {
                                itemValue = item.yesRank;
                                totalYes += itemValue;
                                
                            }
                            else if (item.evaluationFieldValue == "No")
                            {
                                itemValue = item.noRank;
                                totalNo += itemValue;
                            }
                            else if (item.evaluationFieldValue == "Na")
                            {
                                itemValue = item.naRank;
                            }


                        }

                    }

                    section.sectionYesTotalValue = totalYes;
                    section.sectionNoTotalValue = totalNo;

                    //update the App State
                    updateAdtSectionInAppState(section);

                }

            }

            var totalField = PanelPmAuditDisposalTool.ADT_SECTION_TOTAL_TXVS[sectionEmisId];
            totalField.Text = "YES SCORE " + totalYes.ToString() + ", NO SCORE " + totalNo.ToString();

            return totalYes;

        }
        
        public void updateAstSectionInAppState(SectionEvaluationAst section)
        {

            List<SectionEvaluationAst> sectionsUpdated = new List<SectionEvaluationAst>();

            foreach (var oldSec in auditSystemsToolSectionEvaluations)
            {
                if (section.sectionEmisId == oldSec.sectionEmisId)
                {
                    sectionsUpdated.Add(section);
                }
                else
                {
                    sectionsUpdated.Add(oldSec);
                }
            }
            
            AppStateAuditTools.auditSystemsToolSectionEvaluations = sectionsUpdated;

            //call the main UI to update the section score
            iAstAnalyisUpdater.updateAstSectionScoresInAnalyisTab(section);

        }

        public void updateAdtSectionInAppState(SectionEvaluationAdt section)
        {

            List<SectionEvaluationAdt> sectionsUpdated = new List<SectionEvaluationAdt>();

            foreach (var oldSec in auditDisposalToolSectionEvaluations)
            {
                if (section.sectionEmisId == oldSec.sectionEmisId)
                {
                    sectionsUpdated.Add(section);
                }
                else
                {
                    sectionsUpdated.Add(oldSec);
                }
            }

            AppStateAuditTools.auditDisposalToolSectionEvaluations = sectionsUpdated;

            //call the main UI to update the section score
            iAdtAnalyisUpdater.updateAdtSectionScoresInAnalyisTab(section);

        }


    }


}

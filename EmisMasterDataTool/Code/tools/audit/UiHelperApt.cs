﻿using EmisMasterDataTool.Code;
using EmisTool.Code.ui.custom;
using Processor.Entities;
using Processor.Entities.PM;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code
{
    public class UiHelperApt
    {

        public static int FieldCountPmAuditProcessTool = 0;
 
        public static TableLayoutPanel GenerateTableV1(
            AuditProcessToolSection toolSection, 
            out SectionEvaluation sectionEval, 
            AppStateAuditTools appState,
            List<ManagementLetterSection> managementLetterSections)
        {
                                   
            String sectionTableFieldName = "AuditProcessToolSec" + toolSection.Id;

            //initialize the out variable
            sectionEval = new SectionEvaluation();
            sectionEval.sectionEmisId = toolSection.Id;
            sectionEval.name = toolSection.name;
            sectionEval.sectionTableFieldName = sectionTableFieldName;
            var sectionItemsEvalList = sectionEval.sectionItemEvaluations;
                       
            
            //get the list of items in this section
            List<AuditProcessToolSectionItem> sectionItems = toolSection.items;


            int COLUMN_COUNT = 7; 
            int ROW_COUNT = sectionItems.Count + 2; // add the header row and the footer row


            TableLayoutPanel tableLayoutPanel = new TableLayoutPanel();

            //Now we will generate the table, setting up the row and column counts first
            tableLayoutPanel.ColumnCount = COLUMN_COUNT;
            tableLayoutPanel.RowCount = ROW_COUNT;


            for (int columnIndex = 0; columnIndex < COLUMN_COUNT; columnIndex++)
            {

                //First add a column

                float columnWidth = getColumnWidth(columnIndex);
                tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, columnWidth));

                for (int rowIndex = 0; rowIndex < ROW_COUNT; rowIndex++)
                {

                    bool isFooterRow = rowIndex == ROW_COUNT - 1;
                    bool isHeaderRow = rowIndex == 0;

                    //Next, add a row.  Only do this when once, when creating the first column
                    if (columnIndex == 0 && isHeaderRow || columnIndex == 0 && isFooterRow)
                    {
                        tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));
                    }
                    else if (columnIndex == 0)
                    {
                        tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 50));
                    }
                    

                    //generate the cell control
                    Control control;
                    if (isHeaderRow)
                    {
                        control = buildHeaderCellControlV1(columnIndex, toolSection);
                    }
                    else if (isFooterRow)
                    {
                        control = buildFoooterCellControl(columnIndex);

                        if(columnIndex == 2)
                        {
                            PanelPmAuditProcessTool.APT_SECTION_TOTAL_TXVS.Add(toolSection.Id, control);
                        }

                    }
                    else
                    {

                        AuditProcessToolSectionItem currentItem = sectionItems[rowIndex - 1];
                        control = buildBodyCellControlV1(columnIndex, currentItem, managementLetterSections, appState);


                        //begin logic for updating app state  
                        
                        //ensure that the item exists in the dictionary if not add
                        if (!sectionItemsEvalList.ContainsKey(currentItem.emisId))
                        {

                            var sectionEvalItem = new SectionItemEvaluation();
                            sectionEvalItem.sectionEmisId = toolSection.Id;
                            sectionEvalItem.sectionItemEmisId = currentItem.emisId;
                            sectionEvalItem.yesRank = currentItem.yesRank;
                            sectionEvalItem.noRank = currentItem.noRank;
                            sectionEvalItem.naRank = currentItem.naRank;
                            sectionEvalItem.evaluationFieldRole = currentItem.fieldRole;
                            sectionEvalItem.description = currentItem.description;

                            sectionItemsEvalList.Add(currentItem.emisId, sectionEvalItem);

                        }
                        

                        var oldSectionItemEval = sectionItemsEvalList[currentItem.emisId];
                        oldSectionItemEval.evaluationFieldRole = currentItem.fieldRole;
                        
                        //we now update the fields of the sectionEvaItem
                        var sectionEvalItemUpdated = updateSectionEvalItem(oldSectionItemEval, columnIndex, currentItem, control);

                        //update the item in the list
                        sectionItemsEvalList[sectionEvalItemUpdated.sectionEmisId] = sectionEvalItemUpdated;

                        //end logic for updating app state

                    }
                                                          

                    control.Leave += new EventHandler(appState.updatePmAuditProcessToolState);
                    tableLayoutPanel.Controls.Add(control, columnIndex, rowIndex);
                                      

                }

            }


            tableLayoutPanel.Dock = DockStyle.Fill;
            tableLayoutPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanel.AutoScroll = false;
            tableLayoutPanel.AutoSize = true;
            tableLayoutPanel.Name = sectionTableFieldName;
            
            return tableLayoutPanel;

        }

        private static SectionItemEvaluation updateSectionEvalItem(SectionItemEvaluation sectionEvalItemToUpdate, int columnIndex, AuditProcessToolSectionItem sectionItem, Control control)
        {

            int evaluationFieldIndex = 2;
            int findingFieldIndex = 3;
            int exceptionFieldIndex = 4;
            int managementLetterSectionFieldIndex = 5;
            int managementLetterSectionItemFieldIndex = 6;

            if (columnIndex == evaluationFieldIndex)
            {
                sectionEvalItemToUpdate.evaluationFieldName = control.Name;
                sectionEvalItemToUpdate.evaluationFieldType = sectionItem.responseType;
            }
            else if(columnIndex == findingFieldIndex)
            {
                sectionEvalItemToUpdate.findingFieldName = control.Name;
            }
            else if(columnIndex == exceptionFieldIndex)
            {
                sectionEvalItemToUpdate.exceptionFieldName = control.Name;
            }
            else if (columnIndex == managementLetterSectionFieldIndex)
            {
                sectionEvalItemToUpdate.managementLetterSectionFieldName = control.Name;
            }
            else if (columnIndex == managementLetterSectionItemFieldIndex)
            {
                sectionEvalItemToUpdate.managementLetterSectionItemFieldName = control.Name;
            }

            return sectionEvalItemToUpdate;

        }

        private static float getColumnWidth(int columnIndex)
        {

            if(columnIndex == 0)
            {
                return 50F;
            }
            else if(columnIndex == 1)
            {
                return 200F;
            }
            else if (columnIndex == 2)
            {
                return 170F;
            }
            else if (columnIndex == 3)
            {
                return 300F;
            }
            else if (columnIndex == 4)
            {
                return 280F;
            }
            else if (columnIndex == 5 || columnIndex == 6)
            {
                return 120F;
            }
            else
            {
                return 0;
            }

        }
          
    
        private static Control buildBodyCellControlV1(
            int columnIndex, AuditProcessToolSectionItem sectionItem,
            List<ManagementLetterSection> managementLetterSections, AppStateAuditTools appState)
        {

            Control control = new Control();

            FieldCountPmAuditProcessTool++;
            String fieldName = "AuditToolField" + FieldCountPmAuditProcessTool;
            

            if (columnIndex == 0)
            {
                Label labelRank = new Label() { Text = sectionItem.rank.ToString() };
                labelRank.Name = fieldName;
                return labelRank;
            }
            else if (columnIndex == 1)
            {
                String text = sectionItem.description;
                Label labelDesc = new Label() { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft, Text = text, AutoSize = true };
                labelDesc.Name = fieldName;

                //if it has important text show the text
                new ToolTip().SetToolTip(labelDesc, "More description");

                return labelDesc;
            }
            else if (columnIndex == 2)
            {
                //here we return a table layout                 
                Control evaluationFieldControl = buildResponseFieldControl(sectionItem.responseType);
                evaluationFieldControl.Name = fieldName;
                return evaluationFieldControl;
            }
            else if (columnIndex == 3 || columnIndex == 4)
            {

                RichTextBox textbox = new RichTextBox();
                textbox.Height = 50;
                textbox.Width = 350;
                textbox.Dock = DockStyle.Fill;
                textbox.Name = fieldName;
                return textbox;

            }
            else if (columnIndex == 5 )
            {

                ComboBox cbx = new ComboBox();
                cbx.Width = 120;
                cbx.Name = fieldName;
                cbx.DropDownStyle = ComboBoxStyle.DropDownList;
                cbx = WidgetHandler.populateManagementLetterSectionsDropdown(cbx, managementLetterSections);
                cbx.SelectedIndexChanged += appState.iAptAnalyisUpdater.loadExceptionsBasedOnExceptionSectionSelected;
                
                return cbx;

            }
            else if (columnIndex == 6)
            {

                ComboBox cbx = new ComboBox();
                cbx.Items.Add("Select option");
                cbx.Width = 120;
                cbx.Name = fieldName;                
                cbx.DropDownStyle = ComboBoxStyle.DropDownList;
                cbx.SelectedIndex = cbx.FindString("Select option");
                return cbx;

            }
            else
            {
                return new Label();
            }


        }

    
        private static Control buildFoooterCellControl(int columnIndex)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                return new Label();
            }
            else if (columnIndex == 1)
            {
                
                Label labelRank = new Label() { Text = "TOTAL", ForeColor = Color.DarkRed };
                return labelRank;
            }
            else if (columnIndex == 2)
            {
                TextBox textbox = new TextBox();
                textbox.Height = 25;
                textbox.Dock = DockStyle.Fill;
                return textbox;
            }
            else
            {
                return new Label(); 
            }


        }

        private static Control buildHeaderCellControlV1(int columnIndex, AuditProcessToolSection toolSection)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                Label sectionRankLabel = new Label() { Text = toolSection.rank.ToString(), ForeColor = Color.DarkBlue };
                return sectionRankLabel;
            }
            else if (columnIndex == 1)
            {
                Label sectionNameLabel = new Label() { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft,  Text = toolSection.name.ToUpper(), ForeColor = Color.DarkBlue };
                return sectionNameLabel;
            }
            else if (columnIndex == 2)
            {
                return new Label();
            }
            else if (columnIndex == 3)
            {
                Label sectionColumn4Label = new Label() { Dock = DockStyle.Fill, Text = "ROUGH NOTES", AutoSize = false, Width = 400, ForeColor = Color.DarkBlue };
                return sectionColumn4Label;
            }
            else if (columnIndex == 4)
            {
                Label sectionColumn5Label = new Label() {Dock = DockStyle.Fill, Text = "EXCEPTIONS TO INCLUDE IN FINAL REPORT", ForeColor = Color.DarkBlue };
                return sectionColumn5Label;
            }
            else if (columnIndex == 5)
            {
                Label sectionColumn6Label = 
                    new Label() { Text = "Exception Category", Dock = DockStyle.Fill,  ForeColor = Color.DarkBlue };
                return sectionColumn6Label;
            }
            else if (columnIndex == 6)
            {
                Label sectionColumn7Label = new Label() { Text = "Exception Subcategory", Dock = DockStyle.Fill, ForeColor = Color.DarkBlue };
                return sectionColumn7Label;
            }
            else
            {
                return new Label();
            }

        }

        private static Control buildHeaderCellControl(int columnIndex)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                Label sectionRankLabel = new Label() { Text = "1", ForeColor = Color.DarkBlue };
                return sectionRankLabel;
            }
            else if (columnIndex == 1)
            {
                Label sectionNameLabel = new Label() { Text = "SECTION A", ForeColor = Color.DarkBlue };
                return sectionNameLabel;
            }
            else if (columnIndex == 2)
            {
                return new Label();
            }
            else if (columnIndex == 3)
            {
                Label sectionColumn4Label = new Label() { Text = "AUDIT FINDINGS AND EVIDENCE", AutoSize = false, Width = 400, ForeColor = Color.DarkBlue };
                return sectionColumn4Label;
            }
            else if (columnIndex == 4)
            {
                Label sectionColumn5Label = new Label() { Text = "EXCEPTIONS", ForeColor = Color.DarkBlue };
                return sectionColumn5Label;
            }
            else
            {
                return new Label();
            }                  
     
        }

        private static Control buildResponseFieldControl(string fieldType)
        {

            if(fieldType == "yesnona")
            {

                TableLayoutPanel panel = new TableLayoutPanel();
                panel.ColumnCount = 3;
                panel.RowCount = 1;
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 34));
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33));
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33));

                panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));

                panel.Controls.Add(new RadioButton() { Text = "Yes" }, 0, 0);
                panel.Controls.Add(new RadioButton() { Text = "No" }, 1, 0);
                panel.Controls.Add(new RadioButton() { Text = "Na", Checked = true }, 2, 0);

                panel.Dock = DockStyle.Fill;
                return panel;

            }
            else if (fieldType == "date")
            {
                DateTimePicker datePicker = new DateTimePicker();
                datePicker.Height = 25;
                datePicker.Format = DateTimePickerFormat.Short;
                datePicker.Dock = DockStyle.Fill;
                return datePicker;
            }
            else
            {
                TextBox textbox = new TextBox();
                textbox.Height = 20;
                textbox.Dock = DockStyle.Fill;
                return textbox;
            }

        }


        public static TableLayoutPanel GenerateSection1Table(List<User> users)
        {

            int COLUMN_COUNT = 6;
            int ROW_COUNT = 17;

            TableLayoutPanel panel = new TableLayoutPanel();
            
            panel.ColumnCount = COLUMN_COUNT;
            panel.RowCount = ROW_COUNT;

            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 400F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 100F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 100F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 150F));
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));

            //section 1

            Label label = new Label() { Text = "PART A: COMPLIANCE CRITERIA", Dock = DockStyle.Fill };
            panel.Controls.Add(label, 1, 0);// panel.SetColumnSpan(label, 6);

       
            panel.Controls.Add(new Label() { Text = "1" }, 0, 1);
            panel.Controls.Add(new Label() { Text = "IDENTIFICATION", Dock = DockStyle.Fill }, 1, 1);


            panel.Controls.Add(new Label() { Text = "1.1" }, 0, 2);
            panel.Controls.Add(new Label() { Text = "Name of Entity", Dock = DockStyle.Fill }, 1, 2);
            panel.Controls.Add(new TextBox() { Name = "txvSec1EntityName", Dock = DockStyle.Fill }, 2, 2);
            panel.Controls.Add(new Label() { Text = "Sector",  Dock = DockStyle.Fill }, 3, 2);
            panel.Controls.Add(new TextBox() { Name = "txvSect1EntitySector", Dock = DockStyle.Fill }, 4, 2);


            panel.Controls.Add(new Label() { Text = "1.2" }, 0, 3);
            panel.Controls.Add(new Label() { Text = "Audit Period", Dock = DockStyle.Fill }, 1, 3);
            panel.Controls.Add(new TextBox() { Name = "txvSec1AuditPeriod", Dock = DockStyle.Fill }, 2, 3);
            panel.Controls.Add(new Label() { Text = "Reference No.", Dock = DockStyle.Fill }, 3, 3);
            panel.Controls.Add(new TextBox() { Name = "txvSec1ReferenceNo", Dock = DockStyle.Fill }, 4, 3);
                       

            panel.Controls.Add(new Label() { Text = "1.3" }, 0, 4);
            panel.Controls.Add(new Label() { Text = "Contract Description",  Dock = DockStyle.Fill }, 1, 4);
            panel.Controls.Add(new RichTextBox() { Name = "txvSec1ContractDesc", Height = 50, Dock = DockStyle.Fill }, 2, 4);
            

            panel.Controls.Add(new Label() { Text = "1.4" }, 0, 5);
            panel.Controls.Add(new Label() { Text = "Name of Provider", Dock = DockStyle.Fill }, 1, 5);
            panel.Controls.Add(new TextBox() { Name = "txvSec1NameOfProvider", Dock = DockStyle.Fill }, 2, 5);

            panel.Controls.Add(new Label() { Text = "1.5" }, 0, 6);
            panel.Controls.Add(new Label() { Text = "Total Contract Value Inclusive VAT", Dock = DockStyle.Fill }, 1, 6);
            panel.Controls.Add(new TextBox() { Name = "txvSec1TotalContractValue", Dock = DockStyle.Fill }, 2, 6);

            panel.Controls.Add(new Label() { Text = "1.6" }, 0, 7);
            panel.Controls.Add(new Label() { Text = "Type of provider", Dock = DockStyle.Fill }, 1, 7);
            ComboBox cbx1 = new ComboBox() {  Name = "cbxSect1Provider", Dock = DockStyle.Fill };        
            String[] items1 = {"Select Option", "National","Resident","Foreign"}; 
            cbx1.Items.AddRange(items1);
            cbx1.SelectedIndex = 0;
            panel.Controls.Add(cbx1, 2, 7);
            

            panel.Controls.Add(new Label() { Text = "1.7" }, 0, 8);
            panel.Controls.Add(new Label() { Text = "Process Stage", Dock = DockStyle.Fill }, 1, 8);
            ComboBox cbx2 = new ComboBox() { Name = "cbxSect1ProcessStage", Dock = DockStyle.Fill };
            String[] items2 = { "Select Option", "Contract", "Cancelled", "Implementation","Stalled","Completed" };
            cbx2.Items.AddRange(items2);
            cbx2.SelectedIndex = 0;
            panel.Controls.Add(cbx2, 2, 8);

            panel.Controls.Add(new Label() { Text = "1.8" }, 0, 9);
            panel.Controls.Add(new Label() { Text = "Procurement Method", Dock = DockStyle.Fill }, 1, 9);

            ComboBox cbx3 = new ComboBox() { Name = "cbxSect1ProcureMethod", Dock = DockStyle.Fill };
            String[] items3 = {
                "Select Option",
                "Open Domestic Bidding",
                "Open International Bidding",
                "Restricted Domestic Bidding",
                "Restricted International Bidding",
                "RFQ",
                "Selective Bidding",
                "Direct Procurement",
                "Micro",
                "NA"
            };
            cbx3.Items.AddRange(items3);
            cbx3.SelectedIndex = 0;
            panel.Controls.Add(cbx3, 2, 9);

            panel.Controls.Add(new Label() { Text = "1.9" }, 0, 10);
            panel.Controls.Add(new Label() { Text = "Consultant Selection Method", Dock = DockStyle.Fill }, 1, 10);        
            ComboBox cbx4 = new ComboBox() {  Name = "cbxSect1ConsultantSelectioMtd", Dock = DockStyle.Fill };
            String[] items4 = {
                "Select Option",
                "Quality and Cost Based Selection",
                "Quality Based Selection",
                "Fixed Budget Selection",
                "Least Cost Selection",
                "Consultant Qualification Selection",
                "NA"
            };
            cbx4.Items.AddRange(items4);
            cbx4.SelectedIndex = 0;
            panel.Controls.Add(cbx4, 2, 10);

            panel.Controls.Add(new Label() { Text = "1.10" }, 0, 11);
            panel.Controls.Add(new Label() { Text = "Procurement Category", Dock = DockStyle.Fill }, 1, 11);
            ComboBox cbx5 = new ComboBox() {  Name = "cbxSect1ProcureCategory", Dock = DockStyle.Fill };
            String[] items5 = { "Select Option", "Works", "Supplies", "Non Consultancy Services", "Consultancy Services", "NA" };
            cbx5.Items.AddRange(items5);
            cbx5.SelectedIndex = 0;
            panel.Controls.Add(cbx5, 2, 11);


            panel.Controls.Add(new Label() { Text = "1.11" }, 0, 12);
            panel.Controls.Add(new Label() { Text = "Bidder Invitation Method", Dock = DockStyle.Fill }, 1, 12);
            ComboBox cbx6 = new ComboBox() {  Name = "cbxSect1BidInviteMethd", Dock = DockStyle.Fill };
            String[] items6 = { "Select Option", "Bid Notice", "Prequalification", "Shortlist", "Direct", "Other" };
            cbx6.Items.AddRange(items6);
            cbx6.SelectedIndex = 0;
            panel.Controls.Add(cbx6, 2, 12);

            panel.Controls.Add(new Label() { Text = "1.12" }, 0, 13);
            panel.Controls.Add(new Label() { Text = "Bid Submission Method", Dock = DockStyle.Fill }, 1, 13);       
            ComboBox cbx7 = new ComboBox() {  Name = "cbxSect1BidSubmissionMtd", Dock = DockStyle.Fill };
            String[] items7 = { "Select Option", "One stage single Envelope", "One Stage Two envelope", "Two stage method" };           
            cbx7.Items.AddRange(items7);
            cbx7.SelectedIndex = 0;
            panel.Controls.Add(cbx7, 2, 13);


            panel.Controls.Add(new Label() { Text = "1.13" }, 0, 14);
            panel.Controls.Add(new Label() { Text = "Contract Type", Dock = DockStyle.Fill }, 1, 14);

            ComboBox cbx8 = new ComboBox() { Name = "cbxSect1ContractType", Dock = DockStyle.Fill };
            String[] items8 = {
                "Select Option",
                "Lumpsum",
                "Time Based",
                "Admeasurement",
                "Framework",
                "Percentage",
                "Reimbursable",
                "Target Price",
                "Retainer",
                "Success Fee",
                "Force Account",
                "Other"
            };
            cbx8.Items.AddRange(items8);
            cbx8.SelectedIndex = 0;
            panel.Controls.Add(cbx8, 2, 14);

            //
            panel.Controls.Add(new Label() { Text = "" }, 0, 15);
            panel.Controls.Add(new Label() { Text = "Auditor Name", Dock = DockStyle.Fill }, 1, 15);

            ComboBox cbx9 = new ComboBox() { Name = "cbxSect1CreatedBy", Dock = DockStyle.Fill };
            cbx9 = WidgetHandler.populateUsersWithEmailValueCombo(cbx9, users);            
            panel.Controls.Add(cbx9, 2, 15);


            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 16);

            panel.Dock = DockStyle.Fill;
            panel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            panel.AutoScroll = true;
            panel.AutoSize = true;

            return panel;

        }


        public static TableLayoutPanel GenerateCompAndPerfScoreTable()
        {

            TableLayoutPanel tableLayoutPanelCompAndPerf = new TableLayoutPanel();
             
            TextBox txvAptCostRatio = new TextBox() { Name = "txvAptCostRatio", Dock = DockStyle.Fill };
            TextBox txvAptPlanRatio = new TextBox() { Name = "txvAptPlanRatio", Dock = DockStyle.Fill };           
            TextBox txvAptBidResponsiveRate = new TextBox() { Name = "txvAptBidResponsiveRate", Dock = DockStyle.Fill };
            TextBox txvAptBidSubmissionRate = new TextBox() { Name = "txvAptBidSubmissionRate", Dock = DockStyle.Fill }; 
            TextBox txvAptNoOfBids = new TextBox() { Name = "txvAptNoOfBids", Dock = DockStyle.Fill };
            TextBox txvAptPaymentRatio = new TextBox() { Name = "txvAptPaymentRatio", Dock = DockStyle.Fill };
            TextBox txvAptCompletionRatio = new TextBox() { Name = "txvAptCompletionRatio", Dock = DockStyle.Fill }; 
            TextBox txvAptProcureRatio = new TextBox() { Name = "txvAptProcureRatio", Dock = DockStyle.Fill };

            Label labelCostRatio = new Label() { Name = "labelCostRatio", Text = "Cost Ratio" };
            Label labelPlanRatio = new Label() { Name = "labelPlanRatio", Text = "Plan Ratio" };
            Label labelBidResponsiveRate = new Label() { Name = "labelBidResponsiveRate", Text = "Bid Responsive Rate" }; 
            Label labelBidSubmissionRate = new Label() { Name = "labelBidSubmissionRate", Text = "Bid Submission Rate" };
            Label labelNoOfBidsReceived = new Label() { Name = "labelNoOfBidsReceived", Text = "Number of Bids Received" };
            Label labelPaymentRatio = new Label() { Name = "labelPaymentRatio", Text = "Payment Ratio" };
            Label labelCompletionRation = new Label() { Name = "labelCompletionRation", Text = "Completion Ratio" };
            Label labelProcureRatio = new Label() { Name = "labelProcureRatio", Text = "Procure Ratio" }; 
            Label labelProcProcessCompLevel = new Label() { Name = "labelProcProcessCompLevel", Text = "Procurement Process Compliance Level" };

            TextBox txvAptComplianceLevel = new TextBox() { Name = "txvAptComplianceLevel", Dock = DockStyle.Fill };
            
            // 
            // tableLayoutPanelCompAndPerf
            // 
            tableLayoutPanelCompAndPerf.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right)));
            tableLayoutPanelCompAndPerf.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanelCompAndPerf.ColumnCount = 9;
            tableLayoutPanelCompAndPerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanelCompAndPerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            tableLayoutPanelCompAndPerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            tableLayoutPanelCompAndPerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            tableLayoutPanelCompAndPerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            tableLayoutPanelCompAndPerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            tableLayoutPanelCompAndPerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            tableLayoutPanelCompAndPerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));
            tableLayoutPanelCompAndPerf.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 10F));


            tableLayoutPanelCompAndPerf.Controls.Add(txvAptCostRatio, 8, 1);

            tableLayoutPanelCompAndPerf.Controls.Add(txvAptPlanRatio, 7, 1);

            tableLayoutPanelCompAndPerf.Controls.Add(txvAptBidResponsiveRate, 6, 1);

            tableLayoutPanelCompAndPerf.Controls.Add(txvAptBidSubmissionRate, 5, 1);

            tableLayoutPanelCompAndPerf.Controls.Add(txvAptNoOfBids, 4, 1);

            tableLayoutPanelCompAndPerf.Controls.Add(txvAptPaymentRatio, 3, 1);

            tableLayoutPanelCompAndPerf.Controls.Add(txvAptCompletionRatio, 2, 1);

            tableLayoutPanelCompAndPerf.Controls.Add(txvAptProcureRatio, 1, 1);

            tableLayoutPanelCompAndPerf.Controls.Add(labelCostRatio, 8, 0);

            tableLayoutPanelCompAndPerf.Controls.Add(labelPlanRatio, 7, 0);

            tableLayoutPanelCompAndPerf.Controls.Add(labelBidResponsiveRate, 6, 0);

            tableLayoutPanelCompAndPerf.Controls.Add(labelBidSubmissionRate, 5, 0);

            tableLayoutPanelCompAndPerf.Controls.Add(labelNoOfBidsReceived, 4, 0);

            tableLayoutPanelCompAndPerf.Controls.Add(labelPaymentRatio, 3, 0);

            tableLayoutPanelCompAndPerf.Controls.Add(labelCompletionRation, 2, 0);

            tableLayoutPanelCompAndPerf.Controls.Add(labelProcureRatio, 1, 0);

            tableLayoutPanelCompAndPerf.Controls.Add(labelProcProcessCompLevel, 0, 0);

            tableLayoutPanelCompAndPerf.Controls.Add(txvAptComplianceLevel, 0, 1);
            tableLayoutPanelCompAndPerf.Name = "tableLayoutPanelCompAndPerf";
            tableLayoutPanelCompAndPerf.RowCount = 2;
            tableLayoutPanelCompAndPerf.RowStyles.Add(new RowStyle(SizeType.Absolute, 32F));
            tableLayoutPanelCompAndPerf.RowStyles.Add(new RowStyle(SizeType.Absolute, 32F));


            tableLayoutPanelCompAndPerf.Dock = DockStyle.None;
            tableLayoutPanelCompAndPerf.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanelCompAndPerf.AutoScroll = true;
            tableLayoutPanelCompAndPerf.AutoSize = true;

            return tableLayoutPanelCompAndPerf;

        }


    }

}

﻿using EmisMasterDataTool.Code;
using EmisTool.Code.ui.custom;
using Processor.Entities;
using Processor.Entities.PM.systems;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace EmisTool.Code
{
    public class UiHelperAst
    {

        public static int FieldCountPmAuditProcessTool = 0;

        public static TableLayoutPanel GenerateTableV1(
            AuditSystemsToolSection toolSection, out SectionEvaluationAst sectionEval,
            AppStateAuditTools appState, List<ManagementLetterSection> managementLetterSections)
        {
                                   
            String sectionTableFieldName = "AuditSystemsToolSec" + toolSection.Id;

            //initialize the out variable
            sectionEval = new SectionEvaluationAst();
            sectionEval.sectionEmisId = toolSection.Id;
            sectionEval.name = toolSection.name;
            sectionEval.sectionTableFieldName = sectionTableFieldName;
            var sectionItemsEvalList = sectionEval.sectionItemEvaluations;
                       
            
            //get the list of items in this section
            List<AuditSystemsToolSectionItem> sectionItems = toolSection.items;


            int COLUMN_COUNT = 7;
            int ROW_COUNT = sectionItems.Count + 2; // add the header row and the footer row


            TableLayoutPanel tableLayoutPanel = new TableLayoutPanel();

            //Now we will generate the table, setting up the row and column counts first
            tableLayoutPanel.ColumnCount = COLUMN_COUNT;
            tableLayoutPanel.RowCount = ROW_COUNT;


            for (int columnIndex = 0; columnIndex < COLUMN_COUNT; columnIndex++)
            {

                //First add a column

                float columnWidth = getColumnWidth(columnIndex);
                tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, columnWidth));

                for (int rowIndex = 0; rowIndex < ROW_COUNT; rowIndex++)
                {

                    bool isFooterRow = rowIndex == ROW_COUNT - 1;
                    bool isHeaderRow = rowIndex == 0;

                    //Next, add a row.  Only do this when once, when creating the first column
                    if (columnIndex == 0 && isHeaderRow || columnIndex == 0 && isFooterRow)
                    {
                        tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));
                    }
                    else if (columnIndex == 0)
                    {
                        tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 52));
                    }
                    

                    //generate the cell control
                    Control control;
                    if (isHeaderRow)
                    {
                        control = buildHeaderCellControlV1(columnIndex, toolSection);
                    }
                    else if (isFooterRow)
                    {
                        control = buildFoooterCellControl(columnIndex);

                        if(columnIndex == 2)
                        {
                            PanelPmAuditSystemsTool.AST_SECTION_TOTAL_TXVS.Add(toolSection.Id, control);
                        }

                    }
                    else
                    {

                        AuditSystemsToolSectionItem currentItem = sectionItems[rowIndex - 1];
                        control = buildBodyCellControlV1(columnIndex, currentItem, managementLetterSections, appState);


                        //begin logic for updating app state  
                        
                        //ensure that the item exists in the dictionary if not add
                        if (!sectionItemsEvalList.ContainsKey(currentItem.emisId))
                        {

                            var sectionEvalItem = new SectionItemEvaluationAst();
                            sectionEvalItem.sectionEmisId = toolSection.Id;
                            sectionEvalItem.sectionItemEmisId = currentItem.emisId;
                            sectionEvalItem.yesRank = currentItem.yesRank;
                            sectionEvalItem.noRank = currentItem.noRank;
                            sectionEvalItem.naRank = currentItem.naRank;
                            sectionEvalItem.description = currentItem.description;

                            if (currentItem.description.ToLower() == "Total procurement budget in the audit period (UGX)".ToLower())
                            {
                                sectionEvalItem.evaluationFieldRole = currentItem.description;
                            }
                            else if (currentItem.description.ToLower() == "Total actual procurements in the audit period (UGX)".ToLower())
                            {
                                sectionEvalItem.evaluationFieldRole = currentItem.description;
                            }
                            else if (currentItem.description.ToLower() == "Total value of procurements in the audit period".ToLower())
                            {
                                sectionEvalItem.evaluationFieldRole = currentItem.description;
                            }
                            else if (currentItem.description.ToLower() == "Total value of open bidding contracts in the audit period".ToLower())
                            {
                                sectionEvalItem.evaluationFieldRole = currentItem.description;
                            }
                            else
                            {
                                sectionEvalItem.evaluationFieldRole = currentItem.description;
                            }                            

                            sectionItemsEvalList.Add(currentItem.emisId, sectionEvalItem);

                        }
                        

                        var oldSectionItemEval = sectionItemsEvalList[currentItem.emisId];
                        oldSectionItemEval.evaluationFieldRole = currentItem.fieldRole;
                        oldSectionItemEval.evaluationFieldRole = currentItem.description;

                        //we now update the fields of the sectionEvaItem
                        var sectionEvalItemUpdated = updateSectionEvalItemAst(oldSectionItemEval, columnIndex, currentItem, control);

                        //update the item in the list
                        sectionItemsEvalList[sectionEvalItemUpdated.sectionEmisId] = sectionEvalItemUpdated;

                        //end logic for updating app state

                    }                                      

                    control.Leave += new EventHandler(appState.updatePmAuditSystemsToolState);
                    tableLayoutPanel.Controls.Add(control, columnIndex, rowIndex);
                                      

                }

            }

            tableLayoutPanel.Dock = DockStyle.Fill;
            tableLayoutPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanel.AutoScroll = false;
            tableLayoutPanel.AutoSize = true;
            tableLayoutPanel.Name = sectionTableFieldName;
            
            return tableLayoutPanel;

        }

        private static SectionItemEvaluationAst updateSectionEvalItemAst(SectionItemEvaluationAst sectionEvalItemToUpdate, int columnIndex, AuditSystemsToolSectionItem sectionItem, Control control)
        {

            int evaluationFieldIndex = 2;
            int findingFieldIndex = 3;
            int exceptionFieldIndex = 4;
            int managementLetterSectionFieldIndex = 5;
            int managementLetterSectionItemFieldIndex = 6;

            if (columnIndex == evaluationFieldIndex)
            {
                sectionEvalItemToUpdate.evaluationFieldName = control.Name;
                sectionEvalItemToUpdate.evaluationFieldType = sectionItem.responseType;
            }
            else if(columnIndex == findingFieldIndex)
            {
                sectionEvalItemToUpdate.findingFieldName = control.Name;
            }
            else if(columnIndex == exceptionFieldIndex)
            {
                sectionEvalItemToUpdate.exceptionFieldName = control.Name;
            }
            else if (columnIndex == managementLetterSectionFieldIndex)
            {
                sectionEvalItemToUpdate.managementLetterSectionFieldName = control.Name;
            }
            else if (columnIndex == managementLetterSectionItemFieldIndex)
            {
                sectionEvalItemToUpdate.managementLetterSectionItemFieldName = control.Name;
            }

            return sectionEvalItemToUpdate;

        }

        private static float getColumnWidth(int columnIndex)
        {

            if (columnIndex == 0)
            {
                return 50F;
            }
            else if (columnIndex == 1)
            {
                return 200F;
            }
            else if (columnIndex == 2)
            {
                return 170F;
            }
            else if (columnIndex == 3)
            {
                return 300F;
            }
            else if (columnIndex == 4)
            {
                return 280F;
            }
            else if (columnIndex == 5 || columnIndex == 6)
            {
                return 120F;
            }
            else
            {
                return 0;
            }

        }

        private static Control buildBodyCellControlV1(
            int columnIndex, AuditSystemsToolSectionItem sectionItem, List<ManagementLetterSection> managementLetterSections, AppStateAuditTools appState)
        {

            Control control = new Control();

            FieldCountPmAuditProcessTool++;
            String fieldName = "AuditSystemsToolField" + FieldCountPmAuditProcessTool;


            if (columnIndex == 0)
            {
                Label labelRank = new Label() { Text = sectionItem.rank.ToString() };
                labelRank.Name = fieldName;
                return labelRank;
            }
            else if (columnIndex == 1)
            {
                String text = sectionItem.description;
                Label labelDesc = new Label() { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft, Text = text, AutoSize = true };
                labelDesc.Name = fieldName;

                //if it has important text show the text
                new ToolTip().SetToolTip(labelDesc, "More description");

                return labelDesc;
            }
            else if (columnIndex == 2)
            {
                //here we return a table layout                 
                Control evaluationFieldControl = buildResponseFieldControl(sectionItem.responseType);
                evaluationFieldControl.Name = fieldName;
                return evaluationFieldControl;
            }
            else if (columnIndex == 3 || columnIndex == 4)
            {

                RichTextBox textbox = new RichTextBox();
                textbox.Height = 50;
                textbox.Width = 350;
                textbox.Dock = DockStyle.Fill;
                textbox.Name = fieldName;
                return textbox;

            }
            else if (columnIndex == 5)
            {

                ComboBox cbx = new ComboBox();
                cbx.Width = 120;
                cbx.Name = fieldName;
                cbx.DropDownStyle = ComboBoxStyle.DropDownList;
                cbx = WidgetHandler.populateManagementLetterSectionsDropdown(cbx, managementLetterSections);
                cbx.SelectedIndexChanged += appState.iAstAnalyisUpdater.loadExceptionsBasedOnExceptionSectionSelected;

                return cbx;

            }
            else if (columnIndex == 6)
            {

                ComboBox cbx = new ComboBox();
                cbx.Items.Add("Select option");
                cbx.Width = 120;
                cbx.Name = fieldName;
                cbx.DropDownStyle = ComboBoxStyle.DropDownList;
                cbx.SelectedIndex = cbx.FindString("Select option");
                return cbx;

            }
            else
            {
                return new Label();
            }


        }
             
        private static Control buildFoooterCellControl(int columnIndex)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                return new Label();
            }
            else if (columnIndex == 1)
            {
                
                Label labelRank = new Label() { Text = "TOTAL", ForeColor = Color.DarkRed };
                return labelRank;
            }
            else if (columnIndex == 2)
            {
                TextBox textbox = new TextBox();
                textbox.Height = 25;
                textbox.Dock = DockStyle.Fill;
                return textbox;
            }
            else
            {
                return new Label(); 
            }


        }

        private static Control buildHeaderCellControlV1(int columnIndex, AuditSystemsToolSection toolSection)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                Label sectionRankLabel = new Label() { Text = toolSection.rank.ToString(), ForeColor = Color.DarkBlue };
                return sectionRankLabel;
            }
            else if (columnIndex == 1)
            {
                Label sectionNameLabel = new Label() { Text = toolSection.name.ToUpper(), ForeColor = Color.DarkBlue };
                return sectionNameLabel;
            }
            else if (columnIndex == 2)
            {
                return new Label();
            }
            else if (columnIndex == 3)
            {
                Label sectionColumn4Label = new Label() { Text = "AUDIT FINDINGS AND EVIDENCE", AutoSize = false, Width = 400, ForeColor = Color.DarkBlue };
                return sectionColumn4Label;
            }
            else if (columnIndex == 4)
            {
                Label sectionColumn5Label = new Label() { Text = "EXCEPTIONS", ForeColor = Color.DarkBlue };
                return sectionColumn5Label;
            }
            else
            {
                return new Label();
            }

        }

        private static Control buildResponseFieldControl(string fieldType)
        {

            if(fieldType == "yesnona")
            {

                TableLayoutPanel panel = new TableLayoutPanel();
                panel.ColumnCount = 3;
                panel.RowCount = 1;
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 34));
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33));
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33));

                panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));

                panel.Controls.Add(new RadioButton() { Text = "Yes" }, 0, 0);
                panel.Controls.Add(new RadioButton() { Text = "No" }, 1, 0);
                panel.Controls.Add(new RadioButton() { Text = "Na", Checked = true }, 2, 0);

                panel.Dock = DockStyle.Fill;
                return panel;

            }
            else if (fieldType == "date")
            {
                DateTimePicker datePicker = new DateTimePicker();
                datePicker.Height = 25;
                datePicker.Format = DateTimePickerFormat.Short;
                datePicker.Dock = DockStyle.Fill;
                return datePicker;
            }
            else
            {
                TextBox textbox = new TextBox();
                textbox.Height = 25;
                textbox.Dock = DockStyle.Fill;
                return textbox;
            }

        }

        public static TableLayoutPanel GenerateIdentificationSectionTable(List<User> users)
        {

            int COLUMN_COUNT = 2;
            int ROW_COUNT = 8;

            TableLayoutPanel panel = new TableLayoutPanel();
            
            panel.ColumnCount = COLUMN_COUNT;
            panel.RowCount = ROW_COUNT;
            
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 350F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 650F));

            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));

            //section 1

            Label label = new Label() { Text = "PROCUREMENT SYSTEMS AUDIT CRITERIA", Dock = DockStyle.Fill };
            panel.Controls.Add(label, 1, 0);
                  
            panel.Controls.Add(new Label() { Text = "IDENTIFICATION", Dock = DockStyle.Fill }, 0, 1);
                        
            panel.Controls.Add(new Label() { Text = "Name of Procuring and Disposal Entity", Dock = DockStyle.Fill }, 0, 2);
            panel.Controls.Add(new TextBox() { Name = "txvAstNameOfEntity", Dock = DockStyle.Fill }, 1, 2);
                        
            panel.Controls.Add(new Label() { Text = "Town Location of Entity", Dock = DockStyle.Fill }, 0, 3);
            panel.Controls.Add(new TextBox() { Name = "txvAstLocationOfEntity", Dock = DockStyle.Fill }, 1, 3);
            
            panel.Controls.Add(new Label() { Text = "Sector of the Entity", Dock = DockStyle.Fill }, 0, 4);
            panel.Controls.Add(new TextBox() { Name = "txvAstSector", Dock = DockStyle.Fill }, 1, 4);
                       
            panel.Controls.Add(new Label() { Text = "Audit Period", Dock = DockStyle.Fill }, 0, 5);
            panel.Controls.Add(new TextBox() { Name = "txvAstAuditPeriod", Dock = DockStyle.Fill }, 1, 5);

            panel.Controls.Add(new Label() { Text = "Audit Name", Dock = DockStyle.Fill }, 0, 6);
            ComboBox cbxAuditor = new ComboBox() { Name = "cbxAstCreatedBy", Dock = DockStyle.Fill };
            cbxAuditor = WidgetHandler.populateUsersWithEmailValueCombo(cbxAuditor, users);
            panel.Controls.Add(cbxAuditor, 1, 6);
            
            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 7);

            panel.Dock = DockStyle.Fill;
            panel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            panel.AutoScroll = true;
            panel.AutoSize = true;

            return panel;

        }
        

        public static TableLayoutPanel GenerateSection13Table()
        {

            int COLUMN_COUNT = 5;
            int ROW_COUNT = 15;

            TableLayoutPanel panel = new TableLayoutPanel();

            panel.ColumnCount = COLUMN_COUNT;
            panel.RowCount = ROW_COUNT;

            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 250F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 250F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 250F));
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));

            panel.Controls.Add(new Label() { Text = "" }, 0, 0);

            panel.Controls.Add(new Label() { Text = "13" }, 0, 1);
            Label label = new Label() { Text = "SUMMARY OF KEY FINDINGS, CONCLUSIONS, AND RECOMMENDATIONS", Dock = DockStyle.Fill };
            panel.Controls.Add(label, 1, 1);            
            panel.Controls.Add(new Label() { Text = "Key Positive Findings and Conclusions", Dock = DockStyle.Fill }, 2, 1);
            panel.Controls.Add(new Label() { Text = "Key Negative Findings and Conclusions", Dock = DockStyle.Fill }, 3, 1);
            panel.Controls.Add(new Label() { Text = "Key Recommended Actions", Dock = DockStyle.Fill }, 4, 1);

            
            panel.Controls.Add(new Label() { Text = "(a)   Accounting Officer:", Dock = DockStyle.Fill }, 1, 2);
            panel.Controls.Add(new RichTextBox() { Name = "txvAoPositive", Dock = DockStyle.Fill, Height=40 }, 2, 2);
            panel.Controls.Add(new RichTextBox() { Name = "txvAoNegative", Dock = DockStyle.Fill, Height=40 }, 3, 2);
            panel.Controls.Add(new RichTextBox() { Name = "txvAoRecom", Dock = DockStyle.Fill, Height=40 }, 4, 2);

            panel.Controls.Add(new Label() { Text = "(b) Procurement and Disposal Unit:", Dock = DockStyle.Fill }, 1, 3);
            panel.Controls.Add(new RichTextBox() { Name = "txvPduPositive", Dock = DockStyle.Fill, Height = 40 }, 2, 3);
            panel.Controls.Add(new RichTextBox() { Name = "txvPduNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 3);
            panel.Controls.Add(new RichTextBox() { Name = "txvPduRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 3);

            panel.Controls.Add(new Label() { Text = "(c) Contracts committee:", Dock = DockStyle.Fill }, 1, 4);
            panel.Controls.Add(new RichTextBox() { Name = "txvContractsCommittePositive", Dock = DockStyle.Fill, Height = 40 }, 2, 4);
            panel.Controls.Add(new RichTextBox() { Name = "txvContractsCommitteNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 4);
            panel.Controls.Add(new RichTextBox() { Name = "txvContractsCommitteRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 4);

            panel.Controls.Add(new Label() { Text = "(d) User Departments:", Dock = DockStyle.Fill }, 1, 5);
            panel.Controls.Add(new RichTextBox() { Name = "txvUserDeptPositive", Dock = DockStyle.Fill, Height = 40 }, 2, 5);
            panel.Controls.Add(new RichTextBox() { Name = "txvUserDeptNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 5);
            panel.Controls.Add(new RichTextBox() { Name = "txvUserDeptRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 5);

            panel.Controls.Add(new Label() { Text = "(e) Evaluation Committees:", Dock = DockStyle.Fill }, 1, 6);
            panel.Controls.Add(new RichTextBox() { Name = "txvEvalCommittePositive", Dock = DockStyle.Fill, Height = 40 }, 2, 6);
            panel.Controls.Add(new RichTextBox() { Name = "txvEvalCommitteNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 6);
            panel.Controls.Add(new RichTextBox() { Name = "txvEvalCommitteRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 6);

            panel.Controls.Add(new Label() { Text = "(f) Internal Controls:", Dock = DockStyle.Fill }, 1, 7);
            panel.Controls.Add(new RichTextBox() { Name = "txvInternalControlsPositive", Dock = DockStyle.Fill, Height = 40 }, 2, 7);
            panel.Controls.Add(new RichTextBox() { Name = "txvInternalControlsNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 7);
            panel.Controls.Add(new RichTextBox() { Name = "txvInternalControlsRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 7);

            panel.Controls.Add(new Label() { Text = "(g) Procurement Planning:", Dock = DockStyle.Fill }, 1, 8);
            panel.Controls.Add(new RichTextBox() { Name = "txvProcPlanPositive", Dock = DockStyle.Fill, Height = 40 }, 2, 8);
            panel.Controls.Add(new RichTextBox() { Name = "txvProcPlanNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 8);
            panel.Controls.Add(new RichTextBox() { Name = "txvProcPlanRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 8);

            panel.Controls.Add(new Label() { Text = "(h) Records and Reports:", Dock = DockStyle.Fill }, 1, 9);
            panel.Controls.Add(new RichTextBox() { Name = "txvRecordsAndReportsPositive", Dock = DockStyle.Fill, Height = 40 }, 2, 9);
            panel.Controls.Add(new RichTextBox() { Name = "txvRecordsAndReportsNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 9);
            panel.Controls.Add(new RichTextBox() { Name = "txvRecordsAndReportsRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 9);

            panel.Controls.Add(new Label() { Text = "(i)    Compliance Score:", Dock = DockStyle.Fill }, 1, 10);
            panel.Controls.Add(new RichTextBox() { Name = "txvCompliancePositive", Dock = DockStyle.Fill, Height = 40 }, 2, 10);
            panel.Controls.Add(new RichTextBox() { Name = "txvComplianceNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 10);
            panel.Controls.Add(new RichTextBox() { Name = "txvComplianceRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 10);           

            panel.Controls.Add(new Label() { Text = "(j) Budget Absorption:", Dock = DockStyle.Fill }, 1, 11);
            panel.Controls.Add(new RichTextBox() { Name = "txvBudgetAbsorptionPositive", Dock = DockStyle.Fill, Height = 40 }, 2, 11);
            panel.Controls.Add(new RichTextBox() { Name = "txvBudgetAbsorptionNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 11);
            panel.Controls.Add(new RichTextBox() { Name = "txvBudgetAbsorptionRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 11);

            panel.Controls.Add(new Label() { Text = "(k) Open Bidding Value", Dock = DockStyle.Fill }, 1, 12);
            panel.Controls.Add(new RichTextBox() { Name = "txvOpenBiddingValuePositive", Dock = DockStyle.Fill, Height = 40 }, 2, 12);
            panel.Controls.Add(new RichTextBox() { Name = "txvOpenBiddingValueNegative", Dock = DockStyle.Fill, Height = 40 }, 3, 12);
            panel.Controls.Add(new RichTextBox() { Name = "txvOpenBiddingValueRecom", Dock = DockStyle.Fill, Height = 40 }, 4, 12);
            
            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 13);

            panel.Dock = DockStyle.Fill;
            panel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            panel.AutoScroll = true;
            panel.AutoSize = true;

            return panel;

        }
        
        public static TableLayoutPanel GenerateAnalysisTabTable(
            EventHandler calcAvgCompLevel, EventHandler calcProcBudgetAbsorptionRate, EventHandler calcProportionValueOfContractsOnOpenBidding)
        {

           Label labelHeaderPercentScore = new Label();
           Label labelHeaderTotalScore = new Label();
           Label labelHeaderNoScore = new Label();
           Label labelHeaderYesScore = new Label();
           Label labelHeaderRank = new Label();
           Label labelHeaderDesc = new Label();
           Label labelRankAO = new Label();
           Label labelDescAO = new Label();
           Label labelDescPdu = new Label();
           Label labelRankPdu = new Label();
           Label labelDescCC = new Label();
           Label labelDescUD = new Label();
           Label labelDescEC = new Label();
           Label labelDescIC = new Label();
           Label labelDescCompLevel = new Label();
           Label labelRankCC = new Label();
           Label labelUD = new Label();
           Label labelRankEC = new Label();
           Label labelRankIC = new Label();
           Label labelRankCompLevel = new Label();
           TextBox txvPercentSystemsComplainceLevel = new TextBox();
           TextBox txvPercentInternalControls = new TextBox();
           TextBox txvTotalInternalControls = new TextBox();
           TextBox txvNoTotalInternalControls = new TextBox();
           TextBox txvYesTotalInternalControls = new TextBox();
           TextBox txvPercentEvalCommitte = new TextBox();
           TextBox txvTotalEvalCommitte = new TextBox();
           TextBox txvNoTotalEvalCommitte = new TextBox();
           TextBox txvYesTotalEvalCommitte = new TextBox();
           TextBox txvPercentUseDept = new TextBox();
           TextBox txvTotalUseDept = new TextBox();
           TextBox txvNoTotalUseDept = new TextBox();
           TextBox txvYesTotalUseDept = new TextBox();
           TextBox txvPercentContractsCommittee = new TextBox();
           TextBox txvTotalContractsCommittee = new TextBox();
           TextBox txvNoTotalContractsCommittee = new TextBox();
           TextBox txvYesTotalContractsCommittee = new TextBox();
           TextBox txvPercentPDU = new TextBox();
           TextBox txvTotalPDU = new TextBox();
           TextBox txvNoTotalPDU = new TextBox();
           TextBox txvYesTotalPDU = new TextBox();
           TextBox txvPercentAccountOfficer = new TextBox();
           TextBox txvTotalAccountOfficer = new TextBox();
           TextBox txvNoTotalAccountOfficer = new TextBox();
           TextBox txvYesTotalAccountOfficer = new TextBox();
           Label labelBudgetHeaderRank = new Label();
           Label labelBudgetHeaderDesc = new Label();
           Label labelRankTotalProcBudget = new Label();
           Label labelDescTotalProcBudget = new Label();
           Label labelRankActualProcBudget = new Label();
           Label labelDescActualProcBudget = new Label();
           Label labelRankProcBudgetAbsorptionRate = new Label();
           Label labelDescAbsorptionRate = new Label();
           Label labelHeaderRankOpenBids = new Label();
           Label labelHeaderDescOpenBids = new Label();
           Label labelRankValueOfProc = new Label();
           Label labelDescValueOfProc = new Label();
           Label labelRankValueOfOpenBidContracts = new Label();
           Label labelDescValueOfOpenBids = new Label();
           Label labelRankProportionValueOfContracts = new Label();
           Label labelDescProportionValOfContractsOnOpenBids = new Label();
           TextBox txvTotalProcBudget = new TextBox();
           TextBox txvTotalActualProc = new TextBox();
           TextBox txvTotalValueOfProcs = new TextBox();
           TextBox txvTotalValueOfOpenBidContracts = new TextBox();
           TextBox txvProportionOfContractsOnOpenBi = new TextBox();
           TextBox txvProcBudgetAbsorptioRate = new TextBox();


            // 
            // txvPercentSystemsComplainceLevel
            // 
           txvPercentSystemsComplainceLevel.Name = "txvPercentSystemsComplainceLevel";
            // 
            // txvPercentInternalControls
            // 
           txvPercentInternalControls.Name = "txvPercentInternalControls";
           txvPercentInternalControls.TextChanged += new System.EventHandler(calcAvgCompLevel);
            // 
            // txvTotalInternalControls
            // 
           txvTotalInternalControls.Name = "txvTotalInternalControls";
            // 
            // txvNoTotalInternalControls
            // 
           txvNoTotalInternalControls.Name = "txvNoTotalInternalControls";
            // 
            // txvYesTotalInternalControls
            // 
           txvYesTotalInternalControls.Name = "txvYesTotalInternalControls";
            // 
            // txvPercentEvalCommitte
            // 
            txvPercentEvalCommitte.Name = "txvPercentEvalCommitte";
            txvPercentEvalCommitte.TextChanged += new System.EventHandler(calcAvgCompLevel);
            // 
            // txvTotalEvalCommitte
            txvTotalEvalCommitte.Name = "txvTotalEvalCommitte";
            // 
            // txvNoTotalEvalCommitte
            // 
            txvNoTotalEvalCommitte.Name = "txvNoTotalEvalCommitte";
            // 
            // txvYesTotalEvalCommitte
            // 
            txvYesTotalEvalCommitte.Name = "txvYesTotalEvalCommitte";
            // 
            // txvPercentUseDept
            // 
            txvPercentUseDept.Name = "txvPercentUseDept";
            txvPercentUseDept.TextChanged += new System.EventHandler(calcAvgCompLevel);
            // 
            // txvTotalUseDept
            // 
            txvTotalUseDept.Name = "txvTotalUseDept";
            // 
            // txvNoTotalUseDept
            // 
            txvNoTotalUseDept.Name = "txvNoTotalUseDept";
            // 
            // txvYesTotalUseDept
            // 
            txvYesTotalUseDept.Name = "txvYesTotalUseDept";
            // 
            // txvPercentContractsCommittee
            // 
            txvPercentContractsCommittee.Name = "txvPercentContractsCommittee";
            txvPercentContractsCommittee.TextChanged += new System.EventHandler(calcAvgCompLevel);
            // 
            // txvTotalContractsCommittee
            // 
            txvTotalContractsCommittee.Name = "txvTotalContractsCommittee";
            // 
            // txvNoTotalContractsCommittee
            // 
            txvNoTotalContractsCommittee.Name = "txvNoTotalContractsCommittee";
            // 
            // txvYesTotalContractsCommittee
            // 
            txvYesTotalContractsCommittee.Name = "txvYesTotalContractsCommittee";
            // 
            // txvPercentPDU
            // 
            txvPercentPDU.Name = "txvPercentPDU";
            txvPercentPDU.TextChanged += new System.EventHandler(calcAvgCompLevel);
            // 
            // txvTotalPDU
            // 
            txvTotalPDU.Name = "txvTotalPDU";
            // 
            // txvNoTotalPDU
            // 
            txvNoTotalPDU.Name = "txvNoTotalPDU";
            // 
            // txvYesTotalPDU
            // 
            txvYesTotalPDU.Name = "txvYesTotalPDU";
            // 
            // txvPercentAccountOfficer
            // 
            txvPercentAccountOfficer.Name = "txvPercentAccountOfficer";
            txvPercentAccountOfficer.TextChanged += new System.EventHandler(calcAvgCompLevel);
            // 
            // txvTotalAccountOfficer
            // 
            txvTotalAccountOfficer.Name = "txvTotalAccountOfficer";
            // 
            // txvNoTotalAccountOfficer
            // 
            txvNoTotalAccountOfficer.Name = "txvNoTotalAccountOfficer";
            // 
            // labelRankAO
            // 
            labelRankAO.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelRankAO.AutoSize = true;
            labelRankAO.Name = "labelRankAO";
            labelRankAO.Text = "10.1";
            labelRankAO.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelHeaderPercentScore
            // 
            labelHeaderPercentScore.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelHeaderPercentScore.AutoSize = true;
            labelHeaderPercentScore.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelHeaderPercentScore.Name = "labelHeaderPercentScore";
            labelHeaderPercentScore.Text = "Score(%)";
            labelHeaderPercentScore.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelHeaderTotalScore
            // 
            labelHeaderTotalScore.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelHeaderTotalScore.AutoSize = true;
            labelHeaderTotalScore.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelHeaderTotalScore.Name = "labelHeaderTotalScore";
            labelHeaderTotalScore.Text = "Total";
            labelHeaderTotalScore.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelHeaderNoScore
            // 
            labelHeaderNoScore.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelHeaderNoScore.AutoSize = true;
            labelHeaderNoScore.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelHeaderNoScore.Name = "labelHeaderNoScore";
            labelHeaderNoScore.Text = "No";
            labelHeaderNoScore.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelHeaderYesScore
            // 
            labelHeaderYesScore.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelHeaderYesScore.AutoSize = true;
            labelHeaderYesScore.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelHeaderYesScore.Name = "labelHeaderYesScore";
            labelHeaderYesScore.Text = "Yes";
            labelHeaderYesScore.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelHeaderRank
            // 
            labelHeaderRank.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelHeaderRank.AutoSize = true;
            labelHeaderRank.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelHeaderRank.Name = "labelHeaderRank";
            labelHeaderRank.Text = "10";
            labelHeaderRank.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelHeaderDesc
            // 
            labelHeaderDesc.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelHeaderDesc.AutoSize = true;
            labelHeaderDesc.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelHeaderDesc.Name = "labelHeaderDesc";
            labelHeaderDesc.Text = "Compliance Indicators";
            labelHeaderDesc.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelDescAO
            // 
            labelDescAO.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelDescAO.AutoSize = true;
            labelDescAO.Name = "labelDescAO";
            labelDescAO.Text = "Accounting Officer";
            labelDescAO.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelDescPdu
            // 
            labelDescPdu.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelDescPdu.AutoSize = true;
            labelDescPdu.Name = "labelDescPdu";
            labelDescPdu.Text = "Procurement and Disposal Unit";
            labelDescPdu.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelRankPdu
            // 
            labelRankPdu.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelRankPdu.AutoSize = true;
            labelRankPdu.Name = "labelRankPdu";
            labelRankPdu.Text = "10.2";
            labelRankPdu.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelDescCC
            // 
            labelDescCC.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelDescCC.AutoSize = true;
            labelDescCC.Name = "labelDescCC";
            labelDescCC.Text = "Contracts Committee";
            labelDescCC.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelDescUD
            // 
            labelDescUD.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelDescUD.AutoSize = true;
            labelDescUD.Name = "labelDescUD";
            labelDescUD.Text = "User Departments";
            labelDescUD.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelDescEC
            // 
            labelDescEC.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelDescEC.AutoSize = true;
            labelDescEC.Name = "labelDescEC";
            labelDescEC.Text = "Evaluation Committees";
            labelDescEC.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelDescIC
            // 
            labelDescIC.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelDescIC.AutoSize = true;
            labelDescIC.Name = "labelDescIC";
            labelDescIC.Text = "Internal Controls";
            labelDescIC.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelDescCompLevel
            // 
            labelDescCompLevel.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelDescCompLevel.AutoSize = true;
            labelDescCompLevel.Name = "labelDescCompLevel";
            labelDescCompLevel.Text = "Procurement Systems Compliance Level";
            labelDescCompLevel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelRankCC
            // 
            labelRankCC.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelRankCC.AutoSize = true;
            labelRankCC.Name = "labelRankCC";
            labelRankCC.Text = "10.3";
            labelRankCC.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelUD
            // 
            labelUD.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelUD.AutoSize = true;
            labelUD.Name = "labelUD";
            labelUD.Text = "10.4";
            labelUD.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelRankEC
            // 
            labelRankEC.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelRankEC.AutoSize = true;
            labelRankEC.Name = "labelRankEC";
            labelRankEC.Text = "10.5";
            labelRankEC.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelRankIC
            // 
            labelRankIC.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelRankIC.AutoSize = true;
            labelRankIC.Name = "labelRankIC";
            labelRankIC.Text = "10.6";
            labelRankIC.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // labelRankCompLevel
            // 
            labelRankCompLevel.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelRankCompLevel.AutoSize = true;
            labelRankCompLevel.Name = "labelRankCompLevel";
            labelRankCompLevel.Text = "10.9";
            labelRankCompLevel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // txvYesTotalAccountOfficer
            // 
            txvYesTotalAccountOfficer.Name = "txvYesTotalAccountOfficer";
            // 
            // labelBudgetHeaderRank
            // 
            labelBudgetHeaderRank.AutoSize = true;
            labelBudgetHeaderRank.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelBudgetHeaderRank.Name = "labelBudgetHeaderRank";
            labelBudgetHeaderRank.Text = "11";
            // 
            // labelBudgetHeaderDesc
            // 
            labelBudgetHeaderDesc.AutoSize = true;
            labelBudgetHeaderDesc.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelBudgetHeaderDesc.Name = "labelBudgetHeaderDesc";
            labelBudgetHeaderDesc.Text = "BUDGET";
            // 
            // labelRankTotalProcBudget
            // 
            labelRankTotalProcBudget.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelRankTotalProcBudget.AutoSize = true;
            labelRankTotalProcBudget.Name = "labelRankTotalProcBudget";
            labelRankTotalProcBudget.Text = "11.1";
            // 
            // labelDescTotalProcBudget
            // 
            labelDescTotalProcBudget.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelDescTotalProcBudget.AutoSize = true;
            labelDescTotalProcBudget.Name = "labelDescTotalProcBudget";
            labelDescTotalProcBudget.Text = "Total procurement budget in the audit period (UGX)";
            // 
            // labelRankActualProcBudget
            // 
            labelRankActualProcBudget.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelRankActualProcBudget.AutoSize = true;
            labelRankActualProcBudget.Name = "labelRankActualProcBudget";
            labelRankActualProcBudget.Text = "11.2";
            // 
            // labelDescActualProcBudget
            // 
            labelDescActualProcBudget.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelDescActualProcBudget.AutoSize = true;
            labelDescActualProcBudget.Name = "labelDescActualProcBudget";
            labelDescActualProcBudget.Text = "Total actual procurements in the audit period (UGX)";
            // 
            // labelRankProcBudgetAbsorptionRate
            // 
            labelRankProcBudgetAbsorptionRate.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelRankProcBudgetAbsorptionRate.AutoSize = true;
            labelRankProcBudgetAbsorptionRate.Name = "labelRankProcBudgetAbsorptionRate";
            labelRankProcBudgetAbsorptionRate.Text = "11.3";
            // 
            // labelDescAbsorptionRate
            // 
            labelDescAbsorptionRate.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelDescAbsorptionRate.AutoSize = true;
            labelDescAbsorptionRate.Name = "labelDescAbsorptionRate";
            labelDescAbsorptionRate.Text = "Procurement Budget Absorption Rate (%)";
            // 
            // labelHeaderRankOpenBids
            // 
            labelHeaderRankOpenBids.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelHeaderRankOpenBids.AutoSize = true;
            labelHeaderRankOpenBids.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelHeaderRankOpenBids.Location = new Point(4, 365);
            labelHeaderRankOpenBids.Name = "labelHeaderRankOpenBids";
            labelHeaderRankOpenBids.Size = new Size(21, 25);
            labelHeaderRankOpenBids.TabIndex = 56;
            labelHeaderRankOpenBids.Text = "12";
            // 
            // labelHeaderDescOpenBids
            // 
            labelHeaderDescOpenBids.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelHeaderDescOpenBids.AutoSize = true;
            labelHeaderDescOpenBids.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            labelHeaderDescOpenBids.Name = "labelHeaderDescOpenBids";
            labelHeaderDescOpenBids.Text = "OPEN BIDS";
            // 
            // labelRankValueOfProc
            // 
            labelRankValueOfProc.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelRankValueOfProc.AutoSize = true;
            labelRankValueOfProc.Name = "labelRankValueOfProc";
            labelRankValueOfProc.Text = "12.1";
            // 
            // labelDescValueOfProc
            // 
            labelDescValueOfProc.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelDescValueOfProc.AutoSize = true;
            labelDescValueOfProc.Name = "labelDescValueOfProc";
            labelDescValueOfProc.Text = "Total value of procurements in the audit period";
            // 
            // labelRankValueOfOpenBidContracts
            // 
            labelRankValueOfOpenBidContracts.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom)| AnchorStyles.Left)));
            labelRankValueOfOpenBidContracts.AutoSize = true;
            labelRankValueOfOpenBidContracts.Name = "labelRankValueOfOpenBidContracts";
            labelRankValueOfOpenBidContracts.Text = "12.2";
            // 
            // labelDescValueOfOpenBids
            // 
            labelDescValueOfOpenBids.Anchor = ((AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left)));
            labelDescValueOfOpenBids.AutoSize = true;
            labelDescValueOfOpenBids.Name = "labelDescValueOfOpenBids";
            labelDescValueOfOpenBids.Text = "Total value of open bidding contracts in the audit period";
            // 
            // labelRankProportionValueOfContracts
            // 
            labelRankProportionValueOfContracts.AutoSize = true;
            labelRankProportionValueOfContracts.Name = "labelRankProportionValueOfContracts";
            labelRankProportionValueOfContracts.Text = "12.3";
            // 
            // labelDescProportionValOfContractsOnOpenBids
            // 
            labelDescProportionValOfContractsOnOpenBids.AutoSize = true;
            labelDescProportionValOfContractsOnOpenBids.Name = "labelDescProportionValOfContractsOnOpenBids";
            labelDescProportionValOfContractsOnOpenBids.Text = "Proportion value of contracts on open bidding (%)";
            // 
            // txvTotalProcBudget
            //
            txvTotalProcBudget.Name = "txvTotalProcBudget";
            txvTotalProcBudget.TextChanged += new System.EventHandler(calcProcBudgetAbsorptionRate);
            // 
            // txvTotalActualProc
            //
            txvTotalActualProc.Name = "txvTotalActualProc";
            txvTotalActualProc.TextChanged += new System.EventHandler(calcProcBudgetAbsorptionRate);
            // 
            // txvProcBudgetAbsorptioRate
            //
            txvProcBudgetAbsorptioRate.Name = "txvProcBudgetAbsorptioRate";
            // 
            // txvTotalValueOfProcs
            //
            txvTotalValueOfProcs.Name = "txvTotalValueOfProcs";
            txvTotalValueOfProcs.TextChanged += new System.EventHandler(calcProportionValueOfContractsOnOpenBidding);
            // 
            // txvTotalValueOfOpenBidContracts
            //
            txvTotalValueOfOpenBidContracts.Name = "txvTotalValueOfOpenBidContracts";
            txvTotalValueOfOpenBidContracts.TextChanged += new System.EventHandler(calcProportionValueOfContractsOnOpenBidding);
            // 
            // txvProportionOfContractsOnOpenBi
            // 
            txvProportionOfContractsOnOpenBi.Name = "txvProportionOfContractsOnOpenBi";

            var tableLayoutPanelAstAnalysis = new TableLayoutPanel();

            // 
            // tableLayoutPanelAstAnalysis
            // 
           tableLayoutPanelAstAnalysis.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
           tableLayoutPanelAstAnalysis.AutoScroll = true;
           tableLayoutPanelAstAnalysis.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
           tableLayoutPanelAstAnalysis.ColumnCount = 6;
           tableLayoutPanelAstAnalysis.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50.263158F));
           tableLayoutPanelAstAnalysis.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 520.63158F));
           tableLayoutPanelAstAnalysis.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 100.52632F));
           tableLayoutPanelAstAnalysis.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 100.52632F));
           tableLayoutPanelAstAnalysis.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 100.52632F));
           tableLayoutPanelAstAnalysis.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 100.52632F));
           tableLayoutPanelAstAnalysis.Controls.Add(txvPercentSystemsComplainceLevel, 5, 7);
           tableLayoutPanelAstAnalysis.Controls.Add(txvPercentInternalControls, 5, 6);
           tableLayoutPanelAstAnalysis.Controls.Add(txvTotalInternalControls, 4, 6);
           tableLayoutPanelAstAnalysis.Controls.Add(txvNoTotalInternalControls, 3, 6);
           tableLayoutPanelAstAnalysis.Controls.Add(txvYesTotalInternalControls, 2, 6);
            tableLayoutPanelAstAnalysis.Controls.Add(txvPercentEvalCommitte, 5, 5);
            tableLayoutPanelAstAnalysis.Controls.Add(txvTotalEvalCommitte, 4, 5);
            tableLayoutPanelAstAnalysis.Controls.Add(txvNoTotalEvalCommitte, 3, 5);
            tableLayoutPanelAstAnalysis.Controls.Add(txvYesTotalEvalCommitte, 2, 5);
            tableLayoutPanelAstAnalysis.Controls.Add(txvPercentUseDept, 5, 4);
            tableLayoutPanelAstAnalysis.Controls.Add(txvTotalUseDept, 4, 4);
            tableLayoutPanelAstAnalysis.Controls.Add(txvNoTotalUseDept, 3, 4);
            tableLayoutPanelAstAnalysis.Controls.Add(txvYesTotalUseDept, 2, 4);
            tableLayoutPanelAstAnalysis.Controls.Add(txvPercentContractsCommittee, 5, 3);
            tableLayoutPanelAstAnalysis.Controls.Add(txvTotalContractsCommittee, 4, 3);
            tableLayoutPanelAstAnalysis.Controls.Add(txvNoTotalContractsCommittee, 3, 3);
            tableLayoutPanelAstAnalysis.Controls.Add(txvYesTotalContractsCommittee, 2, 3);
            tableLayoutPanelAstAnalysis.Controls.Add(txvPercentPDU, 5, 2);
            tableLayoutPanelAstAnalysis.Controls.Add(txvTotalPDU, 4, 2);
            tableLayoutPanelAstAnalysis.Controls.Add(txvNoTotalPDU, 3, 2);
            tableLayoutPanelAstAnalysis.Controls.Add(txvYesTotalPDU, 2, 2);
            tableLayoutPanelAstAnalysis.Controls.Add(txvPercentAccountOfficer, 5, 1);
            tableLayoutPanelAstAnalysis.Controls.Add(txvTotalAccountOfficer, 4, 1);
            tableLayoutPanelAstAnalysis.Controls.Add(txvNoTotalAccountOfficer, 3, 1);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankAO, 0, 1);
            tableLayoutPanelAstAnalysis.Controls.Add(labelHeaderPercentScore, 5, 0);
            tableLayoutPanelAstAnalysis.Controls.Add(labelHeaderTotalScore, 4, 0);
            tableLayoutPanelAstAnalysis.Controls.Add(labelHeaderNoScore, 3, 0);
            tableLayoutPanelAstAnalysis.Controls.Add(labelHeaderYesScore, 2, 0);
            tableLayoutPanelAstAnalysis.Controls.Add(labelHeaderRank, 0, 0);
            tableLayoutPanelAstAnalysis.Controls.Add(labelHeaderDesc, 1, 0);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescAO, 1, 1);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescPdu, 1, 2);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankPdu, 0, 2);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescCC, 1, 3);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescUD, 1, 4);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescEC, 1, 5);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescIC, 1, 6);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescCompLevel, 1, 7);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankCC, 0, 3);
            tableLayoutPanelAstAnalysis.Controls.Add(labelUD, 0, 4);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankEC, 0, 5);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankIC, 0, 6);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankCompLevel, 0, 7);
            tableLayoutPanelAstAnalysis.Controls.Add(txvYesTotalAccountOfficer, 2, 1);
            tableLayoutPanelAstAnalysis.Controls.Add(labelBudgetHeaderRank, 0, 9);
            tableLayoutPanelAstAnalysis.Controls.Add(labelBudgetHeaderDesc, 1, 9);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankTotalProcBudget, 0, 10);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescTotalProcBudget, 1, 10);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankActualProcBudget, 0, 11);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescActualProcBudget, 1, 11);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankProcBudgetAbsorptionRate, 0, 12);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescAbsorptionRate, 1, 12);
            tableLayoutPanelAstAnalysis.Controls.Add(labelHeaderRankOpenBids, 0, 14);
            tableLayoutPanelAstAnalysis.Controls.Add(labelHeaderDescOpenBids, 1, 14);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankValueOfProc, 0, 15);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescValueOfProc, 1, 15);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankValueOfOpenBidContracts, 0, 16);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescValueOfOpenBids, 1, 16);
            tableLayoutPanelAstAnalysis.Controls.Add(labelRankProportionValueOfContracts, 0, 17);
            tableLayoutPanelAstAnalysis.Controls.Add(labelDescProportionValOfContractsOnOpenBids, 1, 17);
            tableLayoutPanelAstAnalysis.Controls.Add(txvTotalProcBudget, 5, 10);
            tableLayoutPanelAstAnalysis.Controls.Add(txvTotalActualProc, 5, 11);
            tableLayoutPanelAstAnalysis.Controls.Add(txvProcBudgetAbsorptioRate, 5, 12);
            tableLayoutPanelAstAnalysis.Controls.Add(txvTotalValueOfProcs, 5, 15);
            tableLayoutPanelAstAnalysis.Controls.Add(txvTotalValueOfOpenBidContracts, 5, 16);
            tableLayoutPanelAstAnalysis.Controls.Add(txvProportionOfContractsOnOpenBi, 5, 17);
            tableLayoutPanelAstAnalysis.Location = new Point(3, 3);
            tableLayoutPanelAstAnalysis.Name = "tableLayoutPanelAstAnalysis";
            tableLayoutPanelAstAnalysis.RowCount = 21;
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            tableLayoutPanelAstAnalysis.RowStyles.Add(new RowStyle(SizeType.Absolute, 25F));
            
            return tableLayoutPanelAstAnalysis;

        }

    }

}

﻿using Processor.Entities.PM.disposal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmisTool.Code
{
    public interface IAdtAnalyisUpdater
    {

        void updateAdtSectionScoresInAnalyisTab(SectionEvaluationAdt section);
        void autofillAdtAnalysisTabTextFieldBasedOnEvalItemValue(SectionItemEvaluationAdt sectionItem);
        void loadExceptionsBasedOnExceptionSectionSelected(object sender, System.EventArgs e);

    }
}

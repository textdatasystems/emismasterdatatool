﻿
using EmisMasterDataTool.Code;
using EmisTool.Code.ui.custom;
using Processor.Entities;
using Processor.Entities.PM;
using Processor.Entities.PM.disposal;
using Processor.Entities.PM.systems;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code
{
    public class UiHelperAdt
    {

        public static int FieldCountPmAuditProcessTool = 0;

        public static TableLayoutPanel GenerateTableV1(
            AuditDisposalToolSection toolSection, out SectionEvaluationAdt sectionEval,
            AppStateAuditTools appState,
            List<ManagementLetterSection> managementLetterSections)
        {
                                   
            String sectionTableFieldName = "AuditDisposalToolSec" + toolSection.Id;

            //initialize the out variable
            sectionEval = new SectionEvaluationAdt();
            sectionEval.sectionEmisId = toolSection.Id;
            sectionEval.name = toolSection.name;
            sectionEval.sectionTableFieldName = sectionTableFieldName;
            var sectionItemsEvalList = sectionEval.sectionItemEvaluations;
                       
            
            //get the list of items in this section
            List<AuditDisposalToolSectionItem> sectionItems = toolSection.items;


            int COLUMN_COUNT = 7;
            int ROW_COUNT = sectionItems.Count + 2; // add the header row and the footer row


            TableLayoutPanel tableLayoutPanel = new TableLayoutPanel();

            //Now we will generate the table, setting up the row and column counts first
            tableLayoutPanel.ColumnCount = COLUMN_COUNT;
            tableLayoutPanel.RowCount = ROW_COUNT;


            for (int columnIndex = 0; columnIndex < COLUMN_COUNT; columnIndex++)
            {

                //First add a column

                float columnWidth = getColumnWidth(columnIndex);
                tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, columnWidth));

                for (int rowIndex = 0; rowIndex < ROW_COUNT; rowIndex++)
                {

                    bool isFooterRow = rowIndex == ROW_COUNT - 1;
                    bool isHeaderRow = rowIndex == 0;

                    //Next, add a row.  Only do this when once, when creating the first column
                    if (columnIndex == 0 && isHeaderRow || columnIndex == 0 && isFooterRow)
                    {
                        tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));
                    }
                    else if (columnIndex == 0)
                    {
                        tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));
                    }
                    

                    //generate the cell control
                    Control control;
                    if (isHeaderRow)
                    {
                        control = buildHeaderCellControlV1(columnIndex, toolSection);
                    }
                    else if (isFooterRow)
                    {
                        control = buildFoooterCellControl(columnIndex);

                        if(columnIndex == 2)
                        {
                            PanelPmAuditDisposalTool.ADT_SECTION_TOTAL_TXVS.Add(toolSection.Id, control);
                        }

                    }
                    else
                    {

                        AuditDisposalToolSectionItem currentItem = sectionItems[rowIndex - 1];
                        control = buildBodyCellControlV1(columnIndex, currentItem, managementLetterSections, appState);


                        //begin logic for updating app state  
                        
                        //ensure that the item exists in the dictionary if not add
                        if (!sectionItemsEvalList.ContainsKey(currentItem.emisId))
                        {

                            var sectionEvalItem = new SectionItemEvaluationAdt();
                            sectionEvalItem.sectionEmisId = toolSection.Id;
                            sectionEvalItem.sectionItemEmisId = currentItem.emisId;
                            sectionEvalItem.yesRank = currentItem.yesRank;
                            sectionEvalItem.noRank = currentItem.noRank;
                            sectionEvalItem.naRank = currentItem.naRank;
                            sectionEvalItem.description = currentItem.description;

                            sectionItemsEvalList.Add(currentItem.emisId, sectionEvalItem);

                        }
                        

                        var oldSectionItemEval = sectionItemsEvalList[currentItem.emisId];
                        oldSectionItemEval.evaluationFieldRole = currentItem.fieldRole;
                        oldSectionItemEval.evaluationFieldRole = currentItem.description;
                        oldSectionItemEval.description = currentItem.description;

                        //we now update the fields of the sectionEvaItem
                        var sectionEvalItemUpdated = updateSectionEvalItemAdt(oldSectionItemEval, columnIndex, currentItem, control);

                        //update the item in the list
                        sectionItemsEvalList[sectionEvalItemUpdated.sectionEmisId] = sectionEvalItemUpdated;

                        //end logic for updating app state

                    }

                    control.Leave += appState.updatePmAuditDisposalToolState;
                    tableLayoutPanel.Controls.Add(control, columnIndex, rowIndex);
                                      

                }

            }


            tableLayoutPanel.Dock = DockStyle.Fill;
            tableLayoutPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanel.AutoScroll = false;
            tableLayoutPanel.AutoSize = true;
            tableLayoutPanel.Name = sectionTableFieldName;
            
            return tableLayoutPanel;

        }

        private static SectionItemEvaluationAdt updateSectionEvalItemAdt(SectionItemEvaluationAdt sectionEvalItemToUpdate, int columnIndex, AuditDisposalToolSectionItem sectionItem, Control control)
        {

            int evaluationFieldIndex = 2;
            int findingFieldIndex = 3;
            int exceptionFieldIndex = 4;
            int managementLetterSectionFieldIndex = 5;
            int managementLetterSectionItemFieldIndex = 6;

            if (columnIndex == evaluationFieldIndex)
            {
                sectionEvalItemToUpdate.evaluationFieldName = control.Name;
                sectionEvalItemToUpdate.evaluationFieldType = sectionItem.responseType;
            }
            else if(columnIndex == findingFieldIndex)
            {
                sectionEvalItemToUpdate.findingFieldName = control.Name;
            }
            else if(columnIndex == exceptionFieldIndex)
            {
                sectionEvalItemToUpdate.exceptionFieldName = control.Name;
            }
            else if (columnIndex == managementLetterSectionFieldIndex)
            {
                sectionEvalItemToUpdate.managementLetterSectionFieldName = control.Name;
            }
            else if (columnIndex == managementLetterSectionItemFieldIndex)
            {
                sectionEvalItemToUpdate.managementLetterSectionItemFieldName = control.Name;
            }

            return sectionEvalItemToUpdate;

        }

        private static float getColumnWidth(int columnIndex)
        {

            if (columnIndex == 0)
            {
                return 50F;
            }
            else if (columnIndex == 1)
            {
                return 200F;
            }
            else if (columnIndex == 2)
            {
                return 170F;
            }
            else if (columnIndex == 3)
            {
                return 300F;
            }
            else if (columnIndex == 4)
            {
                return 280F;
            }
            else if (columnIndex == 5 || columnIndex == 6)
            {
                return 120F;
            }
            else
            {
                return 0;
            }

        }

        private static Control buildBodyCellControlV1(
            int columnIndex, AuditDisposalToolSectionItem sectionItem,
            List<ManagementLetterSection> managementLetterSections, AppStateAuditTools appState)
        {

            Control control = new Control();

            FieldCountPmAuditProcessTool++;
            String fieldName = "AuditDisposalToolField" + FieldCountPmAuditProcessTool;


            if (columnIndex == 0)
            {
                Label labelRank = new Label() { Text = sectionItem.rank.ToString() };
                labelRank.Name = fieldName;
                return labelRank;
            }
            else if (columnIndex == 1)
            {
                String text = sectionItem.description;
                Label labelDesc = new Label() { Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft, Text = text, AutoSize = true };
                labelDesc.Name = fieldName;

                //if it has important text show the text
                new ToolTip().SetToolTip(labelDesc, "More description");

                return labelDesc;
            }
            else if (columnIndex == 2)
            {
                //here we return a table layout                 
                Control evaluationFieldControl = buildResponseFieldControl(sectionItem.responseType);
                evaluationFieldControl.Name = fieldName;
                return evaluationFieldControl;
            }
            else if (columnIndex == 3 || columnIndex == 4)
            {

                RichTextBox textbox = new RichTextBox();
                textbox.Height = 50;
                textbox.Width = 350;
                textbox.Dock = DockStyle.Fill;
                textbox.Name = fieldName;
                return textbox;

            }
            else if (columnIndex == 5)
            {

                ComboBox cbx = new ComboBox();
                cbx.Width = 120;
                cbx.Name = fieldName;
                cbx.DropDownStyle = ComboBoxStyle.DropDownList;
                cbx = WidgetHandler.populateManagementLetterSectionsDropdown(cbx, managementLetterSections);
                cbx.SelectedIndexChanged += appState.iAdtAnalyisUpdater.loadExceptionsBasedOnExceptionSectionSelected;

                return cbx;

            }
            else if (columnIndex == 6)
            {

                ComboBox cbx = new ComboBox();
                cbx.Items.Add("Select option");
                cbx.Width = 120;
                cbx.Name = fieldName;
                cbx.DropDownStyle = ComboBoxStyle.DropDownList;
                cbx.SelectedIndex = cbx.FindString("Select option");
                return cbx;

            }
            else
            {
                return new Label();
            }



        }

        private static Control buildBodyCellControl(int columnIndex, string fieldType, String textContent = "Rank")
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                Label labelRank = new Label() { Text = textContent};
                return labelRank;
            }
            else if (columnIndex == 1)
            {
                //here we return a table layout 
                String text = textContent == "Rank" ? "Item Description" : textContent;
                Label labelRank = new Label() { Text = text };
                
                //if it has important text show the text
                new ToolTip().SetToolTip(labelRank, "More description");

                return labelRank;
            }
            else if(columnIndex == 2)
            {
                //here we return a table layout                 
                return buildResponseFieldControl(fieldType);
            }
            else
            {

                RichTextBox textbox = new RichTextBox();
                textbox.Height = 50;
                textbox.Width = 400;                           
                return textbox;

            }
                      

        }
        
        private static Control buildFoooterCellControl(int columnIndex)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                return new Label();
            }
            else if (columnIndex == 1)
            {
                
                Label labelRank = new Label() { Text = "TOTAL", ForeColor = Color.DarkRed };
                return labelRank;
            }
            else if (columnIndex == 2)
            {
                TextBox textbox = new TextBox();
                textbox.Height = 25;
                textbox.Dock = DockStyle.Fill;
                return textbox;
            }
            else
            {
                return new Label(); 
            }


        }

        private static Control buildHeaderCellControlV1(int columnIndex, AuditDisposalToolSection toolSection)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                Label sectionRankLabel = new Label() { Text = toolSection.rank.ToString(), ForeColor = Color.DarkBlue };
                return sectionRankLabel;
            }
            else if (columnIndex == 1)
            {
                Label sectionNameLabel = new Label() { Text = toolSection.name.ToUpper(), ForeColor = Color.DarkBlue, Dock=DockStyle.Fill };
                return sectionNameLabel;
            }
            else if (columnIndex == 2)
            {
                return new Label();
            }
            else if (columnIndex == 3)
            {
                Label sectionColumn4Label = new Label() { Dock = DockStyle.Fill, Text = "ROUGH NOTES", AutoSize = false, Width = 400, ForeColor = Color.DarkBlue };
                return sectionColumn4Label;
            }
            else if (columnIndex == 4)
            {
                Label sectionColumn5Label = new Label() { Dock = DockStyle.Fill, Text = "EXCEPTIONS TO INCLUDE IN FINAL REPORT", ForeColor = Color.DarkBlue };
                return sectionColumn5Label;
            }
            else if (columnIndex == 5)
            {
                Label sectionColumn6Label =
                    new Label() { Text = "Exception Category", Dock = DockStyle.Fill, ForeColor = Color.DarkBlue };
                return sectionColumn6Label;
            }
            else if (columnIndex == 6)
            {
                Label sectionColumn7Label = new Label() { Text = "Exception Subcategory", Dock = DockStyle.Fill, ForeColor = Color.DarkBlue };
                return sectionColumn7Label;
            }
            else
            {
                return new Label();
            }

        }

        private static Control buildHeaderCellControl(int columnIndex)
        {

            Control control = new Control();

            if (columnIndex == 0)
            {
                Label sectionRankLabel = new Label() { Text = "1", ForeColor = Color.DarkBlue };
                return sectionRankLabel;
            }
            else if (columnIndex == 1)
            {
                Label sectionNameLabel = new Label() { Text = "SECTION A", ForeColor = Color.DarkBlue };
                return sectionNameLabel;
            }
            else if (columnIndex == 2)
            {
                return new Label();
            }
            else if (columnIndex == 3)
            {
                Label sectionColumn4Label = new Label() { Text = "AUDIT FINDINGS AND EVIDENCE", AutoSize = false, Width = 400, ForeColor = Color.DarkBlue };
                return sectionColumn4Label;
            }
            else if (columnIndex == 4)
            {
                Label sectionColumn5Label = new Label() { Text = "EXCEPTIONS", ForeColor = Color.DarkBlue };
                return sectionColumn5Label;
            }
            else
            {
                return new Label();
            }                  
     
        }

        private static Control buildResponseFieldControl(string fieldType)
        {

            if(fieldType == "yesnona")
            {

                TableLayoutPanel panel = new TableLayoutPanel();
                panel.ColumnCount = 3;
                panel.RowCount = 1;
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 34));
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33));
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33));

                panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 25));

                panel.Controls.Add(new RadioButton() { Text = "Yes" }, 0, 0);
                panel.Controls.Add(new RadioButton() { Text = "No" }, 1, 0);
                panel.Controls.Add(new RadioButton() { Text = "Na", Checked = true }, 2, 0);

                panel.Dock = DockStyle.Fill;
                return panel;

            }
            else if (fieldType == "date")
            {
                DateTimePicker datePicker = new DateTimePicker();
                datePicker.Height = 25;
                datePicker.Format = DateTimePickerFormat.Short;
                datePicker.Dock = DockStyle.Fill;
                return datePicker;
            }
            else
            {
                TextBox textbox = new TextBox();
                textbox.Height = 25;
                textbox.Dock = DockStyle.Fill;
                return textbox;
            }

        }


        public static TableLayoutPanel GenerateIdentificationSectionTable(List<User> users)
        {

            int COLUMN_COUNT = 4;
            int ROW_COUNT = 13;

            TableLayoutPanel panel = new TableLayoutPanel();
            
            panel.ColumnCount = COLUMN_COUNT;
            panel.RowCount = ROW_COUNT;

            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 350F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));

            //section 1

            Label label = new Label() { Text = "ASSETS DISPOSAL PROCESS AUDIT CRITERIA", Dock = DockStyle.Fill };
            panel.Controls.Add(label, 1, 0);
                  
            panel.Controls.Add(new Label() { Text = "1.0", Dock = DockStyle.Fill }, 0, 1);
            panel.Controls.Add(new Label() { Text = "IDENTIFICATION", Dock = DockStyle.Fill }, 1, 1);

            panel.Controls.Add(new Label() { Text = "1.1", Dock = DockStyle.Fill }, 0, 2);
            panel.Controls.Add(new Label() { Text = "Name of Entity", Dock = DockStyle.Fill }, 1, 2);
            panel.Controls.Add(new TextBox() { Name = "txvAdtNameOfEntity", Dock = DockStyle.Fill }, 2, 2);
                        
            panel.Controls.Add(new Label() { Text = "1.2", Dock = DockStyle.Fill }, 0, 3);
            panel.Controls.Add(new Label() { Text = "Disposal Reference No.", Dock = DockStyle.Fill }, 1, 3);
            panel.Controls.Add(new TextBox() { Name = "txvAdtReferenceNo", Dock = DockStyle.Fill }, 2, 3);
            
            panel.Controls.Add(new Label() { Text = "1.3", Dock = DockStyle.Fill }, 0, 4);
            panel.Controls.Add(new Label() { Text = "Audit Period", Dock = DockStyle.Fill }, 1, 4);
            panel.Controls.Add(new TextBox() { Name = "txvAdtAuditPeriod", Dock = DockStyle.Fill }, 2, 4);
                       
            panel.Controls.Add(new Label() { Text = "1.4", Dock = DockStyle.Fill }, 0, 5);
            panel.Controls.Add(new Label() { Text = "Asset Description", Dock = DockStyle.Fill }, 1, 5);
            panel.Controls.Add(new TextBox() { Name = "txvAdtAssetDescription", Dock = DockStyle.Fill }, 2, 5);

            panel.Controls.Add(new Label() { Text = "1.5", Dock = DockStyle.Fill }, 0, 6);
            panel.Controls.Add(new Label() { Text = "Name of Buyer", Dock = DockStyle.Fill }, 1, 6);
            panel.Controls.Add(new TextBox() { Name = "txvAdtNameOfBuyer", Dock = DockStyle.Fill }, 2, 6);

            panel.Controls.Add(new Label() { Text = "1.6", Dock = DockStyle.Fill }, 0, 7);
            panel.Controls.Add(new Label() { Text = "Disposal Value", Dock = DockStyle.Fill }, 1, 7);
            panel.Controls.Add(new TextBox() { Name = "txvAdtDisposalValue", Dock = DockStyle.Fill }, 2, 7);
            
            panel.Controls.Add(new Label() { Text = "1.7", Dock = DockStyle.Fill }, 0, 8);
            panel.Controls.Add(new Label() { Text = "Valuation Amount", Dock = DockStyle.Fill }, 1, 8);
            panel.Controls.Add(new TextBox() { Name = "txvAdtValuationAmount", Dock = DockStyle.Fill }, 2, 8);

            panel.Controls.Add(new Label() { Text = "1.8", Dock = DockStyle.Fill }, 0, 9);
            panel.Controls.Add(new Label() { Text = "Disposal Method", Dock = DockStyle.Fill }, 1, 9);
            ComboBox cbx1 = new ComboBox() { Name = "cbxAdtDisposalMethod", Dock = DockStyle.Fill };
            cbx1.Items.Add("Select Option");
            cbx1.Items.Add("Bidding");
            cbx1.Items.Add("Auction");
            cbx1.Items.Add("Direct");
            cbx1.Items.Add("Officers");
            cbx1.Items.Add("Destruction");
            cbx1.Items.Add("Conversion");
            cbx1.Items.Add("Trade-In");
            cbx1.Items.Add("Transfer");
            cbx1.Items.Add("Donation");
            cbx1.SelectedIndex = 0;
            panel.Controls.Add(cbx1, 2, 9);
            
            ComboBox cbxAdtAuditorName = new ComboBox() { Name = "cbxAdtAuditorName", Dock = DockStyle.Fill };
            cbxAdtAuditorName = WidgetHandler.populateUsersWithEmailValueCombo(cbxAdtAuditorName, users);

            panel.Controls.Add(new Label() { Text = "", Dock = DockStyle.Fill }, 0, 10);
            panel.Controls.Add(new Label() { Text = "Auditor Name", Dock = DockStyle.Fill }, 1, 10);
            panel.Controls.Add(cbxAdtAuditorName, 2, 10);


            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 11);

            panel.Dock = DockStyle.Fill;
            panel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            panel.AutoScroll = true;
            panel.AutoSize = true;

            return panel;

        }


        public static TableLayoutPanel GenerateDisposalAnalysisTable(EventHandler eventHandlerComplianceScoreTextChange)
        {

            int COLUMN_COUNT = 2;
            int ROW_COUNT = 15;

            TableLayoutPanel panel = new TableLayoutPanel();

            panel.ColumnCount = COLUMN_COUNT;
            panel.RowCount = ROW_COUNT;
            
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            panel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            
            
            panel.Controls.Add(new Label() { Text = "COMPLIANCE ANALYSIS", Dock = DockStyle.Fill }, 0, 0);
            panel.Controls.Add(new Label() { Text = "COMPLIANCE SCORE", Dock = DockStyle.Fill }, 1, 0);
            
            panel.Controls.Add(new Label() { Text = "Plan", Dock = DockStyle.Fill }, 0, 1);
            TextBox txvPlan = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_PLAN, Dock = DockStyle.Fill };
            txvPlan.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvPlan, 1, 1);
            
            panel.Controls.Add(new Label() { Text = "Initiation", Dock = DockStyle.Fill }, 0, 2);
            TextBox txvInitiation = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_INITIATION, Dock = DockStyle.Fill };
            txvInitiation.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvInitiation, 1, 2);
            
            panel.Controls.Add(new Label() { Text = "Public Bidding", Dock = DockStyle.Fill }, 0, 3);
            TextBox txvPublicBidding = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_PUBLIC_BIDDING, Dock = DockStyle.Fill };
            txvPublicBidding.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvPublicBidding, 1, 3);
            
            panel.Controls.Add(new Label() { Text = "Public Auction", Dock = DockStyle.Fill }, 0, 4);
            TextBox txvPublicAuction = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_PUBLIC_AUCTION, Dock = DockStyle.Fill };
            txvPublicAuction.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvPublicAuction, 1, 4);

            panel.Controls.Add(new Label() { Text = "Direct Negotiations", Dock = DockStyle.Fill }, 0, 5);
            TextBox txvDirectNegotiations = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_DIRECT_NEGOTIATIONS, Dock = DockStyle.Fill };
            txvDirectNegotiations.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvDirectNegotiations, 1, 5);

            panel.Controls.Add(new Label() { Text = "Public Officers", Dock = DockStyle.Fill }, 0, 6);
            TextBox txvPublicOfficers = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_PUBLIC_OFFICERS, Dock = DockStyle.Fill };
            txvPublicOfficers.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvPublicOfficers, 1, 6);

            panel.Controls.Add(new Label() { Text = "Destruction", Dock = DockStyle.Fill }, 0, 7);
            TextBox txvDestruction = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_DESTRUCTION, Dock = DockStyle.Fill };
            txvDestruction.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvDestruction, 1, 7);

            panel.Controls.Add(new Label() { Text = "Conversion", Dock = DockStyle.Fill }, 0, 8);
            TextBox txvConversion = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_CONVERSION, Dock = DockStyle.Fill };
            txvConversion.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvConversion, 1, 8);

            panel.Controls.Add(new Label() { Text = "Trade-In", Dock = DockStyle.Fill }, 0, 9);
            TextBox txvTradeIn = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_TRADE_IN, Dock = DockStyle.Fill };
            txvTradeIn.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvTradeIn, 1, 9);

            panel.Controls.Add(new Label() { Text = "Transfer", Dock = DockStyle.Fill }, 0, 10);
            TextBox txvTransfer = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_TRANSFER, Dock = DockStyle.Fill };
            txvTransfer.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvTransfer, 1, 10);

            panel.Controls.Add(new Label() { Text = "Donation", Dock = DockStyle.Fill }, 0, 11);
            TextBox txvDonation = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_DONATION, Dock = DockStyle.Fill };
            txvDonation.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvDonation, 1, 11);

            panel.Controls.Add(new Label() { Text = "Delivery", Dock = DockStyle.Fill }, 0, 12);
            TextBox txvDelivery = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_DELIVERY, Dock = DockStyle.Fill };
            txvDelivery.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvDelivery, 1, 12);

            panel.Controls.Add(new Label() { Text = "Records", Dock = DockStyle.Fill }, 0, 13);
            TextBox txvRecords = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_RECORDS, Dock = DockStyle.Fill };
            txvRecords.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvRecords, 1, 13);

            panel.Controls.Add(new Label() { Text = "AVERAGE  COMPLIANCE LEVEL", Dock = DockStyle.Fill }, 0, 14);
            TextBox txvAverage = new TextBox() { Name = PanelPmAuditDisposalTool.FLD_SCORE_AVERAGE, Dock = DockStyle.Fill };
            txvAverage.TextChanged += eventHandlerComplianceScoreTextChange;
            panel.Controls.Add(txvAverage, 1, 14);
                       

            //spacer
            panel.Controls.Add(new Label() { Text = "" }, 0, 15);

            //panel.Dock = DockStyle.Fill;
            panel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            panel.AutoScroll = true;
            panel.AutoSize = true;

            return panel;

        }



    }

}

﻿using EmisMasterDataTool.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code
{
    public class CalculationHelper
    {

        public void aptInitiationStartDelayTime(Panel panel)
        {
            String startDateField = "Sect101PercentScore";
            String endDateField = "Sect102PercentScore";
            String answerField = "Sect111PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);
        }

        public void aptRequisitionTime(Panel panel)
        {
            String startDateField = "Sect102PercentScore";
            String endDateField = "Sect103PercentScore";
            String answerField = "Sect112PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);
        }

        public void aptBidSubmissionPeriod(Panel panel)
        {
            String startDateField = "Sect103PercentScore";
            String endDateField = "Sect104PercentScore";
            String answerField = "Sect113PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);
        }
        public void aptBidEvaluationTime(Panel panel)
        {
            String startDateField = "Sect104PercentScore";
            String endDateField = "Sect105PercentScore";
            String answerField = "Sect114PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);
        }

        public void aptContractAwardPeriod(Panel panel)
        {
            String startDateField = "Sect105PercentScore";
            String endDateField = "Sect106PercentScore";
            String answerField = "Sect115PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);

        }
        public void aptNoBEBdisplayperiod(Panel panel)
        {
            String startDateField = "Sect107PercentScore";
            String endDateField = "Sect108PercentScore";
            String answerField = "Sect116PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);
        }
        public void aptContractingTime(Panel panel)
        {
            String startDateField = "Sect108PercentScore";
            String endDateField = "Sect109PercentScore";
            String answerField = "Sect117PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);

        }
        public void aptActualProcurementTime(Panel panel)
        {
            String startDateField = "Sect102PercentScore";
            String endDateField = "Sect109PercentScore";
            String answerField = "Sect118PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);

        }
        public void aptPlannedProcurementTime(Panel panel)
        {
            String startDateField = "Sect101PercentScore";
            String endDateField = "Sect1010PercentScore";
            String answerField = "Sect119PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);
        }
        public void aptProcurementTimeOverrun(Panel panel)
        {
            String startDateField = "Sect118PercentScore";
            String endDateField = "Sect119PercentScore";
            String answerField = "Sect1110PercentScore";
            calculatNumberDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);
        }
        public void aptContractualCompletionTime(Panel panel)
        {
            String startDateField = "Sect109PercentScore";
            String endDateField = "Sect1011PercentScore";
            String answerField = "Sect1111PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);

        }
        public void aptActualCompletionTime(Panel panel)
        {
            String startDateField = "Sect109PercentScore";
            String endDateField = "Sect1012PercentScore";
            String answerField = "Sect1112PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);
        }
        public void aptCompletionTimeOverrun(Panel panel)
        {
            String startDateField = "Sect1112PercentScore";
            String endDateField = "Sect1111PercentScore";
            String answerField = "Sect1113PercentScore";
            calculatNumberDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);
        }
        public void aptContractualpaymentperiodindays(Panel panel)
        {
            String startDateField = "";
            String endDateField = "";
            String answerField = "Sect1114PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);

        }
        public void aptActualPaymentPeriodinDays(Panel panel)
        {
            String startDateField = "Sect1013PercentScore";
            String endDateField = "Sect1014PercentScore";
            String answerField = "Sect1115PercentScore";
            calculateDateDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);

            //aptProcurementTimeOverrun(panel);

        }
        public void aptPaymentPeriodOverrun(Panel panel)
        {
            String startDateField = "Sect1115PercentScore";
            String endDateField = "Sect1114PercentScore";
            String answerField = "Sect1116PercentScore";
            calculatNumberDiffAndPopulateAnswerField(panel, startDateField, endDateField, answerField);

        }

        public void aptProcurementTimeOverrunPercent(Panel panel)
        {
            String topValueField = "Sect1110PercentScore";
            String bottomValueField = "Sect119PercentScore";
            String answerField = "Sect1117PercentScore";
            calculateRatioExpressToPercentageAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }
        public void aptCompletionTimeOverrunPercent(Panel panel)
        {
            String topValueField = "Sect1113PercentScore";
            String bottomValueField = "Sect1111PercentScore";
            String answerField = "Sect1118PercentScore";
            calculateRatioExpressToPercentageAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }
        public void aptPaymentPeriodOverrunPercent(Panel panel)
        {
            String topValueField = "Sect1116PercentScore";
            String bottomValueField = "Sect1114PercentScore";
            String answerField = "Sect1119PercentScore";
            calculateRatioExpressToPercentageAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }
        public void aptProcureRatio(Panel panel)
        {
            String topValueField = "Sect118PercentScore";
            String bottomValueField = "Sect119PercentScore";
            String answerField = "Sect1120PercentScore";
            calculateRatioAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }
        public void aptCompletionRatio(Panel panel)
        {
            String topValueField = "Sect1112PercentScore";
            String bottomValueField = "Sect1111PercentScore";
            String answerField = "Sect1121PercentScore";
            calculateRatioAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }
        public void aptPaymentRatio(Panel panel)
        {
            String topValueField = "Sect1115PercentScore";
            String bottomValueField = "Sect1114PercentScore";
            String answerField = "Sect1122PercentScore";
            calculateRatioAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }


        public void aptBidSubmissionRate(Panel panel)
        {
            String topValueField = "Sect122PercentScore"; 
            String bottomValueField = "Sect121PercentScore"; ;
            String answerField = "Sect125PercentScore";
            calculateRatioAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }

        public void aptBidResponsiveRate(Panel panel)
        {
            String topValueField = "Sect124PercentScore";
            String bottomValueField = "Sect122PercentScore";
            String answerField = "Sect126PercentScore";
            calculateRatioAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }


        public void aptBudgetVarianceAmount(Panel panel)
        {
            String firstNoField = "Sect134PercentScore";
            String secondNoField = "Sect131PercentScore";
            String answerField = "Sect137PercentScore";
            calculatNumberDiffAndPopulateAnswerField(panel, firstNoField, secondNoField, answerField);

        }


        public void aptBudgetVariance(Panel panel)
        {
            String topValueField = "Sect137PercentScore";
            String bottomValueField = "Sect131PercentScore";
            String answerField = "Sect138PercentScore";
            calculateRatioExpressToPercentageAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }

        public void aptCostoverrun(Panel panel)
        {
            String topValueField = "Sect136PercentScore";
            String bottomValueField = "Sect134PercentScore";
            String answerField = "Sect139PercentScore";
            calculateRatioExpressToPercentageAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }

        public void aptPlanRatio(Panel panel)
        {
            String topValueField = "Sect134PercentScore";
            String bottomValueField = "Sect131PercentScore";
            String answerField = "Sect1310PercentScore";
            calculateRatioAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }

        public void aptCostRatio(Panel panel)
        {
            String topValueField = "Sect135PercentScore";
            String bottomValueField = "Sect134PercentScore";
            String answerField = "Sect1311PercentScore";
            calculateRatioAndPopulateAnswerField(panel, topValueField, bottomValueField, answerField);
        }

        public void aptProcurementProcessComplianceLevel(Panel panel)
        {

            var list = new List<String> { "Sect97PercentScore", "Sect96PercentScore", "Sect95PercentScore", "Sect94PercentScore", "Sect93PercentScore",
                                          "Sect92PercentScore", "Sect91PercentScore" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(panel, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(panel, "Sect98PercentScore"));
        }


        private static void calculateDateDiffAndPopulateAnswerField(Panel panel, string startDateField, string endDateField, string answerField)
        {

            DateTime startDate;
            DateTime endDate;

            String start = null;
            String end = null;

            if (String.IsNullOrEmpty(startDateField) || String.IsNullOrEmpty(endDateField))
            {
                return;
            }


            //attempt to get the start date and end date

            var txvStartFields = panel.Controls.Find(startDateField, true);
            if (txvStartFields != null && txvStartFields.Length > 0)
            {
                start = txvStartFields[0].Text;
            }
            
            var txvEndFields = panel.Controls.Find(endDateField, true);
            if (txvEndFields != null && txvEndFields.Length > 0)
            {
                end = txvEndFields[0].Text;
            }
            

            //if any of the dates is null we just return
            if(String.IsNullOrEmpty(start) || String.IsNullOrEmpty(end))
            {
                return;
            }


            //now we have the dates, lets get the difference

            if (DateTime.TryParse(start, out startDate) && DateTime.TryParse(end, out endDate))
            {

                // var days = (endDate - startDate).TotalDays;
                var days = GetNumberOfWorkingDays(startDate, endDate); //GetBusinessDays(startDate, endDate);

                var txvs = panel.Controls.Find(answerField, true);
                if (txvs != null && txvs.Length > 0)
                {
                    txvs[0].Text = days.ToString();
                }

            }
            else
            {
                var txvs = panel.Controls.Find(answerField, true);
                if (txvs != null && txvs.Length > 0)
                {
                    txvs[0].Text = "";
                }

            }

        }


        private static int GetNumberOfWorkingDays(DateTime start, DateTime stop)
        {
            var days = (stop - start).Days + 1;
            return workDaysInFullWeeks(days) + workDaysInPartialWeek(start.DayOfWeek, days);
        }

        private static int workDaysInFullWeeks(int totalDays)
        {
            return (totalDays / 7) * 5;
        }

        private static int workDaysInPartialWeek(DayOfWeek firstDay, int totalDays)
        {
            var remainingDays = totalDays % 7;
            var daysToSaturday = (int)DayOfWeek.Saturday - (int)firstDay;
            if (remainingDays <= daysToSaturday)
                return remainingDays;
            /* daysToSaturday are the days before the weekend,
             * the rest of the expression computes the days remaining after we
             * ignore Saturday and Sunday
             */
            // Range ends in a Saturday or in a Sunday
            if (remainingDays <= daysToSaturday + 2)
                return daysToSaturday;
            // Range ends after a Sunday
            else
                return remainingDays - 2;
        }
        public static int GetBusinessDays(DateTime start, DateTime end)
        {

            if (start.DayOfWeek == DayOfWeek.Saturday)
            {
                start = start.AddDays(2);
            }
            else if (start.DayOfWeek == DayOfWeek.Sunday)
            {
                start = start.AddDays(1);
            }

            if (end.DayOfWeek == DayOfWeek.Saturday)
            {
                end = end.AddDays(-1);
            }
            else if (end.DayOfWeek == DayOfWeek.Sunday)
            {
                end = end.AddDays(-2);
            }

            int diff = (int)end.Subtract(start).TotalDays;

            int result = diff / 7 * 5 + diff % 7;

            if (end.DayOfWeek < start.DayOfWeek)
            {
                return result - 2;
            }
            else
            {
                return result;
            }
        }


        private static void calculateRatioExpressToPercentageAndPopulateAnswerField(Panel panel, string topValueField, string bottomValueField, string answerField)
        {

            long topValue;
            long bottomValue;

            String top = null;
            String bottom = null;

            if (String.IsNullOrEmpty(topValueField) || String.IsNullOrEmpty(bottomValueField))
            {
                return;
            }


            //attempt to get the top value and bottom

            var txvTopValFields = panel.Controls.Find(topValueField, true);
            if (txvTopValFields != null && txvTopValFields.Length > 0)
            {
                top = txvTopValFields[0].Text;
            }

            var txvBottomValFields = panel.Controls.Find(bottomValueField, true);
            if (txvBottomValFields != null && txvBottomValFields.Length > 0)
            {
                bottom = txvBottomValFields[0].Text;
            }


            //if any of the dates is null we just return
            if (String.IsNullOrEmpty(top) || String.IsNullOrEmpty(bottom))
            {
                return;
            }


            //now we have the dates, lets get the difference

            if (long.TryParse(top, out topValue) && long.TryParse(bottom, out bottomValue))
            {

                //division by 0
                if (bottomValue == 0)
                {
                    return;
                }

                var ratio = ((double)topValue/ bottomValue);

                var txvs = panel.Controls.Find(answerField, true);
                if (txvs != null && txvs.Length > 0)
                {
                    txvs[0].Text = (Math.Round(ratio,3) * 100).ToString();
                }

            }
            else
            {
                var txvs = panel.Controls.Find(answerField, true);
                if (txvs != null && txvs.Length > 0)
                {
                    txvs[0].Text = "";
                }

            }

        }

        private static void calculateRatioAndPopulateAnswerField(Panel panel, string topValueField, string bottomValueField, string answerField)
        {

            long topValue;
            long bottomValue;

            String top = null;
            String bottom = null;

            if (String.IsNullOrEmpty(topValueField) || String.IsNullOrEmpty(bottomValueField))
            {
                return;
            }


            //attempt to get the top value and bottom

            var txvTopValFields = panel.Controls.Find(topValueField, true);
            if (txvTopValFields != null && txvTopValFields.Length > 0)
            {
                top = txvTopValFields[0].Text;
            }

            var txvBottomValFields = panel.Controls.Find(bottomValueField, true);
            if (txvBottomValFields != null && txvBottomValFields.Length > 0)
            {
                bottom = txvBottomValFields[0].Text;
            }


            //if any of the dates is null we just return
            if (String.IsNullOrEmpty(top) || String.IsNullOrEmpty(bottom))
            {
                return;
            }


            //now we have the dates, lets get the difference

            if (long.TryParse(top, out topValue) && long.TryParse(bottom, out bottomValue))
            {

                //division by 0
                if (bottomValue == 0)
                {
                    return;
                }

                var ratio = ((double)topValue / bottomValue);

                var txvs = panel.Controls.Find(answerField, true);
                if (txvs != null && txvs.Length > 0)
                {
                    txvs[0].Text = (Math.Round(ratio, 3)).ToString();
                }

            }
            else
            {
                var txvs = panel.Controls.Find(answerField, true);
                if (txvs != null && txvs.Length > 0)
                {
                    txvs[0].Text = "";
                }

            }

        }





        private static void calculatNumberDiffAndPopulateAnswerField(Panel panel, string firstNoField, string secondNoField, string answerField)
        {

            long firstNo;
            long secondNo;

            String first = null;
            String second = null;

            if (String.IsNullOrEmpty(firstNoField) || String.IsNullOrEmpty(secondNoField))
            {
                return;
            }


            //attempt to get the top value and bottom

            var txvFirstValFields = panel.Controls.Find(firstNoField, true);
            if (txvFirstValFields != null && txvFirstValFields.Length > 0)
            {
                first = txvFirstValFields[0].Text;
            }

            var txvSecondValFields = panel.Controls.Find(secondNoField, true);
            if (txvSecondValFields != null && txvSecondValFields.Length > 0)
            {
                second = txvSecondValFields[0].Text;
            }


            //if any of the dates is null we just return
            if (String.IsNullOrEmpty(first) || String.IsNullOrEmpty(second))
            {
                return;
            }


            //now we have the dates, lets get the difference

            if (long.TryParse(first, out firstNo) && long.TryParse(second, out secondNo))
            {
             
                var difference = (firstNo - secondNo);

                var txvs = panel.Controls.Find(answerField, true);
                if (txvs != null && txvs.Length > 0)
                {
                    txvs[0].Text = difference.ToString();
                }

            }
            else
            {
                var txvs = panel.Controls.Find(answerField, true);
                if (txvs != null && txvs.Length > 0)
                {
                    txvs[0].Text = "";
                }

            }

        }


        internal void autoFillCompAndPerfScoreFields(Control parentControl, String currentFieldName, String value)
        {

            var dictValueFieldNameAnswerFieldName = new Dictionary<String, String>();

            dictValueFieldNameAnswerFieldName.Add("Sect98PercentScore", "txvAptComplianceLevel");
            dictValueFieldNameAnswerFieldName.Add("Sect1120PercentScore", "txvAptProcureRatio");
            dictValueFieldNameAnswerFieldName.Add("Sect1121PercentScore", "txvAptCompletionRatio");
            dictValueFieldNameAnswerFieldName.Add("Sect1122PercentScore", "txvAptPaymentRatio");
            dictValueFieldNameAnswerFieldName.Add("Sect122PercentScore", "txvAptNoOfBids");
            dictValueFieldNameAnswerFieldName.Add("Sect125PercentScore", "txvAptBidSubmissionRate");
            dictValueFieldNameAnswerFieldName.Add("Sect126PercentScore", "txvAptBidResponsiveRate");
            dictValueFieldNameAnswerFieldName.Add("Sect1310PercentScore", "txvAptPlanRatio");
            dictValueFieldNameAnswerFieldName.Add("Sect1311PercentScore", "txvAptCostRatio");

            if (String.IsNullOrEmpty(currentFieldName))
            {
                return;
            }

            String answerFieldName = dictValueFieldNameAnswerFieldName[currentFieldName];

            if (String.IsNullOrEmpty(answerFieldName))
            {
                return;
            }

            var answerTxvs = parentControl.Controls.Find(answerFieldName, true);
            if (answerTxvs != null && answerTxvs.Length > 0)
            {
                answerTxvs[0].Text = value;
            }
            
        }

        internal void calculateCaseAverageScore(long yesCount, long noCount, Control control)
        {

            long totalResponses = yesCount + noCount;

            //division by zero
            if (totalResponses == 0 || control == null)
            {
                return;
            }

            double yesPercentage = (yesCount * 100.0) / totalResponses;
            control.Text = Math.Round(yesPercentage, 0).ToString();

        }

        internal void calculateAverageExcludingNullAndEmpty(List<String> numbers, Control control, Control mirrorControl = null)
        {

            if (numbers.Count == 0)
            {
                return;
            }

            //get only the valid numbers
            List<long> validNumbers = new List<long>();
            foreach (var number in numbers)
            {
                long dValue = 0;
                if (long.TryParse(number, out dValue))
                {
                    validNumbers.Add(dValue);
                }
            }

            //calculate average            
            var average = validNumbers.Count == 0 ? 0 : validNumbers.Average();

            //display the average in the supplied field
            if (control != null)
            {
                control.Text = Math.Round(average, 0).ToString();
            }

            //this guy also displays the same info
            if (mirrorControl != null)
            {
                mirrorControl.Text = Math.Round(average, 0).ToString();
            }

        }


        internal void calculateCalculateCstAverageScore(long yesCount, long noCount, Control control)
        {

            long totalResponses = yesCount + noCount;

            //division by zero
            if (totalResponses == 0 || control == null)
            {
                return;
            }

            double yesPercentage = (yesCount * 100.0) / totalResponses;
            control.Text = Math.Round(yesPercentage, 0).ToString();

        }



    }

}

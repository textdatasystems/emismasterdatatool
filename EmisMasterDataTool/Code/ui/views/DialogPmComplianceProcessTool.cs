﻿using EmisTool.Code.ui.custom;
using Processor.Entities.api;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool.Code.ui.views
{
    public partial class DialogPmComplianceProcessTool : Form
    {

        private int activityId;
        private int sampleFileLocalId;
        private ApiPmCompProcessTool toolToEdit;

        public DialogPmComplianceProcessTool(int activityId, int sampleFileLocalId, ApiPmCompProcessTool toolToEdit = null)
        {

            InitializeComponent();
            this.activityId = activityId;
            this.sampleFileLocalId = sampleFileLocalId;
            this.toolToEdit = toolToEdit;

            initializeControls();

        }

        private void initializeControls()
        {
            var panelPmComplianceProcessTool = new PanelPmComplianceProcessTool(activityId, sampleFileLocalId, toolToEdit);
            panelPmComplianceProcessTool.Dock = DockStyle.Fill;
            panelContainer.Controls.Add(panelPmComplianceProcessTool);
        }

        #region .. Double Buffered function ..
        public static void SetDoubleBuffered(System.Windows.Forms.Control c)
        {
            if (System.Windows.Forms.SystemInformation.TerminalServerSession)
                return;
            System.Reflection.PropertyInfo aProp = typeof(System.Windows.Forms.Control).GetProperty("DoubleBuffered", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            aProp.SetValue(c, true, null);
        }

        #endregion


        #region .. code for Flucuring ..

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        #endregion

    }


}

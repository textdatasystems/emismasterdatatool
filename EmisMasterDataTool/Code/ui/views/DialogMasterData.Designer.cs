﻿namespace EmisMasterDataTool.Code.ui.views
{
    partial class DialogMasterData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogMasterData));
            this.panelContainerMasterData = new System.Windows.Forms.Panel();
            this.gpBoxEmisServerUploads = new System.Windows.Forms.GroupBox();
            this.gpBoxEmisServerDownloads = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.gpBoxServerDownloadsGeneral = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDownloadProcurementRoles = new System.Windows.Forms.Button();
            this.btnDownloadProcurementMethods = new System.Windows.Forms.Button();
            this.btnDownloadProviders = new System.Windows.Forms.Button();
            this.btnDownloadEntityPeople = new System.Windows.Forms.Button();
            this.btnDownloadPpdaUsers = new System.Windows.Forms.Button();
            this.labelProcRolesLastDownloadDate = new System.Windows.Forms.Label();
            this.labelProcMethodsLastDownloadDate = new System.Windows.Forms.Label();
            this.labelProvidersLastDownloadDate = new System.Windows.Forms.Label();
            this.labelEntyPeopleLastDownloadDate = new System.Windows.Forms.Label();
            this.labelPpdaUsersLastDownloadDate = new System.Windows.Forms.Label();
            this.labelEntitiesLastDownloadDate = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnDownloadEntities = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelFinancialYrsLastDownloadDate = new System.Windows.Forms.Label();
            this.labelDistrictsLastDownloadDate = new System.Windows.Forms.Label();
            this.labelFundingSourcesLastDownloadDate = new System.Windows.Forms.Label();
            this.labelMgtLetterSectionsLastDownloadDate = new System.Windows.Forms.Label();
            this.btnDownloadFinancialYears = new System.Windows.Forms.Button();
            this.btnDownloadDistricts = new System.Windows.Forms.Button();
            this.btnDownloadMgtLetterSections = new System.Windows.Forms.Button();
            this.btnDownloadFundingSources = new System.Windows.Forms.Button();
            this.gpBoxServerDownloadsPmActivities = new System.Windows.Forms.GroupBox();
            this.gridViewPmActivitiesSyncStatus = new System.Windows.Forms.DataGridView();
            this.financialYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastDowndloadDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Download = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cbxFinancialYears = new System.Windows.Forms.ComboBox();
            this.btnDownloadInitialFyActivities = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.gbBoxGeneralInfoCbActivities = new System.Windows.Forms.GroupBox();
            this.gbBoxEmisServerDetails = new System.Windows.Forms.GroupBox();
            this.btnSaveServerDetails = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txvServerBaseUrl = new System.Windows.Forms.TextBox();
            this.txvServerUsername = new System.Windows.Forms.TextBox();
            this.txvServerPassword = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.labelPpdaOfficesLastDownloadDate = new System.Windows.Forms.Label();
            this.btnDownloadPpdaOffices = new System.Windows.Forms.Button();
            this.panelContainerMasterData.SuspendLayout();
            this.gpBoxEmisServerDownloads.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gpBoxServerDownloadsGeneral.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.gpBoxServerDownloadsPmActivities.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPmActivitiesSyncStatus)).BeginInit();
            this.gbBoxEmisServerDetails.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelContainerMasterData
            // 
            this.panelContainerMasterData.Controls.Add(this.gpBoxEmisServerUploads);
            this.panelContainerMasterData.Controls.Add(this.gpBoxEmisServerDownloads);
            this.panelContainerMasterData.Controls.Add(this.gbBoxEmisServerDetails);
            this.panelContainerMasterData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContainerMasterData.Location = new System.Drawing.Point(0, 0);
            this.panelContainerMasterData.Name = "panelContainerMasterData";
            this.panelContainerMasterData.Padding = new System.Windows.Forms.Padding(10);
            this.panelContainerMasterData.Size = new System.Drawing.Size(1184, 661);
            this.panelContainerMasterData.TabIndex = 0;
            // 
            // gpBoxEmisServerUploads
            // 
            this.gpBoxEmisServerUploads.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpBoxEmisServerUploads.Location = new System.Drawing.Point(14, 608);
            this.gpBoxEmisServerUploads.Name = "gpBoxEmisServerUploads";
            this.gpBoxEmisServerUploads.Size = new System.Drawing.Size(1159, 41);
            this.gpBoxEmisServerUploads.TabIndex = 2;
            this.gpBoxEmisServerUploads.TabStop = false;
            this.gpBoxEmisServerUploads.Text = "EMIS Server Uploads";
            // 
            // gpBoxEmisServerDownloads
            // 
            this.gpBoxEmisServerDownloads.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpBoxEmisServerDownloads.Controls.Add(this.tableLayoutPanel2);
            this.gpBoxEmisServerDownloads.Location = new System.Drawing.Point(13, 151);
            this.gpBoxEmisServerDownloads.Name = "gpBoxEmisServerDownloads";
            this.gpBoxEmisServerDownloads.Padding = new System.Windows.Forms.Padding(10);
            this.gpBoxEmisServerDownloads.Size = new System.Drawing.Size(1159, 451);
            this.gpBoxEmisServerDownloads.TabIndex = 1;
            this.gpBoxEmisServerDownloads.TabStop = false;
            this.gpBoxEmisServerDownloads.Text = "EMIS Server Downloads";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel2.Controls.Add(this.gpBoxServerDownloadsGeneral, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.gpBoxServerDownloadsPmActivities, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.gbBoxGeneralInfoCbActivities, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(10, 25);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1139, 416);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // gpBoxServerDownloadsGeneral
            // 
            this.gpBoxServerDownloadsGeneral.Controls.Add(this.tableLayoutPanel3);
            this.gpBoxServerDownloadsGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpBoxServerDownloadsGeneral.Location = new System.Drawing.Point(4, 4);
            this.gpBoxServerDownloadsGeneral.Name = "gpBoxServerDownloadsGeneral";
            this.gpBoxServerDownloadsGeneral.Size = new System.Drawing.Size(334, 408);
            this.gpBoxServerDownloadsGeneral.TabIndex = 0;
            this.gpBoxServerDownloadsGeneral.TabStop = false;
            this.gpBoxServerDownloadsGeneral.Text = "General Information";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadProcurementRoles, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadProcurementMethods, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadProviders, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadEntityPeople, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadPpdaUsers, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.labelProcRolesLastDownloadDate, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.labelProcMethodsLastDownloadDate, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.labelProvidersLastDownloadDate, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.labelEntyPeopleLastDownloadDate, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.labelPpdaUsersLastDownloadDate, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.labelEntitiesLastDownloadDate, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label6, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadEntities, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label13, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.label18, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.labelFinancialYrsLastDownloadDate, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.labelDistrictsLastDownloadDate, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.labelFundingSourcesLastDownloadDate, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.labelMgtLetterSectionsLastDownloadDate, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadFinancialYears, 2, 7);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadDistricts, 2, 8);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadMgtLetterSections, 2, 10);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadFundingSources, 2, 9);
            this.tableLayoutPanel3.Controls.Add(this.label15, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.labelPpdaOfficesLastDownloadDate, 1, 11);
            this.tableLayoutPanel3.Controls.Add(this.btnDownloadPpdaOffices, 2, 11);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(7, 22);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 12;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(321, 380);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // btnDownloadProcurementRoles
            // 
            this.btnDownloadProcurementRoles.Location = new System.Drawing.Point(258, 190);
            this.btnDownloadProcurementRoles.Name = "btnDownloadProcurementRoles";
            this.btnDownloadProcurementRoles.Size = new System.Drawing.Size(59, 24);
            this.btnDownloadProcurementRoles.TabIndex = 25;
            this.btnDownloadProcurementRoles.Text = "Sync";
            this.btnDownloadProcurementRoles.UseVisualStyleBackColor = true;
            this.btnDownloadProcurementRoles.Click += new System.EventHandler(this.btnDownloadProcurementRoles_Click);
            // 
            // btnDownloadProcurementMethods
            // 
            this.btnDownloadProcurementMethods.Location = new System.Drawing.Point(258, 159);
            this.btnDownloadProcurementMethods.Name = "btnDownloadProcurementMethods";
            this.btnDownloadProcurementMethods.Size = new System.Drawing.Size(59, 24);
            this.btnDownloadProcurementMethods.TabIndex = 24;
            this.btnDownloadProcurementMethods.Text = "Sync";
            this.btnDownloadProcurementMethods.UseVisualStyleBackColor = true;
            this.btnDownloadProcurementMethods.Click += new System.EventHandler(this.btnDownloadProcurementMethods_Click);
            // 
            // btnDownloadProviders
            // 
            this.btnDownloadProviders.Location = new System.Drawing.Point(258, 128);
            this.btnDownloadProviders.Name = "btnDownloadProviders";
            this.btnDownloadProviders.Size = new System.Drawing.Size(59, 24);
            this.btnDownloadProviders.TabIndex = 23;
            this.btnDownloadProviders.Text = "Sync";
            this.btnDownloadProviders.UseVisualStyleBackColor = true;
            this.btnDownloadProviders.Click += new System.EventHandler(this.btnDownloadProviders_Click);
            // 
            // btnDownloadEntityPeople
            // 
            this.btnDownloadEntityPeople.Location = new System.Drawing.Point(258, 97);
            this.btnDownloadEntityPeople.Name = "btnDownloadEntityPeople";
            this.btnDownloadEntityPeople.Size = new System.Drawing.Size(59, 24);
            this.btnDownloadEntityPeople.TabIndex = 22;
            this.btnDownloadEntityPeople.Text = "Sync";
            this.btnDownloadEntityPeople.UseVisualStyleBackColor = true;
            // 
            // btnDownloadPpdaUsers
            // 
            this.btnDownloadPpdaUsers.Location = new System.Drawing.Point(258, 66);
            this.btnDownloadPpdaUsers.Name = "btnDownloadPpdaUsers";
            this.btnDownloadPpdaUsers.Size = new System.Drawing.Size(59, 24);
            this.btnDownloadPpdaUsers.TabIndex = 21;
            this.btnDownloadPpdaUsers.Text = "Sync";
            this.btnDownloadPpdaUsers.UseVisualStyleBackColor = true;
            this.btnDownloadPpdaUsers.Click += new System.EventHandler(this.btnDownloadPpdaUsers_Click);
            // 
            // labelProcRolesLastDownloadDate
            // 
            this.labelProcRolesLastDownloadDate.AutoSize = true;
            this.labelProcRolesLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProcRolesLastDownloadDate.Location = new System.Drawing.Point(131, 187);
            this.labelProcRolesLastDownloadDate.Name = "labelProcRolesLastDownloadDate";
            this.labelProcRolesLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelProcRolesLastDownloadDate.TabIndex = 19;
            this.labelProcRolesLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelProcRolesLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelProcMethodsLastDownloadDate
            // 
            this.labelProcMethodsLastDownloadDate.AutoSize = true;
            this.labelProcMethodsLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProcMethodsLastDownloadDate.Location = new System.Drawing.Point(131, 156);
            this.labelProcMethodsLastDownloadDate.Name = "labelProcMethodsLastDownloadDate";
            this.labelProcMethodsLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelProcMethodsLastDownloadDate.TabIndex = 17;
            this.labelProcMethodsLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelProcMethodsLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelProvidersLastDownloadDate
            // 
            this.labelProvidersLastDownloadDate.AutoSize = true;
            this.labelProvidersLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProvidersLastDownloadDate.Location = new System.Drawing.Point(131, 125);
            this.labelProvidersLastDownloadDate.Name = "labelProvidersLastDownloadDate";
            this.labelProvidersLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelProvidersLastDownloadDate.TabIndex = 15;
            this.labelProvidersLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelProvidersLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEntyPeopleLastDownloadDate
            // 
            this.labelEntyPeopleLastDownloadDate.AutoSize = true;
            this.labelEntyPeopleLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEntyPeopleLastDownloadDate.Location = new System.Drawing.Point(131, 94);
            this.labelEntyPeopleLastDownloadDate.Name = "labelEntyPeopleLastDownloadDate";
            this.labelEntyPeopleLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelEntyPeopleLastDownloadDate.TabIndex = 13;
            this.labelEntyPeopleLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelEntyPeopleLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPpdaUsersLastDownloadDate
            // 
            this.labelPpdaUsersLastDownloadDate.AutoSize = true;
            this.labelPpdaUsersLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPpdaUsersLastDownloadDate.Location = new System.Drawing.Point(131, 63);
            this.labelPpdaUsersLastDownloadDate.Name = "labelPpdaUsersLastDownloadDate";
            this.labelPpdaUsersLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelPpdaUsersLastDownloadDate.TabIndex = 11;
            this.labelPpdaUsersLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelPpdaUsersLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelEntitiesLastDownloadDate
            // 
            this.labelEntitiesLastDownloadDate.AutoSize = true;
            this.labelEntitiesLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelEntitiesLastDownloadDate.Location = new System.Drawing.Point(131, 32);
            this.labelEntitiesLastDownloadDate.Name = "labelEntitiesLastDownloadDate";
            this.labelEntitiesLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelEntitiesLastDownloadDate.TabIndex = 9;
            this.labelEntitiesLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelEntitiesLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 30);
            this.label4.TabIndex = 0;
            this.label4.Text = "Data Type";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(131, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 30);
            this.label5.TabIndex = 1;
            this.label5.Text = "Last Download Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(258, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 30);
            this.label6.TabIndex = 2;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(4, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 30);
            this.label7.TabIndex = 3;
            this.label7.Text = "Entities";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(4, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 30);
            this.label8.TabIndex = 4;
            this.label8.Text = "PPDA Users";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(4, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 30);
            this.label9.TabIndex = 5;
            this.label9.Text = "Entity People";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(4, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 30);
            this.label10.TabIndex = 6;
            this.label10.Text = "Providers";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(4, 156);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 30);
            this.label11.TabIndex = 7;
            this.label11.Text = "Procurement Methods";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(4, 187);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 30);
            this.label12.TabIndex = 8;
            this.label12.Text = "Procurement Roles";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDownloadEntities
            // 
            this.btnDownloadEntities.Location = new System.Drawing.Point(258, 35);
            this.btnDownloadEntities.Name = "btnDownloadEntities";
            this.btnDownloadEntities.Size = new System.Drawing.Size(59, 24);
            this.btnDownloadEntities.TabIndex = 20;
            this.btnDownloadEntities.Text = "Sync";
            this.btnDownloadEntities.UseVisualStyleBackColor = true;
            this.btnDownloadEntities.Click += new System.EventHandler(this.btnDownloadEntities_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(4, 218);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 30);
            this.label13.TabIndex = 26;
            this.label13.Text = "Financial Years";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(4, 280);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(120, 30);
            this.label14.TabIndex = 27;
            this.label14.Text = "Funding Sources";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Location = new System.Drawing.Point(4, 249);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 30);
            this.label16.TabIndex = 28;
            this.label16.Text = "Districts";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Location = new System.Drawing.Point(4, 311);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(120, 30);
            this.label18.TabIndex = 29;
            this.label18.Text = "Management Letter Sections";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFinancialYrsLastDownloadDate
            // 
            this.labelFinancialYrsLastDownloadDate.AutoSize = true;
            this.labelFinancialYrsLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFinancialYrsLastDownloadDate.Location = new System.Drawing.Point(131, 218);
            this.labelFinancialYrsLastDownloadDate.Name = "labelFinancialYrsLastDownloadDate";
            this.labelFinancialYrsLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelFinancialYrsLastDownloadDate.TabIndex = 30;
            this.labelFinancialYrsLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelFinancialYrsLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelDistrictsLastDownloadDate
            // 
            this.labelDistrictsLastDownloadDate.AutoSize = true;
            this.labelDistrictsLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDistrictsLastDownloadDate.Location = new System.Drawing.Point(131, 249);
            this.labelDistrictsLastDownloadDate.Name = "labelDistrictsLastDownloadDate";
            this.labelDistrictsLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelDistrictsLastDownloadDate.TabIndex = 31;
            this.labelDistrictsLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelDistrictsLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFundingSourcesLastDownloadDate
            // 
            this.labelFundingSourcesLastDownloadDate.AutoSize = true;
            this.labelFundingSourcesLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFundingSourcesLastDownloadDate.Location = new System.Drawing.Point(131, 280);
            this.labelFundingSourcesLastDownloadDate.Name = "labelFundingSourcesLastDownloadDate";
            this.labelFundingSourcesLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelFundingSourcesLastDownloadDate.TabIndex = 32;
            this.labelFundingSourcesLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelFundingSourcesLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelMgtLetterSectionsLastDownloadDate
            // 
            this.labelMgtLetterSectionsLastDownloadDate.AutoSize = true;
            this.labelMgtLetterSectionsLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMgtLetterSectionsLastDownloadDate.Location = new System.Drawing.Point(131, 311);
            this.labelMgtLetterSectionsLastDownloadDate.Name = "labelMgtLetterSectionsLastDownloadDate";
            this.labelMgtLetterSectionsLastDownloadDate.Size = new System.Drawing.Size(120, 30);
            this.labelMgtLetterSectionsLastDownloadDate.TabIndex = 33;
            this.labelMgtLetterSectionsLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelMgtLetterSectionsLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDownloadFinancialYears
            // 
            this.btnDownloadFinancialYears.Location = new System.Drawing.Point(258, 221);
            this.btnDownloadFinancialYears.Name = "btnDownloadFinancialYears";
            this.btnDownloadFinancialYears.Size = new System.Drawing.Size(59, 23);
            this.btnDownloadFinancialYears.TabIndex = 34;
            this.btnDownloadFinancialYears.Text = "Sync";
            this.btnDownloadFinancialYears.UseVisualStyleBackColor = true;
            this.btnDownloadFinancialYears.Click += new System.EventHandler(this.btnDownloadFinancialYears_Click);
            // 
            // btnDownloadDistricts
            // 
            this.btnDownloadDistricts.Location = new System.Drawing.Point(258, 252);
            this.btnDownloadDistricts.Name = "btnDownloadDistricts";
            this.btnDownloadDistricts.Size = new System.Drawing.Size(59, 23);
            this.btnDownloadDistricts.TabIndex = 35;
            this.btnDownloadDistricts.Text = "Sync";
            this.btnDownloadDistricts.UseVisualStyleBackColor = true;
            this.btnDownloadDistricts.Click += new System.EventHandler(this.btnDownloadDistricts_Click);
            // 
            // btnDownloadMgtLetterSections
            // 
            this.btnDownloadMgtLetterSections.Location = new System.Drawing.Point(258, 314);
            this.btnDownloadMgtLetterSections.Name = "btnDownloadMgtLetterSections";
            this.btnDownloadMgtLetterSections.Size = new System.Drawing.Size(59, 23);
            this.btnDownloadMgtLetterSections.TabIndex = 37;
            this.btnDownloadMgtLetterSections.Text = "Sync";
            this.btnDownloadMgtLetterSections.UseVisualStyleBackColor = true;
            this.btnDownloadMgtLetterSections.Click += new System.EventHandler(this.btnDownloadMgtLetterSections_Click);
            // 
            // btnDownloadFundingSources
            // 
            this.btnDownloadFundingSources.Location = new System.Drawing.Point(258, 283);
            this.btnDownloadFundingSources.Name = "btnDownloadFundingSources";
            this.btnDownloadFundingSources.Size = new System.Drawing.Size(59, 23);
            this.btnDownloadFundingSources.TabIndex = 38;
            this.btnDownloadFundingSources.Text = "Sync";
            this.btnDownloadFundingSources.UseVisualStyleBackColor = true;
            this.btnDownloadFundingSources.Click += new System.EventHandler(this.btnDownloadFundingSources_Click);
            // 
            // gpBoxServerDownloadsPmActivities
            // 
            this.gpBoxServerDownloadsPmActivities.Controls.Add(this.gridViewPmActivitiesSyncStatus);
            this.gpBoxServerDownloadsPmActivities.Controls.Add(this.cbxFinancialYears);
            this.gpBoxServerDownloadsPmActivities.Controls.Add(this.btnDownloadInitialFyActivities);
            this.gpBoxServerDownloadsPmActivities.Controls.Add(this.label22);
            this.gpBoxServerDownloadsPmActivities.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpBoxServerDownloadsPmActivities.Location = new System.Drawing.Point(345, 4);
            this.gpBoxServerDownloadsPmActivities.Name = "gpBoxServerDownloadsPmActivities";
            this.gpBoxServerDownloadsPmActivities.Size = new System.Drawing.Size(391, 408);
            this.gpBoxServerDownloadsPmActivities.TabIndex = 1;
            this.gpBoxServerDownloadsPmActivities.TabStop = false;
            this.gpBoxServerDownloadsPmActivities.Text = "PM Activities";
            // 
            // gridViewPmActivitiesSyncStatus
            // 
            this.gridViewPmActivitiesSyncStatus.AllowUserToAddRows = false;
            this.gridViewPmActivitiesSyncStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridViewPmActivitiesSyncStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewPmActivitiesSyncStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.financialYear,
            this.DataType,
            this.lastDowndloadDate,
            this.Download});
            this.gridViewPmActivitiesSyncStatus.Location = new System.Drawing.Point(6, 49);
            this.gridViewPmActivitiesSyncStatus.Name = "gridViewPmActivitiesSyncStatus";
            this.gridViewPmActivitiesSyncStatus.ReadOnly = true;
            this.gridViewPmActivitiesSyncStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridViewPmActivitiesSyncStatus.Size = new System.Drawing.Size(379, 353);
            this.gridViewPmActivitiesSyncStatus.TabIndex = 4;
            this.gridViewPmActivitiesSyncStatus.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridViewPmActivitiesSyncStatus_CellContentClick);
            this.gridViewPmActivitiesSyncStatus.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.gridViewPmActivitiesSyncStatus_DataBindingComplete);
            // 
            // financialYear
            // 
            this.financialYear.DataPropertyName = "dataType";
            this.financialYear.HeaderText = "Financial Year";
            this.financialYear.Name = "financialYear";
            this.financialYear.ReadOnly = true;
            this.financialYear.Visible = false;
            // 
            // DataType
            // 
            this.DataType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DataType.DataPropertyName = "dataType";
            this.DataType.FillWeight = 40F;
            this.DataType.HeaderText = "Financial Year";
            this.DataType.Name = "DataType";
            this.DataType.ReadOnly = true;
            // 
            // lastDowndloadDate
            // 
            this.lastDowndloadDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.lastDowndloadDate.DataPropertyName = "lastUpdateDate";
            this.lastDowndloadDate.FillWeight = 40F;
            this.lastDowndloadDate.HeaderText = "Last Download Date";
            this.lastDowndloadDate.Name = "lastDowndloadDate";
            this.lastDowndloadDate.ReadOnly = true;
            // 
            // Download
            // 
            this.Download.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Download.FillWeight = 20F;
            this.Download.HeaderText = "Download";
            this.Download.Name = "Download";
            this.Download.ReadOnly = true;
            this.Download.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Download.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Download.Text = "Sync";
            this.Download.UseColumnTextForButtonValue = true;
            // 
            // cbxFinancialYears
            // 
            this.cbxFinancialYears.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxFinancialYears.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxFinancialYears.FormattingEnabled = true;
            this.cbxFinancialYears.Location = new System.Drawing.Point(177, 22);
            this.cbxFinancialYears.Name = "cbxFinancialYears";
            this.cbxFinancialYears.Size = new System.Drawing.Size(121, 21);
            this.cbxFinancialYears.TabIndex = 3;
            // 
            // btnDownloadInitialFyActivities
            // 
            this.btnDownloadInitialFyActivities.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDownloadInitialFyActivities.Location = new System.Drawing.Point(304, 20);
            this.btnDownloadInitialFyActivities.Name = "btnDownloadInitialFyActivities";
            this.btnDownloadInitialFyActivities.Size = new System.Drawing.Size(75, 23);
            this.btnDownloadInitialFyActivities.TabIndex = 2;
            this.btnDownloadInitialFyActivities.Text = "Download";
            this.btnDownloadInitialFyActivities.UseVisualStyleBackColor = true;
            this.btnDownloadInitialFyActivities.Click += new System.EventHandler(this.btnDownloadInitialFyActivities_Click);
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(95, 25);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(76, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Financial Year";
            // 
            // gbBoxGeneralInfoCbActivities
            // 
            this.gbBoxGeneralInfoCbActivities.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbBoxGeneralInfoCbActivities.Location = new System.Drawing.Point(743, 4);
            this.gbBoxGeneralInfoCbActivities.Name = "gbBoxGeneralInfoCbActivities";
            this.gbBoxGeneralInfoCbActivities.Size = new System.Drawing.Size(392, 408);
            this.gbBoxGeneralInfoCbActivities.TabIndex = 2;
            this.gbBoxGeneralInfoCbActivities.TabStop = false;
            this.gbBoxGeneralInfoCbActivities.Text = "CB Activities";
            // 
            // gbBoxEmisServerDetails
            // 
            this.gbBoxEmisServerDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBoxEmisServerDetails.Controls.Add(this.btnSaveServerDetails);
            this.gbBoxEmisServerDetails.Controls.Add(this.tableLayoutPanel1);
            this.gbBoxEmisServerDetails.Location = new System.Drawing.Point(14, 13);
            this.gbBoxEmisServerDetails.Name = "gbBoxEmisServerDetails";
            this.gbBoxEmisServerDetails.Size = new System.Drawing.Size(1158, 132);
            this.gbBoxEmisServerDetails.TabIndex = 0;
            this.gbBoxEmisServerDetails.TabStop = false;
            this.gbBoxEmisServerDetails.Text = "EMIS Server Details";
            // 
            // btnSaveServerDetails
            // 
            this.btnSaveServerDetails.Location = new System.Drawing.Point(117, 102);
            this.btnSaveServerDetails.Name = "btnSaveServerDetails";
            this.btnSaveServerDetails.Size = new System.Drawing.Size(137, 23);
            this.btnSaveServerDetails.TabIndex = 1;
            this.btnSaveServerDetails.Text = "Save Details";
            this.btnSaveServerDetails.UseVisualStyleBackColor = true;
            this.btnSaveServerDetails.Click += new System.EventHandler(this.btnSaveServerDetails_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txvServerBaseUrl, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txvServerUsername, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txvServerPassword, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(362, 75);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "URL:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Username:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Password:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txvServerBaseUrl
            // 
            this.txvServerBaseUrl.Location = new System.Drawing.Point(111, 3);
            this.txvServerBaseUrl.Name = "txvServerBaseUrl";
            this.txvServerBaseUrl.Size = new System.Drawing.Size(248, 22);
            this.txvServerBaseUrl.TabIndex = 3;
            // 
            // txvServerUsername
            // 
            this.txvServerUsername.Location = new System.Drawing.Point(111, 28);
            this.txvServerUsername.Name = "txvServerUsername";
            this.txvServerUsername.Size = new System.Drawing.Size(248, 22);
            this.txvServerUsername.TabIndex = 4;
            // 
            // txvServerPassword
            // 
            this.txvServerPassword.Location = new System.Drawing.Point(111, 53);
            this.txvServerPassword.Name = "txvServerPassword";
            this.txvServerPassword.Size = new System.Drawing.Size(248, 22);
            this.txvServerPassword.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(4, 342);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 37);
            this.label15.TabIndex = 39;
            this.label15.Text = "PPDA Offices";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPpdaOfficesLastDownloadDate
            // 
            this.labelPpdaOfficesLastDownloadDate.AutoSize = true;
            this.labelPpdaOfficesLastDownloadDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPpdaOfficesLastDownloadDate.Location = new System.Drawing.Point(131, 342);
            this.labelPpdaOfficesLastDownloadDate.Name = "labelPpdaOfficesLastDownloadDate";
            this.labelPpdaOfficesLastDownloadDate.Size = new System.Drawing.Size(120, 37);
            this.labelPpdaOfficesLastDownloadDate.TabIndex = 40;
            this.labelPpdaOfficesLastDownloadDate.Text = "yyyy-MM-dd HH:mm:ss";
            this.labelPpdaOfficesLastDownloadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDownloadPpdaOffices
            // 
            this.btnDownloadPpdaOffices.Location = new System.Drawing.Point(258, 345);
            this.btnDownloadPpdaOffices.Name = "btnDownloadPpdaOffices";
            this.btnDownloadPpdaOffices.Size = new System.Drawing.Size(59, 23);
            this.btnDownloadPpdaOffices.TabIndex = 41;
            this.btnDownloadPpdaOffices.Text = "Sync";
            this.btnDownloadPpdaOffices.UseVisualStyleBackColor = true;
            this.btnDownloadPpdaOffices.Click += new System.EventHandler(this.btnDownloadPpdaOffices_Click);
            // 
            // DialogMasterData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.panelContainerMasterData);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DialogMasterData";
            this.Text = "Master Data - Entity Management System";
            this.panelContainerMasterData.ResumeLayout(false);
            this.gpBoxEmisServerDownloads.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.gpBoxServerDownloadsGeneral.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.gpBoxServerDownloadsPmActivities.ResumeLayout(false);
            this.gpBoxServerDownloadsPmActivities.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPmActivitiesSyncStatus)).EndInit();
            this.gbBoxEmisServerDetails.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContainerMasterData;
        private System.Windows.Forms.GroupBox gpBoxEmisServerUploads;
        private System.Windows.Forms.GroupBox gpBoxEmisServerDownloads;
        private System.Windows.Forms.GroupBox gbBoxEmisServerDetails;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txvServerBaseUrl;
        private System.Windows.Forms.TextBox txvServerUsername;
        private System.Windows.Forms.Button btnSaveServerDetails;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox gpBoxServerDownloadsGeneral;
        private System.Windows.Forms.GroupBox gpBoxServerDownloadsPmActivities;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelProcRolesLastDownloadDate;
        private System.Windows.Forms.Label labelProcMethodsLastDownloadDate;
        private System.Windows.Forms.Label labelProvidersLastDownloadDate;
        private System.Windows.Forms.Label labelEntyPeopleLastDownloadDate;
        private System.Windows.Forms.Label labelPpdaUsersLastDownloadDate;
        private System.Windows.Forms.Label labelEntitiesLastDownloadDate;
        private System.Windows.Forms.Button btnDownloadProcurementRoles;
        private System.Windows.Forms.Button btnDownloadProcurementMethods;
        private System.Windows.Forms.Button btnDownloadProviders;
        private System.Windows.Forms.Button btnDownloadEntityPeople;
        private System.Windows.Forms.Button btnDownloadPpdaUsers;
        private System.Windows.Forms.Button btnDownloadEntities;
        private System.Windows.Forms.GroupBox gbBoxGeneralInfoCbActivities;
        private System.Windows.Forms.MaskedTextBox txvServerPassword;
        private System.Windows.Forms.ComboBox cbxFinancialYears;
        private System.Windows.Forms.Button btnDownloadInitialFyActivities;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridView gridViewPmActivitiesSyncStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn financialYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataType;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastDowndloadDate;
        private System.Windows.Forms.DataGridViewButtonColumn Download;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelFinancialYrsLastDownloadDate;
        private System.Windows.Forms.Label labelDistrictsLastDownloadDate;
        private System.Windows.Forms.Label labelFundingSourcesLastDownloadDate;
        private System.Windows.Forms.Label labelMgtLetterSectionsLastDownloadDate;
        private System.Windows.Forms.Button btnDownloadFinancialYears;
        private System.Windows.Forms.Button btnDownloadDistricts;
        private System.Windows.Forms.Button btnDownloadMgtLetterSections;
        private System.Windows.Forms.Button btnDownloadFundingSources;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelPpdaOfficesLastDownloadDate;
        private System.Windows.Forms.Button btnDownloadPpdaOffices;
    }
}
﻿using Processor.ControlObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SQLite;
using Processor.Entities;

namespace EmisMasterDataTool.Code.ui.views
{
    public partial class DialogMasterData : Form
    {
        public DialogMasterData()
        {

            InitializeComponent();

            loadOfflineToolStatus();

        }

        private void loadOfflineToolStatus()
        {

            DatabaseHandler dbHandler = new DatabaseHandler();
            var dbConn = DatabaseHandler.dbConnection();
       
            //load server details
            loadServerDetails(dbHandler, dbConn);

            //load FY 
            loadFinancialYears();

            //display general master data, last sync dates
            loadGeneralDataLastSyncDates();

            //pm activitites sync status
            loadPmActivitiesSyncStatus();

        }

        private void loadPmActivitiesSyncStatus()
        {

            var dbHandler = new DatabaseHandler();
            var serverSyncStatus = dbHandler.getServerSyncStatus(DatabaseHandler.dbConnection(),Globals.SYNC_DATA_TYPE_CAT_PM_ACTIVITY);

            DataTable dataTable = new SharedCommons().ToDataTable<ServerSyncStatus>(serverSyncStatus);

            gridViewPmActivitiesSyncStatus.AutoGenerateColumns = false;
            gridViewPmActivitiesSyncStatus.DataSource = dataTable;

        }

        private void loadGeneralDataLastSyncDates()
        {

            DatabaseHandler dbHandler = new DatabaseHandler();
            var dbConn = DatabaseHandler.dbConnection();

            var statuses = dbHandler.getServerSyncStatus(dbConn, Globals.SYNC_DATA_TYPE_CAT_GENERAL);
            foreach(var serverSyncStatus in statuses)
            {

                String lastUpdateDate = serverSyncStatus == null ? AppConstants.PLACE_HOLDER_DATE : serverSyncStatus.lastUpdateDate;
                String dataType = serverSyncStatus.dataType;

                updateTheSystemSettingStatus(lastUpdateDate, dataType);

            }


        }

        private void updateTheSystemSettingStatus(string lastUpdateDate, string dataType)
        {

            if (dataType == Globals.SYNC_DATA_TYPE_ENTITIES)
            {
                labelEntitiesLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_PPDA_USERS)
            {
                labelPpdaUsersLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_ENTITY_PEOPLE)
            {
                labelEntyPeopleLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_PROVIDERS)
            {
                labelProvidersLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_PROCUREMENT_METHODS)
            {
                labelProcMethodsLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_PROCUREMENT_ROLES)
            {
                labelProcRolesLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_FINANCIAL_YEARS)
            {
                labelFinancialYrsLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_DISTRICTS)
            {
                labelDistrictsLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_MGT_LETTER_SECTIONS)
            {
                labelMgtLetterSectionsLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_FUNDING_SOURCES)
            {
                labelFundingSourcesLastDownloadDate.Text = lastUpdateDate;
            }
            else if (dataType == Globals.SYNC_DATA_TYPE_PPDA_OFFICES)
            {
                labelPpdaOfficesLastDownloadDate.Text = lastUpdateDate;
            }

        }

        private void loadFinancialYears()
        {

            List<FinacialYear> financials = new DatabaseHandler().allFinancialYears(DatabaseHandler.dbConnection());
            List<ServerSyncStatus> serverSyncStatus = new DatabaseHandler().getServerSyncStatus(DatabaseHandler.dbConnection(), Globals.SYNC_DATA_TYPE_CAT_PM_ACTIVITY);

            List<String> fyWithSyncEntries = new List<string>();
            foreach(var item in serverSyncStatus)
            {
                fyWithSyncEntries.Add(item.dataType);
            }


            List<FinacialYear> updatedFY = new List<FinacialYear>();            

            foreach(var item in financials)
            {
                if (fyWithSyncEntries.Contains(item.financial_year))
                {
                    continue;
                }
                updatedFY.Add(item);
            }

            WidgetHandler.populateFinancialYearsDropdown(cbxFinancialYears, updatedFY);

        }

        private void loadServerDetails(DatabaseHandler dbHandler, SQLiteConnection dbConn)
        {

            var paramServerUrl = dbHandler.getSystemParameter(dbConn, Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            var paramServerUsername = dbHandler.getSystemParameter(dbConn, Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_USERNAME);
            var paramServerPassword = dbHandler.getSystemParameter(dbConn, Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_PASSWORD);
            
            txvServerBaseUrl.Text = paramServerUrl == null ? "" : paramServerUrl.value;
            txvServerUsername.Text = paramServerUsername == null ? "" : paramServerUsername.value;
            txvServerPassword.Text = paramServerPassword == null ? "" : paramServerPassword.value;

        }

        private void gpBoxServerDownloadsTraining_Enter(object sender, EventArgs e)
        {

        }

        private void btnSaveServerDetails_Click(object sender, EventArgs e)
        {

            String baseUrl = txvServerBaseUrl.Text;
            if (String.IsNullOrEmpty(baseUrl))
            {
                WidgetHandler.showMessage("Please input the server base URL");
                return;
            }


            String username = txvServerUsername.Text;
            String password = txvServerPassword.Text;

            SystemSetting settingBaseUrl = new SystemSetting();
            settingBaseUrl.groupCode = Globals.GROUP_CODE_EMIS_SERVER;
            settingBaseUrl.valueCode = Globals.VALUE_CODE_EMIS_SERVER_URL;
            settingBaseUrl.value = baseUrl;
            settingBaseUrl.description = "EMIS api base URL";

            DatabaseHandler databaseHandler = new DatabaseHandler();

            var exists = databaseHandler.getSystemParameter(DatabaseHandler.dbConnection(), Globals.GROUP_CODE_EMIS_SERVER, Globals.VALUE_CODE_EMIS_SERVER_URL);
            if(exists != null)
            {
                settingBaseUrl.Id = exists.Id;
                databaseHandler.updateSystemSetting(DatabaseHandler.dbConnection(), settingBaseUrl);
            }
            else
            {
                databaseHandler.saveSystemSetting(DatabaseHandler.dbConnection(), settingBaseUrl);
            }

            WidgetHandler.showMessage("Server details successfully updated", false);

        }

        private void btnDownloadEntities_Click(object sender, EventArgs e)
        {

            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadEntities();
                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }catch(Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }

        }

        private void btnDownloadInitialFyActivities_Click(object sender, EventArgs e)
        {

            try
            {

                if (cbxFinancialYears.SelectedIndex == 0)
                {
                    WidgetHandler.showMessage("Please select a Financial Year");
                    return;
                }

                var fyEmisId = (int)cbxFinancialYears.SelectedValue;
                String financialYear = (String)cbxFinancialYears.Text;
                
                new DataLoader().loadPmActivities(financialYear);
                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully downloaded", false);

            }
            catch(Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on loading data ["+exception.Message+"]", true);
            }

        }

        private void gridViewPmActivitiesSyncStatus_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            gridViewPmActivitiesSyncStatus.ClearSelection();
        }

        private void gridViewPmActivitiesSyncStatus_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1 || e.ColumnIndex != 3)
            {
                return;
            }
               

            var cell = dgv.Rows[e.RowIndex].Cells[0];
            String financialYear = (String)cell.Value;

            new DataLoader().loadPmActivities(financialYear);

            //reload the details
            loadOfflineToolStatus();
            WidgetHandler.showMessage("Data successfully downloaded", false);
            
        }

        private void btnDownloadPpdaUsers_Click(object sender, EventArgs e)
        {

            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadUsers();

                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }
            catch (Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }

        }

        private void btnDownloadFinancialYears_Click(object sender, EventArgs e)
        {

            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadFinancialYears();

                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }
            catch (Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }

        }

        private void btnDownloadDistricts_Click(object sender, EventArgs e)
        {

            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadDistricts();

                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }
            catch (Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }

        }

        private void btnDownloadMgtLetterSections_Click(object sender, EventArgs e)
        {

            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadManagementLetterSections();

                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }
            catch (Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }

        }

        private void btnDownloadProviders_Click(object sender, EventArgs e)
        {

            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadProviders();

                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }
            catch (Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }
        }

        private void btnDownloadFundingSources_Click(object sender, EventArgs e)
        {

            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadFundingSources();

                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }
            catch (Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }

        }

        private void btnDownloadProcurementRoles_Click(object sender, EventArgs e)
        {

            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadProcurementRoles();

                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }
            catch (Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }

        }

        private void btnDownloadProcurementMethods_Click(object sender, EventArgs e)
        {

            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadProcurementMethods();

                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }
            catch (Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }

        }

        private void btnDownloadPpdaOffices_Click(object sender, EventArgs e)
        {
            try
            {

                var dataLoader = new DataLoader();
                dataLoader.loadPpdaOffices();

                //reload the details
                loadOfflineToolStatus();

                WidgetHandler.showMessage("Data successfully updated", false);

            }
            catch (Exception exception)
            {
                WidgetHandler.showMessage("Error occurred on updating data [" + exception.Message + "]");
            }
        }
    }

}

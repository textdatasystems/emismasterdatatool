﻿using EmisTool.Code.ui.custom;
using Processor.Entities.api;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool.Code.ui.views
{
    public partial class DialogPmAuditDisposalTool : Form
    {
        private int activityId;
        private ApiPmAuditDisposalTool toolToUpdate;

        public DialogPmAuditDisposalTool(int activityId, ApiPmAuditDisposalTool toolToEdit = null)
        {

            InitializeComponent();
            this.activityId = activityId;
            this.toolToUpdate = toolToEdit;

            initializeControls();

        }

        private void initializeControls()
        {

            var panelPmAuditDisposalTool = new PanelPmAuditDisposalTool(activityId, toolToUpdate);
            panelPmAuditDisposalTool.Dock = DockStyle.Fill;
            panelContainer.Controls.Add(panelPmAuditDisposalTool);

        }

    }
}

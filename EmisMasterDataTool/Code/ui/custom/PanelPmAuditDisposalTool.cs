﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Processor.Entities.PM.disposal;
using Processor.ControlObjects;
using Processor.Entities.api;
using EmisMasterDataTool.Code;

namespace EmisTool.Code.ui.custom
{
    public class PanelPmAuditDisposalTool : Panel, IAdtAnalyisUpdater
    {

        //fields used by the tool

        //analysis tab fields
        public const String FLD_SCORE_PLAN = "txvAdtScorePlan"; 
        public const String FLD_SCORE_INITIATION = "txvAdtScoreInitiation"; 
        public const String FLD_SCORE_PUBLIC_BIDDING = "txvAdtScorePublicBidding"; 
        public const String FLD_SCORE_PUBLIC_AUCTION = "txvAdtScorePublicAuction"; 
        public const String FLD_SCORE_DIRECT_NEGOTIATIONS = "txvAdtScoreDirectNegotiations"; 
        public const String FLD_SCORE_PUBLIC_OFFICERS = "txvAdtScorePublicOfficers"; 
        public const String FLD_SCORE_DESTRUCTION = "txvAdtScoreDestruction"; 
        public const String FLD_SCORE_CONVERSION = "txvAdtScoreConversion"; 
        public const String FLD_SCORE_TRADE_IN = "txvAdtScoreTradeIn"; 
        public const String FLD_SCORE_TRANSFER = "txvAdtScoreTransfer"; 
        public const String FLD_SCORE_DONATION = "txvAdtScoreDonation"; 
        public const String FLD_SCORE_DELIVERY = "txvAdtScoreDelivery"; 
        public const String FLD_SCORE_RECORDS = "txvAdtScoreRecords"; 
        public const String FLD_SCORE_AVERAGE = "txvAdtScoreAverage"; 


        //holds the app state
        private AppStateAuditTools appState;

        //event handlers
        public EventHandler eventHandlerComplainceScoreTextChange { get; set; }

        //controls for total fields of dynamically created sections
        public static Dictionary<long, Control> ADT_SECTION_TOTAL_TXVS = new Dictionary<long, Control>();

        private int activityId;
        private bool inEditMode = false;
        private String toolReferenceInEditMode = "";


        public PanelPmAuditDisposalTool(int activityId, ApiPmAuditDisposalTool toolToUpdate = null)
        {

            this.activityId = activityId;

            //clear the dictionary to avoid duplicate keys
            ADT_SECTION_TOTAL_TXVS.Clear();

            //set the app state
            appState = new AppStateAuditTools();
            appState.iAdtAnalyisUpdater = this;            

            //set up the event handlers
            setUpEventHandlers();

            //initialize the controls
            initializeControls();

            //load data
            loadData(toolToUpdate);            

        }

        private void loadData(ApiPmAuditDisposalTool toolToEdit)
        {

            if (toolToEdit != null)
            {
                populateTool(toolToEdit);
                return;
            }

            //it's a new tool creation
            loadActivityGeneralDetails(activityId);

        }

        private void loadActivityGeneralDetails(int activityId)
        {

            DatabaseHandler dbHandler = new DatabaseHandler();
            var activity = dbHandler.findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityId);

            if (activity == null)
            {
                WidgetHandler.showMessage("Failed to find PM activity with ID [" + activityId + "]");
                return;
            }

            var ident = new ApiPmAuditDisposalToolIdentification();
            ident.nameOfEntity = activity.entityName;

            populateIdentificationSection(ident);
            
        }

        private void populateTool(ApiPmAuditDisposalTool tool)
        {

            //
            //set status as edit, set the tool reference to edit, make the save button show update
            //
            inEditMode = true;
            toolReferenceInEditMode = tool.toolReference;
            Control btnSaveTool = WidgetHandler.findControlByName(this, "btnSaveTool");
            if (btnSaveTool != null) { btnSaveTool.Text = "Update Tool"; }

            //fill the person who created the tool
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxAdtAuditorName", tool.createdBy);

            //fill identification
            populateIdentificationSection(tool.identification);

            //fill analysis
            populateAnalysis(tool.analysis);

            //fill dynamic sections
            populateDynamicSections(tool.sections);
            
        }

        private void setUpEventHandlers()
        {           
            eventHandlerComplainceScoreTextChange = calculateAdtAverageScore;
        }

        private void initializeControls()
        {

            this.Name = "panelPmAuditDisposalTool";
            this.Padding = new Padding(10);
            
            //tab control
            TabControl tabControlPmAuditDisposalTool = getTabControlPmAuditDisposalTool();
            
            //button section     
            Panel panelButtonContainer = getButtonContainerPanel();


            //this is just to hold the to controls, 1 for the buttons and the other for the tab control
            TableLayoutPanel tablePaneMainContainer = new TableLayoutPanel();
            tablePaneMainContainer.Dock = DockStyle.Fill;
            tablePaneMainContainer.ColumnCount = 1;
            tablePaneMainContainer.RowCount = 2;
            tablePaneMainContainer.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

            //add the tab control to the main container spanning 90% of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 90));
            tablePaneMainContainer.Controls.Add(tabControlPmAuditDisposalTool, 0, 0);
            // add the buttons panel to the main container spanning 10 % of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 10));
            tablePaneMainContainer.Controls.Add(panelButtonContainer, 0, 1);

            //add tab control to panel
            this.Controls.Add(tablePaneMainContainer);

        }

        private TabControl getTabControlPmAuditDisposalTool()
        {

            TabControl tabControlPmAuditDisposalTool = new TabControl();
            tabControlPmAuditDisposalTool.Name = "tabControlPmAuditDisposalTool";
            tabControlPmAuditDisposalTool.Dock = DockStyle.None;
            tabControlPmAuditDisposalTool.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            //tab pages for the tab control
            TabPage tabPageDisposalWorksheet = GetTabPageDisposalWorksheet();
            TabPage tabPageDisposalAnalysis = GetTabPageDisposalAnalysis();
            TabPage tabPageGraph = GetTabPageGraph();

            //add tab pages to tab control
            tabControlPmAuditDisposalTool.TabPages.Add(tabPageDisposalWorksheet);
            tabControlPmAuditDisposalTool.TabPages.Add(tabPageDisposalAnalysis);
            tabControlPmAuditDisposalTool.TabPages.Add(tabPageGraph);
            return tabControlPmAuditDisposalTool;

        }

        private Panel getButtonContainerPanel()
        {

            Panel panelButtonContainer = new Panel();
            panelButtonContainer.Height = 40;
            panelButtonContainer.Dock = DockStyle.Fill;

            Button btnSaveTool = new Button() { Text = "Save Tool", Width = 150, Anchor = AnchorStyles.Bottom | AnchorStyles.Left, Margin = new Padding(0) };
            btnSaveTool.Click += btnSaveToolClicked;
            panelButtonContainer.Controls.Add(btnSaveTool);

            TableLayoutPanel tabPanel = new TableLayoutPanel() { Anchor = AnchorStyles.Bottom | AnchorStyles.Right, Margin = new Padding(0) };
            tabPanel.ColumnCount = 3;
            tabPanel.RowCount = 1;
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 35));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40));
            tabPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 35));
            // tabPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tabPanel.Width = 200;
            tabPanel.Height = 38;

            Label labelLoadTool = new Label() { Text = "Tool Id", Dock = DockStyle.Fill, TextAlign = System.Drawing.ContentAlignment.MiddleRight };
            TextBox txvLoadTool = new TextBox() { Name = "txvLoadTool", Width = 100 };
            Button btnLoadTool = new Button() { Name = "btnLoadTool", Text = "Load Tool" };
            btnLoadTool.Click += btnLoadToolClicked;

            tabPanel.Controls.Add(labelLoadTool, 0, 0);
            tabPanel.Controls.Add(txvLoadTool, 1, 0);
            tabPanel.Controls.Add(btnLoadTool, 2, 0);

           // panelButtonContainer.Controls.Add(tabPanel);
            return panelButtonContainer;

        }

        private void btnLoadToolClicked(object sender, EventArgs e)
        {

            String toolId = WidgetHandler.findCtlValByCtlName(this, "txvLoadTool");

            //check if it's a valid ID
            long lToolId = 0;
            if (!long.TryParse(toolId, out lToolId))
            {
                WidgetHandler.showMessage("Tool ID must be a number", true);
                return;
            }

            //get the tool
            var dbConn = DatabaseHandler.dbConnection();
            var tool = new DatabaseHandler().getPmAuditDisposalToolByIdOrToolReference(dbConn, 0, lToolId);

            //tool not found
            if (tool == null)
            {
                WidgetHandler.showMessage("Tool with ID[" + toolId + "] not found", true);
                return;
            }

            //fill identification
            populateIdentificationSection(tool.identification);

            //fill analysis
            populateAnalysis(tool.analysis);

            //fill sections
            populateDynamicSections(tool.sections);


            //we are done
            WidgetHandler.showMessage("Data successfully loaded", false);

        }

        private void populateDynamicSections(List<ApiPmAuditDisposalToolSection> sections)
        {

            //foreach section
            //find the corresponding state section
            //iterate through as you match the item emis ID

            foreach (var section in sections)
            {

                foreach (var stateSection in AppStateAuditTools.auditDisposalToolSectionEvaluations)
                {

                    if (stateSection.sectionEmisId == section.sectionEmisId)
                    {
                        populateCurrentSectionValues(section, stateSection);
                        break;
                    }

                }

                var total = "YES SCORE " + section.yesTotal.ToString() + ", NO SCORE " + section.noTotal.ToString();
                var totalField = ADT_SECTION_TOTAL_TXVS[section.sectionEmisId];
                if (totalField != null) { totalField.Text = total; }

            }

        }

        private void populateCurrentSectionValues(ApiPmAuditDisposalToolSection section, SectionEvaluationAdt stateSection)
        {

            var sectionItems = section.items;
            var stateSectionItems = stateSection.sectionItemEvaluations;

            //we declare a new dictionary to hold the updated items that have the values
            Dictionary<long, SectionItemEvaluationAdt> stateSectionItemsWithValues = new Dictionary<long, SectionItemEvaluationAdt>();

            foreach (var item in sectionItems)
            {

                if (stateSectionItems.ContainsKey(item.itemEmisId))
                {

                    var stateItem = stateSectionItems[item.itemEmisId];
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.evaluationFieldName, item.evaluationFieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.exceptionFieldName, item.exception);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.findingFieldName, item.finding);
                    WidgetHandler.findAndFillComboBoxValByCtlName(this, stateItem.managementLetterSectionFieldName, item.managementLetterSectionId);
                    WidgetHandler.findAndFillComboBoxValByCtlName(this, stateItem.managementLetterSectionItemFieldName, item.managementLetterSectionItemId);
                    
                    //we now need to update this item with the values
                    stateItem.evaluationFieldValue = item.evaluationFieldValue;
                    stateItem.findingFieldValue = item.finding;
                    stateItem.exceptionFieldValue = item.exception;
                    stateItem.managementLetterSectionFieldValue = item.managementLetterSectionId;
                    stateItem.managementLetterSectionItemFieldValue = item.managementLetterSectionItemId;
                    stateItem.description = item.description;

                    stateSectionItemsWithValues[item.itemEmisId] = stateItem;
                }

            }

            //update the state section
            stateSection.sectionItemEvaluations = stateSectionItemsWithValues;
            appState.updateAdtSectionInAppState(stateSection);

        }

        private void populateAnalysis(ApiPmAuditDisposalToolAnalysis analysis)
        {

            if (analysis == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_PLAN, analysis.scorePlan);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_INITIATION, analysis.scoreInitiation);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_PUBLIC_BIDDING, analysis.scorePublicBidding);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_PUBLIC_AUCTION, analysis.scorePublicAuction);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_DIRECT_NEGOTIATIONS, analysis.scoreDirectNegotiations);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_PUBLIC_OFFICERS, analysis.scorePublicOfficers);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_DESTRUCTION, analysis.scoreDestruction);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_CONVERSION, analysis.scoreConversion);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_TRADE_IN, analysis.scoreTradeIn);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_TRANSFER, analysis.scoreTransfer);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_DONATION, analysis.scoreDonation);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_DELIVERY, analysis.scoreDelivery);
            WidgetHandler.findAndFillCtlValByCtlName(this, FLD_SCORE_RECORDS, analysis.scoreRecords);

        }

        private void populateIdentificationSection(ApiPmAuditDisposalToolIdentification ident)
        {

            if (ident == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAdtNameOfEntity", ident.nameOfEntity);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAdtReferenceNo", ident.referenceNo);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAdtAuditPeriod", ident.auditPeriod);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAdtAssetDescription", ident.assetDescription);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAdtNameOfBuyer", ident.nameOfBuyer);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAdtDisposalValue", ident.disposalValue);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAdtValuationAmount", ident.valuationAmount);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxAdtDisposalMethod", ident.disposalMethod);

        }

        private TabPage GetTabPageGraph()
        {
            return new TabPage("Graph");
        }

        private TabPage GetTabPageDisposalAnalysis()
        {

            Panel panelContainer = new Panel();
            panelContainer.Dock = DockStyle.Fill;

            //get the table with the tab controls
            var tableDisposalAnalysis = UiHelperAdt.GenerateDisposalAnalysisTable(eventHandlerComplainceScoreTextChange);

            //add table to container panel
            panelContainer.Controls.Add(tableDisposalAnalysis);           
           
            TabPage tabPageDisposalAnalysis = new TabPage("Disposal Analysis");
            tableDisposalAnalysis.Padding = new Padding(20);
            tabPageDisposalAnalysis.Controls.Add(panelContainer);

            return tabPageDisposalAnalysis;

        }

        private TabPage GetTabPageDisposalWorksheet()
        {

            TabPage tabPageDisposalWorksheet = new TabPage("Disposal");
            tabPageDisposalWorksheet.Name = "tabPageDisposalWorksheet";

            //get the panel with the accordion list
            Panel panelAccordionContainer = GetPanelAccordionContainer();

            //add panel containing the accordion to the page
            tabPageDisposalWorksheet.Controls.Add(panelAccordionContainer);

            return tabPageDisposalWorksheet;

        }

        private Panel GetPanelAccordionContainer()
        {

            Panel panelAccordionContainer = new Panel();
            panelAccordionContainer.Name = "panelAccordionContainer";
            panelAccordionContainer.Dock = DockStyle.Fill;

            panelAccordionContainer.Padding = new Padding(10);

            //get accordion panel with the different sections
            AccordionPanelAuditDisposal accordionPanelAuditDisposal = new AccordionPanelAuditDisposal(appState);
            accordionPanelAuditDisposal.Name = "accordionPanelAuditDisposal";

            //add accordion panel to the container
            panelAccordionContainer.Controls.Add(accordionPanelAuditDisposal);

            return panelAccordionContainer;

        }


        public void calculateAdtAverageScore(object sender, EventArgs e)
        {

            String[] scoreFieldNames = new String[] {
                FLD_SCORE_PLAN,
                FLD_SCORE_INITIATION,
                FLD_SCORE_PUBLIC_BIDDING,
                FLD_SCORE_PUBLIC_AUCTION,
                FLD_SCORE_DIRECT_NEGOTIATIONS,
                FLD_SCORE_PUBLIC_OFFICERS,
                FLD_SCORE_DESTRUCTION,
                FLD_SCORE_CONVERSION,
                FLD_SCORE_TRADE_IN,
                FLD_SCORE_TRANSFER,
                FLD_SCORE_DONATION,
                FLD_SCORE_DELIVERY,
                FLD_SCORE_RECORDS
                 };

            long noOfSections = 0;
            long totalScores = 0;

            foreach (String field in scoreFieldNames)
            {                

                var control = WidgetHandler.findControlByName(this, field);

                long currentScore = 0;
                if (control != null && long.TryParse(control.Text, out currentScore))
                {

                    //increment valid sections
                    noOfSections += 1;
                    //increment the total scores                  
                    totalScores += currentScore;

                }

            }

            //division by zero
            if (noOfSections == 0)
            {
                return;
            }

            //average
            double average = totalScores / noOfSections;
            Control controlAverageScore = WidgetHandler.findControlByName(this, FLD_SCORE_AVERAGE);

            if(controlAverageScore == null) { return; }
            controlAverageScore.Text = Math.Round(average, 1).ToString();

        }

        public void updateAdtSectionScoresInAnalyisTab(SectionEvaluationAdt section)
        {

            var sectionName = section.name;
            sectionName = String.IsNullOrEmpty(sectionName) ? sectionName : sectionName.ToUpper();

            var totalValueYes = section.sectionYesTotalValue;
            var totalValueNo = section.sectionNoTotalValue;

            String percentageField = getFieldNameForPercentageFieldInTheAnalysisTab(sectionName);

            if (percentageField != null)
            {
                fillPmAdtAnaysisSecTotals(totalValueYes, totalValueNo, percentageField);
            }

        }

        private void fillPmAdtAnaysisSecTotals(long totalValueYes, long totalValueNo, String percentFieldName)
        {

            //calculate the %score
            var percent = (totalValueYes * 100.0) / (totalValueYes + totalValueNo);

            var txvPercent = WidgetHandler.findControlByName(this, percentFieldName);
            if (txvPercent != null)
            {
                txvPercent.Text = Math.Round(percent, 1).ToString();
            }

        }

        private static string getFieldNameForPercentageFieldInTheAnalysisTab(string sectionName)
        {

            String percentageField = null;

            if (sectionName == "Plan".ToUpper())
            {
                percentageField = FLD_SCORE_PLAN;
            }
            else if (sectionName == "Initiation".ToUpper())
            {
                percentageField = FLD_SCORE_INITIATION;
            }
            else if (sectionName == "Public Bidding".ToUpper())
            {
                percentageField = FLD_SCORE_PUBLIC_BIDDING;
            }
            else if (sectionName == "Public Auction".ToUpper())
            {
                percentageField = FLD_SCORE_PUBLIC_AUCTION;
            }
            else if (sectionName == "Direct Negotiation".ToUpper())
            {
                percentageField = FLD_SCORE_DIRECT_NEGOTIATIONS;
            }
            else if (sectionName == "Public Officers".ToUpper())
            {
                percentageField = FLD_SCORE_PUBLIC_OFFICERS;
            }
            else if (sectionName == "Destruction".ToUpper())
            {
                percentageField = FLD_SCORE_DESTRUCTION;
            }
            else if (sectionName == "CONVERSION OR CLASSIFICATION".ToUpper())
            {
                percentageField = FLD_SCORE_CONVERSION;
            }
            else if (sectionName == "Trade-In".ToUpper())
            {
                percentageField = FLD_SCORE_TRADE_IN;
            }
            else if (sectionName == "Transfer".ToUpper())
            {
                percentageField = FLD_SCORE_TRANSFER;
            }
            else if (sectionName == "Donation".ToUpper())
            {
                percentageField = FLD_SCORE_DONATION;
            }
            else if (sectionName == "Delivery".ToUpper())
            {
                percentageField = FLD_SCORE_DELIVERY;
            }
            else if (sectionName == "Records".ToUpper())
            {
                percentageField = FLD_SCORE_RECORDS;
            }

            return percentageField;
        }

        public void autofillAdtAnalysisTabTextFieldBasedOnEvalItemValue(SectionItemEvaluationAdt sectionItem)
        {           

        }


        private void btnSaveToolClicked(object sender, EventArgs e)
        {

            if (!inEditMode)
            {
                saveTool();
                return;
            }
            else
            {
                updateTool();
            }
            
        }

        private void updateTool()
        {

            //check if the tool name probably changed
            String entityName = WidgetHandler.findCtlValByCtlName(this, "txvAdtNameOfEntity");
            String auditPeriod = WidgetHandler.findCtlValByCtlName(this, "txvAdtAuditPeriod");
            String disposalRefNo = WidgetHandler.findCtlValByCtlName(this, "txvAdtReferenceNo");

            String newToolName = SharedCommons.getToolName(AppConstants.PREFIX_ADT, entityName, auditPeriod, activityId);

            //tool Reference might have changed
            if (!apiDataUpdateToolReference(newToolName, out String message))
            {
                WidgetHandler.showMessage(message);
                return;
            }

            //clear existing data
            new DatabaseHandler().clearAuditDisposalToolOldData(DatabaseHandler.dbConnection(), toolReferenceInEditMode);

            //save new tool data
            saveToolData(entityName, auditPeriod, disposalRefNo, newToolName);

        }

        private bool apiDataUpdateToolReference(string newToolName, out string message)
        {

            DatabaseHandler db = new DatabaseHandler();
            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();

            if (toolReferenceInEditMode != newToolName && db.auditDisposalToolNameExists(sQLiteConnection, newToolName))
            {
                message = "Tool with name [" + newToolName + "] already exists.";
                return false;
            }

            var tool = db.getPmAuditDisposalToolByIdOrToolReference(sQLiteConnection, activityId);
            if (tool == null)
            {
                message = "Tool for activity ID [" + activityId + "] not found.";
                return false;
            }

            tool.toolName = newToolName;
            tool.toolReference = newToolName;
            tool.activityId = activityId;
            tool.createdBy = getAuditorName();
            tool.toolType = AppConstants.PREFIX_ADT;

            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.createdOn = dateTimeNow;
            tool.updatedOn = dateTimeNow;

            db.updatePmAdt(sQLiteConnection, tool);

            message = "SUCCESS";
            return true;

        }

        private void saveTool()
        {

            String entityName = WidgetHandler.findCtlValByCtlName(this, "txvAdtNameOfEntity");
            String auditPeriod = WidgetHandler.findCtlValByCtlName(this, "txvAdtAuditPeriod");
            String disposalRefNo = WidgetHandler.findCtlValByCtlName(this, "txvAdtReferenceNo");
            string createdBy = getAuditorName();

            //check if the required initial fields are supplied
            if (!validPmAuditDisposalToolData(entityName, auditPeriod, disposalRefNo, createdBy))
            {
                return;
            }

            //create a tool reference and save the tool header
            String toolName = SharedCommons.getToolName(AppConstants.PREFIX_ADT, entityName, auditPeriod, activityId);
            String toolRef = toolName;

            if (!apiDataSavePmAuditDisposalTool(toolName, toolRef, activityId, out String message, createdBy))
            {
                WidgetHandler.showMessage(message, true);
                return;
            }

            saveToolData(entityName, auditPeriod, disposalRefNo, toolRef);

        }

        private void saveToolData(string entityName, string auditPeriod, string disposalRefNo, string toolRef)
        {

            //save the tool identification data
            apiDataSavePmAuditDisposalToolIdentification(entityName, disposalRefNo, auditPeriod, toolRef);

            //save the sections and section items
            apiDataSavePmAuditDisposalToolDynamicSectionsData(toolRef);

            //save the analysis
            apiDataSaveAuditDisposalToolAnalysis(toolRef);

            //everything has been successfully saved
            WidgetHandler.showMessage("Tool successfully saved with tool reference [" + toolRef + "]", false);

        }

        private bool validPmAuditDisposalToolData(string entityName, string auditPeriod, string disposalRefNo, string createdBy)
        {

            if (String.IsNullOrEmpty(entityName))
            {
                WidgetHandler.showMessage("Please enter the entity name", true);
                return false;
            }

            if (String.IsNullOrEmpty(auditPeriod))
            {
                WidgetHandler.showMessage("Please enter the audit period", true);
                return false;
            }

            if (String.IsNullOrEmpty(disposalRefNo))
            {
                WidgetHandler.showMessage("Please enter the disposal reference number", true);
                return false;
            }

            if (String.IsNullOrEmpty(createdBy))
            {
                WidgetHandler.showMessage("Please select Auditor", true);
                return false;
            }

            return true;

        }

        private static bool apiDataSavePmAuditDisposalTool(string toolName, string toolRef, int activityId, out String message, string createdBy)
        {

            DatabaseHandler db = new DatabaseHandler();
            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();

            if (db.auditDisposalToolNameExists(sQLiteConnection, toolName))
            {
                message = "Tool with name [" + toolName + "] already exists.";
                return false;
            }


            ApiPmAuditDisposalTool tool = new ApiPmAuditDisposalTool();
            tool.toolName = toolName;
            tool.toolReference = toolRef;
            tool.activityId = activityId;
            tool.createdBy = createdBy;
            tool.toolType = AppConstants.PREFIX_ADT;

            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.createdOn = dateTimeNow;
            tool.updatedOn = dateTimeNow;
            
            db.savePmAdt(sQLiteConnection, tool);

            message = "SUCCESS";
            return true;

        }

        private void apiDataSavePmAuditDisposalToolIdentification(string entityName, string disposalRefNo, string auditPeriod, string toolRef)
        {

            var toolIdentification = new ApiPmAuditDisposalToolIdentification();
            toolIdentification.toolReference = toolRef;

            toolIdentification.nameOfEntity = entityName;
            toolIdentification.referenceNo = disposalRefNo;
            toolIdentification.auditPeriod = auditPeriod;

            Control parent = this;

            Control ctlAssetDesc = WidgetHandler.findControlByName(parent, "txvAdtAssetDescription");
            String assetDesc = ctlAssetDesc == null ? "" : ctlAssetDesc.Text;
            toolIdentification.assetDescription = assetDesc;

            Control ctlBuyer = WidgetHandler.findControlByName(parent, "txvAdtNameOfBuyer");
            String buyer = ctlBuyer == null ? "" : ctlBuyer.Text;
            toolIdentification.nameOfBuyer = buyer;

            Control ctlDisposalValue = WidgetHandler.findControlByName(parent, "txvAdtDisposalValue");
            String disposalValue = ctlDisposalValue == null ? "" : ctlDisposalValue.Text;
            toolIdentification.disposalValue = disposalValue;

            Control ctlValuationAmount = WidgetHandler.findControlByName(parent, "txvAdtValuationAmount");
            String valuationAmount = ctlValuationAmount == null ? "" : ctlValuationAmount.Text;
            toolIdentification.valuationAmount = valuationAmount;

            Control cbxDisposalMethod = WidgetHandler.findControlByName(parent, "cbxAdtDisposalMethod");
            String disposalMethod = cbxDisposalMethod == null ? "" : cbxDisposalMethod.Text;
            toolIdentification.disposalMethod = disposalMethod;

            new DatabaseHandler().saveApiPmAuditDisposalToolIdentification(DatabaseHandler.dbConnection(), toolIdentification);

        }
        
        private static void apiDataSavePmAuditDisposalToolDynamicSectionsData(string toolRef)
        {

            var sectionData = AppStateAuditTools.auditDisposalToolSectionEvaluations;

            foreach (var section in sectionData)
            {

                var sectionItems = section.sectionItemEvaluations;

                ApiPmAuditDisposalToolSection apiToolSection = new ApiPmAuditDisposalToolSection();
                apiToolSection.sectionEmisId = section.sectionEmisId;
                apiToolSection.toolReference = toolRef;
                apiToolSection.name = section.name;
                apiToolSection.yesTotal = section.sectionYesTotalValue;
                apiToolSection.noTotal = section.sectionNoTotalValue;


                DatabaseHandler.saveApiPmAuditDisposalToolSection(DatabaseHandler.dbConnection(), apiToolSection);

                //after saving the section we proceed to save the section items
                var apiPmAuditDisposalToolSectionItems = new List<ApiPmAuditDisposalToolSectionItem>();

                foreach (KeyValuePair<long, SectionItemEvaluationAdt> entry in sectionItems)
                {

                    var item = entry.Value;

                    var apiToolSectionItem = new ApiPmAuditDisposalToolSectionItem();
                    apiToolSectionItem.sectionEmisId = section.sectionEmisId;
                    apiToolSectionItem.toolReference = toolRef;
                    apiToolSectionItem.itemEmisId = item.sectionItemEmisId;
                    apiToolSectionItem.evaluationFieldRole = item.evaluationFieldRole;
                    apiToolSectionItem.evaluationFieldType = item.evaluationFieldType;
                    apiToolSectionItem.evaluationFieldValue = item.evaluationFieldValue;
                    apiToolSectionItem.finding = item.findingFieldValue;
                    apiToolSectionItem.exception = item.exceptionFieldValue;
                    apiToolSectionItem.yesRank = item.yesRank;
                    apiToolSectionItem.noRank = item.noRank;
                    apiToolSectionItem.naRank = item.naRank;

                    apiToolSectionItem.description = item.description;
                    apiToolSectionItem.managementLetterSectionId = item.managementLetterSectionFieldValue;
                    apiToolSectionItem.managementLetterSectionItemId = item.managementLetterSectionItemFieldValue;

                    apiPmAuditDisposalToolSectionItems.Add(apiToolSectionItem);

                }

                //save the list of items to the database
                DatabaseHandler.saveApiPmAuditDisposalToolSectionItem(DatabaseHandler.dbConnection(), apiPmAuditDisposalToolSectionItems);

            }
          
        }

        private void apiDataSaveAuditDisposalToolAnalysis(string toolRef)
        {

            var data = new ApiPmAuditDisposalToolAnalysis();
            data.toolReference = toolRef;

            String AdtScorePlan = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_PLAN);
            String AdtScoreInitiation = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_INITIATION);
            String AdtScorePublicBidding = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_PUBLIC_BIDDING);
            String dtScorePublicAuction = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_PUBLIC_AUCTION);
            String AdtScoreDirectNegotiations = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_DIRECT_NEGOTIATIONS);
            String AdtScorePublicOfficers = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_PUBLIC_OFFICERS);
            String AdtScoreDestruction = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_DESTRUCTION);
            String AdtScoreConversion = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_CONVERSION);
            String AdtScoreTradeIn = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_TRADE_IN);
            String AdtScoreTransfer = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_TRANSFER);
            String AdtScoreDonation = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_DONATION);
            String AdtScoreDelivery = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_DELIVERY);
            String AdtScoreRecords = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_RECORDS);

            String AdtScoreAverage = WidgetHandler.findCtlValByCtlName(this, FLD_SCORE_AVERAGE);

            data.scorePlan = AdtScorePlan;
            data.scoreInitiation = AdtScoreInitiation;
            data.scorePublicBidding = AdtScorePublicBidding;
            data.scorePublicAuction = dtScorePublicAuction;
            data.scoreDirectNegotiations = AdtScoreDirectNegotiations;
            data.scorePublicOfficers = AdtScorePublicOfficers;
            data.scoreDestruction = AdtScoreDestruction;
            data.scoreConversion = AdtScoreConversion;
            data.scoreTradeIn = AdtScoreTradeIn;
            data.scoreTransfer = AdtScoreTransfer;
            data.scoreDonation = AdtScoreDonation;
            data.scoreDelivery = AdtScoreDelivery;
            data.scoreRecords = AdtScoreRecords;

            data.scoreAverage = AdtScoreAverage;

            DatabaseHandler.saveApiPmAuditDisposalToolAnalysis(DatabaseHandler.dbConnection(), data);

        }

        private string getAuditorName()
        {
            var cbxCreatedBy = (ComboBox)WidgetHandler.findControlByName(this, "cbxAdtAuditorName");
            String createdBy = cbxCreatedBy != null && cbxCreatedBy.SelectedValue != null ? (String)cbxCreatedBy.SelectedValue : "";
            return createdBy;
        }

        public void loadExceptionsBasedOnExceptionSectionSelected(object sender, EventArgs e)
        {

            ComboBox cbx = (ComboBox)sender;
            String fieldName = cbx.Name;
            int exceptionSectionId = cbx != null && cbx.SelectedValue != null ? (int)cbx.SelectedValue : 0;
            if (exceptionSectionId == 0)
            {
                return;
            }

            var exceptions = new DatabaseHandler().getManagementLetterSectionExceptions(DatabaseHandler.dbConnection(), exceptionSectionId);

            foreach (var section in AppStateAuditTools.auditDisposalToolSectionEvaluations)
            {

                var items = section.sectionItemEvaluations;

                foreach (KeyValuePair<long, SectionItemEvaluationAdt> entry in section.sectionItemEvaluations)
                {

                    var item = entry.Value;

                    //we found the matching colum
                    if (item.managementLetterSectionFieldName == fieldName)
                    {

                        String exceptionField = item.managementLetterSectionItemFieldName;
                        var cbxExceptions = WidgetHandler.findControlByName(this, exceptionField);
                        if (cbxExceptions != null && cbxExceptions is ComboBox)
                        {
                            var comboxBox = (ComboBox)cbxExceptions;
                            WidgetHandler.populateManagementLetterSectionExceptionsDropdown(comboxBox, exceptions);
                        }
                        return;

                    }

                }

            }

        }

    }

}

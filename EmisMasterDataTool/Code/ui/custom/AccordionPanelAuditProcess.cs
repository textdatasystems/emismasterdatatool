﻿using Processor.ControlObjects;
using Processor.Entities.PM;
using System;
using System.Windows.Forms;

namespace EmisTool.Code.ui.custom
{

    internal class AccordionPanelAuditProcess : AccordionPanel
    {

        public AccordionPanelAuditProcess(AppStateAuditTools appState)
        {

            DatabaseHandler databaseHandler = new DatabaseHandler();
            SQLite.SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            //reset app state to remove old data
            AppStateAuditTools.auditProcessToolSectionEvaluations.Clear();
            UiHelperApt.FieldCountPmAuditProcessTool = 0;

            //identification section
            String sectionHeader = "IDENTIFICATION";

            //get users list
            var users = databaseHandler.allUsers(dbConn); 
            var section1 = UiHelperApt.GenerateSection1Table(users);

            //add section to main container
            accordion.Add(section1, sectionHeader, null, 1, false);

            //dynamic sections
           var sections = databaseHandler.getPmAuditProcessToolSections(dbConn);

            //get the management letter sections at once
            var managementLetterSections = databaseHandler.getManagementLetterSections(dbConn);

            foreach (var section in sections)
            {

                //will hold the state for this section
                SectionEvaluation sectionEvaluation = null;
                TableLayoutPanel sectionTable = UiHelperApt.GenerateTableV1(section, out sectionEvaluation, appState, managementLetterSections);

                String header = section.name;
                accordion.Add(sectionTable, header, null, 1, false);

                //check if section does not exists in app state
                if (!AppStateAuditTools.sectionEvaluationAptExists(sectionEvaluation.sectionEmisId) && sectionEvaluation.sectionEmisId != 0)
                {
                    AppStateAuditTools.auditProcessToolSectionEvaluations.Add(sectionEvaluation);
                }

            }                       

        }

    }


}

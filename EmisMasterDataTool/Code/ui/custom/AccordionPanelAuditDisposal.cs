﻿using Processor.ControlObjects;
using Processor.Entities.PM.disposal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code.ui.custom
{
  
    internal class AccordionPanelAuditDisposal : AccordionPanel
    {

        public AccordionPanelAuditDisposal(AppStateAuditTools appState)
        {

            DatabaseHandler databaseHandler = new DatabaseHandler();
            SQLite.SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            //reset app state to remove old data
            AppStateAuditTools.auditDisposalToolSectionEvaluations.Clear();
            UiHelperAdt.FieldCountPmAuditProcessTool = 0;

            //identification section
            String sectionHeader = "IDENTIFICATION";

            //users
            var users = databaseHandler.allUsers(dbConn);

            var section1 = UiHelperAdt.GenerateIdentificationSectionTable(users);
            accordion.Add(section1, sectionHeader, null, 1, false);

            //mgt letter sections
            var mgtLetterSections = databaseHandler.getManagementLetterSections(dbConn);

            //dynamic sections            
            var sections = databaseHandler.getPmAuditDisposalToolSections(dbConn);
            foreach (var section in sections)
            {

                //will hold the state for this section
                SectionEvaluationAdt sectionEvaluation = null;
                TableLayoutPanel sectionTable = UiHelperAdt.GenerateTableV1(section, out sectionEvaluation, appState, mgtLetterSections);

                String header = section.name;
                accordion.Add(sectionTable, header, null, 1, false);

                //check if section does not exists in app state
                if (!AppStateAuditTools.sectionEvaluationAdtExists(sectionEvaluation.sectionEmisId) && sectionEvaluation.sectionEmisId != 0)
                {
                    AppStateAuditTools.auditDisposalToolSectionEvaluations.Add(sectionEvaluation);
                }

            }

        }

    }


}

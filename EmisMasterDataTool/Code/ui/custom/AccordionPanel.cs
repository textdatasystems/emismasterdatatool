﻿using Opulos.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code.ui.custom
{

    internal class AccordionPanel : Panel
    {

        public Accordion accordion = new Accordion();

        public AccordionPanel()
        {
            Dock = DockStyle.Fill;

            accordion.HorizontalScroll.Maximum = 0;
            accordion.AutoScroll = false;
            accordion.VerticalScroll.Visible = false;
            accordion.AutoScroll = true;

            accordion.Insets = new Padding(10);
            accordion.ContentPadding = new Padding(10);
            accordion.ContentMargin = new Padding(0);
            accordion.CheckBoxMargin = new Padding(0);
            accordion.OpenOneOnly = true;
            accordion.ResizeBarsFadeInMillis = 0;
            accordion.ResizeBarsFadeOutMillis = 0;
            accordion.AnimateOpenMillis = 0;
            accordion.AnimateCloseMillis = 0;

            Controls.Add(accordion);

        }

    }


}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Processor.ControlObjects;
using Processor.Entities.api;
using Processor.Entities.PM;
using EmisMasterDataTool.Code;

namespace EmisTool.Code.ui.custom
{
    public class PanelPmAuditProcessTool : Panel, IAptAnalyisUpdater
    {      
        
        //holds the app state
        private AppStateAuditTools appState;

        //controls for total fields of dynamically created sections
        public static Dictionary<long, Control> APT_SECTION_TOTAL_TXVS = new Dictionary<long, Control>();

        private int activityId;
        private int sampleFileLocalId;
        private bool inEditMode = false;
        private String toolReferenceInEditMode = "";

        public PanelPmAuditProcessTool(int activityId, int sampleFileLocalId, ApiPmAuditProcessTools toolToEdit = null)
        {

            this.activityId = activityId;
            this.sampleFileLocalId = sampleFileLocalId;

            //clear the dictionary to avoid duplicate keys
            APT_SECTION_TOTAL_TXVS.Clear();

            //set the app state
            appState = new AppStateAuditTools();
            appState.iAptAnalyisUpdater = this;            
            
            //initialize the controls
            initializeControls();

            //load data
            loadData(toolToEdit);

        }

        private void loadData(ApiPmAuditProcessTools toolToEdit)
        {

            if (toolToEdit != null)
            {
                populateTool(toolToEdit);
                return;
            }

            //it's a new tool creation
            loadActivityGeneralDetails(activityId, sampleFileLocalId);

        }

        private void populateTool(ApiPmAuditProcessTools tool)
        {

            //
            //set status as edit, set the tool reference to edit, make the save button show update
            //
            string toolReference = tool.toolReference;

            setAppIntoEditMode(toolReference);

            //fill the person who created the tool
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxSect1CreatedBy", tool.createdBy);

            //fill identification
            populateIdentificationSection(tool.identification);

            //fill dynamic sections
            populateDynamicSections(tool.sections);

            //fill analysis data
            populateAnalysisData(tool);

            //fill the comp & perf score
            populateCompAndPerfScore(tool.compAndPerfScore);

        }

        private void setAppIntoEditMode(string toolReference)
        {

            inEditMode = true;
            toolReferenceInEditMode = toolReference;

            Control btnSaveTool = WidgetHandler.findControlByName(this, "btnSaveTool");
            if (btnSaveTool != null) { btnSaveTool.Text = "Update Tool"; }

        }

        private void loadActivityGeneralDetails(int activityId, int sampleFileLocalId)
        {

            DatabaseHandler dbHandler = new DatabaseHandler();

            var activity = dbHandler.findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityId);
            if (activity == null)
            {
                WidgetHandler.showMessage("Failed to find PM activity with ID [" + activityId + "]");
                return;
            }

            var sampleFile = dbHandler.getCustomPmAuditSampleFileByActivityIdAndLocalId(DatabaseHandler.dbConnection(), activityId, sampleFileLocalId);
            if(sampleFile == null)
            {
                return;
            }

            ApiPmAuditProcessToolIdentification ident = new ApiPmAuditProcessToolIdentification();
            ident.nameOfEntity = activity.entityName;
            ident.totalContractValue = sampleFile.contract_value.ToString();
            ident.procurementMethod = sampleFile.procurement_method;
            ident.referenceNo = sampleFile.procurement_reference;
            ident.nameOfProvider = sampleFile.provider;
            ident.contractDesc = sampleFile.procurement_subject;

            populateIdentificationSection(ident);

        }

        private void initializeControls()
        {

            this.Name = "panelPmAuditProcessTool";
            this.Padding = new Padding(10);


            //tab control
            TabControl tabControlPmAuditProcessTool = getTabControlPmAuditProcessTool();


            //button section           
            Panel panelButtonContainer = getButtonContainerPanel();


            //this is just to hold the to controls, 1 for the buttons and the other for the tab control
            TableLayoutPanel tablePaneMainContainer = new TableLayoutPanel();
            tablePaneMainContainer.Dock = DockStyle.Fill;
            tablePaneMainContainer.ColumnCount = 1;
            tablePaneMainContainer.RowCount = 2;
            tablePaneMainContainer.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

            //add the tab control to the main container spanning 90% of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 90));
            tablePaneMainContainer.Controls.Add(tabControlPmAuditProcessTool, 0, 0);
            // add the buttons panel to the main container spanning 10 % of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 10));
            tablePaneMainContainer.Controls.Add(panelButtonContainer, 0, 1);

            //add tab control to panel
            this.Controls.Add(tablePaneMainContainer);

        }

        private Panel getButtonContainerPanel()
        {

            Panel panelButtonContainer = new Panel();
            panelButtonContainer.Height = 40;
            panelButtonContainer.Dock = DockStyle.Fill;

            Button btnSaveTool = new Button() { Text = "Save Tool", Name = "btnSaveTool", Width = 150, Anchor = AnchorStyles.Bottom | AnchorStyles.Left, Margin = new Padding(0) };
            btnSaveTool.Click += btnSaveToolClicked;
            panelButtonContainer.Controls.Add(btnSaveTool);

            TableLayoutPanel tabPanel = new TableLayoutPanel() { Anchor = AnchorStyles.Bottom | AnchorStyles.Right, Margin = new Padding(0) };
            tabPanel.ColumnCount = 3;
            tabPanel.RowCount = 1;
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 35));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40));
            tabPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 35));
            // tabPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tabPanel.Width = 200;
            tabPanel.Height = 38;

            Label labelLoadTool = new Label() { Text = "Tool Id", Dock = DockStyle.Fill, TextAlign = System.Drawing.ContentAlignment.MiddleRight };
            TextBox txvLoadTool = new TextBox() { Name = "txvLoadTool", Width = 100 };
            Button btnLoadTool = new Button() { Name = "btnLoadTool", Text = "Load Tool" };
            btnLoadTool.Click += btnLoadToolClicked;

            tabPanel.Controls.Add(labelLoadTool, 0, 0);
            tabPanel.Controls.Add(txvLoadTool, 1, 0);
            tabPanel.Controls.Add(btnLoadTool, 2, 0);

           // panelButtonContainer.Controls.Add(tabPanel);
            return panelButtonContainer;

        }

        private TabControl getTabControlPmAuditProcessTool()
        {

            TabControl tabControlPmAuditProcessTool = new TabControl();
            tabControlPmAuditProcessTool.Name = "tabControlPmAuditProcessTool";
            tabControlPmAuditProcessTool.Dock = DockStyle.None;
            tabControlPmAuditProcessTool.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            //tab pages for the tab control
            TabPage tabPageProcessWorksheet = GetTabPageProcessWorksheet();
            TabPage tabPageProcessAnalysis = GetTabPageProcessAnalysis();
            TabPage tabPageCompAndPerfScore = GetTabPageCompAndPerfScore();

            //add tab pages to tab control
            tabControlPmAuditProcessTool.TabPages.Add(tabPageProcessWorksheet);
            tabControlPmAuditProcessTool.TabPages.Add(tabPageProcessAnalysis);
            tabControlPmAuditProcessTool.TabPages.Add(tabPageCompAndPerfScore);
            return tabControlPmAuditProcessTool;

        }

        private TabPage GetTabPageCompAndPerfScore()
        {
            
            Panel panelContainer = new Panel();
            panelContainer.Dock = DockStyle.Fill;

            //get the CompAndPerfScor tab content table
            var tableCompAndPerfScore = UiHelperApt.GenerateCompAndPerfScoreTable();

            //add table to container panel
            panelContainer.Controls.Add(tableCompAndPerfScore);

            TabPage tabPageCompAndPerfScore = new TabPage("Comp & Perf Score");
            tableCompAndPerfScore.Padding = new Padding(20);
            tabPageCompAndPerfScore.Controls.Add(panelContainer);

            return tabPageCompAndPerfScore;

        }

        private TabPage GetTabPageProcessAnalysis()
        {

            Panel panelContainer = new Panel();
            panelContainer.Dock = DockStyle.Fill;

            //get the analyis tab content table
            var uiHelperAnalysis = new UiHelperAptAnalysis();
            uiHelperAnalysis.iAptAnalyisUpdater = this;
            var tableAnalysis = uiHelperAnalysis.GenerateProcessAnalyisTable();
            
            //add table to container panel
            panelContainer.Controls.Add(tableAnalysis);           
           
            TabPage tabPageDisposalAnalysis = new TabPage("Process Analysis");
            tableAnalysis.Padding = new Padding(20);
            tabPageDisposalAnalysis.Controls.Add(panelContainer);

            return tabPageDisposalAnalysis;

        }

        private TabPage GetTabPageProcessWorksheet()
        {

            TabPage tabPageProcessWorksheet = new TabPage("Process Worksheet");
            tabPageProcessWorksheet.Name = "tabPageProcessWorksheet";

            //get the panel with the accordion list
            Panel panelAccordionContainer = GetPanelAccordionContainer();

            //add panel containing the accordion to the page
            tabPageProcessWorksheet.Controls.Add(panelAccordionContainer);

            return tabPageProcessWorksheet;

        }

        private Panel GetPanelAccordionContainer()
        {

            Panel panelAccordionContainer = new Panel();
            panelAccordionContainer.Name = "panelAccordionContainer";
            panelAccordionContainer.Dock = DockStyle.Fill;

            panelAccordionContainer.Padding = new Padding(10);

            //get accordion panel with the different sections
            AccordionPanelAuditProcess accordionPanelAuditProcess = new AccordionPanelAuditProcess(appState);
            accordionPanelAuditProcess.Name = "accordionPanelAuditProcess";

            //add accordion panel to the container
            panelAccordionContainer.Controls.Add(accordionPanelAuditProcess);

            return panelAccordionContainer;

        }

        private void btnSaveToolClicked(object sender, EventArgs e)
        {

            if (!inEditMode)
            {
                saveTool();
            }
            else
            {
                updateTool();
            }           

        }

        private void updateTool()
        {

            //check if the tool name probably changed
            String entityName = WidgetHandler.findCtlValByCtlName(this, "txvSec1EntityName");
            String referenceNo = WidgetHandler.findCtlValByCtlName(this, "txvSec1ReferenceNo");
            String auditPeriod = WidgetHandler.findCtlValByCtlName(this, "txvSec1AuditPeriod");

            String newToolName = SharedCommons.getToolName(AppConstants.PREFIX_APT, entityName, auditPeriod, activityId, sampleFileLocalId);

            //tool Reference might have changed
            if (!apiDataUpdateToolReference(newToolName, out String message))
            {
                WidgetHandler.showMessage(message);
                return;
            }

            //clear existing data
            new DatabaseHandler().clearAuditProcessToolOldData(DatabaseHandler.dbConnection(), toolReferenceInEditMode);

            //save new tool data
            saveToolData(entityName, referenceNo, auditPeriod, newToolName);
            toolReferenceInEditMode = newToolName;

        }

        private bool apiDataUpdateToolReference(string newToolName, out string message)
        {

            DatabaseHandler db = new DatabaseHandler();
            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();

            if (toolReferenceInEditMode != newToolName && db.auditProcessToolNameExists(sQLiteConnection, newToolName))
            {
                message = "Tool with name [" + newToolName + "] already exists.";
                return false;
            }

            var tool = db.getPmAuditProcessToolByIdOrActivityIdAndSampleFileLocalId(sQLiteConnection, activityId, sampleFileLocalId);
            if (tool == null)
            {
                message = "Tool for activity ID [" + activityId + "] and sample file ID ["+sampleFileLocalId+"] not found.";
                return false;
            }


            tool.toolName = newToolName;
            tool.toolReference = newToolName;
            tool.activityId = activityId;
            tool.createdBy = getAuditorName();
            tool.toolType = AppConstants.PREFIX_APT;


            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.updatedOn = dateTimeNow;

            db.updatePmApt(sQLiteConnection, tool);

            message = "SUCCESS";
            return true;

        }

        private void saveTool()
        {

            String entityName = WidgetHandler.findCtlValByCtlName(this, "txvSec1EntityName");
            String referenceNo = WidgetHandler.findCtlValByCtlName(this, "txvSec1ReferenceNo");
            String auditPeriod = WidgetHandler.findCtlValByCtlName(this, "txvSec1AuditPeriod");
            string createdBy = getAuditorName();

            //check if the required initial fields are supplied
            if (!validPmAuditProcessToolData(entityName, auditPeriod, referenceNo, createdBy))
            {
                return;
            }

            //create a tool reference and save the tool header
            String toolName = SharedCommons.getToolName(AppConstants.PREFIX_APT, entityName, auditPeriod, activityId, sampleFileLocalId);
            String toolRef = toolName;

            String message = "";
            long toolId = 0;

            if (!apiDataSavePmAuditProcessTool(toolName, toolRef, activityId, sampleFileLocalId, out message, out toolId, createdBy))
            {
                WidgetHandler.showMessage(message, true);
                return;
            }

            saveToolData(entityName, referenceNo, auditPeriod, toolRef);

        }

        private string getAuditorName()
        {
            var cbxCreatedBy = (ComboBox)WidgetHandler.findControlByName(this, "cbxSect1CreatedBy");
            String createdBy = cbxCreatedBy != null && cbxCreatedBy.SelectedValue != null ? (String)cbxCreatedBy.SelectedValue : "";
            return createdBy;
        }

        private void saveToolData(string entityName, string referenceNo, string auditPeriod, string toolRef)
        {

            //save the tool identification data
            apiDataSavePmAuditToolIdentification(entityName, referenceNo, auditPeriod, toolRef);

            //save the sections and section items
            apiDataSavePmAuditProcessToolDynamicSections(toolRef);

            //save analysis data
            apiDataSaveAnalysis(toolRef);

            //save the comp and perf scores
            apiDataSaveCompAndPerfScores(toolRef);


            setAppIntoEditMode(toolRef);

            //everything has been successfully saved
            WidgetHandler.showMessage("Tool successfully saved with tool reference [" + toolRef + "]", false);

        }

        private void apiDataSaveAnalysis(string toolRef)
        {

            //we supposed to save the analysis in 5 tables
            //compliance, completion, cost, process dates, process time

            ApiPmAuditProcessToolAnalysisCompliance dataCompliance = getApiPmAuditProcessToolAnalysisCompliance(toolRef);
            ApiPmAuditProcessToolAnalysisCompletion dataCompletion = getApiPmAuditProcessToolAnalysisCompletion(toolRef);
            ApiPmAuditProcessToolAnalysisCost dataCost = getApiPmAuditProcessToolAnalysisCost(toolRef);
            ApiPmAuditProcessToolAnalysisProcessDates dataProcessDates = getApiPmAuditProcessToolAnalysisProcessDates(toolRef);
            ApiPmAuditProcessToolAnalysisProcessTime dataProcessTimes = getApiPmAuditProcessToolAnalysisProcessTime(toolRef);

            DatabaseHandler db = new DatabaseHandler();
            var dbConn = DatabaseHandler.dbConnection();

            db.saveApiPmAudiProcessToolAnalysisCompliance(dbConn, dataCompliance);
            db.saveApiPmAuditProcessToolAnalysisCompletion(dbConn, dataCompletion);
            db.saveApiPmAuditProcessToolAnalysisCost(dbConn, dataCost);
            db.saveApiPmAuditProcessToolAnalysisProcessDates(dbConn, dataProcessDates);
            db.saveApiPmAuditProcessToolAnalysisProcessTime(dbConn, dataProcessTimes);

        }

        private ApiPmAuditProcessToolAnalysisProcessTime getApiPmAuditProcessToolAnalysisProcessTime(String toolRef)
        {
            var data = new ApiPmAuditProcessToolAnalysisProcessTime();
            data.toolReference = toolRef;

            data.initiationStartDelayTime = WidgetHandler.findCtlValByCtlName(this, "Sect111PercentScore");
            data.requistionTime = WidgetHandler.findCtlValByCtlName(this, "Sect112PercentScore");
            data.bidSubmissionPeriod = WidgetHandler.findCtlValByCtlName(this, "Sect113PercentScore");
            data.bidEvaluationTime = WidgetHandler.findCtlValByCtlName(this, "Sect114PercentScore");
            data.contractAwardPeriod = WidgetHandler.findCtlValByCtlName(this, "Sect115PercentScore");
            data.noBEBDelayPeriod = WidgetHandler.findCtlValByCtlName(this, "Sect116PercentScore");
            data.contractingTime = WidgetHandler.findCtlValByCtlName(this, "Sect117PercentScore");
            data.actualProcTime = WidgetHandler.findCtlValByCtlName(this, "Sect118PercentScore");
            data.plannedProcTime = WidgetHandler.findCtlValByCtlName(this, "Sect119PercentScore");
            data.procTimeOverun = WidgetHandler.findCtlValByCtlName(this, "Sect1110PercentScore");
            data.contractualCompletionTime = WidgetHandler.findCtlValByCtlName(this, "Sect1111PercentScore");
            data.actualCompletionTime = WidgetHandler.findCtlValByCtlName(this, "Sect1112PercentScore");
            data.completionTimeOverun = WidgetHandler.findCtlValByCtlName(this, "Sect1113PercentScore");
            data.contractualPaymentPeriodInDays = WidgetHandler.findCtlValByCtlName(this, "Sect1114PercentScore");
            data.actualPaymentPeriodInDays = WidgetHandler.findCtlValByCtlName(this, "Sect1115PercentScore");
            data.paymentPeriodOverun = WidgetHandler.findCtlValByCtlName(this, "Sect1116PercentScore");
            data.procTimeOverunPercentage = WidgetHandler.findCtlValByCtlName(this, "Sect1117PercentScore");
            data.completionTimeOverunPercentage = WidgetHandler.findCtlValByCtlName(this, "Sect1118PercentScore");
            data.paymentPeriodOverunPercentage = WidgetHandler.findCtlValByCtlName(this, "Sect1119PercentScore");
            data.procureRatio = WidgetHandler.findCtlValByCtlName(this, "Sect1120PercentScore");
            data.completionRatio = WidgetHandler.findCtlValByCtlName(this, "Sect1121PercentScore");
            data.paymentRatio = WidgetHandler.findCtlValByCtlName(this, "Sect1122PercentScore");

            return data;
        }

        private ApiPmAuditProcessToolAnalysisProcessDates getApiPmAuditProcessToolAnalysisProcessDates(String toolRef)
        {

            var data = new ApiPmAuditProcessToolAnalysisProcessDates();
            data.toolReference = toolRef;

            data.startDatePerProcPlan = WidgetHandler.findCtlValByCtlName(this, "Sect101PercentScore");
            data.dateOfInitiationRequest = WidgetHandler.findCtlValByCtlName(this, "Sect102PercentScore");
            data.dateOfBidInvitationNote = WidgetHandler.findCtlValByCtlName(this, "Sect103PercentScore");
            data.dateOfBidSubmission = WidgetHandler.findCtlValByCtlName(this, "Sect104PercentScore");
            data.dateOfBidEvaluationReport = WidgetHandler.findCtlValByCtlName(this, "Sect105PercentScore");
            data.dateOfContractAward = WidgetHandler.findCtlValByCtlName(this, "Sect106PercentScore");
            data.dateOfNoticeOfBestBidder = WidgetHandler.findCtlValByCtlName(this, "Sect107PercentScore");
            data.dateOfExpiryOfNoBEB = WidgetHandler.findCtlValByCtlName(this, "Sect108PercentScore");
            data.dateOfContractSigning = WidgetHandler.findCtlValByCtlName(this, "Sect109PercentScore");
            data.dateOfContractSigningPerProcPlan = WidgetHandler.findCtlValByCtlName(this, "Sect1010PercentScore");
            data.contractualCompletionDate = WidgetHandler.findCtlValByCtlName(this, "Sect1011PercentScore");
            data.actualCompletionDate = WidgetHandler.findCtlValByCtlName(this, "Sect1012PercentScore");
            data.dateOfCertificationOfInvoice = WidgetHandler.findCtlValByCtlName(this, "Sect1013PercentScore");
            data.dateInvoicePaid = WidgetHandler.findCtlValByCtlName(this, "Sect1014PercentScore");
            
            return data;
        }

        private ApiPmAuditProcessToolAnalysisCost getApiPmAuditProcessToolAnalysisCost(String toolRef)
        {

            var data = new ApiPmAuditProcessToolAnalysisCost();
            data.toolReference = toolRef;

            data.procPlanTotalCostValue = WidgetHandler.findCtlValByCtlName(this, "Sect131PercentScore");
            data.accountingOfficerMarketPrice = WidgetHandler.findCtlValByCtlName(this, "Sect132PercentScore");

            data.bestBidder1Name = WidgetHandler.findCtlValByCtlName(this, "txvBiddder1Name");
            data.bestBidder1Price = WidgetHandler.findCtlValByCtlName(this, "txvBiddder1Price");

            data.bestBidder2Name = WidgetHandler.findCtlValByCtlName(this, "txvBiddder2Name");
            data.bestBidder2Price = WidgetHandler.findCtlValByCtlName(this, "txvBiddder2Price");

            data.bestBidder3Name = WidgetHandler.findCtlValByCtlName(this, "txvBiddder3Name");
            data.bestBidder3Price = WidgetHandler.findCtlValByCtlName(this, "txvBiddder3Price");

            data.bestBidder4Name = WidgetHandler.findCtlValByCtlName(this, "txvBiddder4Name");
            data.bestBidder4Price = WidgetHandler.findCtlValByCtlName(this, "txvBiddder4Price");

            data.bestBidder5Name = WidgetHandler.findCtlValByCtlName(this, "txvBiddder5Name");
            data.bestBidder5Price = WidgetHandler.findCtlValByCtlName(this, "txvBiddder5Price");

            data.bestBidder6Name = WidgetHandler.findCtlValByCtlName(this, "txvBiddder6Name");
            data.bestBidder6Price = WidgetHandler.findCtlValByCtlName(this, "txvBiddder6Price");

            data.bestBidder7Name = WidgetHandler.findCtlValByCtlName(this, "txvBiddder7Name");
            data.bestBidder7Price = WidgetHandler.findCtlValByCtlName(this, "txvBiddder7Price");

            data.totalContractAwardPrice = WidgetHandler.findCtlValByCtlName(this, "Sect134PercentScore");
            data.finalContractCostOnCompletion = WidgetHandler.findCtlValByCtlName(this, "Sect135PercentScore");
            data.costOverunAmount = WidgetHandler.findCtlValByCtlName(this, "Sect136PercentScore");
            data.budgetVarianceAmount = WidgetHandler.findCtlValByCtlName(this, "Sect137PercentScore");
            data.budgetVariancePercentage = WidgetHandler.findCtlValByCtlName(this, "Sect138PercentScore");
            data.costOVerunPercentage = WidgetHandler.findCtlValByCtlName(this, "Sect139PercentScore");
            data.planRatio = WidgetHandler.findCtlValByCtlName(this, "Sect1310PercentScore");
            data.costRatio = WidgetHandler.findCtlValByCtlName(this, "Sect1311PercentScore");
           
            return data;
        }

        private ApiPmAuditProcessToolAnalysisCompletion getApiPmAuditProcessToolAnalysisCompletion(String toolRef)
        {

            var data = new ApiPmAuditProcessToolAnalysisCompletion();
            data.toolReference = toolRef;

            data.numberOfBiddersInvited = WidgetHandler.findCtlValByCtlName(this, "Sect121PercentScore");
            data.numberOfBidsReceived = WidgetHandler.findCtlValByCtlName(this, "Sect122PercentScore");
            data.numberOfBidsPassedPreliminaryExam = WidgetHandler.findCtlValByCtlName(this, "Sect123PercentScore");
            data.numberOfTechnicallyResponsiveBids = WidgetHandler.findCtlValByCtlName(this, "Sect124PercentScore");
            data.bidSubmissionRate = WidgetHandler.findCtlValByCtlName(this, "Sect125PercentScore");
            data.bidResponsiveRate = WidgetHandler.findCtlValByCtlName(this, "Sect126PercentScore");

            return data;

        }

        private ApiPmAuditProcessToolAnalysisCompliance getApiPmAuditProcessToolAnalysisCompliance(String toolRef)
        {

            var data = new ApiPmAuditProcessToolAnalysisCompliance();
            data.toolReference = toolRef;

            data.proPlanningAndInitiationYesScore = WidgetHandler.findCtlValByCtlName(this, "Sect91YesScore");
            data.proPlanningAndInitiationNoScore = WidgetHandler.findCtlValByCtlName(this, "Sect91NoScore");
            data.proPlanningAndInitiationPercentScore = WidgetHandler.findCtlValByCtlName(this, "Sect91TotalScore");
            data.proPlanningAndInitiationPercentScore = WidgetHandler.findCtlValByCtlName(this, "Sect91PercentScore");

            data.biddingDocumentYesScore = WidgetHandler.findCtlValByCtlName(this, "Sect92YesScore");
            data.biddingDocumentNoScore = WidgetHandler.findCtlValByCtlName(this, "Sect92NoScore");
            data.biddingDocumentTotalScore = WidgetHandler.findCtlValByCtlName(this, "Sect92TotalScore");
            data.biddingDocumentPercentScore = WidgetHandler.findCtlValByCtlName(this, "Sect92PercentScore");

            data.biddingYesScore = WidgetHandler.findCtlValByCtlName(this, "Sect93YesScore");
            data.biddingNoScore = WidgetHandler.findCtlValByCtlName(this, "Sect93NoScore");
            data.biddingTotalScore = WidgetHandler.findCtlValByCtlName(this, "Sect93TotalScore");
            data.biddingPercentScore = WidgetHandler.findCtlValByCtlName(this, "Sect93PercentScore");

            data.bidEvaluationYesScore = WidgetHandler.findCtlValByCtlName(this, "Sect94YesScore");
            data.bidEvaluationNoScore = WidgetHandler.findCtlValByCtlName(this, "Sect94NoScore");
            data.bidEvaluationTotalScore = WidgetHandler.findCtlValByCtlName(this, "Sect94TotalScore");
            data.bidEvaluationPercentScore = WidgetHandler.findCtlValByCtlName(this, "Sect94PercentScore");

            data.procContractingYesScore = WidgetHandler.findCtlValByCtlName(this, "Sect95YesScore");
            data.procContractingNoScore = WidgetHandler.findCtlValByCtlName(this, "Sect95NoScore");
            data.procContractingTotalScore = WidgetHandler.findCtlValByCtlName(this, "Sect95TotalScore");
            data.procContractingPercentScore = WidgetHandler.findCtlValByCtlName(this, "Sect95PercentScore");

            data.contractManagementYesScore = WidgetHandler.findCtlValByCtlName(this, "Sect96YesScore");
            data.contractManagementNoScore = WidgetHandler.findCtlValByCtlName(this, "Sect96NoScore");
            data.contractManagementTotalScore = WidgetHandler.findCtlValByCtlName(this, "Sect96TotalScore");
            data.contractManagementPercentScore = WidgetHandler.findCtlValByCtlName(this, "Sect96PercentScore");

            data.recordsKeepingYesScore = WidgetHandler.findCtlValByCtlName(this, "Sect97YesScore");
            data.recordsKeepingNoScore = WidgetHandler.findCtlValByCtlName(this, "Sect97NoScore");
            data.recordsKeepingTotalScore = WidgetHandler.findCtlValByCtlName(this, "Sect97TotalScore");
            data.recordsKeepingPercentScore = WidgetHandler.findCtlValByCtlName(this, "Sect97PercentScore");

            data.procProcessComplianceLevel = WidgetHandler.findCtlValByCtlName(this, "Sect98PercentScore");

            return data;
        }

        private void apiDataSaveCompAndPerfScores(string toolRef)
        {

            String complianceLevel = WidgetHandler.findCtlValByCtlName(this, "txvAptComplianceLevel");
            String procureRatio = WidgetHandler.findCtlValByCtlName(this, "txvAptProcureRatio");
            String completionRatio = WidgetHandler.findCtlValByCtlName(this, "txvAptCompletionRatio"); 
            String paymentRatio = WidgetHandler.findCtlValByCtlName(this, "txvAptPaymentRatio"); 
            String noOfBids = WidgetHandler.findCtlValByCtlName(this, "txvAptNoOfBids");
            String bidSubmissionRate = WidgetHandler.findCtlValByCtlName(this, "txvAptBidSubmissionRate");
            String bidResponsiveRate = WidgetHandler.findCtlValByCtlName(this, "txvAptBidResponsiveRate"); 
            String planRatio = WidgetHandler.findCtlValByCtlName(this, "txvAptPlanRatio"); 
            String costRatio = WidgetHandler.findCtlValByCtlName(this, "txvAptCostRatio");

            long lComplianceLevel = 0;
            long lProcureRatio = 0;
            long lCompletionRatio = 0;
            long lPaymentRatio = 0;
            long lNoOfBids = 0;
            long lBidSubmissionRate = 0;
            long lBidResponsiveRate = 0;
            long lPlanRatio = 0;
            long lCostRatio = 0;

            long.TryParse(complianceLevel, out lComplianceLevel);
            long.TryParse(procureRatio, out lProcureRatio);
            long.TryParse(completionRatio, out lCompletionRatio);
            long.TryParse(paymentRatio, out lPaymentRatio);
            long.TryParse(noOfBids, out lNoOfBids);
            long.TryParse(bidSubmissionRate, out lBidSubmissionRate);
            long.TryParse(bidResponsiveRate, out lBidResponsiveRate);
            long.TryParse(planRatio, out lPlanRatio);
            long.TryParse(costRatio, out lCostRatio);

            var compAndPerfScore = new ApiPmAuditProcessToolCompAndPerfScore();
            compAndPerfScore.bidResponsiveRate = lBidResponsiveRate;
            compAndPerfScore.bidSubmissionRatio = lBidSubmissionRate;
            compAndPerfScore.completionRatio = lCompletionRatio;
            compAndPerfScore.costRatio = lCostRatio;
            compAndPerfScore.numberOfBidsReceived = lNoOfBids;
            compAndPerfScore.paymentRatio = lPaymentRatio;
            compAndPerfScore.planRatio = lPlanRatio;
            compAndPerfScore.procurementProcessComplianceLevel = lComplianceLevel;
            compAndPerfScore.procureRatio = lProcureRatio;
            compAndPerfScore.toolReference = toolRef;

            DatabaseHandler.saveApiPmAuditProcessToolCompAndPerfScore(DatabaseHandler.dbConnection(), compAndPerfScore);

        }

        private static void apiDataSavePmAuditProcessToolDynamicSections(string toolRef)
        {

            var sectionData = AppStateAuditTools.auditProcessToolSectionEvaluations;

            foreach (var section in sectionData)
            {

                var sectionItems = section.sectionItemEvaluations;

                ApiPmAuditProcessToolSection apiToolSection = new ApiPmAuditProcessToolSection();
                apiToolSection.sectionEmisId = section.sectionEmisId;
                apiToolSection.toolReference = toolRef;
                apiToolSection.name = section.name;
                apiToolSection.yesTotal = section.sectionYesTotalValue;
                apiToolSection.noTotal = section.sectionNoTotalValue;


                DatabaseHandler.saveApiPmAuditProcessToolSection(DatabaseHandler.dbConnection(), apiToolSection);

                //after saving the section we proceed to save the section items
                var apiPmAuditProcessToolSectionItems = new List<ApiPmAuditProcessToolSectionItem>();

                foreach (KeyValuePair<long, SectionItemEvaluation> entry in sectionItems)
                {

                    var item = entry.Value;

                    var apiToolSectionItem = new ApiPmAuditProcessToolSectionItem();
                    apiToolSectionItem.sectionEmisId = section.sectionEmisId;
                    apiToolSectionItem.toolReference = toolRef;                    
                    apiToolSectionItem.itemEmisId = item.sectionItemEmisId;
                    apiToolSectionItem.evaluationFieldRole = item.evaluationFieldRole;
                    apiToolSectionItem.evaluationFieldType = item.evaluationFieldType;
                    apiToolSectionItem.evaluationFieldValue = item.evaluationFieldValue;

                    /*
                    Switch these values for now, such that they can be captured at the server side
                    */
                    apiToolSectionItem.finding = item.exceptionFieldValue; // item.findingFieldValue;
                    apiToolSectionItem.exception = item.findingFieldValue; // item.exceptionFieldValue;
                    apiToolSectionItem.yesRank = item.yesRank;
                    apiToolSectionItem.noRank = item.noRank;
                    apiToolSectionItem.naRank = item.naRank;
                    apiToolSectionItem.description = item.description;
                    apiToolSectionItem.managementLetterSectionId = item.managementLetterSectionFieldValue;
                    apiToolSectionItem.managementLetterSectionItemId = item.managementLetterSectionItemFieldValue;

                    apiPmAuditProcessToolSectionItems.Add(apiToolSectionItem);

                }

                //save the list of items to the database
                DatabaseHandler.saveApiPmAuditProcessToolSectionItem(DatabaseHandler.dbConnection(), apiPmAuditProcessToolSectionItems);

            }
        }

        private void apiDataSavePmAuditToolIdentification(string entityName, string referenceNo, string auditPeriod, string toolRef)
        {

            var ident = new ApiPmAuditProcessToolIdentification();
            ident.nameOfEntity = entityName;
            ident.referenceNo = referenceNo;
            ident.toolReference = toolRef;
            ident.auditPeriod = auditPeriod;

            Control ctlSector = WidgetHandler.findControlByName(this,"txvSect1EntitySector");
            String sector = ctlSector == null ? "" : ctlSector.Text;
            ident.sector = sector;

            Control ctlContractDesc = WidgetHandler.findControlByName(this,"txvSec1ContractDesc");
            String contractDesc = ctlContractDesc == null ? "" : ctlContractDesc.Text;
            ident.contractDesc = contractDesc;

            Control ctlProvider = WidgetHandler.findControlByName(this,"txvSec1NameOfProvider");
            String provider = ctlProvider == null ? "" : ctlProvider.Text;
            ident.nameOfProvider = provider;

            Control ctlContractValue = WidgetHandler.findControlByName(this,"txvSec1TotalContractValue");
            String contractValue = ctlContractValue == null ? "" : ctlContractValue.Text;
            ident.totalContractValue = contractValue;

            Control ctlTypeOfProvider = WidgetHandler.findControlByName(this,"cbxSect1Provider");
            String typeOfProvider = ctlTypeOfProvider == null ? "" : ctlTypeOfProvider.Text;
            ident.typeOfProvider = typeOfProvider;

            Control cbxSect1ProcessStage = WidgetHandler.findControlByName(this,"cbxSect1ProcessStage");
            String processStage = cbxSect1ProcessStage == null ? "" : cbxSect1ProcessStage.Text;
            ident.processStage = processStage;


            Control cbxSect1ProcureMethod = WidgetHandler.findControlByName(this,"cbxSect1ProcureMethod");
            String procureMethd = cbxSect1ProcureMethod == null ? "" : cbxSect1ProcureMethod.Text;
            ident.procurementMethod = procureMethd;


            Control cbxSect1ConsultantSelectioMtd = WidgetHandler.findControlByName(this,"cbxSect1ConsultantSelectioMtd");
            String consultSelMtd = cbxSect1ConsultantSelectioMtd == null ? "" : cbxSect1ConsultantSelectioMtd.Text;
            ident.consultantSelectionMethod = consultSelMtd;

            Control cbxSect1ProcureCategory = WidgetHandler.findControlByName(this,"cbxSect1ProcureCategory");
            String procureCat = cbxSect1ProcureCategory == null ? "" : cbxSect1ProcureCategory.Text;
            ident.procurementCategory = procureCat;

            Control cbxSect1BidInviteMethd = WidgetHandler.findControlByName(this,"cbxSect1BidInviteMethd");
            String bidInviteMtd = cbxSect1BidInviteMethd == null ? "" : cbxSect1BidInviteMethd.Text;
            ident.bidderInvivationMethod = bidInviteMtd;

            Control cbxSect1BidSubmissionMtd = WidgetHandler.findControlByName(this,"cbxSect1BidSubmissionMtd");
            String bidSubmissionMtd = cbxSect1BidSubmissionMtd == null ? "" : cbxSect1BidSubmissionMtd.Text;
            ident.bidSubmissionMethod = bidSubmissionMtd;

            Control cbxSect1ContractType = WidgetHandler.findControlByName(this,"cbxSect1ContractType");
            String contractType = cbxSect1ContractType == null ? "" : cbxSect1ContractType.Text;
            ident.contractType = contractType;            

            new DatabaseHandler().saveApiPmAuditProcessToolIdentification(DatabaseHandler.dbConnection(), ident);

        }

        private static bool apiDataSavePmAuditProcessTool(
            string toolName, string toolRef, int activityId, int sampleFileId,
            out String message, out long toolId, string createdBy)
        {

            toolId = 0;

            DatabaseHandler db = new DatabaseHandler();
            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();

            if (db.auditProcessToolNameExists(sQLiteConnection, toolName))
            {
                message = "Tool with name [" + toolName + "] already exists.";
                return false;
            }


            ApiPmAuditProcessTools tool = new ApiPmAuditProcessTools();
            tool.toolName = toolName;
            tool.toolReference = toolRef;
            tool.activityId = activityId;
            tool.sampleFileLocalId = sampleFileId;
            tool.toolType = AppConstants.PREFIX_APT;

            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.createdOn = dateTimeNow;
            tool.updatedOn = dateTimeNow;
            tool.createdBy = createdBy;

            //save the tool
            toolId = db.savePmApt(sQLiteConnection, tool);
            
            message = "SUCCESS";
            return true;

        }


        private bool validPmAuditProcessToolData(string entityName, string auditPeriod, string referenceNo, string createdBy)
        {

            if (String.IsNullOrEmpty(entityName))
            {
                WidgetHandler.showMessage("Please enter the entity name", true);
                return false;
            }

            if (String.IsNullOrEmpty(auditPeriod))
            {
                WidgetHandler.showMessage("Please enter the audit period", true);
                return false;
            }

            if (String.IsNullOrEmpty(referenceNo))
            {
                WidgetHandler.showMessage("Please enter the reference number", true);
                return false;
            }

            if (String.IsNullOrEmpty(createdBy))
            {
                WidgetHandler.showMessage("Please select the person who created the file", true);
                return false;
            }

            return true;

        }


        public void updateSectionScoresInAnalyisTab(SectionEvaluation section)
        {

            var sectionName = section.name;
            sectionName = String.IsNullOrEmpty(sectionName) ? sectionName : sectionName.ToUpper();

            var totalValueYes = section.sectionYesTotalValue;
            var totalValueNo = section.sectionNoTotalValue;

            if (sectionName == "Procurement planning and initiation".ToUpper())
            {
                fillPmAptAnaysisSecTotals("Sect91YesScore", totalValueYes, "Sect91NoScore", totalValueNo, "Sect91TotalScore", "Sect91PercentScore");

            }
            else if (sectionName == "Bidding Document".ToUpper())
            {
                fillPmAptAnaysisSecTotals("Sect92YesScore", totalValueYes, "Sect92NoScore", totalValueNo, "Sect92TotalScore", "Sect92PercentScore");
            }
            else if (sectionName == "Bidding".ToUpper())
            {
                fillPmAptAnaysisSecTotals("Sect93YesScore", totalValueYes, "Sect93NoScore", totalValueNo, "Sect93TotalScore", "Sect93PercentScore");
            }
            else if (sectionName == "Evaluation".ToUpper())
            {
                fillPmAptAnaysisSecTotals("Sect94YesScore", totalValueYes, "Sect94NoScore", totalValueNo, "Sect94TotalScore", "Sect94PercentScore");
            }
            else if (sectionName == "Contracting".ToUpper())
            {
                fillPmAptAnaysisSecTotals("Sect95YesScore", totalValueYes, "Sect95NoScore", totalValueNo, "Sect95TotalScore", "Sect95PercentScore");
            }
            else if (sectionName == "Contract Management".ToUpper())
            {
                fillPmAptAnaysisSecTotals("Sect96YesScore", totalValueYes, "Sect96NoScore", totalValueNo, "Sect96TotalScore", "Sect96PercentScore");
            }
            else if (sectionName == "Records".ToUpper())
            {
                fillPmAptAnaysisSecTotals("Sect97YesScore", totalValueYes, "Sect97NoScore", totalValueNo, "Sect97TotalScore", "Sect97PercentScore");
            }

        }

        private void fillPmAptAnaysisSecTotals(String yesTotalFieldName, long totalValueYes, String noTotalFieldName, long totalValueNo, String sumFieldName, String percentFieldName)
        {

            var txvYes = WidgetHandler.findControlByName(this, yesTotalFieldName);
            if (txvYes != null)
            {
                txvYes.Text = totalValueYes.ToString();
            }

            var txvNo = WidgetHandler.findControlByName(this, noTotalFieldName);
            if (txvNo != null)
            {
                txvNo.Text = totalValueNo.ToString();
            }

            //call the total score
            var sum = totalValueYes + totalValueNo;
            var txvSum = WidgetHandler.findControlByName(this, sumFieldName);
            if (txvSum != null)
            {
                txvSum.Text = sum.ToString();
            }


            //calculate the %compliance
            var percent = (totalValueYes * 100.0) / sum;
            var txvPercent = WidgetHandler.findControlByName(this, percentFieldName);
            if (txvPercent != null )
            {
                txvPercent.Text = Math.Round(percent, 1).ToString();
            }

        }


        public void autofillAnalysisTabTextFieldBasedOnEvalItemValue(SectionItemEvaluation sectionItem)
        {
            if (sectionItem == null)
            {
                return;
            }

            var itemRole = sectionItem.evaluationFieldRole;
            if (itemRole == null)
            {
                return;
            }

            if (itemRole == "Procurement plan total cost inclusive VAT")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect131PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Start date per procurement plan")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect101PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Accounting officer market price inclusive VAT")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect132PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date of initiation request")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect102PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date of bid submission")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect104PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Number of bidders invited to bid")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect121PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date of bid invitation notice")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect103PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Number of received bids")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect122PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Number of bids passed preliminary examination")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect123PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Number of technically responsive bids")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect124PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Evaluated Bids Bidder 5 Name")
            {
                //fillPmAptAnaysisSecDateAndTextFields("test", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Evaluated Bids Bidder 4 Name")
            {
                //fillPmAptAnaysisSecDateAndTextFields("test", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Evaluated Bids Bidder 3 Name")
            {
                //fillPmAptAnaysisSecDateAndTextFields("test", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Evaluated Bids Bidder 2 Name")
            {
                //fillPmAptAnaysisSecDateAndTextFields("test", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Evaluated Bids Bidder 1 Name")
            {
                //fillPmAptAnaysisSecDateAndTextFields("test", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date of bid evaluation report")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect105PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date of contract award")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect106PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date of notice of best evaluated bidder")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect107PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date of expiry of NoBEB")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect108PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date of contract signature")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect109PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Total contract award price inclusive VAT")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect134PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date or contract signature per procurement plan")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect1010PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Contractual completion date")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect1011PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Actual completion date")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect1012PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Contractual payment period in days")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect1114PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Final contract cost on completion")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect135PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date of certification of invoice")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect1013PercentScore", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Date invoice paid")
            {
                fillPmAptAnaysisSecDateAndTextFields("Sect1014PercentScore", sectionItem.evaluationFieldValue);
            }
        }

        private void fillPmAptAnaysisSecDateAndTextFields(String fieldName1, String fieldValue1, String fieldName2 = null, String fieldValue2 = null)
        {

            var txv1 = WidgetHandler.findControlByName(this, fieldName1); 
            if (txv1 != null)
            {
                txv1.Text = fieldValue1;
            }

            if (fieldName2 != null)
            {

                var txvs2 = WidgetHandler.findControlByName(this, fieldName2);
                if (txvs2 != null)
                {
                    txvs2.Text = fieldValue2;
                }

            }

        }

        public void calculateAnaysisTabFieldValues(object sender, EventArgs e)
        {

            CalculationHelper calcHelper = new CalculationHelper();

            calcHelper.aptInitiationStartDelayTime(this);
            calcHelper.aptRequisitionTime(this);
            calcHelper.aptBidSubmissionPeriod(this);
            calcHelper.aptBidEvaluationTime(this);
            calcHelper.aptContractAwardPeriod(this);
            calcHelper.aptNoBEBdisplayperiod(this);
            calcHelper.aptContractingTime(this);
            calcHelper.aptActualProcurementTime(this);
            calcHelper.aptPlannedProcurementTime(this);
            calcHelper.aptProcurementTimeOverrun(this);
            calcHelper.aptContractualCompletionTime(this);
            calcHelper.aptActualCompletionTime(this);
            calcHelper.aptCompletionTimeOverrun(this);
            calcHelper.aptContractualpaymentperiodindays(this);
            calcHelper.aptActualPaymentPeriodinDays(this);
            calcHelper.aptPaymentPeriodOverrun(this);


            calcHelper.aptProcurementTimeOverrunPercent(this);
            calcHelper.aptCompletionTimeOverrunPercent(this);
            calcHelper.aptPaymentPeriodOverrunPercent(this);
            calcHelper.aptProcureRatio(this);
            calcHelper.aptCompletionRatio(this);
            calcHelper.aptPaymentRatio(this);


            calcHelper.aptBidSubmissionRate(this);
            calcHelper.aptBidResponsiveRate(this);
            calcHelper.aptBudgetVarianceAmount(this);
            calcHelper.aptBudgetVariance(this);
            calcHelper.aptCostoverrun(this);
            calcHelper.aptPlanRatio(this);
            calcHelper.aptCostRatio(this);
                        
        }

        public void autoFillCompAndPerfTabScores(object sender, EventArgs e)
        {

            var control = (Control)sender;

            if (control == null)
            {
                return;
            }

            CalculationHelper calcHelper = new CalculationHelper();
            calcHelper.autoFillCompAndPerfScoreFields(this, control.Name, control.Text);

        }

        private void btnLoadToolClicked(object sender, EventArgs e)
        {

            String toolId = WidgetHandler.findCtlValByCtlName(this, "txvLoadTool");

            //check if it's a valid ID
            long lToolId = 0;
            if(!long.TryParse(toolId, out lToolId))
            {
                WidgetHandler.showMessage("Tool ID must be a number", true);
                return;
            }

            //get the tool
            var dbConn = DatabaseHandler.dbConnection();
            var tool = new DatabaseHandler().getPmAuditProcessToolByIdOrActivityIdAndSampleFileLocalId(dbConn, 0, 0, lToolId);

            //tool not found
            if(tool == null)
            {
                WidgetHandler.showMessage("Tool with ID[" + toolId + "] not found", true);
                return;
            }

            //fill identification
            populateIdentificationSection(tool.identification);

            //fill dynamic sections
            populateDynamicSections(tool.sections);

            //fill analysis data
            populateAnalysisData(tool);

            //fill the comp & perf score
            populateCompAndPerfScore(tool.compAndPerfScore);

            //we are done
            WidgetHandler.showMessage("Data successfully loaded", false);

        }

        private void populateAnalysisData(ApiPmAuditProcessTools tool)
        {

            populateAnalysisCompliance(tool.analysisCompliance);
            populateAnalysisCompletion(tool.analysisCompletion);
            populateAnalysisCost(tool.analysisCost);
            populateAnalysisProcessDates(tool.analysisProcessDates);
            populateAnalysisProcessTimes(tool.analysisProcessTimes);

        }

        private void populateAnalysisProcessTimes(ApiPmAuditProcessToolAnalysisProcessTime data)
        {

            if(data == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect111PercentScore", data.initiationStartDelayTime);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect112PercentScore", data.requistionTime);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect113PercentScore", data.bidSubmissionPeriod);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect114PercentScore", data.bidEvaluationTime);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect115PercentScore", data.contractAwardPeriod);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect116PercentScore", data.noBEBDelayPeriod);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect117PercentScore", data.contractingTime);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect118PercentScore", data.actualProcTime);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect119PercentScore", data.plannedProcTime);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1110PercentScore", data.procTimeOverun);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1111PercentScore", data.contractualCompletionTime);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1112PercentScore", data.actualCompletionTime);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1113PercentScore", data.completionTimeOverun);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1114PercentScore", data.contractualPaymentPeriodInDays);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1115PercentScore", data.actualPaymentPeriodInDays);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1116PercentScore", data.paymentPeriodOverun);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1117PercentScore", data.procTimeOverunPercentage);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1118PercentScore", data.completionTimeOverunPercentage);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1119PercentScore", data.paymentPeriodOverunPercentage);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1120PercentScore", data.procureRatio);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1121PercentScore", data.completionRatio);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1122PercentScore", data.paymentRatio);

        }

        private void populateAnalysisProcessDates(ApiPmAuditProcessToolAnalysisProcessDates data)
        {

            if(data == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect101PercentScore", data.startDatePerProcPlan);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect102PercentScore", data.dateOfInitiationRequest);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect103PercentScore", data.dateOfBidInvitationNote);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect104PercentScore", data.dateOfBidSubmission);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect105PercentScore", data.dateOfBidEvaluationReport);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect106PercentScore", data.dateOfContractAward);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect107PercentScore", data.dateOfNoticeOfBestBidder);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect108PercentScore", data.dateOfExpiryOfNoBEB);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect109PercentScore", data.dateOfContractSigning);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1010PercentScore", data.dateOfContractSigningPerProcPlan);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1011PercentScore", data.contractualCompletionDate);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1012PercentScore", data.actualCompletionDate);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1013PercentScore", data.dateOfCertificationOfInvoice);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1014PercentScore", data.dateInvoicePaid);

        }

        private void populateAnalysisCost(ApiPmAuditProcessToolAnalysisCost data)
        {
            if(data == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect131PercentScore", data.procPlanTotalCostValue);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect132PercentScore", data.accountingOfficerMarketPrice);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder1Name", data.bestBidder1Name);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder1Price", data.bestBidder1Price);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder2Name", data.bestBidder2Name);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder2Price", data.bestBidder2Price);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder3Name", data.bestBidder3Name);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder3Price", data.bestBidder3Price);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder4Name", data.bestBidder4Name);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder4Price", data.bestBidder4Price);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder5Name", data.bestBidder5Name);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder5Price", data.bestBidder5Price);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder6Name", data.bestBidder6Name);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder6Price", data.bestBidder6Price);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder7Name", data.bestBidder7Name);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBiddder7Price", data.bestBidder7Price);

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect134PercentScore", data.totalContractAwardPrice);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect135PercentScore", data.finalContractCostOnCompletion);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect136PercentScore", data.costOverunAmount);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect137PercentScore", data.budgetVarianceAmount);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect138PercentScore", data.budgetVariancePercentage);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect139PercentScore", data.costOVerunPercentage);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1310PercentScore", data.planRatio);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect1311PercentScore", data.costRatio);

        }

        private void populateAnalysisCompletion(ApiPmAuditProcessToolAnalysisCompletion data)
        {

            if(data == null)
            {
                return;
            } 

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect121PercentScore", data.numberOfBiddersInvited);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect122PercentScore", data.numberOfBidsReceived);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect123PercentScore", data.numberOfBidsPassedPreliminaryExam);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect124PercentScore", data.numberOfTechnicallyResponsiveBids);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect125PercentScore", data.bidSubmissionRate);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect126PercentScore", data.bidResponsiveRate);

        }

        private void populateAnalysisCompliance(ApiPmAuditProcessToolAnalysisCompliance data)
        {

            if(data == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect91YesScore", data.proPlanningAndInitiationYesScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect91NoScore", data.proPlanningAndInitiationNoScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect91TotalScore", data.proPlanningAndInitiationPercentScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect91PercentScore", data.proPlanningAndInitiationPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect92YesScore", data.biddingDocumentYesScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect92NoScore", data.biddingDocumentNoScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect92TotalScore", data.biddingDocumentTotalScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect92PercentScore",data.biddingDocumentPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect93YesScore", data.biddingYesScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect93NoScore", data.biddingNoScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect93TotalScore", data.biddingTotalScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect93PercentScore", data.biddingPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect94YesScore", data.bidEvaluationYesScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect94NoScore", data.bidEvaluationNoScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect94TotalScore", data.bidEvaluationTotalScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect94PercentScore", data.bidEvaluationPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect95YesScore", data.procContractingYesScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect95NoScore", data.procContractingNoScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect95TotalScore", data.procContractingTotalScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect95PercentScore", data.procContractingPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect96YesScore", data.contractManagementYesScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect96NoScore", data.contractManagementNoScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect96TotalScore", data.contractManagementTotalScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect96PercentScore", data.contractManagementPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect97YesScore", data.recordsKeepingYesScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect97NoScore", data.recordsKeepingNoScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect97TotalScore", data.recordsKeepingTotalScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect97PercentScore", data.recordsKeepingPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "Sect98PercentScore", data.procProcessComplianceLevel);

        }

        private void populateCompAndPerfScore(ApiPmAuditProcessToolCompAndPerfScore compAndPerfScore)
        {
            
            if(compAndPerfScore == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAptComplianceLevel", compAndPerfScore.procurementProcessComplianceLevel.ToString());
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAptProcureRatio", compAndPerfScore.procureRatio.ToString());
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAptCompletionRatio", compAndPerfScore.completionRatio.ToString());
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAptPaymentRatio", compAndPerfScore.paymentRatio.ToString());
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAptNoOfBids", compAndPerfScore.numberOfBidsReceived.ToString());
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAptBidSubmissionRate", compAndPerfScore.bidSubmissionRatio.ToString());
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAptBidResponsiveRate", compAndPerfScore.bidResponsiveRate.ToString());
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAptPlanRatio", compAndPerfScore.planRatio.ToString());
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAptCostRatio", compAndPerfScore.costRatio.ToString());

        }

        private void populateDynamicSections(List<ApiPmAuditProcessToolSection> sections)
        {

            //foreach section
            //find the corresponding state section
            //iterate through as you match the item emis ID
            
            foreach(var section in sections)
            {

                foreach(var stateSection in AppStateAuditTools.auditProcessToolSectionEvaluations)
                {

                    if(stateSection.sectionEmisId == section.sectionEmisId)
                    {
                        populateCurrentSectionValues(section, stateSection);
                        break;
                    }

                }
                
                var total = "YES SCORE " + section.yesTotal.ToString() + ", NO SCORE " + section.noTotal.ToString();
                var totalField = APT_SECTION_TOTAL_TXVS[section.sectionEmisId];
                if(totalField != null) { totalField.Text = total; }
                
            }
            
        }

        private void populateCurrentSectionValues(ApiPmAuditProcessToolSection section, SectionEvaluation stateSection)
        {

            var sectionItems = section.items;
            var stateSectionItems = stateSection.sectionItemEvaluations;

            //we declare a new dictionary to hold the updated items that have the values
            Dictionary<long, SectionItemEvaluation> stateSectionItemsWithValues = new Dictionary<long, SectionItemEvaluation>();

            foreach(var item in sectionItems)
            {

                if (stateSectionItems.ContainsKey(item.itemEmisId))
                {

                    var stateItem = stateSectionItems[item.itemEmisId];
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.evaluationFieldName, item.evaluationFieldValue);
                    //WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.exceptionFieldName, item.exception);
                    //WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.findingFieldName, item.finding);
                    //swap
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.exceptionFieldName, item.finding);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.findingFieldName, item.exception);
                    WidgetHandler.findAndFillComboBoxValByCtlName(this, stateItem.managementLetterSectionFieldName, item.managementLetterSectionId);
                    WidgetHandler.findAndFillComboBoxValByCtlName(this, stateItem.managementLetterSectionItemFieldName, item.managementLetterSectionItemId);

                    //we now need to update this item with the values
                    stateItem.evaluationFieldValue = item.evaluationFieldValue;
                    stateItem.findingFieldValue = item.finding;
                    stateItem.exceptionFieldValue = item.exception;                   
                    stateItem.managementLetterSectionFieldValue = item.managementLetterSectionId;
                    stateItem.managementLetterSectionItemFieldValue = item.managementLetterSectionItemId;
                    stateItem.description = item.description;

                    stateSectionItemsWithValues[item.itemEmisId] = stateItem;

                }

            }

            //update the state section
            stateSection.sectionItemEvaluations = stateSectionItemsWithValues;
            appState.updateAptSectionInAppState(stateSection);

        }

        private void populateIdentificationSection(ApiPmAuditProcessToolIdentification ident)
        {

            if(ident == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvSec1EntityName", ident.nameOfEntity);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvSect1EntitySector", ident.sector);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvSec1AuditPeriod", ident.auditPeriod);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvSec1ContractDesc", ident.contractDesc);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvSec1NameOfProvider", ident.nameOfProvider);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvSec1ReferenceNo", ident.referenceNo);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvSec1TotalContractValue", ident.totalContractValue);

            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxSect1Provider", ident.typeOfProvider);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxSect1ProcessStage", ident.processStage);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxSect1ProcureMethod", ident.procurementMethod);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxSect1ConsultantSelectioMtd", ident.consultantSelectionMethod);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxSect1ProcureCategory", ident.procurementCategory);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxSect1BidInviteMethd", ident.bidderInvivationMethod);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxSect1BidSubmissionMtd", ident.bidSubmissionMethod);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxSect1ContractType", ident.contractType);

        }

        public void calculateProcurementComplianceLevel(object sender, EventArgs e)
        {

            new CalculationHelper().aptProcurementProcessComplianceLevel(this);

        }

        public void loadExceptionsBasedOnExceptionSectionSelected(object sender, EventArgs e)
        {

            ComboBox cbx = (ComboBox)sender;
            String fieldName = cbx.Name;
            int exceptionSectionId = cbx != null && cbx.SelectedValue != null ? (int)cbx.SelectedValue : 0;
            if(exceptionSectionId == 0)
            {
                return;
            }

            var exceptions = new DatabaseHandler().getManagementLetterSectionExceptions(DatabaseHandler.dbConnection(), exceptionSectionId);

            foreach(var section in AppStateAuditTools.auditProcessToolSectionEvaluations)
            {

                var items = section.sectionItemEvaluations;

                foreach (KeyValuePair<long, SectionItemEvaluation> entry in section.sectionItemEvaluations)
                {

                    var item = entry.Value;

                    //we found the matching colum
                    if (item.managementLetterSectionFieldName == fieldName)
                    {

                        String exceptionField = item.managementLetterSectionItemFieldName;
                        var cbxExceptions = WidgetHandler.findControlByName(this,exceptionField);
                        if(cbxExceptions != null && cbxExceptions is ComboBox)
                        {
                            var comboxBox = (ComboBox)cbxExceptions;
                            WidgetHandler.populateManagementLetterSectionExceptionsDropdown(comboxBox, exceptions);
                        }
                        return;

                    }

                }

            }

        }


    }



}

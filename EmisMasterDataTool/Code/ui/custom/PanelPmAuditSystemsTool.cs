﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Processor.Entities.PM.systems;
using Processor.ControlObjects;
using Processor.Entities.api;
using EmisMasterDataTool.Code;

namespace EmisTool.Code.ui.custom
{
    public class PanelPmAuditSystemsTool : Panel, IAstAnalyisUpdater
    {         

        //holds the app state
        private AppStateAuditTools appState;

        //controls for total fields of dynamically created sections for easy access
        public static Dictionary<long, Control> AST_SECTION_TOTAL_TXVS = new Dictionary<long, Control>();

        private int activityId;
        private bool inEditMode = false;
        private String toolReferenceInEditMode = "";
        

        public PanelPmAuditSystemsTool(int activityId, ApiPmAuditSystemsTool toolToEdit = null)
        {

            this.activityId = activityId;

            //clear the dictionary to prevent duplicate keys
            AST_SECTION_TOTAL_TXVS.Clear();

            //set the app state
            appState = new AppStateAuditTools();
            appState.iAstAnalyisUpdater = this;
                        
            //initialize the controls
            initializeControls();

            //load data
            loadData(toolToEdit);

        }

        private void loadData(ApiPmAuditSystemsTool toolToEdit)
        {

            if(toolToEdit != null)
            {                 
                populateTool(toolToEdit);
                return;
            }
            
            //it's a new tool creation
            loadActivityGeneralDetails(activityId);

        }

        private void loadActivityGeneralDetails(int activityId)
        {

            DatabaseHandler dbHandler = new DatabaseHandler();

            var activity = dbHandler.findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityId);

            if (activity == null)
            {
                WidgetHandler.showMessage("Failed to find PM activity with ID [" + activityId + "]");
                return;
            }

            var entity = dbHandler.findEntityByEmisId(DatabaseHandler.dbConnection(), activity.entityId);

            var ident = new ApiPmAuditSystemsToolIndentification();
            ident.town = entity != null ? entity.physicalAddress : "";
            ident.nameOfEntity = activity.entityName;
            ident.sector = "";
            ident.auditPeriod = "";

            populateIdentificationSection(ident);

        }

        private void initializeControls()
        {

            this.Name = "panelPmAuditSystemTool";
            this.Padding = new Padding(10);              
                                
            
            //tab control
            TabControl tabControlPmAuditSystemTool = new TabControl();
            tabControlPmAuditSystemTool.Name = "tabControlPmAuditSystemTool";
            tabControlPmAuditSystemTool.Dock = DockStyle.None;
            tabControlPmAuditSystemTool.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            //tab pages for the tab control
            TabPage tabPageSystemsFindingsWorksheet = GetTabPageSystemsFindingsWorksheet();
            TabPage tabPageSystemsAnalysis = GetTabPageSystemsAnalysis();
            TabPage tabPageGraph = GetTabPageGraph();

            //add tab pages to tab control
            tabControlPmAuditSystemTool.TabPages.Add(tabPageSystemsFindingsWorksheet);
            tabControlPmAuditSystemTool.TabPages.Add(tabPageSystemsAnalysis);
            tabControlPmAuditSystemTool.TabPages.Add(tabPageGraph);                      

            //button section           
            Panel panelButtonContainer = getButtonContainerPanel();

            //this is just to hold the to controls, 1 for the buttons and the other for the tab control
            TableLayoutPanel tablePaneMainContainer = new TableLayoutPanel();
            tablePaneMainContainer.Dock = DockStyle.Fill;
            tablePaneMainContainer.ColumnCount = 1;
            tablePaneMainContainer.RowCount = 2;
            tablePaneMainContainer.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

            //add the tab control to the main container spanning 90% of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 90));
            tablePaneMainContainer.Controls.Add(tabControlPmAuditSystemTool, 0, 0);
            // add the buttons panel to the main container spanning 10 % of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 10));
            tablePaneMainContainer.Controls.Add(panelButtonContainer, 0, 1);


            //add container table to panel
            this.Controls.Add(tablePaneMainContainer);

        }

        private Panel getButtonContainerPanel()
        {

            Panel panelButtonContainer = new Panel();
            panelButtonContainer.Height = 40;
            panelButtonContainer.Dock = DockStyle.Fill;

            Button btnSaveTool = new Button() { Text = "Save Tool", Name = "btnSaveTool", Width = 150, Anchor = AnchorStyles.Bottom | AnchorStyles.Left, Margin = new Padding(0) };
            btnSaveTool.Click += btnSaveToolClicked;
            panelButtonContainer.Controls.Add(btnSaveTool);

            TableLayoutPanel tabPanel = new TableLayoutPanel() { Anchor = AnchorStyles.Bottom | AnchorStyles.Right, Margin = new Padding(0) };
            tabPanel.ColumnCount = 3;
            tabPanel.RowCount = 1;
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 35));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40));
            tabPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 35));
            tabPanel.Width = 200;
            tabPanel.Height = 38;

            Label labelLoadTool = new Label() { Text = "Tool Id", Dock = DockStyle.Fill, TextAlign = System.Drawing.ContentAlignment.MiddleRight };
            TextBox txvLoadTool = new TextBox() { Name = "txvLoadTool", Width = 100 };
            Button btnLoadTool = new Button() { Name = "btnLoadTool", Text = "Load Tool" };
            btnLoadTool.Click += btnLoadToolClicked;

            tabPanel.Controls.Add(labelLoadTool, 0, 0);
            tabPanel.Controls.Add(txvLoadTool, 1, 0);
            tabPanel.Controls.Add(btnLoadTool, 2, 0);

           // panelButtonContainer.Controls.Add(tabPanel);
            return panelButtonContainer;

        }

        private void btnLoadToolClicked(object sender, EventArgs e)
        {

            String toolId = WidgetHandler.findCtlValByCtlName(this, "txvLoadTool");

            //check if it's a valid ID
            long lToolId = 0;
            if (!long.TryParse(toolId, out lToolId))
            {
                WidgetHandler.showMessage("Tool ID must be a number", true);
                return;
            }

            //get the tool
            var dbConn = DatabaseHandler.dbConnection();
            var tool = new DatabaseHandler().getPmAuditSystemsToolByIdOrToolReference(dbConn, 0, lToolId);

            //tool not found
            if (tool == null)
            {
                WidgetHandler.showMessage("Tool with ID[" + toolId + "] not found", true);
                return;
            }

            populateTool(tool);

        }

        private void populateTool(ApiPmAuditSystemsTool tool)
        {

            //
            //set status as edit, set the tool reference to edit, make the save button show update
            //
            inEditMode = true;
            toolReferenceInEditMode = tool.toolReference;
            Control btnSaveTool = WidgetHandler.findControlByName(this, "btnSaveTool");
            if(btnSaveTool != null) { btnSaveTool.Text = "Update Tool"; }

            //fill identification
            populateIdentificationSection(tool.identification);

            //fill analysis
            populateAnalysis(tool.analysis);

            //fill dynamic sections
            populateDynamicSections(tool.sections);

            //fill findings
            populateFindings(tool.findings);
            

        }

        private void populateFindings(ApiPmAuditSystemsToolFindingConclusionsAndRecommendations findings)
        {

            if (findings == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAoPositive", findings.accountingOfficerPositiveFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAoNegative", findings.accountingOfficerNegativeFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAoRecom", findings.accountingOfficerRecommendations);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPduPositive", findings.pduPositiveFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPduNegative", findings.pduNegativeFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPduRecom", findings.pduRecommendations);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvContractsCommittePositive", findings.contractsCommitteePositiveFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvContractsCommitteNegative", findings.contractsCommitteeNegativeFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvContractsCommitteRecom", findings.contractsCommitteeRecommendations);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvUserDeptPositive", findings.userDeptPositiveFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvUserDeptNegative", findings.userDeptNegativeFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvUserDeptRecom", findings.userDeptRecommendations);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvEvalCommittePositive", findings.evalCommitteePositiveFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvEvalCommitteNegative", findings.evalCommitteeNegativeFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvEvalCommitteRecom", findings.evalCommitteeRecommendations);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvInternalControlsPositive", findings.internalControlsPositiveFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvInternalControlsNegative", findings.internalControlsNegativeFindings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvInternalControlsRecom", findings.internalControlsRecommendations);

        }

        private void populateDynamicSections(List<ApiPmAuditSystemsToolSection> sections)
        {

            //foreach section
            //find the corresponding state section
            //iterate through as you match the item emis ID

            foreach (var section in sections)
            {

                foreach (var stateSection in AppStateAuditTools.auditSystemsToolSectionEvaluations)
                {

                    if (stateSection.sectionEmisId == section.sectionEmisId)
                    {
                        populateCurrentSectionValues(section, stateSection);
                        break;
                    }

                }

                var total = "YES SCORE " + section.yesTotal.ToString() + ", NO SCORE " + section.noTotal.ToString();
                var totalField = AST_SECTION_TOTAL_TXVS[section.sectionEmisId];
                if (totalField != null) { totalField.Text = total; }

            }

        }

        private void populateCurrentSectionValues(ApiPmAuditSystemsToolSection section, SectionEvaluationAst stateSection)
        {

            var sectionItems = section.items;
            var stateSectionItems = stateSection.sectionItemEvaluations;

            //we declare a new dictionary to hold the updated items that have the values
            Dictionary<long, SectionItemEvaluationAst> stateSectionItemsWithValues = new Dictionary<long, SectionItemEvaluationAst>();

            foreach (var item in sectionItems)
            {

                if (stateSectionItems.ContainsKey(item.itemEmisId))
                {
                    var stateItem = stateSectionItems[item.itemEmisId];
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.evaluationFieldName, item.evaluationFieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.exceptionFieldName, item.exception);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.findingFieldName, item.finding);
                    WidgetHandler.findAndFillComboBoxValByCtlName(this, stateItem.managementLetterSectionFieldName, item.managementLetterSectionId);
                    WidgetHandler.findAndFillComboBoxValByCtlName(this, stateItem.managementLetterSectionItemFieldName, item.managementLetterSectionItemId);
                    //we now need to update this item with the values
                    stateItem.evaluationFieldValue = item.evaluationFieldValue;
                    stateItem.findingFieldValue = item.finding;
                    stateItem.exceptionFieldValue = item.exception;
                    stateItem.managementLetterSectionFieldValue = item.managementLetterSectionId;
                    stateItem.managementLetterSectionItemFieldValue = item.managementLetterSectionItemId;
                    stateItem.description = item.description;

                    stateSectionItemsWithValues[item.itemEmisId] = stateItem;

                }

            }

            //update the state section
            stateSection.sectionItemEvaluations = stateSectionItemsWithValues;
            appState.updateAstSectionInAppState(stateSection);

        }

        private void populateAnalysis(ApiPmAuditSystemsToolAnalysis analysis)
        {

            if (analysis == null)
            {
                return;
            }
            
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvYesTotalAccountOfficer", analysis.accountingOfficerYesTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNoTotalAccountOfficer", analysis.accountingOfficerNoTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalAccountOfficer", analysis.accountingOfficerTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPercentAccountOfficer", analysis.accountingOfficerPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvYesTotalPDU", analysis.pduYesTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNoTotalPDU", analysis.pduNoTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalPDU", analysis.pduTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPercentPDU", analysis.pduPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvYesTotalContractsCommittee", analysis.contractsCommitteYesTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNoTotalContractsCommittee", analysis.contractsCommitteNoTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalContractsCommittee", analysis.contractsCommitteTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPercentContractsCommittee", analysis.contractsCommittePercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvYesTotalUseDept", analysis.userDeptsYesTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNoTotalUseDept", analysis.userDeptsNoTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalUseDept", analysis.userDeptsTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPercentUseDept", analysis.userDeptsPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvYesTotalEvalCommitte", analysis.evalCommitteeYesTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNoTotalEvalCommitte", analysis.evalCommitteeNoTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalEvalCommitte", analysis.evalCommitteeTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPercentEvalCommitte", analysis.evalCommitteePercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvYesTotalInternalControls", analysis.internalControlsYesTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNoTotalInternalControls", analysis.internalControlsNoTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalInternalControls", analysis.internalControlsTotal);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPercentInternalControls", analysis.internalControlsPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPercentSystemsComplainceLevel", analysis.complianceLevelPercentScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalProcBudget", analysis.totalProcBudget);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalActualProc", analysis.totalActualProcs);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvProcBudgetAbsorptioRate", analysis.procBudgetAbsorptionRate);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalValueOfProcs", analysis.totalValueOfProcs);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalValueOfOpenBidContracts", analysis.totalvalueOfOpenBidContracts);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvProportionOfContractsOnOpenBi", analysis.proportionValueOfContractsOnOpenBid);

        }

        private void populateIdentificationSection(ApiPmAuditSystemsToolIndentification ident)
        {

            if (ident == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAstNameOfEntity", ident.nameOfEntity);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAstLocationOfEntity", ident.town);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAstSector", ident.sector);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAstAuditPeriod", ident.auditPeriod);

        }

        private TabPage GetTabPageGraph()
        {
            
            Panel panelContainer = new Panel();
            panelContainer.Dock = DockStyle.Fill;

            TabPage tabPageGraph = new TabPage("Graph");           
            tabPageGraph.Controls.Add(panelContainer);

            return tabPageGraph;

        }

        private TabPage GetTabPageSystemsAnalysis()
        {

            Panel panelContainer = new Panel();
            panelContainer.Dock = DockStyle.Fill;

            //get the analyis tab content table
            var tableAnalysis = UiHelperAst.GenerateAnalysisTabTable(calculateAverageComplianceLevel, calculateProcurementBudgetAbsorptionRate, calculateProportionValueOfContractsOnOpenBidding);
            tableAnalysis.Padding = new Padding(20);

            //add table to container panel
            panelContainer.Controls.Add(tableAnalysis);

            TabPage tabPageDisposalAnalysis = new TabPage("Systems Analysis");            
            tabPageDisposalAnalysis.Controls.Add(panelContainer);

            return tabPageDisposalAnalysis;

        }

        private TabPage GetTabPageSystemsFindingsWorksheet()
        {

            TabPage tabPageSystemsFindingsWorksheet = new TabPage("Systems Findings");
            tabPageSystemsFindingsWorksheet.Name = "tabPageSystemsFindingsWorksheet";

            //get the panel with the accordion list
            Panel panelAccordionContainer = GetPanelAccordionContainer();

            //add panel containing the accordion to the page
            tabPageSystemsFindingsWorksheet.Controls.Add(panelAccordionContainer);

            return tabPageSystemsFindingsWorksheet;

        }

        private Panel GetPanelAccordionContainer()
        {

            Panel panelAccordionContainer = new Panel();
            panelAccordionContainer.Name = "panelAccordionContainer";
            panelAccordionContainer.Dock = DockStyle.Fill;

            panelAccordionContainer.Padding = new Padding(10);

            //get accordion panel with the different sections
            AccordionPanelAuditSystems accordionPanelAuditSystems = new AccordionPanelAuditSystems(appState);
            accordionPanelAuditSystems.Name = "accordionPanelAuditSystems";

            //add accordion panel to the container
            panelAccordionContainer.Controls.Add(accordionPanelAuditSystems);

            return panelAccordionContainer;

        }

        private void btnSaveToolClicked(object sender, EventArgs e)
        {

            if (!inEditMode)
            {
                saveTool();
                return;
            }
            else
            {
                updateTool();                
            }            

        }

        private void updateTool()
        {

            //check if the tool name probably changed
            String entityName = WidgetHandler.findCtlValByCtlName(this, "txvAstNameOfEntity");
            String auditPeriod = WidgetHandler.findCtlValByCtlName(this, "txvAstAuditPeriod");

            String newToolName = SharedCommons.getToolName(AppConstants.PREFIX_AST, entityName, auditPeriod, activityId);

            //tool Reference might have changed
            if (!apiDataUpdateToolReference(newToolName, out String message))
            {
                WidgetHandler.showMessage(message);
                return;
            }

            //clear existing data
            new DatabaseHandler().clearAuditSystemToolOldData(DatabaseHandler.dbConnection(),toolReferenceInEditMode);

            //save new tool data
            saveToolData(entityName, auditPeriod, newToolName);

        }
                

        private bool apiDataUpdateToolReference(string newToolName, out String message)
        {

            DatabaseHandler db = new DatabaseHandler();
            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();

            if (toolReferenceInEditMode != newToolName && db.auditSystemsToolNameExists(sQLiteConnection, newToolName))
            {
                message = "Tool with name [" + newToolName + "] already exists.";
                return false;
            }

            var tool = db.getPmAuditSystemsToolByIdOrToolReference(sQLiteConnection, activityId);
            if(tool == null)
            {
                message = "Tool for activity ID [" + activityId + "] not found.";
                return false;
            }

            tool.toolName = newToolName;
            tool.toolReference = newToolName;
            tool.activityId = activityId;
            tool.createdBy = getAuditorName();
            tool.toolType = AppConstants.PREFIX_AST;


            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.createdOn = dateTimeNow;
            tool.updatedOn = dateTimeNow;

            db.updatePmAst(sQLiteConnection, tool);

            message = "SUCCESS";
            return true;

        }

        private void saveTool()
        {

            String entityName = WidgetHandler.findCtlValByCtlName(this, "txvAstNameOfEntity");
            String auditPeriod = WidgetHandler.findCtlValByCtlName(this, "txvAstAuditPeriod");
            string createdBy = getAuditorName();

            //check if the required required fields are supplied
            if (!validPmAuditSystemsToolData(entityName, auditPeriod, createdBy))
            {
                return;
            }

            string toolName = SharedCommons.getToolName(AppConstants.PREFIX_AST, entityName, auditPeriod, activityId);
            String toolRef = toolName;

            //Save the tool header
            String message = "";
            if (!apiDataSavePmAuditSystemsTool(toolName, toolRef, activityId, out message,createdBy))
            {
                WidgetHandler.showMessage(message, true);
                return;
            }

            saveToolData(entityName, auditPeriod, toolRef);

        }

        private void saveToolData(string entityName, string auditPeriod, string toolRef)
        {

            //save the tool identification data
            apiDataSavePmAuditSystemsToolIdentification(entityName, auditPeriod, toolRef);


            //save the sections and section items
            apiDataSavePmAuditSystemsToolDynamicSectionsData(toolRef);


            //save the key findings, conclusions and recommendations
            apiDataSaveAuditSystemsToolFindingConclusionsAndRecoms(toolRef);


            //save the analysis
            apiDataSaveAuditSystemsToolAnalysis(toolRef);

            //everything has been successfully saved
            WidgetHandler.showMessage("Tool successfully saved with reference [" + toolRef + "]", false);

        }

        private bool validPmAuditSystemsToolData(string entityName, string auditPeriod, string createdBy)
        {

            if (String.IsNullOrEmpty(entityName))
            {
                WidgetHandler.showMessage("Please enter the entity name", true);
                return false;
            }

            if (String.IsNullOrEmpty(auditPeriod))
            {
                WidgetHandler.showMessage("Please enter the audit period", true);
                return false;
            }

            if (String.IsNullOrEmpty(createdBy))
            {
                WidgetHandler.showMessage("Please select Auditor", true);
                return false;
            }

            return true;

        }

        private static bool apiDataSavePmAuditSystemsTool(
            string toolName, string toolRef, int activityId, out String message, string createdBy)
        {

            DatabaseHandler db = new DatabaseHandler();
            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();

            if (db.auditSystemsToolNameExists(sQLiteConnection, toolName))
            {
                message = "Tool with name [" + toolName + "] already exists.";
                return false;
            }

            ApiPmAuditSystemsTool tool = new ApiPmAuditSystemsTool();
            tool.toolName = toolName;
            tool.toolReference = toolRef;
            tool.activityId = activityId;
            tool.createdBy = createdBy;
            tool.toolType = AppConstants.PREFIX_AST;

            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.createdOn = dateTimeNow;
            tool.updatedOn = dateTimeNow;

            db.savePmAst(sQLiteConnection, tool);

            message = "SUCCESS";
            return true;

        }

        private void apiDataSavePmAuditSystemsToolIdentification(string entityName, string auditPeriod, string toolRef)
        {

            var toolIdentification = new ApiPmAuditSystemsToolIndentification();
            toolIdentification.toolReference = toolRef;

            toolIdentification.nameOfEntity = entityName;
            toolIdentification.auditPeriod = auditPeriod;

            Control parent = this;

            Control ctlSector = WidgetHandler.findControlByName(parent, "txvAstSector");
            String sector = ctlSector == null ? "" : ctlSector.Text;
            toolIdentification.sector = sector;

            Control ctlTown = WidgetHandler.findControlByName(parent, "txvAstLocationOfEntity");
            String town = ctlTown == null ? "" : ctlTown.Text;
            toolIdentification.town = town;

            new DatabaseHandler().saveApiPmAuditSystemsToolIdentification(DatabaseHandler.dbConnection(), toolIdentification);
        }

        private void apiDataSaveAuditSystemsToolFindingConclusionsAndRecoms(string toolRef)
        {

            var data = new ApiPmAuditSystemsToolFindingConclusionsAndRecommendations();
            data.toolReference = toolRef;

            Control parent = this;

            String aoPositive = WidgetHandler.findControlByName(parent, "txvAoPositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvAoPositive").Text;
            String aoNegative = WidgetHandler.findControlByName(parent, "txvAoNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvAoNegative").Text;
            String aoRecom = WidgetHandler.findControlByName(parent, "txvAoRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvAoRecom").Text;
            data.accountingOfficerPositiveFindings = aoPositive;
            data.accountingOfficerNegativeFindings = aoNegative;
            data.accountingOfficerRecommendations = aoRecom;

            String pduPositive = WidgetHandler.findControlByName(parent, "txvPduPositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvPduPositive").Text;
            String pduNegative = WidgetHandler.findControlByName(parent, "txvPduNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvPduNegative").Text;
            String pduRecom = WidgetHandler.findControlByName(parent, "txvPduRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvPduRecom").Text;
            data.pduPositiveFindings = pduPositive;
            data.pduNegativeFindings = pduNegative;
            data.pduRecommendations = pduRecom;

            String contractsCommittePositive = WidgetHandler.findControlByName(parent, "txvContractsCommittePositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvContractsCommittePositive").Text;
            String contractsCommitteNegative = WidgetHandler.findControlByName(parent, "txvContractsCommitteNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvContractsCommitteNegative").Text;
            String contractsCommitteRecom = WidgetHandler.findControlByName(parent, "txvContractsCommitteRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvContractsCommitteRecom").Text;
            data.contractsCommitteePositiveFindings = contractsCommittePositive;
            data.contractsCommitteeNegativeFindings = contractsCommitteNegative;
            data.contractsCommitteeRecommendations = contractsCommitteRecom;

            String userDeptPositive = WidgetHandler.findControlByName(parent, "txvUserDeptPositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvUserDeptPositive").Text;
            String userDeptNegative = WidgetHandler.findControlByName(parent, "txvUserDeptNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvUserDeptNegative").Text;
            String userDeptRecom = WidgetHandler.findControlByName(parent, "txvUserDeptRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvUserDeptRecom").Text;
            data.userDeptPositiveFindings = userDeptPositive;
            data.userDeptNegativeFindings = userDeptNegative;
            data.userDeptRecommendations = userDeptRecom;

            String evalCommittePositive = WidgetHandler.findControlByName(parent, "txvEvalCommittePositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvEvalCommittePositive").Text;
            String evalCommitteNegative = WidgetHandler.findControlByName(parent, "txvEvalCommitteNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvEvalCommitteNegative").Text;
            String evalCommitteRecom = WidgetHandler.findControlByName(parent, "txvEvalCommitteRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvEvalCommitteRecom").Text;
            data.evalCommitteePositiveFindings = evalCommittePositive;
            data.evalCommitteeNegativeFindings = evalCommitteNegative;
            data.evalCommitteeRecommendations = evalCommitteRecom;

            String internalControlsPositive = WidgetHandler.findControlByName(parent, "txvInternalControlsPositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvInternalControlsPositive").Text;
            String internalControlsNegative = WidgetHandler.findControlByName(parent, "txvInternalControlsNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvInternalControlsNegative").Text;
            String internalControlsRecom = WidgetHandler.findControlByName(parent, "txvInternalControlsRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvInternalControlsRecom").Text;
            data.internalControlsPositiveFindings = internalControlsPositive;
            data.internalControlsNegativeFindings = internalControlsNegative;
            data.internalControlsRecommendations = internalControlsRecom;

            String procPlanPositive = WidgetHandler.findControlByName(parent, "txvProcPlanPositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvProcPlanPositive").Text;
            String procPlanNegative = WidgetHandler.findControlByName(parent, "txvProcPlanNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvProcPlanNegative").Text;
            String procPlanRecom = WidgetHandler.findControlByName(parent, "txvProcPlanRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvProcPlanRecom").Text;
            data.procPlanningPositiveFindings = procPlanPositive;
            data.procPlanningNegativeFindings = procPlanPositive;
            data.procPlanningRecommendations = procPlanRecom;

            String recordsAndReportsPositive = WidgetHandler.findControlByName(parent, "txvRecordsAndReportsPositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvRecordsAndReportsPositive").Text;
            String recordsAndReportsNegative = WidgetHandler.findControlByName(parent, "txvRecordsAndReportsNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvRecordsAndReportsNegative").Text;
            String recordsAndReportsRecom = WidgetHandler.findControlByName(parent, "txvRecordsAndReportsRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvRecordsAndReportsRecom").Text;
            data.recordsReportsPositiveFindings = recordsAndReportsPositive;
            data.recordsReportsNegativeFindings = recordsAndReportsNegative;
            data.recordsReportsRecommendations = recordsAndReportsRecom;

            String compliancePositive = WidgetHandler.findControlByName(parent, "txvCompliancePositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvCompliancePositive").Text;
            String complianceNegative = WidgetHandler.findControlByName(parent, "txvComplianceNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvComplianceNegative").Text;
            String complianceRecom = WidgetHandler.findControlByName(parent, "txvComplianceRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvComplianceRecom").Text;
            data.complainceScorePositiveFindings = compliancePositive;
            data.complainceScoreNegativeFindings = complianceNegative;
            data.complainceScoreRecommendations = complianceRecom;

            String budgetAbsorptionPositive = WidgetHandler.findControlByName(parent, "txvBudgetAbsorptionPositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvBudgetAbsorptionPositive").Text;
            String budgetAbsorptionNegative = WidgetHandler.findControlByName(parent, "txvBudgetAbsorptionNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvBudgetAbsorptionNegative").Text;
            String budgetAbsorptionRecom = WidgetHandler.findControlByName(parent, "txvBudgetAbsorptionRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvBudgetAbsorptionRecom").Text;
            data.budgetAbsorptionPositiveFindings = budgetAbsorptionPositive;
            data.budgetAbsorptionNegativeFindings = budgetAbsorptionNegative;
            data.budgetAbsorptionRecommendations = budgetAbsorptionRecom;

            String openBiddingValuePositive = WidgetHandler.findControlByName(parent, "txvOpenBiddingValuePositive") == null ? "" : WidgetHandler.findControlByName(parent, "txvOpenBiddingValuePositive").Text;
            String openBiddingValueNegative = WidgetHandler.findControlByName(parent, "txvOpenBiddingValueNegative") == null ? "" : WidgetHandler.findControlByName(parent, "txvOpenBiddingValueNegative").Text;
            String openBiddingValueRecom = WidgetHandler.findControlByName(parent, "txvOpenBiddingValueRecom") == null ? "" : WidgetHandler.findControlByName(parent, "txvOpenBiddingValueRecom").Text;
            data.openBiddingvaluePositiveFindings = openBiddingValuePositive;
            data.openBiddingvalueNegativeFindings = openBiddingValueNegative;
            data.openBiddingvalueRecommendations = openBiddingValueRecom;

            DatabaseHandler.saveApiPmAuditSystemsToolFindingsConclusionsAndRecom(DatabaseHandler.dbConnection(), data);

        }

        private void apiDataSaveAuditSystemsToolAnalysis(string toolRef)
        {

            var data = new ApiPmAuditSystemsToolAnalysis();
            data.toolReference = toolRef;

            String YesTotalAccountOfficer = WidgetHandler.findCtlValByCtlName(this, "txvYesTotalAccountOfficer");
            String NoTotalAccountOfficer = WidgetHandler.findCtlValByCtlName(this, "txvNoTotalAccountOfficer");
            String TotalAccountOfficer = WidgetHandler.findCtlValByCtlName(this, "txvTotalAccountOfficer");
            String PercentAccountOfficer = WidgetHandler.findCtlValByCtlName(this, "txvPercentAccountOfficer");
            data.accountingOfficerYesTotal = YesTotalAccountOfficer;
            data.accountingOfficerNoTotal = NoTotalAccountOfficer;
            data.accountingOfficerTotal = TotalAccountOfficer;
            data.accountingOfficerPercentScore = PercentAccountOfficer;

            String YesTotalPDU = WidgetHandler.findCtlValByCtlName(this, "txvYesTotalPDU");
            String NoTotalPDU = WidgetHandler.findCtlValByCtlName(this, "txvNoTotalPDU");
            String TotalPDU = WidgetHandler.findCtlValByCtlName(this, "txvTotalPDU");
            String PercentPDU = WidgetHandler.findCtlValByCtlName(this, "txvPercentPDU");
            data.pduYesTotal = YesTotalPDU;
            data.pduNoTotal = NoTotalPDU;
            data.pduTotal = TotalPDU;
            data.pduPercentScore = PercentPDU;

            String YesTotalContractsCommittee = WidgetHandler.findCtlValByCtlName(this, "txvYesTotalContractsCommittee");
            String NoTotalContractsCommittee = WidgetHandler.findCtlValByCtlName(this, "txvNoTotalContractsCommittee");
            String TotalContractsCommittee = WidgetHandler.findCtlValByCtlName(this, "txvTotalContractsCommittee");
            String PercentContractsCommittee = WidgetHandler.findCtlValByCtlName(this, "txvPercentContractsCommittee");
            data.contractsCommitteYesTotal = YesTotalContractsCommittee;
            data.contractsCommitteNoTotal = NoTotalContractsCommittee;
            data.contractsCommitteTotal = TotalContractsCommittee;
            data.contractsCommittePercentScore = PercentContractsCommittee;

            String YesTotalUseDept = WidgetHandler.findCtlValByCtlName(this, "txvYesTotalUseDept");
            String NoTotalUseDept = WidgetHandler.findCtlValByCtlName(this, "txvNoTotalUseDept");
            String TotalUseDept = WidgetHandler.findCtlValByCtlName(this, "txvTotalUseDept");
            String PercentUseDept = WidgetHandler.findCtlValByCtlName(this, "txvPercentUseDept");
            data.userDeptsYesTotal = YesTotalPDU;
            data.userDeptsNoTotal = NoTotalPDU;
            data.userDeptsTotal = TotalPDU;
            data.userDeptsPercentScore = PercentUseDept;

            String YesTotalEvalCommitte = WidgetHandler.findCtlValByCtlName(this, "txvYesTotalEvalCommitte");
            String NoTotalEvalCommitte = WidgetHandler.findCtlValByCtlName(this, "txvNoTotalEvalCommitte");
            String TotalEvalCommitte = WidgetHandler.findCtlValByCtlName(this, "txvTotalEvalCommitte");
            String PercentEvalCommitte = WidgetHandler.findCtlValByCtlName(this, "txvPercentEvalCommitte");
            data.evalCommitteeYesTotal = YesTotalEvalCommitte;
            data.evalCommitteeNoTotal = NoTotalEvalCommitte;
            data.evalCommitteeTotal = TotalEvalCommitte;
            data.evalCommitteePercentScore = PercentEvalCommitte;

            String YesTotalInternalControls = WidgetHandler.findCtlValByCtlName(this, "txvYesTotalInternalControls");
            String NoTotalInternalControls = WidgetHandler.findCtlValByCtlName(this, "txvNoTotalInternalControls");
            String TotalInternalControls = WidgetHandler.findCtlValByCtlName(this, "txvTotalInternalControls");
            String PercentInternalControls = WidgetHandler.findCtlValByCtlName(this, "txvPercentInternalControls");
            data.internalControlsYesTotal = YesTotalInternalControls;
            data.internalControlsNoTotal = NoTotalInternalControls;
            data.internalControlsTotal = TotalInternalControls;
            data.internalControlsPercentScore = PercentInternalControls;

            String PercentSystemsComplainceLevel = WidgetHandler.findCtlValByCtlName(this, "txvPercentSystemsComplainceLevel");
            data.complianceLevelPercentScore = PercentSystemsComplainceLevel;

            String TotalProcBudget = WidgetHandler.findCtlValByCtlName(this, "txvTotalProcBudget");
            String TotalActualProc = WidgetHandler.findCtlValByCtlName(this, "txvTotalActualProc");
            String ProcBudgetAbsorptioRate = WidgetHandler.findCtlValByCtlName(this, "txvProcBudgetAbsorptioRate");
            data.totalProcBudget = TotalProcBudget;
            data.totalActualProcs = TotalActualProc;
            data.procBudgetAbsorptionRate = ProcBudgetAbsorptioRate;

            String TotalValueOfProcs = WidgetHandler.findCtlValByCtlName(this, "txvTotalValueOfProcs");
            String TotalValueOfOpenBidContracts = WidgetHandler.findCtlValByCtlName(this, "txvTotalValueOfOpenBidContracts");
            String ProportionOfContractsOnOpenBi = WidgetHandler.findCtlValByCtlName(this, "txvProportionOfContractsOnOpenBi");
            data.totalValueOfProcs = TotalValueOfProcs;
            data.totalvalueOfOpenBidContracts = TotalValueOfOpenBidContracts;
            data.proportionValueOfContractsOnOpenBid = ProportionOfContractsOnOpenBi;

            DatabaseHandler.saveApiPmAuditSystemsToolAnalysis(DatabaseHandler.dbConnection(), data);

        }

        private static void apiDataSavePmAuditSystemsToolDynamicSectionsData(string toolRef)
        {

            var sectionData = AppStateAuditTools.auditSystemsToolSectionEvaluations;

            foreach (var section in sectionData)
            {

                var sectionItems = section.sectionItemEvaluations;

                ApiPmAuditSystemsToolSection apiToolSection = new ApiPmAuditSystemsToolSection();
                apiToolSection.sectionEmisId = section.sectionEmisId;
                apiToolSection.toolReference = toolRef;
                apiToolSection.name = section.name;
                apiToolSection.yesTotal = section.sectionYesTotalValue;
                apiToolSection.noTotal = section.sectionNoTotalValue;


                DatabaseHandler.saveApiPmAuditSystemsToolSection(DatabaseHandler.dbConnection(), apiToolSection);

                //after saving the section we proceed to save the section items
                var apiPmAuditSystemsToolSectionItems = new List<ApiPmAuditSystemsToolSectionItem>();

                foreach (KeyValuePair<long, SectionItemEvaluationAst> entry in sectionItems)
                {

                    var item = entry.Value;

                    var apiToolSectionItem = new ApiPmAuditSystemsToolSectionItem();
                    apiToolSectionItem.sectionEmisId = section.sectionEmisId;
                    apiToolSectionItem.toolReference = toolRef;
                    apiToolSectionItem.description = item.description;
                    apiToolSectionItem.itemEmisId = item.sectionItemEmisId;
                    apiToolSectionItem.evaluationFieldRole = item.evaluationFieldRole;
                    apiToolSectionItem.evaluationFieldType = item.evaluationFieldType;
                    apiToolSectionItem.evaluationFieldValue = item.evaluationFieldValue;
                    apiToolSectionItem.finding = item.findingFieldValue;
                    apiToolSectionItem.exception = item.exceptionFieldValue;
                    apiToolSectionItem.yesRank = item.yesRank;
                    apiToolSectionItem.noRank = item.noRank;
                    apiToolSectionItem.naRank = item.naRank;

                    apiToolSectionItem.managementLetterSectionId = item.managementLetterSectionFieldValue;
                    apiToolSectionItem.managementLetterSectionItemId = item.managementLetterSectionItemFieldValue;

                    apiPmAuditSystemsToolSectionItems.Add(apiToolSectionItem);

                }

                //save the list of items to the database
                DatabaseHandler.saveApiPmAuditSystemsToolSectionItem(DatabaseHandler.dbConnection(), apiPmAuditSystemsToolSectionItems);

            }
        }


        public void updateAstSectionScoresInAnalyisTab(SectionEvaluationAst section)
        {

            var sectionName = section.name;
            sectionName = String.IsNullOrEmpty(sectionName) ? sectionName : sectionName.ToUpper();

            var totalValueYes = section.sectionYesTotalValue;
            var totalValueNo = section.sectionNoTotalValue;

            if (sectionName == "ACCOUNTING OFFICER".ToUpper())
            {
                fillPmAstAnaysisSecTotals("txvYesTotalAccountOfficer", totalValueYes, "txvNoTotalAccountOfficer", totalValueNo, "txvTotalAccountOfficer", "txvPercentAccountOfficer");

            }
            else if (sectionName == "PROCUREMENT & DISPOSAL UNIT".ToUpper())
            {
                fillPmAstAnaysisSecTotals("txvYesTotalPDU", totalValueYes, "txvNoTotalPDU", totalValueNo, "txvTotalPDU", "txvPercentPDU");
            }
            else if (sectionName == "CONTRACTS COMMITTEE".ToUpper())
            {
                fillPmAstAnaysisSecTotals("txvYesTotalContractsCommittee", totalValueYes, "txvNoTotalContractsCommittee", totalValueNo, "txvTotalContractsCommittee", "txvPercentContractsCommittee");
            }
            else if (sectionName == "USER DEPARTMENTS".ToUpper())
            {
                fillPmAstAnaysisSecTotals("txvYesTotalUseDept", totalValueYes, "txvNoTotalUseDept", totalValueNo, "txvTotalUseDept", "txvPercentUseDept");
            }
            else if (sectionName == "EVALUATION COMMITTEES".ToUpper())
            {
                fillPmAstAnaysisSecTotals("txvYesTotalEvalCommitte", totalValueYes, "txvNoTotalEvalCommitte", totalValueNo, "txvTotalEvalCommitte", "txvPercentEvalCommitte");
            }
            else if (sectionName == "INTERNAL CONTROLS".ToUpper())
            {
                fillPmAstAnaysisSecTotals("txvYesTotalInternalControls", totalValueYes, "txvNoTotalInternalControls", totalValueNo, "txvTotalInternalControls", "txvPercentInternalControls");
            }

        }

        private void fillPmAstAnaysisSecTotals(String yesTotalFieldName, long totalValueYes, String noTotalFieldName, long totalValueNo, String sumFieldName, String percentFieldName)
        {

            var txvsYes = WidgetHandler.findControlByName(this, yesTotalFieldName);
            if (txvsYes != null)
            {
                txvsYes.Text = totalValueYes.ToString();
            }

            var txvsNo = WidgetHandler.findControlByName(this, noTotalFieldName);
            if (txvsNo != null)
            {
                txvsNo.Text = totalValueNo.ToString();
            }

            //call the total score
            var sum = totalValueYes + totalValueNo;
            var txvSum = WidgetHandler.findControlByName(this, sumFieldName);
            if (txvSum != null)
            {
                txvSum.Text = sum.ToString();
            }

            //calculate the %score
            var percent = (totalValueYes * 100.0) / sum;
            var txvPercent = WidgetHandler.findControlByName(this, percentFieldName);
            if (txvPercent != null)
            {
                txvPercent.Text = Math.Round(percent, 1).ToString();
            }

        }


        public void autofillAstAnalysisTabTextFieldBasedOnEvalItemValue(SectionItemEvaluationAst sectionItem)
        {
            if (sectionItem == null)
            {
                return;
            }

            var itemRole = sectionItem.evaluationFieldRole;
            if (itemRole == null)
            {
                return;
            }

            if (itemRole == "Total procurement budget in the audit period (UGX)")
            {
                fillPmAstAnaysisSecTextFields("txvTotalProcBudget", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Total actual procurements in the audit period (UGX)")
            {
                fillPmAstAnaysisSecTextFields("txvTotalActualProc", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Total value of procurements in the audit period")
            {                
                fillPmAstAnaysisSecTextFields("txvTotalValueOfProcs", sectionItem.evaluationFieldValue);
            }
            else if (itemRole == "Total value of open bidding contracts in the audit period")
            {
                fillPmAstAnaysisSecTextFields("txvTotalValueOfOpenBidContracts", sectionItem.evaluationFieldValue);
            }

        }

        private void fillPmAstAnaysisSecTextFields(String fieldName1, String fieldValue1, String fieldName2 = null, String fieldValue2 = null)
        {

            var txvs = WidgetHandler.findControlByName(this, fieldName1);
            if (txvs != null)
            {
                txvs.Text = fieldValue1;
            }

            if (fieldName2 != null)
            {

                var txvs2 = WidgetHandler.findControlByName(this, fieldName2);
                if (txvs2 != null)
                {
                    txvs2.Text = fieldValue2;
                }

            }

        }


        private void calculateAverageComplianceLevel(object sender, EventArgs e)
        {
            try
            {

                double scoreAO = 0, scorePdu = 0, scoreContractsCommitte = 0, scoreUserDept = 0, scoreEvalCommitte = 0, scoreInternalControls = 0;

                var txvAo = WidgetHandler.findControlByName(this, "txvPercentAccountOfficer");
                if (txvAo != null)
                {
                    String percentAO = txvAo.Text;
                    double.TryParse(percentAO, out scoreAO);
                }

                var txvPDU = WidgetHandler.findControlByName(this, "txvPercentPDU"); 
                if (txvPDU != null)
                {
                    String percentPDU = txvPDU.Text;
                    double.TryParse(percentPDU, out scorePdu);
                }

                var txvsContractsCommittee = WidgetHandler.findControlByName(this, "txvPercentContractsCommittee"); 
                if (txvsContractsCommittee != null)
                {
                    String percentContractsCommittee = txvsContractsCommittee.Text;
                    double.TryParse(percentContractsCommittee, out scoreContractsCommitte);
                }


                var txvsUserDept = WidgetHandler.findControlByName(this, "txvPercentUseDept");
                if (txvsUserDept != null)
                {
                    String percentUserDept = txvAo.Text;
                    double.TryParse(percentUserDept, out scoreUserDept);
                }


                var txvsEvalCommitte = WidgetHandler.findControlByName(this, "txvPercentEvalCommitte");
                if (txvsEvalCommitte != null)
                {
                    String percentEvalCommittee = txvsEvalCommitte.Text;
                    double.TryParse(percentEvalCommittee, out scoreEvalCommitte);
                }

                var txvsInternalControls = WidgetHandler.findControlByName(this, "txvPercentInternalControls");
                if (txvsInternalControls != null)
                {
                    String percentInternalControls = txvsInternalControls.Text;
                    double.TryParse(percentInternalControls, out scoreInternalControls);
                }

                var txvSystemsComplainceLevel = WidgetHandler.findControlByName(this, "txvPercentSystemsComplainceLevel");
                if (txvSystemsComplainceLevel != null)
                {
                    double complianceLevel = ((scoreAO + scoreContractsCommitte + scoreEvalCommitte + scoreInternalControls + scorePdu + scoreUserDept) / 6);
                    txvSystemsComplainceLevel.Text = Math.Round(complianceLevel, 1).ToString();
                }

            }
            catch (Exception ex)
            {

            }

        }

        private void calculateProcurementBudgetAbsorptionRate(object sender, EventArgs e)
        {
            try
            {

                double totalProcBudget = 0, totalActualProcs = 0;

                var txvs1 = WidgetHandler.findControlByName(this, "txvTotalProcBudget"); 
                if (txvs1 != null)
                {
                    String valTotalProcBudegt = txvs1.Text;
                    double.TryParse(valTotalProcBudegt, out totalProcBudget);
                }

                var txvs2 = WidgetHandler.findControlByName(this, "txvTotalActualProc");
                if (txvs2 != null)
                {
                    String valTotalActualProcs = txvs2.Text;
                    double.TryParse(valTotalActualProcs, out totalActualProcs);
                }

                var txvs3 = WidgetHandler.findControlByName(this, "txvProcBudgetAbsorptioRate");
                if (txvs3 != null)
                {
                    var rate = totalActualProcs / totalProcBudget;
                    txvs3.Text = Math.Round(rate, 1).ToString();
                }

            }
            catch (Exception ex)
            {

            }

        }

        private void calculateProportionValueOfContractsOnOpenBidding(object sender, EventArgs e)
        {
            try
            {

                double totalValueOfProcs = 0, totalValOfOpenBiddingContracts = 0;

                var txvs1 = WidgetHandler.findControlByName(this, "txvTotalValueOfProcs"); 
                if (txvs1 != null)
                {
                    String valTotalProcBudegt = txvs1.Text;
                    double.TryParse(valTotalProcBudegt, out totalValueOfProcs);
                }

                var txvs2 = WidgetHandler.findControlByName(this, "txvTotalValueOfOpenBidContracts");
                if (txvs2 != null)
                {
                    String valTotalActualProcs = txvs2.Text;
                    double.TryParse(valTotalActualProcs, out totalValOfOpenBiddingContracts);
                }

                var txvs3 = WidgetHandler.findControlByName(this, "txvProportionOfContractsOnOpenBi");
                if (txvs3 != null)
                {
                    var rate = totalValOfOpenBiddingContracts / totalValueOfProcs;
                    txvs3.Text = Math.Round(rate, 1).ToString();
                }
            }
            catch (Exception ex)
            {

            }

        }

        private string getAuditorName()
        {

            var cbxCreatedBy = (ComboBox)WidgetHandler.findControlByName(this, "cbxAstCreatedBy");
            String createdBy = cbxCreatedBy != null && cbxCreatedBy.SelectedValue != null ? (String)cbxCreatedBy.SelectedValue : "";
            return createdBy;

        }

        public void loadExceptionsBasedOnExceptionSectionSelected(object sender, EventArgs e)
        {

            ComboBox cbx = (ComboBox)sender;
            String fieldName = cbx.Name;
            int exceptionSectionId = cbx != null && cbx.SelectedValue != null ? (int)cbx.SelectedValue : 0;
            if (exceptionSectionId == 0)
            {
                return;
            }

            var exceptions = new DatabaseHandler().getManagementLetterSectionExceptions(DatabaseHandler.dbConnection(), exceptionSectionId);

            foreach (var section in AppStateAuditTools.auditSystemsToolSectionEvaluations)
            {

                var items = section.sectionItemEvaluations;

                foreach (KeyValuePair<long, SectionItemEvaluationAst> entry in section.sectionItemEvaluations)
                {

                    var item = entry.Value;

                    //we found the matching colum
                    if (item.managementLetterSectionFieldName == fieldName)
                    {

                        String exceptionField = item.managementLetterSectionItemFieldName;
                        var cbxExceptions = WidgetHandler.findControlByName(this, exceptionField);
                        if (cbxExceptions != null && cbxExceptions is ComboBox)
                        {
                            var comboxBox = (ComboBox)cbxExceptions;
                            WidgetHandler.populateManagementLetterSectionExceptionsDropdown(comboxBox, exceptions);
                        }
                        return;

                    }

                }

            }

        }



    }



}

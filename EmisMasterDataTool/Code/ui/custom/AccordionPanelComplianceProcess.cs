﻿using Processor.ControlObjects;
using Processor.Entities.PM.compliance.process;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisTool.Code.ui.custom
{
    internal class AccordionPanelComplianceProcess : AccordionPanel
    {

        public AccordionPanelComplianceProcess(AppStateCompliance appState)
        {
                        

            //reset the app state to remove old data
            AppStateCompliance.complianceProcessToolSectionEvaluations.Clear();
            UiHelperCpt.FieldCountPmAuditProcessTool = 0;

            //dynamic sections

            var sections = new DatabaseHandler().getPmComplianceProcessToolSections(DatabaseHandler.dbConnection());
            foreach (var section in sections)
            {

                //will hold the state for this section
                SectionEvaluationCpt sectionEvaluation = null;
                TableLayoutPanel sectionTable = UiHelperCpt.GenerateTableV1(section, out sectionEvaluation, appState);

                String header = section.name;
                accordion.Add(sectionTable, header, null, 1, false);

                //check if section does not exists in app state
                if (!AppStateCompliance.sectionEvaluationCptExists(sectionEvaluation.sectionEmisId) && sectionEvaluation.sectionEmisId != 0)
                {
                    AppStateCompliance.complianceProcessToolSectionEvaluations.Add(sectionEvaluation);
                }

            }

        }

    }



}

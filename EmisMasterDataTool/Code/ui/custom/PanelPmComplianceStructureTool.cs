﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Processor.ControlObjects;
using Processor.Entities.api;
using EmisMasterDataTool.Code;

namespace EmisTool.Code.ui.custom
{
    public class PanelPmComplianceStructureTool : Panel
    {

        private int activityId;
        private bool inEditMode = false;
        private String toolReferenceInEditMode = "";

        public PanelPmComplianceStructureTool(int activityId, ApiPmCompStructTool toolToUpdate = null)
        {            
            
            initializeControls();
            this.activityId = activityId;

            //load data
            loadData(toolToUpdate);

        }

        private void loadData(ApiPmCompStructTool toolToEdit)
        {

            if (toolToEdit != null)
            {
                populateTool(toolToEdit);
                return;
            }

            //it's a new tool creation
            loadActivityGeneralDetails(activityId);

        }

        private void populateTool(ApiPmCompStructTool tool)
        {

            //
            //set status as edit, set the tool reference to edit, make the save button show update
            //
            inEditMode = true;
            toolReferenceInEditMode = tool.toolReference;
            Control btnSaveTool = WidgetHandler.findControlByName(this, "btnSaveTool");
            if (btnSaveTool != null) { btnSaveTool.Text = "Update Tool"; }

            //fill general info
            populateGeneralInfo(tool.generalInfo);

            //fill contracts committe
            populateContractsCommitteMembers(tool.contractsCommitteeMembers);

            //fill departments
            populateDepartments(tool.departments);

            //fill pdu members
            populatePduMembers(tool.pduMembers);

            //fill reports
            populateReportsSubmitted(tool.reports);

            //fill spend
            populateSpend(tool.spend);

        }

        private void loadActivityGeneralDetails(int activityId)
        {

            DatabaseHandler dbHandler = new DatabaseHandler();

            var activity = dbHandler.findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityId);

            if (activity == null)
            {
                WidgetHandler.showMessage("Failed to find PM activity with ID [" + activityId + "]");
                return;
            }

            var generalInfo = new ApiPmCompStructToolGeneralInfo();
            generalInfo.nameOfEntity = activity.entityName;

            populateGeneralInfo(generalInfo);

        }

        private void initializeControls()
        {

            this.Name = "panelPmComplianceStructureTool";
            this.Padding = new Padding(10);

            //
            // tabControlPmComplianceStructureTool
            //
            TabControl tabControlPmComplianceStructureTool = getTabControlPmComplianceStructureTool();

            //
            // panelButtonContainer
            //
            Panel panelButtonContainer = getButtonContainerPanel();

            //
            // tablePaneMainContainer : this is just to hold the 2 controls, 1 for the buttons and the other for the tab control
            //
            TableLayoutPanel tablePaneMainContainer = new TableLayoutPanel();
            tablePaneMainContainer.Dock = DockStyle.Fill;
            tablePaneMainContainer.ColumnCount = 1;
            tablePaneMainContainer.RowCount = 2;
            tablePaneMainContainer.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

            //add the tab control to the main container spanning 90% of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 90));
            tablePaneMainContainer.Controls.Add(tabControlPmComplianceStructureTool, 0, 0);
            // add the buttons panel to the main container spanning 10 % of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 10));
            tablePaneMainContainer.Controls.Add(panelButtonContainer, 0, 1);

            //add tab control to panel
            this.Controls.Add(tablePaneMainContainer);

        }

        private TabControl getTabControlPmComplianceStructureTool()
        {
            
            //
            // tabPageAboutEntity
            //
            TabPage tabPageAboutEntity = GetTabPageAboutEntity();

            //
            // tabPageSpend
            //
            TabPage tabPageSpend = GetTabPageSpend();

            //
            // tabControlPmComplianceProcessTool
            //
            TabControl tabControlPmComplianceProcessTool = new TabControl();
            tabControlPmComplianceProcessTool.Name = "tabControlPmComplianceProcessTool";
            tabControlPmComplianceProcessTool.Dock = DockStyle.None;
            tabControlPmComplianceProcessTool.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            tabControlPmComplianceProcessTool.TabPages.Add(tabPageAboutEntity);
            tabControlPmComplianceProcessTool.TabPages.Add(tabPageSpend);
            return tabControlPmComplianceProcessTool;

        }

        private Panel getButtonContainerPanel()
        {

            Panel panelButtonContainer = new Panel();
            panelButtonContainer.Height = 40;
            panelButtonContainer.Dock = DockStyle.Fill;

            Button btnSaveTool = new Button() { Text = "Save Tool", Name = "btnSaveTool", Width = 150, Anchor = AnchorStyles.Bottom | AnchorStyles.Left, Margin = new Padding(0) };
            btnSaveTool.Click += btnSaveToolClicked;
            panelButtonContainer.Controls.Add(btnSaveTool);

            TableLayoutPanel tabPanel = new TableLayoutPanel() { Anchor = AnchorStyles.Bottom | AnchorStyles.Right, Margin = new Padding(0) };
            tabPanel.ColumnCount = 3;
            tabPanel.RowCount = 1;
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 35));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40));
            tabPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 35));
            // tabPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tabPanel.Width = 200;
            tabPanel.Height = 38;

            Label labelLoadTool = new Label() { Text = "Tool Id", Dock = DockStyle.Fill, TextAlign = System.Drawing.ContentAlignment.MiddleRight };
            TextBox txvLoadTool = new TextBox() { Name = "txvLoadTool", Width = 100 };
            Button btnLoadTool = new Button() { Name = "btnLoadTool", Text = "Load Tool" };
            btnLoadTool.Click += btnLoadToolClicked;

            tabPanel.Controls.Add(labelLoadTool, 0, 0);
            tabPanel.Controls.Add(txvLoadTool, 1, 0);
            tabPanel.Controls.Add(btnLoadTool, 2, 0);

            //panelButtonContainer.Controls.Add(tabPanel);
            return panelButtonContainer;

        }

        private void btnLoadToolClicked(object sender, EventArgs e)
        {

            String toolId = WidgetHandler.findCtlValByCtlName(this, "txvLoadTool");

            //check if it's a valid ID
            long lToolId = 0;
            if (!long.TryParse(toolId, out lToolId))
            {
                WidgetHandler.showMessage("Tool ID must be a number", true);
                return;
            }

            //get the tool
            var dbConn = DatabaseHandler.dbConnection();
            var tool = new DatabaseHandler().getPmCompStructToolByIdOrActivityId(dbConn, 0, lToolId);

            //tool not found
            if (tool == null)
            {
                WidgetHandler.showMessage("Tool with ID[" + toolId + "] not found", true);
                return;
            }

            //fill general info
            populateGeneralInfo(tool.generalInfo);

            //fill contracts committe
            populateContractsCommitteMembers(tool.contractsCommitteeMembers);

            //fill departments
            populateDepartments(tool.departments);

            //fill pdu members
            populatePduMembers(tool.pduMembers);

            //fill reports
            populateReportsSubmitted(tool.reports);

            //fill spend
            populateSpend(tool.spend);


            //we are done
            WidgetHandler.showMessage("Data successfully loaded", false);

        }

        private void populateSpend(ApiPmCompStructToolSpend spend)
        {

            if(spend == null)
            {
                return;
            }
            
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroJan", spend.macroJan);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroFeb", spend.macroFeb);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroMar", spend.macroMar);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroApr", spend.macroApr);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroMay", spend.macroMay);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroJun", spend.macroJun);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroJul", spend.macroJul);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroAug", spend.macroAug);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroSept", spend.macroSept);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroOct", spend.macroOct);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroNov", spend.macroNov);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroDec", spend.macroDec);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMacroTotal", spend.macroTotal);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroJan", spend.microJan);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroFeb", spend.microFeb);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroMar", spend.microMar);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroApr", spend.microApr);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroMay", spend.microMay);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroJun", spend.microJun);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroJul", spend.microJul);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroAug", spend.microAug);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroSept", spend.microSept);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroOct", spend.microOct);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroNov", spend.microNov);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroDec", spend.microDec);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMicroTotal", spend.microTotal);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalMacroAndMicro",spend.totalMacroAndMicro);
            
        }

        private void populateReportsSubmitted(List<ApiPmCompStructToolReportsSubmitted> reports)
        {
            
            String fieldPrefixName = "txvReportName";
            String fieldPrefixDate = "txvReportDate";

            int row = 1;
            foreach (var report in reports)
            {
                String currentFieldNameName = fieldPrefixName + row;
                String currentFieldNameDate = fieldPrefixDate + row;

                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNameName, report.report);
                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNameDate, report.dateSubmitted);
                row++;
            }

        }

        private void populatePduMembers(List<ApiPmCompStructToolPduMembers> pduMembers)
        {
            
            String fieldPrefixName = "txvPduMemberName";
            String fieldPrefixQualification = "txvPduMemberQualif";
            String fieldPrefixPosition = "txvPduMemberPosition";
            String fieldPrefixDateOfApproval = "txvPduMemberApprovalDate";

            int row = 1;
            foreach (var member in pduMembers)
            {
                String currentFieldNameName = fieldPrefixName + row;
                String currentFieldNameQualification = fieldPrefixQualification + row;
                String currentFieldNamePosition = fieldPrefixPosition + row;
                String currentFieldNameDateOfApproval = fieldPrefixDateOfApproval + row;

                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNameName, member.name);
                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNameQualification, member.qualification);
                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNamePosition, member.positionOnUnit);
                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNameDateOfApproval, member.appointmentDate);
                row++;
            }

        }

        private void populateDepartments(List<ApiPmCompStructToolDepts> departments)
        {
           
            String fieldPrefix = "txvDept";

            int row = 1;
            foreach(var dept in departments)
            {
                String currentFieldName = fieldPrefix + row;
                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldName, dept.deptName);
                row++;
            }

        }

        private void populateContractsCommitteMembers(List<ApiPmCompStructToolContractsCommitteMembers> contractsCommitteeMembers)
        {
            
            String fieldPrefixName = "txvMemberName";
            String fieldPrefixPosition = "txvMemberPosition";
            String fieldPrefixSubstantivePos = "txvMemberSubstantivePos";           
            String fieldPrefixDateOfApproval = "txvMemberDateOfApproval";

            int row = 2;
            foreach (var member in contractsCommitteeMembers)
            {
                String currentFieldNameName = fieldPrefixName + row;
                String currentFieldNamePosition = fieldPrefixPosition + row;
                String currentFieldNameSubstantivePos = fieldPrefixSubstantivePos + row;
                String currentFieldNameDateOfApproval = fieldPrefixDateOfApproval + row;

                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNameName, member.name);
                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNamePosition, member.positionOnCommittee);
                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNameSubstantivePos, member.positionInPde);
                WidgetHandler.findAndFillCtlValByCtlName(this, currentFieldNameDateOfApproval, member.approvalDate);
                row++;
            }

        }

        private void populateGeneralInfo(ApiPmCompStructToolGeneralInfo generalInfo)
        {

            if(generalInfo == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNameOfEntity", generalInfo.nameOfEntity);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvSector", generalInfo.sector);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvDateOfOperationalization", generalInfo.dateOfOperationzation);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNoOfStaff", generalInfo.numberOfStaff);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvGeoSpread", generalInfo.geographicalSpread);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalEntityBudget", generalInfo.totalEntityBudget);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvTotalProcBudget", generalInfo.totalProcBugdet);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvVolProcVsAnnualBudget", generalInfo.volOfProcBudgetAsPercentOfPdeBudget);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvSourceOfFunding", generalInfo.fundingSource);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPreviousRatingsAndDate", generalInfo.previousRatings);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNameOfAO", generalInfo.nameOfAccountingOfficer);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxFacilitatedWithInternet", generalInfo.hasSpaceAndInternet);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxAreCCMembersApproved", generalInfo.areContractsCommiteeMembersApproved);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxHasListOfProviders", generalInfo.hasListOfPrequalifiedProviders);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxHasProcPlan", generalInfo.hasApprovedProcPlan);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxUsesFrameworkContracts", generalInfo.usesFrameworkContracts);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxIndCaseHaveProcRefNo", generalInfo.caseHaveStandardRefNumbering);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxUsesBridgedVersionOfProcAds", generalInfo.hasAbridgedVersionOfProcAds);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvNameInternalAuditors", generalInfo.nameOfInternalAuditors);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxAuditorUnderstandsProcRole", generalInfo.auditorHasUnderstaningOfRole);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxWereReportsSubmittedOnTime", generalInfo.wereReportsSubmittedOnTime);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxDoesEntityUploadDataOnGpp", generalInfo.uploadsDataOnGpp);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxReportsInStandardFormat", generalInfo.reportsSubmittedInStandardFormat);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxPpdaQueriesResolved", generalInfo.ppdaQueriesResolved);
            WidgetHandler.findAndFillCtlValByCtlName(this, "cbxHasEntityDisposedOff", generalInfo.hasDisposedOffObsoleteItems);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgProcStructureScore", generalInfo.avgProcStrucutureScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvReviewedBy", generalInfo.reviewBy);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvDateOfReview", generalInfo.dateOfReview);

        }

        private TabPage GetTabPageSpend()
        {

            //
            //tableSpend : get the table with the tab controls
            //
            var tableSpend = UiHelperCst.GenerateSpendTable(calcCstSpendMacro, calcCstSpendMicro);
            tableSpend.Padding = new Padding(10);

            //
            // panelContainer
            //
            Panel panelContainer = new Panel();
            panelContainer.Dock = DockStyle.Fill;
            panelContainer.Controls.Add(tableSpend);

            TabPage tabPageSpend = new TabPage("Spend");            
            tabPageSpend.Controls.Add(panelContainer);
            return tabPageSpend;

        }

        private TabPage GetTabPageAboutEntity()
        {

            //
            // tableAboutEntity
            //
            var tableAboutEntity = UiHelperCst.GenerateAboutEntitySection(calcComplianceStructureToolAvgProctStructureScore);           
            tableAboutEntity.Padding = new Padding(10);

            //
            // panelContainer
            //
            Panel panelContainer = new Panel();
            panelContainer.Dock = DockStyle.Fill;
            panelContainer.Controls.Add(tableAboutEntity);

            //
            // tabPageAboutEntity
            //
            TabPage tabPageAboutEntity = new TabPage("About Entity");
            tabPageAboutEntity.Controls.Add(panelContainer);

            return tabPageAboutEntity;

        }

        public void calcComplianceStructureToolAvgProctStructureScore(object sender, EventArgs e)
        {

            var list = new List<String> {
                "cbxFacilitatedWithInternet", "cbxHasListOfProviders", "cbxUseBridgedVersionOfProcAds",
                "cbxAuditorUnderstandsProcRole", "cbxWereReportsSubmittedOnTime", "cbxDoesEntityUploadDataOnGpp",
                "cbxHasEntityDisposedOff" };

            //track the counts for the different response
            long yesCount = 0;
            long noCount = 0;

           // var tableLayout = tabListComplianceStructure[0];

            foreach (String item in list)
            {

                //find the current control and it's select value
                var answerTxv = WidgetHandler.findControlByName(this, item); 
                if (answerTxv != null)
                {
                    String response = answerTxv.Text;
                    if (response.ToUpper() == "YES")
                    {
                        yesCount += 1;
                    }
                    else if (response.ToUpper() == "NO")
                    {
                        noCount += 1;
                    }
                }

            }

            //find the control with the total
            var avgScoreTxv = WidgetHandler.findControlByName(this, "txvAvgProcStructureScore");
            if (avgScoreTxv != null)
            {
                new CalculationHelper().calculateCalculateCstAverageScore(yesCount, noCount, avgScoreTxv);
            }

        }


        private void btnSaveToolClicked(object sender, EventArgs e)
        {

            if (!inEditMode)
            {
                saveTool();
            }
            else
            {
                updateTool();
            }            

        }

        private void updateTool()
        {

            //check if the tool name probably changed
            String entityName = WidgetHandler.findCtlValByCtlName(this, "txvNameOfEntity");

            String newToolName = SharedCommons.getToolName(AppConstants.PREFIX_CST, entityName, "NoAuditPeriod", activityId);

            //tool Reference might have changed
            if (!apiDataUpdateToolReference(newToolName, out String message))
            {
                WidgetHandler.showMessage(message);
                return;
            }

            //clear existing data
            new DatabaseHandler().clearCompStructToolOldData(DatabaseHandler.dbConnection(), toolReferenceInEditMode);

            //save new tool data
            saveToolData(newToolName);

        }

        private bool apiDataUpdateToolReference(string newToolName, out string message)
        {

            DatabaseHandler db = new DatabaseHandler();
            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();

            if (toolReferenceInEditMode != newToolName && db.compStructToolNameExists(sQLiteConnection, newToolName))
            {
                message = "Tool with name [" + newToolName + "] already exists.";
                return false;
            }

            var tool = db.getPmCompStructToolByIdOrActivityId(sQLiteConnection, activityId);
            if (tool == null)
            {
                message = "Tool for activity ID [" + activityId + "] not found.";
                return false;
            }

            tool.toolName = newToolName;
            tool.toolReference = newToolName;
            tool.activityId = activityId;
            tool.toolType = AppConstants.PREFIX_CST;


            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.createdOn = dateTimeNow;
            tool.updatedOn = dateTimeNow;

            db.updatePmCst(sQLiteConnection, tool);

            message = "SUCCESS";
            return true;

        }

        private void saveTool()
        {

            //save the tool header
            String toolRef = "";
            if (!savePmCstToolHeader(out toolRef))
            {
                return;
            }

            saveToolData(toolRef);

        }

        private void saveToolData(string toolRef)
        {

            //save the general info
            savePmCstToolGeneralInfo(toolRef);

            //save the depts
            savePmCstToolDepts(toolRef);

            //save the cc
            savePmCstToolCCMembers(toolRef);

            //save the PDUs
            savePmCstToolPduMembers(toolRef);

            //save the reports submitted
            savePmCstToolReportsSubmitted(toolRef);

            //save the spend
            savePmCstToolSpend(toolRef);

            //show success message
            WidgetHandler.showMessage("Tool successfully saved with tool reference [" + toolRef + "]", false);

        }

        private void savePmCstToolSpend(string toolRef)
        {

            ApiPmCompStructToolSpend data = new ApiPmCompStructToolSpend();
            data.toolReference = toolRef;

            data.macroJan = WidgetHandler.findCtlValByCtlName(this, "txvMacroJan");
            data.macroFeb = WidgetHandler.findCtlValByCtlName(this, "txvMacroFeb");
            data.macroMar = WidgetHandler.findCtlValByCtlName(this, "txvMacroMar");
            data.macroApr = WidgetHandler.findCtlValByCtlName(this, "txvMacroApr");
            data.macroMay = WidgetHandler.findCtlValByCtlName(this, "txvMacroMay");
            data.macroJun = WidgetHandler.findCtlValByCtlName(this, "txvMacroJun");
            data.macroJul = WidgetHandler.findCtlValByCtlName(this, "txvMacroJul");
            data.macroAug = WidgetHandler.findCtlValByCtlName(this, "txvMacroAug");
            data.macroSept = WidgetHandler.findCtlValByCtlName(this, "txvMacroSept");
            data.macroOct = WidgetHandler.findCtlValByCtlName(this, "txvMacroOct");
            data.macroNov = WidgetHandler.findCtlValByCtlName(this, "txvMacroNov");
            data.macroDec = WidgetHandler.findCtlValByCtlName(this, "txvMacroDec");
            data.macroTotal = WidgetHandler.findCtlValByCtlName(this, "txvMacroTotal");

            data.microJan = WidgetHandler.findCtlValByCtlName(this, "txvMicroJan");
            data.microFeb = WidgetHandler.findCtlValByCtlName(this, "txvMicroFeb");
            data.microMar = WidgetHandler.findCtlValByCtlName(this, "txvMicroMar");
            data.microApr = WidgetHandler.findCtlValByCtlName(this, "txvMicroApr");
            data.microMay = WidgetHandler.findCtlValByCtlName(this, "txvMicroMay");
            data.microJun = WidgetHandler.findCtlValByCtlName(this, "txvMicroJun");
            data.microJul = WidgetHandler.findCtlValByCtlName(this, "txvMicroJul");
            data.microAug = WidgetHandler.findCtlValByCtlName(this, "txvMicroAug");
            data.microSept = WidgetHandler.findCtlValByCtlName(this, "txvMicroSept");
            data.microOct = WidgetHandler.findCtlValByCtlName(this, "txvMicroOct");
            data.microNov = WidgetHandler.findCtlValByCtlName(this, "txvMicroNov");
            data.microDec = WidgetHandler.findCtlValByCtlName(this, "txvMicroDec");
            data.microTotal = WidgetHandler.findCtlValByCtlName(this, "txvMicroTotal");

            data.totalMacroAndMicro = WidgetHandler.findCtlValByCtlName(this, "txvTotalMacroAndMicro");

            new DatabaseHandler().savePmCstSpend(DatabaseHandler.dbConnection(), data);

        }

        private void savePmCstToolReportsSubmitted(string toolRef)
        {
            var data = new List<ApiPmCompStructToolReportsSubmitted>();
            
            var report1 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName1"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate1"));
            var report2 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName2"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate2"));
            var report3 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName3"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate3"));
            var report4 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName4"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate4"));
            var report5 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName5"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate5"));
            var report6 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName6"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate6"));
            var report7 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName7"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate7"));
            var report8 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName8"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate8"));
            var report9 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName9"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate9"));
            var report10 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName10"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate10"));
            var report11 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName11"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate11"));
            var report12 = new ApiPmCompStructToolReportsSubmitted(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvReportName12"), WidgetHandler.findCtlValByCtlName(this, "txvReportDate12"));

            var items = new List<ApiPmCompStructToolReportsSubmitted> { report1, report2, report3, report4, report5, report6, report7, report8, report9, report10, report11, report12 };
            data.AddRange(items);
            new DatabaseHandler().savePmCstReportsSubmitted(DatabaseHandler.dbConnection(), data);
        }

        private void savePmCstToolPduMembers(string toolRef)
        {
            var data = new List<ApiPmCompStructToolPduMembers>();
            
            var member1 = new ApiPmCompStructToolPduMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvPduMemberName1"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberQualif1"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberPosition1"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberApprovalDate1"));
            var member2 = new ApiPmCompStructToolPduMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvPduMemberName2"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberQualif2"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberPosition2"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberApprovalDate2"));
            var member3 = new ApiPmCompStructToolPduMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvPduMemberName3"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberQualif3"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberPosition3"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberApprovalDate3"));
            var member4 = new ApiPmCompStructToolPduMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvPduMemberName4"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberQualif4"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberPosition4"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberApprovalDate4"));
            var member5 = new ApiPmCompStructToolPduMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvPduMemberName5"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberQualif5"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberPosition5"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberApprovalDate5"));
            var member6 = new ApiPmCompStructToolPduMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvPduMemberName6"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberQualif6"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberPosition6"), WidgetHandler.findCtlValByCtlName(this, "txvPduMemberApprovalDate6"));

            data.AddRange(new List<ApiPmCompStructToolPduMembers> { member1, member2, member3, member4, member5, member6 });
            new DatabaseHandler().savePmCstPduMembers(DatabaseHandler.dbConnection(), data);
        }

        private void savePmCstToolCCMembers(string toolRef)
        {
            var data = new List<ApiPmCompStructToolContractsCommitteMembers>();
            
            var ccMember1 = new ApiPmCompStructToolContractsCommitteMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvMemberName2"), WidgetHandler.findCtlValByCtlName(this, "txvMemberPosition2"), WidgetHandler.findCtlValByCtlName(this, "txvMemberSubstantivePos2"), WidgetHandler.findCtlValByCtlName(this, "txvMemberDateOfApproval2"));
            var ccMember2 = new ApiPmCompStructToolContractsCommitteMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvMemberName3"), WidgetHandler.findCtlValByCtlName(this, "txvMemberPosition3"), WidgetHandler.findCtlValByCtlName(this, "txvMemberSubstantivePos3"), WidgetHandler.findCtlValByCtlName(this, "txvMemberDateOfApproval3"));
            var ccMember3 = new ApiPmCompStructToolContractsCommitteMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvMemberName4"), WidgetHandler.findCtlValByCtlName(this, "txvMemberPosition4"), WidgetHandler.findCtlValByCtlName(this, "txvMemberSubstantivePos4"), WidgetHandler.findCtlValByCtlName(this, "txvMemberDateOfApproval4"));
            var ccMember4 = new ApiPmCompStructToolContractsCommitteMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvMemberName5"), WidgetHandler.findCtlValByCtlName(this, "txvMemberPosition5"), WidgetHandler.findCtlValByCtlName(this, "txvMemberSubstantivePos5"), WidgetHandler.findCtlValByCtlName(this, "txvMemberDateOfApproval5"));
            var ccMember5 = new ApiPmCompStructToolContractsCommitteMembers(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvMemberName6"), WidgetHandler.findCtlValByCtlName(this, "txvMemberPosition6"), WidgetHandler.findCtlValByCtlName(this, "txvMemberSubstantivePos6"), WidgetHandler.findCtlValByCtlName(this, "txvMemberDateOfApproval6"));

            data.AddRange(new List<ApiPmCompStructToolContractsCommitteMembers> { ccMember1, ccMember2, ccMember3, ccMember4, ccMember5 });
            new DatabaseHandler().savePmCstCCMembers(DatabaseHandler.dbConnection(), data);

        }


        private void savePmCstToolDepts(string toolRef)
        {

            var data = new List<ApiPmCompStructToolDepts>();


            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept1")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept2")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept3")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept4")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept5")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept6")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept7")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept8")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept9")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept10")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept11")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept12")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept13")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept14")));
            data.Add(new ApiPmCompStructToolDepts(toolRef, WidgetHandler.findCtlValByCtlName(this, "txvDept15")));
            new DatabaseHandler().savePmCstDepts(DatabaseHandler.dbConnection(), data);

        }


        private void savePmCstToolGeneralInfo(string toolRef)
        {

            var data = new ApiPmCompStructToolGeneralInfo();
            data.toolReference = toolRef;

            data.nameOfEntity = WidgetHandler.findCtlValByCtlName(this, "txvNameOfEntity");
            data.sector = WidgetHandler.findCtlValByCtlName(this, "txvSector");
            data.dateOfOperationzation = WidgetHandler.findCtlValByCtlName(this, "txvDateOfOperationalization");
            data.numberOfStaff = WidgetHandler.findCtlValByCtlName(this, "txvNoOfStaff");
            data.geographicalSpread = WidgetHandler.findCtlValByCtlName(this, "txvGeoSpread");
            data.totalEntityBudget = WidgetHandler.findCtlValByCtlName(this, "txvTotalEntityBudget");
            data.totalProcBugdet = WidgetHandler.findCtlValByCtlName(this, "txvTotalProcBudget");
            data.volOfProcBudgetAsPercentOfPdeBudget = WidgetHandler.findCtlValByCtlName(this, "txvVolProcVsAnnualBudget");
            data.fundingSource = WidgetHandler.findCtlValByCtlName(this, "txvSourceOfFunding");
            data.previousRatings = WidgetHandler.findCtlValByCtlName(this, "txvPreviousRatingsAndDate");
            data.nameOfAccountingOfficer = WidgetHandler.findCtlValByCtlName(this, "txvNameOfAO");
            data.hasSpaceAndInternet = WidgetHandler.findCtlValByCtlName(this, "cbxFacilitatedWithInternet");
            data.areContractsCommiteeMembersApproved = WidgetHandler.findCtlValByCtlName(this, "cbxAreCCMembersApproved");
            data.hasListOfPrequalifiedProviders = WidgetHandler.findCtlValByCtlName(this, "cbxHasListOfProviders");
            data.hasApprovedProcPlan = WidgetHandler.findCtlValByCtlName(this, "cbxHasProcPlan");
            data.usesFrameworkContracts = WidgetHandler.findCtlValByCtlName(this, "cbxUsesFrameworkContracts");
            data.caseHaveStandardRefNumbering = WidgetHandler.findCtlValByCtlName(this, "cbxIndCaseHaveProcRefNo");
            data.hasAbridgedVersionOfProcAds = WidgetHandler.findCtlValByCtlName(this, "cbxUsesBridgedVersionOfProcAds");
            data.nameOfInternalAuditors = WidgetHandler.findCtlValByCtlName(this, "txvNameInternalAuditors");
            data.auditorHasUnderstaningOfRole = WidgetHandler.findCtlValByCtlName(this, "cbxAuditorUnderstandsProcRole");
            data.wereReportsSubmittedOnTime = WidgetHandler.findCtlValByCtlName(this, "cbxWereReportsSubmittedOnTime");
            data.uploadsDataOnGpp = WidgetHandler.findCtlValByCtlName(this, "cbxDoesEntityUploadDataOnGpp");
            data.reportsSubmittedInStandardFormat = WidgetHandler.findCtlValByCtlName(this, "cbxReportsInStandardFormat");
            data.ppdaQueriesResolved = WidgetHandler.findCtlValByCtlName(this, "cbxPpdaQueriesResolved");
            data.hasDisposedOffObsoleteItems = WidgetHandler.findCtlValByCtlName(this, "cbxHasEntityDisposedOff");
            data.avgProcStrucutureScore = WidgetHandler.findCtlValByCtlName(this, "txvAvgProcStructureScore");
            data.reviewBy = WidgetHandler.findCtlValByCtlName(this, "txvReviewedBy");
            data.dateOfReview = WidgetHandler.findCtlValByCtlName(this, "txvDateOfReview");

            new DatabaseHandler().savePmCstGeneralInfo(DatabaseHandler.dbConnection(), data);

        }

        private bool savePmCstToolHeader(out String toolReference)
        {
            
            toolReference = null;

            string entityName = WidgetHandler.findCtlValByCtlName(this, "txvNameOfEntity");

            if (String.IsNullOrEmpty(entityName))
            {
                WidgetHandler.showMessage("Please supply the Entity name");
                return false;
            }


            String toolName = SharedCommons.getToolName(AppConstants.PREFIX_CST, entityName, "NoAuditPeriod", activityId);            


            var dbHandler = new DatabaseHandler();
            SQLite.SQLiteConnection dbConn = DatabaseHandler.dbConnection();
            if (dbHandler.compStructToolNameExists(dbConn, toolName))
            {
                WidgetHandler.showMessage("Tool with name [" + toolName + "] already exists");
                return false;
            }

            var tool = new ApiPmCompStructTool();
            tool.toolName = toolName;
            tool.toolReference = toolName;
            tool.activityId = activityId;
            tool.toolType = AppConstants.PREFIX_CST;

            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.createdOn = dateTimeNow;
            tool.updatedOn = dateTimeNow;

            //save
            dbHandler.savePmCst(dbConn, tool);
            toolReference = tool.toolReference;

            return true;

        }

        public void calcCstSpendMacro(object sender, EventArgs e)
        {

            var list = new List<String> {
                "txvMacroJan", "txvMacroFeb", "txvMacroMar", "txvMacroApr", "txvMacroMay", "txvMacroJun",
                "txvMacroJul", "txvMacroAug", "txvMacroSept", "txvMacroOct", "txvMacroNov", "txvMacroDec"
            };

            //track the counts for the different response
            long total = 0;

            foreach (String item in list)
            {

                //find the current control and it's select value
                var answerTxvs = WidgetHandler.findControlByName(this, item);
                if (answerTxvs != null)
                {
                    String response = answerTxvs.Text;
                    long currentValue = 0;
                    long.TryParse(response, out currentValue);
                    total += currentValue;
                }

            }

            //find the control with the total
            var totalMacroTxvs = WidgetHandler.findControlByName(this, "txvMacroTotal");
            if (totalMacroTxvs != null)
            {
                totalMacroTxvs.Text = total.ToString();
                calcCstSumMacroAndMicroExpense();
            }

        }



        public void calcCstSpendMicro(object sender, EventArgs e)
        {

            var list = new List<String> {
                "txvMicroJan", "txvMicroFeb", "txvMicroMar", "txvMicroApr", "txvMicroMay", "txvMicroJun",
                "txvMicroJul", "txvMicroAug", "txvMicroSept", "txvMicroOct", "txvMicroNov", "txvMicroDec"
            };

            //track the counts for the different response
            long total = 0;
            
            foreach (String item in list)
            {

                //find the current control and it's select value
                var answerTxv = WidgetHandler.findControlByName(this, item);
                if (answerTxv != null)
                {
                    String response = answerTxv.Text;
                    long currentValue = 0;
                    long.TryParse(response, out currentValue);
                    total += currentValue;
                }

            }

            //find the control with the total
            var totalMicroTxv = WidgetHandler.findControlByName(this, "txvMicroTotal");
            if (totalMicroTxv != null)
            {
                totalMicroTxv.Text = total.ToString();
                calcCstSumMacroAndMicroExpense();
            }

        }

        public void calcCstSumMacroAndMicroExpense()
        {
            
            long lTotalMicro = 0;
            long lTotalMacro = 0;

            //find the control with the total
            var totalMacroTxvs = WidgetHandler.findControlByName(this, "txvMacroTotal");
            if (totalMacroTxvs != null)
            {
                String totalMacro = totalMacroTxvs.Text;
                long.TryParse(totalMacro, out lTotalMacro);
            }

            //find the control with the total
            var totalMicroTxvs = WidgetHandler.findControlByName(this, "txvMicroTotal");
            if (totalMicroTxvs != null)
            {
                String totalMicro = totalMicroTxvs.Text;
                long.TryParse(totalMicro, out lTotalMicro);
            }

            //find the control with the total
            var totalMicroMacroTxvs = WidgetHandler.findControlByName(this, "txvTotalMacroAndMicro");
            if (totalMicroMacroTxvs != null)
            {
                totalMicroMacroTxvs.Text = (lTotalMicro + lTotalMacro).ToString();
            }

        }

    }
}

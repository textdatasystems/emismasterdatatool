﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Processor.Entities.PM.compliance.process;
using Processor.ControlObjects;
using Processor.Entities.api;
using EmisMasterDataTool.Code;

namespace EmisTool.Code.ui.custom
{
    public class PanelPmComplianceProcessTool : Panel, ICptAnalyisUpdater, IEventHandlersCpt
    {            

        //holds the app state
        private AppStateCompliance appState;

        /// <summary>
        /// Holds the fields for the different case averages for the each section
        /// We keep track of these because we need to set their values
        /// when a score is updated and they are dynamically generating
        /// So for each section, the emisID is the key and the object with the different controls for the
        /// different case averages is the value
        /// </summary>
        public static Dictionary<long, CaseScoresControls> CPT_SECTION_TOTAL_TXVS = new Dictionary<long, CaseScoresControls>();

        private int activityId;
        private int sampleFileLocalId;
        private bool inEditMode = false;
        private String toolReferenceInEditMode = "";

        public PanelPmComplianceProcessTool(int activityId, int sampleFileLocalId, ApiPmCompProcessTool toolToEdit = null)
        {

            this.activityId = activityId;
            this.sampleFileLocalId = sampleFileLocalId;

            //set the app state
            appState = new AppStateCompliance();
            appState.iCptAnalyisUpdater = this;
            
            //initialize the controls
            initializeControls();

            //load data
            loadData(toolToEdit);

        }

        private void loadData(ApiPmCompProcessTool toolToEdit)
        {

            if (toolToEdit != null)
            {
                populateTool(toolToEdit);
                return;
            }
            
        }

        private void populateTool(ApiPmCompProcessTool tool)
        {

            //
            //set status as edit, set the tool reference to edit, make the save button show update
            //
            inEditMode = true;
            toolReferenceInEditMode = tool.toolReference;

            Control btnSaveTool = WidgetHandler.findControlByName(this, "btnSaveTool");
            if (btnSaveTool != null) { btnSaveTool.Text = "Update Tool"; }

            //fill dynamic sections
            populateDynamicSections(tool.sections);

            //fill analysis
            populateAnalysis(tool.analysis);

        }

        private void initializeControls()
        {

            this.Name = "panelPmComplianceProcessTool";
            this.Padding = new Padding(10);

            //tab control and button container
            TabControl tabControlPmComplianceProcessTool = getTabControlPmComplianceProcessTool();  
            Panel panelButtonContainer = getButtonContainerPanel();


            //this is just to hold the to controls, 1 for the buttons and the other for the tab control
            TableLayoutPanel tablePaneMainContainer = new TableLayoutPanel();
            tablePaneMainContainer.Dock = DockStyle.Fill;
            tablePaneMainContainer.ColumnCount = 1;
            tablePaneMainContainer.RowCount = 2;
            tablePaneMainContainer.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

            //add the tab control to the main container spanning 90% of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 90));
            tablePaneMainContainer.Controls.Add(tabControlPmComplianceProcessTool, 0, 0);
            // add the buttons panel to the main container spanning 10 % of the table
            tablePaneMainContainer.RowStyles.Add(new RowStyle(SizeType.Percent, 10));
            tablePaneMainContainer.Controls.Add(panelButtonContainer, 0, 1);

            //add tab control to panel
            this.Controls.Add(tablePaneMainContainer);

        }

        private TabControl getTabControlPmComplianceProcessTool()
        {

            TabControl tabControlPmComplianceProcessTool = new TabControl();
            tabControlPmComplianceProcessTool.Name = "tabControlPmComplianceProcessTool";
            tabControlPmComplianceProcessTool.Dock = DockStyle.None;
            tabControlPmComplianceProcessTool.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            //tab pages for the tab control
            TabPage tabPageProcessWorksheet = GetTabPageProcessWorksheet();
            TabPage tabPageAverageScores = GetTabPageAverageScores();

            //add tab pages to tab control
            tabControlPmComplianceProcessTool.TabPages.Add(tabPageProcessWorksheet);
            tabControlPmComplianceProcessTool.TabPages.Add(tabPageAverageScores);
            return tabControlPmComplianceProcessTool;

        }

        private Panel getButtonContainerPanel()
        {

            Panel panelButtonContainer = new Panel();
            panelButtonContainer.Height = 40;
            panelButtonContainer.Dock = DockStyle.Fill;

            Button btnSaveTool = new Button() { Text = "Save Tool", Name = "btnSaveTool", Width = 150, Anchor = AnchorStyles.Bottom | AnchorStyles.Left, Margin = new Padding(0) };
            btnSaveTool.Click += btnSaveToolClicked;
            panelButtonContainer.Controls.Add(btnSaveTool);

            TableLayoutPanel tabPanel = new TableLayoutPanel() { Anchor = AnchorStyles.Bottom | AnchorStyles.Right, Margin = new Padding(0) };
            tabPanel.ColumnCount = 3;
            tabPanel.RowCount = 1;
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 35));
            tabPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40));
            tabPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 35));
            tabPanel.Width = 200;
            tabPanel.Height = 38;

            Label labelLoadTool = new Label() { Text = "Tool Id", Dock = DockStyle.Fill, TextAlign = System.Drawing.ContentAlignment.MiddleRight };
            TextBox txvLoadTool = new TextBox() { Name = "txvLoadTool", Width = 100 };
            Button btnLoadTool = new Button() { Name = "btnLoadTool", Text = "Load Tool" };
            btnLoadTool.Click += btnLoadToolClicked;

            tabPanel.Controls.Add(labelLoadTool, 0, 0);
            tabPanel.Controls.Add(txvLoadTool, 1, 0);
            tabPanel.Controls.Add(btnLoadTool, 2, 0);

            //panelButtonContainer.Controls.Add(tabPanel);
            return panelButtonContainer;

        }

        private void btnLoadToolClicked(object sender, EventArgs e)
        {

            String toolId = WidgetHandler.findCtlValByCtlName(this, "txvLoadTool");

            //check if it's a valid ID
            long lToolId = 0;
            if (!long.TryParse(toolId, out lToolId))
            {
                WidgetHandler.showMessage("Tool ID must be a number", true);
                return;
            }

            //get the tool
            var dbConn = DatabaseHandler.dbConnection();
            var tool = new DatabaseHandler().getPmCompProcessToolByIdOrActivityIdAndSampleFileLocalId(dbConn, 0, 0, lToolId);

            //tool not found
            if (tool == null)
            {
                WidgetHandler.showMessage("Tool with ID[" + toolId + "] not found", true);
                return;
            }                       

            //fill dynamic sections
            populateDynamicSections(tool.sections);

            //fill analysis
            populateAnalysis(tool.analysis);

            //we are done
            WidgetHandler.showMessage("Data successfully loaded", false);

        }

        private void populateDynamicSections(List<ApiPmCompProcessToolSection> sections)
        {

            //foreach section
            //find the corresponding state section
            //iterate through as you match the item emis ID

            foreach (var section in sections)
            {

                foreach (var stateSection in AppStateCompliance.complianceProcessToolSectionEvaluations)
                {

                    if (stateSection.sectionEmisId == section.sectionEmisId)
                    {
                        populateCurrentSectionValues(section, stateSection);
                        break;
                    }

                }

                populateCaseSectionAvgsScoreFields(section);

            }

        }


        private static void populateCaseSectionAvgsScoreFields(ApiPmCompProcessToolSection section)
        {

            var caseScoreControls = CPT_SECTION_TOTAL_TXVS[section.sectionEmisId];
            if (caseScoreControls != null)
            {

                var calcHandler = new CalculationHelper();
                calcHandler.calculateCaseAverageScore(section.case1Yes, section.case1No, caseScoreControls.case1);
                calcHandler.calculateCaseAverageScore(section.case2Yes, section.case2No, caseScoreControls.case2);
                calcHandler.calculateCaseAverageScore(section.case3Yes, section.case3No, caseScoreControls.case3);
                calcHandler.calculateCaseAverageScore(section.case4Yes, section.case4No, caseScoreControls.case4);
                calcHandler.calculateCaseAverageScore(section.case5Yes, section.case5No, caseScoreControls.case5);
                calcHandler.calculateCaseAverageScore(section.case6Yes, section.case6No, caseScoreControls.case6);
                calcHandler.calculateCaseAverageScore(section.case7Yes, section.case7No, caseScoreControls.case7);
                calcHandler.calculateCaseAverageScore(section.case8Yes, section.case8No, caseScoreControls.case8);
                calcHandler.calculateCaseAverageScore(section.case9Yes, section.case9No, caseScoreControls.case9);
                calcHandler.calculateCaseAverageScore(section.case10Yes, section.case10No, caseScoreControls.case10);

            }

        }

        private void populateCurrentSectionValues(ApiPmCompProcessToolSection section, SectionEvaluationCpt stateSection)
        {

            var sectionItems = section.items;
            var stateSectionItems = stateSection.sectionItemEvaluations;

            foreach (var item in sectionItems)
            {

                if (stateSectionItems.ContainsKey(item.itemEmisId))
                {

                    var stateItem = stateSectionItems[item.itemEmisId];
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case1FieldName, item.case1FieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case2FieldName, item.case2FieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case3FieldName, item.case3FieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case4FieldName, item.case4FieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case5FieldName, item.case5FieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case6FieldName, item.case6FieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case7FieldName, item.case7FieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case8FieldName, item.case8FieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case9FieldName, item.case9FieldValue);
                    WidgetHandler.findAndFillCtlValByCtlName(this, stateItem.case10FieldName, item.case10FieldValue);

                }

            }

        }

        private void populateAnalysis(ApiPmCompProcessToolAnalysis analysis)
        {
            
            if(analysis == null)
            {
                return;
            }

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRAvgScore", analysis.RAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase10", analysis.RCase10);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase9", analysis.RCase9);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase8", analysis.RCase8);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase7", analysis.RCase7);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase6", analysis.RCase6);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase5", analysis.RCase5);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase4", analysis.RCase4);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase3", analysis.RCase3);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase2", analysis.RCase2);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvRCase1", analysis.RCase1);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMAvgScore", analysis.CMAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase10", analysis.CMCase10);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase9", analysis.CMCase9);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase8", analysis.CMCase8);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase7", analysis.CMCase7);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase6", analysis.CMCase6);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase5", analysis.CMCase5);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase4", analysis.CMCase4);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase3", analysis.CMCase3);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase2", analysis.CMCase2);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCMCase1", analysis.CMCase1);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCAvgScore", analysis.CAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase10", analysis.CCase10);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase9", analysis.CCase9);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase8", analysis.CCase8);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase7", analysis.CCase7);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase6", analysis.CCase6);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase5", analysis.CCase5);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase4", analysis.CCase4);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase3", analysis.CCase3);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase2", analysis.CCase2);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvCCase1", analysis.CCase1);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvEAvgScore", analysis.EAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase10", analysis.ECase10);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase9", analysis.ECase9);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase8", analysis.ECase8);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase7", analysis.ECase7);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase6", analysis.ECase6);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase5", analysis.ECase5);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase4", analysis.ECase4);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase3", analysis.ECase3);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase2", analysis.ECase2);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvECase1", analysis.ECase1);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBAvgScore", analysis.BAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase10", analysis.BCase10);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase9", analysis.BCase9);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase8", analysis.BCase8);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase7", analysis.BCase7);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase6", analysis.BCase6);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase5", analysis.BCase5);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase4", analysis.BCase4);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase3", analysis.BCase3);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase2", analysis.BCase2);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBCase1", analysis.BCase1);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDAvgScore", analysis.BDAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase10", analysis.BDCase10);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase9", analysis.BDCase9);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase8", analysis.BDCase8);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase7", analysis.BDCase7);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase6", analysis.BDCase6);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase5", analysis.BDCase5);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase4", analysis.BDCase4);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase3", analysis.BDCase3);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase2", analysis.BDCase2);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvBDCase1", analysis.BDCase1);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPIAvgScore", analysis.PIAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase10", analysis.PICase10);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase9", analysis.PICase9);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase8", analysis.PICase8);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase7", analysis.PICase7);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase6", analysis.PICase6);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase5", analysis.PICase5);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase4", analysis.PICase4);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase3", analysis.PICase3);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase2", analysis.PICase2);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPICase1", analysis.PICase1);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvPSAvgScore", analysis.PSAvgScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgAvgScore", analysis.AvgAvgScore);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase1", analysis.AvgCase1);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase2", analysis.AvgCase2);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase3", analysis.AvgCase3);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase4", analysis.AvgCase4);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase5", analysis.AvgCase5);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase6", analysis.AvgCase6);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase7", analysis.AvgCase7);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase8", analysis.AvgCase8);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase9", analysis.AvgCase9);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvAvgCase10", analysis.AvgCase10);

            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMirrorPIAvgScore", analysis.MirrorPIAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMirrorBDAvgScore", analysis.MirrorBDAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMirrorBAvgScore", analysis.MirrorBAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMirrorEAvgScore", analysis.MirrorEAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMirrorCAvgScore", analysis.MirrorCAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMirrorCMAvgScore", analysis.MirrorCMAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMirrorRAvgScore", analysis.MirrorRAvgScore);
            WidgetHandler.findAndFillCtlValByCtlName(this, "txvMirrorPSAvgScore", analysis.MirrorPSAvgScore);


        }

        private TabPage GetTabPageAverageScores()
        {

            Panel panelContainer = new Panel();
            panelContainer.Dock = DockStyle.Fill;

            //get the table with the tab controls
            var tableAverageScores = UiHelperCpt.GenerateAverageScoresTable(this);
            tableAverageScores.Padding = new Padding(10);

            //add table to container panel
            panelContainer.Controls.Add(tableAverageScores);
            

            TabPage tabPageAverageScores = new TabPage("Average Scores");            
            tabPageAverageScores.Controls.Add(panelContainer);

            return tabPageAverageScores;

        }

        private TabPage GetTabPageProcessWorksheet()
        {

            TabPage tabPageProcessWorksheet = new TabPage("Process Worksheet");
            tabPageProcessWorksheet.Name = "tabPageProcessWorksheet";

            //get the panel with the accordion list
            Panel panelAccordionContainer = GetPanelAccordionContainer();

            //add panel containing the accordion to the page
            tabPageProcessWorksheet.Controls.Add(panelAccordionContainer);

            return tabPageProcessWorksheet;

        }

        private Panel GetPanelAccordionContainer()
        {

            Panel panelAccordionContainer = new Panel();
            panelAccordionContainer.Name = "panelAccordionContainer";
            panelAccordionContainer.Dock = DockStyle.Fill;

            panelAccordionContainer.Padding = new Padding(10);

            //get accordion panel with the different sections
            AccordionPanelComplianceProcess accordionPanelComplianceProcess = new AccordionPanelComplianceProcess(appState);
            accordionPanelComplianceProcess.Name = "accordionPanelComplianceProcess";

            //add accordion panel to the container
            panelAccordionContainer.Controls.Add(accordionPanelComplianceProcess);

            return panelAccordionContainer;

        }

     
        private void btnSaveToolClicked(object sender, EventArgs e)
        {

            if (!inEditMode)
            {
                saveTool();
            }
            else
            {
                updateTool();
            }

           

        }

        private void updateTool()
        {

            var activity = new DatabaseHandler().findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityId);
            if (activity == null)
            {
                WidgetHandler.showMessage("Failed to get activity with ID [" + activityId + "]");
                return;
            }

            //tool Reference might have changed
            String newToolName = SharedCommons.getToolName(AppConstants.PREFIX_CPT, activity.entityName, "NoAuditPeriod", activityId, sampleFileLocalId);            
            
            if (!apiDataUpdateToolReference(newToolName, out String message))
            {
                WidgetHandler.showMessage(message);
                return;
            }

            //clear existing data
            new DatabaseHandler().clearCompProcessToolOldData(DatabaseHandler.dbConnection(), toolReferenceInEditMode);

            //save new tool data
            saveToolData(newToolName);

        }

        private bool apiDataUpdateToolReference(string newToolName, out string message)
        {

            DatabaseHandler db = new DatabaseHandler();
            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();

            if (toolReferenceInEditMode != newToolName && db.compProcessToolNameExists(sQLiteConnection, newToolName))
            {
                message = "Tool with name [" + newToolName + "] already exists.";
                return false;
            }

            var tool = db.getPmCompProcessToolByIdOrActivityIdAndSampleFileLocalId(sQLiteConnection, activityId, sampleFileLocalId);
            if (tool == null)
            {
                message = "Tool for activity ID [" + activityId + "] and sample file ID [" + sampleFileLocalId + "] not found.";
                return false;
            }

            tool.toolName = newToolName;
            tool.toolReference = newToolName;
            tool.activityId = activityId;
            tool.toolType = AppConstants.PREFIX_CPT;


            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.createdOn = dateTimeNow;
            tool.updatedOn = dateTimeNow;

            db.updatePmCpt(sQLiteConnection, tool);

            message = "SUCCESS";
            return true;

        }

        private void saveTool()
        {

            var activity = new DatabaseHandler().findCustomPmActivityByActivityId(DatabaseHandler.dbConnection(), activityId);

            if (activity == null)
            {
                WidgetHandler.showMessage("Failed to get activity with ID [" + activityId + "]");
                return;
            }

            //create a tool reference and save the tool header
            String toolName = SharedCommons.getToolName(AppConstants.PREFIX_CPT, activity.entityName, "NoAuditPeriod", activityId, sampleFileLocalId);
            String toolRef = toolName;

            String message = "";

            if (!apiDataSavePmCompProcessTool(toolName, toolRef, out message))
            {
                WidgetHandler.showMessage(message, true);
                return;
            }

            saveToolData(toolRef);

        }

        private void saveToolData(string toolRef)
        {
            //save the analysis data
            apiDataSavePmCompAnalysisData(toolRef);

            //save sections and section items
            apiDataSaveCompDynamicSectionsData(toolRef);

            //everything has been successfully saved
            WidgetHandler.showMessage("Tool successfully saved with tool reference [" + toolRef + "]", false);
        }

        private void apiDataSaveCompDynamicSectionsData(string toolRef)
        {

            var sectionData = AppStateCompliance.complianceProcessToolSectionEvaluations;

            foreach (var section in sectionData)
            {

                var sectionItems = section.sectionItemEvaluations;
                ApiPmCompProcessToolSection apiToolSection = getApiPmCompProcessToolSection(toolRef, section);                

                //save the section
                DatabaseHandler.saveApiPmCompProcessToolSection(DatabaseHandler.dbConnection(), apiToolSection);

                //after saving the section we proceed to save the section items
                var apiPmCompProcessToolSectionItems = new List<ApiPmCompProcessToolSectionItem>();

                foreach (KeyValuePair<long, SectionItemEvaluationCpt> entry in sectionItems)
                {

                    var item = entry.Value;
                    //get the details of this item
                    ApiPmCompProcessToolSectionItem apiToolSectionItem = getApiPmCompProcessTooSectionItem(toolRef, section, item);

                    apiPmCompProcessToolSectionItems.Add(apiToolSectionItem);

                }

                //save the list of items to the database
                DatabaseHandler.saveApiPmCompProcessToolSectionItem(DatabaseHandler.dbConnection(), apiPmCompProcessToolSectionItems);

            }

        }

        private static ApiPmCompProcessToolSectionItem getApiPmCompProcessTooSectionItem(string toolRef, SectionEvaluationCpt section, SectionItemEvaluationCpt item)
        {

            var apiToolSectionItem = new ApiPmCompProcessToolSectionItem();
            apiToolSectionItem.sectionEmisId = section.sectionEmisId;
            apiToolSectionItem.toolReference = toolRef;
            apiToolSectionItem.itemEmisId = item.sectionItemEmisId;

            apiToolSectionItem.yesRank = item.yesRank;
            apiToolSectionItem.noRank = item.noRank;
            apiToolSectionItem.naRank = item.naRank;

            apiToolSectionItem.case1FieldValue = item.case1FieldValue;
            apiToolSectionItem.case2FieldValue = item.case2FieldValue;
            apiToolSectionItem.case3FieldValue = item.case3FieldValue;
            apiToolSectionItem.case4FieldValue = item.case4FieldValue;
            apiToolSectionItem.case5FieldValue = item.case5FieldValue;
            apiToolSectionItem.case6FieldValue = item.case6FieldValue;
            apiToolSectionItem.case7FieldValue = item.case7FieldValue;
            apiToolSectionItem.case8FieldValue = item.case8FieldValue;
            apiToolSectionItem.case9FieldValue = item.case9FieldValue;
            apiToolSectionItem.case10FieldValue = item.case10FieldValue;
            return apiToolSectionItem;

        }

        private static ApiPmCompProcessToolSection getApiPmCompProcessToolSection(string toolRef, SectionEvaluationCpt section)
        {
            ApiPmCompProcessToolSection apiToolSection = new ApiPmCompProcessToolSection();
            apiToolSection.sectionEmisId = section.sectionEmisId;
            apiToolSection.toolReference = toolRef;
            apiToolSection.name = section.name;

            apiToolSection.case1Yes = section.caseScores.case1Yes;
            apiToolSection.case1No = section.caseScores.case1No;
            apiToolSection.case1AvgPercent = section.caseScores.case1AvgPercent;

            apiToolSection.case2Yes = section.caseScores.case2Yes;
            apiToolSection.case2No = section.caseScores.case2No;
            apiToolSection.case2AvgPercent = section.caseScores.case2AvgPercent;

            apiToolSection.case3Yes = section.caseScores.case3Yes;
            apiToolSection.case3No = section.caseScores.case3No;
            apiToolSection.case3AvgPercent = section.caseScores.case3AvgPercent;

            apiToolSection.case4Yes = section.caseScores.case4Yes;
            apiToolSection.case4No = section.caseScores.case4No;
            apiToolSection.case4AvgPercent = section.caseScores.case4AvgPercent;

            apiToolSection.case5Yes = section.caseScores.case5Yes;
            apiToolSection.case5No = section.caseScores.case5No;
            apiToolSection.case5AvgPercent = section.caseScores.case5AvgPercent;

            apiToolSection.case6Yes = section.caseScores.case6Yes;
            apiToolSection.case6No = section.caseScores.case6No;
            apiToolSection.case6AvgPercent = section.caseScores.case6AvgPercent;

            apiToolSection.case7Yes = section.caseScores.case7Yes;
            apiToolSection.case7No = section.caseScores.case7No;
            apiToolSection.case7AvgPercent = section.caseScores.case7AvgPercent;

            apiToolSection.case8Yes = section.caseScores.case8Yes;
            apiToolSection.case8No = section.caseScores.case8No;
            apiToolSection.case8AvgPercent = section.caseScores.case8AvgPercent;

            apiToolSection.case9Yes = section.caseScores.case9Yes;
            apiToolSection.case9No = section.caseScores.case9No;
            apiToolSection.case9AvgPercent = section.caseScores.case9AvgPercent;

            apiToolSection.case10Yes = section.caseScores.case10Yes;
            apiToolSection.case10No = section.caseScores.case10No;
            apiToolSection.case10AvgPercent = section.caseScores.case10AvgPercent;
            return apiToolSection;
        }

        private void apiDataSavePmCompAnalysisData(string toolRef)
        {

            ApiPmCompProcessToolAnalysis data = new ApiPmCompProcessToolAnalysis();
            data.toolReference = toolRef;

            data.RAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvRAvgScore");
            data.RCase10 = WidgetHandler.findCtlValByCtlName(this, "txvRCase10");
            data.RCase9 = WidgetHandler.findCtlValByCtlName(this, "txvRCase9");
            data.RCase8 = WidgetHandler.findCtlValByCtlName(this, "txvRCase8");
            data.RCase7 = WidgetHandler.findCtlValByCtlName(this, "txvRCase7");
            data.RCase6 = WidgetHandler.findCtlValByCtlName(this, "txvRCase6");
            data.RCase5 = WidgetHandler.findCtlValByCtlName(this, "txvRCase5");
            data.RCase4 = WidgetHandler.findCtlValByCtlName(this, "txvRCase4");
            data.RCase3 = WidgetHandler.findCtlValByCtlName(this, "txvRCase3");
            data.RCase2 = WidgetHandler.findCtlValByCtlName(this, "txvRCase2");
            data.RCase1 = WidgetHandler.findCtlValByCtlName(this, "txvRCase1");

            data.CMAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvCMAvgScore");
            data.CMCase10 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase10");
            data.CMCase9 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase9");
            data.CMCase8 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase8");
            data.CMCase7 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase7");
            data.CMCase6 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase6");
            data.CMCase5 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase5");
            data.CMCase4 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase4");
            data.CMCase3 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase3");
            data.CMCase2 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase2");
            data.CMCase1 = WidgetHandler.findCtlValByCtlName(this, "txvCMCase1");

            data.CAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvCAvgScore");
            data.CCase10 = WidgetHandler.findCtlValByCtlName(this, "txvCCase10");
            data.CCase9 = WidgetHandler.findCtlValByCtlName(this, "txvCCase9");
            data.CCase8 = WidgetHandler.findCtlValByCtlName(this, "txvCCase8");
            data.CCase7 = WidgetHandler.findCtlValByCtlName(this, "txvCCase7");
            data.CCase6 = WidgetHandler.findCtlValByCtlName(this, "txvCCase6");
            data.CCase5 = WidgetHandler.findCtlValByCtlName(this, "txvCCase5");
            data.CCase4 = WidgetHandler.findCtlValByCtlName(this, "txvCCase4");
            data.CCase3 = WidgetHandler.findCtlValByCtlName(this, "txvCCase3");
            data.CCase2 = WidgetHandler.findCtlValByCtlName(this, "txvCCase2");
            data.CCase1 = WidgetHandler.findCtlValByCtlName(this, "txvCCase1");

            data.EAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvEAvgScore");
            data.ECase10 = WidgetHandler.findCtlValByCtlName(this, "txvECase10");
            data.ECase9 = WidgetHandler.findCtlValByCtlName(this, "txvECase9");
            data.ECase8 = WidgetHandler.findCtlValByCtlName(this, "txvECase8");
            data.ECase7 = WidgetHandler.findCtlValByCtlName(this, "txvECase7");
            data.ECase6 = WidgetHandler.findCtlValByCtlName(this, "txvECase6");
            data.ECase5 = WidgetHandler.findCtlValByCtlName(this, "txvECase5");
            data.ECase4 = WidgetHandler.findCtlValByCtlName(this, "txvECase4");
            data.ECase3 = WidgetHandler.findCtlValByCtlName(this, "txvECase3");
            data.ECase2 = WidgetHandler.findCtlValByCtlName(this, "txvECase2");
            data.ECase1 = WidgetHandler.findCtlValByCtlName(this, "txvECase1");

            data.BAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvBAvgScore");
            data.BCase10 = WidgetHandler.findCtlValByCtlName(this, "txvBCase10");
            data.BCase9 = WidgetHandler.findCtlValByCtlName(this, "txvBCase9");
            data.BCase8 = WidgetHandler.findCtlValByCtlName(this, "txvBCase8");
            data.BCase7 = WidgetHandler.findCtlValByCtlName(this, "txvBCase7");
            data.BCase6 = WidgetHandler.findCtlValByCtlName(this, "txvBCase6");
            data.BCase5 = WidgetHandler.findCtlValByCtlName(this, "txvBCase5");
            data.BCase4 = WidgetHandler.findCtlValByCtlName(this, "txvBCase4");
            data.BCase3 = WidgetHandler.findCtlValByCtlName(this, "txvBCase3");
            data.BCase2 = WidgetHandler.findCtlValByCtlName(this, "txvBCase2");
            data.BCase1 = WidgetHandler.findCtlValByCtlName(this, "txvBCase1");

            data.BDAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvBDAvgScore");
            data.BDCase10 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase10");
            data.BDCase9 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase9");
            data.BDCase8 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase8");
            data.BDCase7 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase7");
            data.BDCase6 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase6");
            data.BDCase5 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase5");
            data.BDCase4 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase4");
            data.BDCase3 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase3");
            data.BDCase2 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase2");
            data.BDCase1 = WidgetHandler.findCtlValByCtlName(this, "txvBDCase1");

            data.PIAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvPIAvgScore");
            data.PICase10 = WidgetHandler.findCtlValByCtlName(this, "txvPICase10");
            data.PICase9 = WidgetHandler.findCtlValByCtlName(this, "txvPICase9");
            data.PICase8 = WidgetHandler.findCtlValByCtlName(this, "txvPICase8");
            data.PICase7 = WidgetHandler.findCtlValByCtlName(this, "txvPICase7");
            data.PICase6 = WidgetHandler.findCtlValByCtlName(this, "txvPICase6");
            data.PICase5 = WidgetHandler.findCtlValByCtlName(this, "txvPICase5");
            data.PICase4 = WidgetHandler.findCtlValByCtlName(this, "txvPICase4");
            data.PICase3 = WidgetHandler.findCtlValByCtlName(this, "txvPICase3");
            data.PICase2 = WidgetHandler.findCtlValByCtlName(this, "txvPICase2");
            data.PICase1 = WidgetHandler.findCtlValByCtlName(this, "txvPICase1");

            data.PSAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvPSAvgScore");

            data.AvgAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvAvgAvgScore");

            data.AvgCase1 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase1");
            data.AvgCase2 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase2");
            data.AvgCase3 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase3");
            data.AvgCase4 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase4");
            data.AvgCase5 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase5");
            data.AvgCase6 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase6");
            data.AvgCase7 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase7");
            data.AvgCase8 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase8");
            data.AvgCase9 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase9");
            data.AvgCase10 = WidgetHandler.findCtlValByCtlName(this, "txvAvgCase10");

            data.MirrorPIAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvMirrorPIAvgScore");
            data.MirrorBDAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvMirrorBDAvgScore");
            data.MirrorBAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvMirrorBAvgScore");
            data.MirrorEAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvMirrorEAvgScore");
            data.MirrorCAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvMirrorCAvgScore");
            data.MirrorCMAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvMirrorCMAvgScore");
            data.MirrorRAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvMirrorRAvgScore");
            data.MirrorPSAvgScore = WidgetHandler.findCtlValByCtlName(this, "txvMirrorPSAvgScore");

            new DatabaseHandler().saveApiPmCompProcessToolAnayisis(DatabaseHandler.dbConnection(), data);
            
        }

        private bool apiDataSavePmCompProcessTool(string toolName, string toolRef, out string message)
        {

            DatabaseHandler db = new DatabaseHandler();
            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();

            if (db.compProcessToolNameExists(sQLiteConnection, toolName))
            {
                message = "Tool with name [" + toolName + "] already exists.";
                return false;
            }
            
            ApiPmCompProcessTool tool = new ApiPmCompProcessTool();
            tool.toolName = toolName;
            tool.toolReference = toolRef;
            tool.activityId = activityId;
            tool.sampleFileLocalId = sampleFileLocalId;
            tool.toolType = AppConstants.PREFIX_CPT;

            String dateTimeNow = DateTime.Now.ToString(Globals.DATE_FORMAT);
            tool.createdOn = dateTimeNow;
            tool.updatedOn = dateTimeNow;

            //save the tool
            db.savePmCpt(sQLiteConnection, tool);

            message = "SUCCESS";
            return true;

        }

        public void updateCptSectionScoresInAnalyisTab(SectionEvaluationCpt section)
        {
            
        }

        public void autofillCptAnalysisTabTextFieldBasedOnEvalItemValue(SectionItemEvaluationCpt sectionItem)
        {
           
        }

        public void autoFillAverageScoresTab(object sender, EventArgs e)
        {

            Control control = (Control)sender;
            //first i am going to search for this control, hopefully i find

            foreach (KeyValuePair<long, CaseScoresControls> entry in CPT_SECTION_TOTAL_TXVS)
            {

                // do something with entry.Value or entry.Key
                var item = entry.Value;
                if (item.case1 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase1(sectionName, value);
                }
                else if (item.case2 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase2(sectionName, value);
                }
                else if (item.case3 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase3(sectionName, value);
                }
                else if (item.case4 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase4(sectionName, value);
                }
                else if (item.case5 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase5(sectionName, value);
                }
                else if (item.case6 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase6(sectionName, value);
                }
                else if (item.case7 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase7(sectionName, value);
                }
                else if (item.case8 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase8(sectionName, value);
                }
                else if (item.case9 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase9(sectionName, value);
                }
                else if (item.case10 == control)
                {
                    var section = AppStateCompliance.findSectionEvaluationCptById(entry.Key);
                    String sectionName = section == null ? "" : section.name;
                    String value = control.Text;
                    autoFillAverageScoreForSectionCase10(sectionName, value);
                }

            }

        }

        private void autoFillAverageScoreForSectionCase1(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase1";
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase1"; 
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase1";  
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase1";  
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase1";  
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase1"; 
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase1";
            }

            if(!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }
        private void autoFillAverageScoreForSectionCase2(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase2";  //txvPICase2.Text = value;
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase2";  //txvBDCase2.Text = value;
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase2";  // txvBCase2.Text = value;
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase2";  // txvECase2.Text = value;
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase2";  //txvCCase2.Text = value;
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase2";  //txvCMCase2.Text = value;
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase2";  //txvRCase2.Text = value;
            }

            if (!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }
        private void autoFillAverageScoreForSectionCase3(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase3";  //txvPICase3.Text = value;
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase3";  //txvBDCase3.Text = value;
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase3";  //txvBCase3.Text = value;
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase3";  //txvECase3.Text = value;
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase3";  //txvCCase3.Text = value;
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase3";  //txvCMCase3.Text = value;
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase3";  //txvRCase3.Text = value;
            }

            if (!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }
        private void autoFillAverageScoreForSectionCase4(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase4";  //txvPICase4.Text = value;
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase4";  //txvBDCase4.Text = value;
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase4";  //txvBCase4.Text = value;
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase4";  //txvECase4.Text = value;
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase4";  //txvCCase4.Text = value;
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase4";  //txvCMCase4.Text = value;
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase4";  //txvRCase4.Text = value;
            }

            if (!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }
        private void autoFillAverageScoreForSectionCase5(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase5";  //txvPICase5.Text = value;
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase5";  //txvBDCase5.Text = value;
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase5";  //txvBCase5.Text = value;
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase5";  //txvECase5.Text = value;
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase5";  //txvCCase5.Text = value;
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase5";  //txvCMCase5.Text = value;
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase5";  //txvRCase5.Text = value;
            }

            if (!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }
        private void autoFillAverageScoreForSectionCase6(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase6"; //txvPICase6.Text = value;
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase6"; //txvBDCase6.Text = value;
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase6"; //txvBCase6.Text = value;
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase6"; //txvECase6.Text = value;
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase6"; //txvCCase6.Text = value;
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase6"; //txvCMCase6.Text = value;
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase6"; //txvRCase6.Text = value;
            }

            if (!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }
        private void autoFillAverageScoreForSectionCase7(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase7"; //txvPICase7.Text = value;
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase7"; //txvBDCase7.Text = value;
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase7"; //txvBCase7.Text = value;
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase7"; //txvECase7.Text = value;
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase7"; //txvCCase7.Text = value;
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase7"; //txvCMCase7.Text = value;
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase7"; //txvRCase7.Text = value;
            }

            if (!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }
        private void autoFillAverageScoreForSectionCase8(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase8"; //txvPICase8.Text = value;
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase8"; //txvBDCase8.Text = value;
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase8"; //txvBCase8.Text = value;
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase8"; //txvECase8.Text = value;
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase8"; //txvCCase8.Text = value;
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase8"; //txvCMCase8.Text = value;
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase8"; //txvRCase8.Text = value;
            }

            if (!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }
        private void autoFillAverageScoreForSectionCase9(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase9"; //txvPICase9.Text = value;
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase9"; //txvBDCase9.Text = value;
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase9"; //txvBCase9.Text = value;
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase9"; //txvECase9.Text = value;
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase9"; //txvCCase9.Text = value;
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase9"; //txvCMCase9.Text = value;
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase9"; //txvRCase9.Text = value;
            }

            if (!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }
        private void autoFillAverageScoreForSectionCase10(string sectionName, string value)
        {

            if (String.IsNullOrEmpty(sectionName))
            {
                return;
            }

            sectionName = sectionName.ToUpper();
            String controlName = "";

            if (sectionName == "PLANNING & INITIATION".ToUpper())
            {
                controlName = "txvPICase10"; //txvPICase10.Text = value;
            }
            else if (sectionName == "BIDDING DOCUMENT".ToUpper())
            {
                controlName = "txvBDCase10"; //txvBDCase10.Text = value;
            }
            else if (sectionName == "BIDDING".ToUpper())
            {
                controlName = "txvBCase10"; //txvBCase10.Text = value;
            }
            else if (sectionName == "EVALUATION".ToUpper())
            {
                controlName = "txvECase10"; //txvECase10.Text = value;
            }
            else if (sectionName == "CONTRACTING".ToUpper())
            {
                controlName = "txvCCase10"; //txvCCase10.Text = value;
            }
            else if (sectionName == "CONTRACT MANAGEMENT".ToUpper())
            {
                controlName = "txvCMCase10"; //txvCMCase10.Text = value;
            }
            else if (sectionName == "RECORDS".ToUpper())
            {
                controlName = "txvRCase10"; //txvRCase10.Text = value;
            }

            if (!String.IsNullOrEmpty(controlName))
            {
                WidgetHandler.findAndFillCtlValByCtlName(this, controlName, value);
            }

        }


        public void calculateAveragePlanningAndInitiation(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase1", "txvPICase2", "txvPICase3", "txvPICase4", "txvPICase5", "txvPICase6", "txvPICase7", "txvPICase8", "txvPICase9", "txvPICase10" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvPIAvgScore"), WidgetHandler.findControlByName(this, "txvMirrorPIAvgScore"));

        }
        public void calculateAverageBiddingDocument(object sender, EventArgs e)
        {
            var list = new List<String> { "txvBDCase1", "txvBDCase2", "txvBDCase3", "txvBDCase4", "txvBDCase5", "txvBDCase6", "txvBDCase7", "txvBDCase8", "txvBDCase9", "txvBDCase10" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvBDAvgScore"), WidgetHandler.findControlByName(this, "txvMirrorBDAvgScore"));

        }
        public void calculateAverageBidding(object sender, EventArgs e)
        {
            var list = new List<String> { "txvBCase1", "txvBCase2", "txvBCase3", "txvBCase4", "txvBCase5", "txvBCase6", "txvBCase7", "txvBCase8", "txvBCase9", "txvBCase10" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvBAvgScore"), WidgetHandler.findControlByName(this, "txvMirrorBAvgScore"));

        }
        public void calculateAverageEvaluation(object sender, EventArgs e)
        {
            var list = new List<String> {"txvECase1","txvECase2","txvECase3","txvECase4","txvECase5","txvECase6","txvECase7","txvECase8","txvECase9","txvECase10" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvEAvgScore"), WidgetHandler.findControlByName(this, "txvMirrorEAvgScore") );

        }
        public void calculateAverageContracting(object sender, EventArgs e)
        {
            var list = new List<String> { "txvCCase1", "txvCCase2", "txvCCase3", "txvCCase4", "txvCCase5", "txvCCase6", "txvCCase7", "txvCCase8", "txvCCase9", "txvCCase10" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvCAvgScore"), WidgetHandler.findControlByName(this, "txvMirrorCAvgScore"));
        }
        public void calculateAverageContractMangt(object sender, EventArgs e)
        {
            var list = new List<String> { "txvCMCase1", "txvCMCase2", "txvCMCase3", "txvCMCase4", "txvCMCase5", "txvCMCase6", "txvCMCase7", "txvCMCase8", "txvCMCase9", "txvCMCase10" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvCMAvgScore"), WidgetHandler.findControlByName(this, "txvMirrorCMAvgScore"));
        }
        public void calculateAverageRecords(object sender, EventArgs e)
        {
            var list = new List<String> { "txvRCase1", "txvRCase2", "txvRCase3", "txvRCase4", "txvRCase5", "txvRCase6", "txvRCase7", "txvRCase8", "txvRCase9", "txvRCase10" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvRAvgScore"), WidgetHandler.findControlByName(this, "txvMirrorRAvgScore"));
        }
        

        public void calculateAverageCase1(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase1", "txvBDCase1", "txvBCase1", "txvECase1", "txvCCase1", "txvCMCase1", "txvRCase1" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase1"));
        }
        public void calculateAverageCase2(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase2", "txvBDCase2", "txvBCase2", "txvECase2", "txvCCase2", "txvCMCase2", "txvRCase2" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase2") );
        }
        public void calculateAverageCase3(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase3", "txvBDCase3", "txvBCase3", "txvECase3", "txvCCase3", "txvCMCase3", "txvRCase3" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase3"));

        }
        public void calculateAverageCase4(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase4", "txvBDCase4", "txvBCase4", "txvECase4", "txvCCase4", "txvCMCase4", "txvRCase4" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase4"));
        }
        public void calculateAverageCase5(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase5", "txvBDCase5", "txvBCase5", "txvECase5", "txvCCase5", "txvCMCase5", "txvRCase5" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase5"));
        }
        public void calculateAverageCase6(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase6", "txvBDCase6", "txvBCase6", "txvECase6", "txvCCase6", "txvCMCase6", "txvRCase6" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase6"));
        }
        public void calculateAverageCase7(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase7", "txvBDCase7", "txvBCase7", "txvECase7", "txvCCase7", "txvCMCase7", "txvRCase7" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase7") );
        }
        public void calculateAverageCase8(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase8", "txvBDCase8", "txvBCase8", "txvECase8", "txvCCase8", "txvCMCase8", "txvRCase8" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase8"));
        }
        public void calculateAverageCase9(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase9", "txvBDCase9", "txvBCase9", "txvECase9", "txvCCase9", "txvCMCase9", "txvRCase9" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase9") );
        }
        public void calculateAverageCase10(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPICase10", "txvBDCase10", "txvBCase10", "txvECase10", "txvCCase10", "txvCMCase10", "txvRCase10" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgCase10") );
        }



        public void calculateAvgOfSectionAvgScores(object sender, EventArgs e)
        {
            var list = new List<String> { "txvPIAvgScore", "txvBDAvgScore", "txvBAvgScore", "txvEAvgScore", "txvCAvgScore", "txvCMAvgScore", "txvRAvgScore" };

            List<String> values = new List<string>();
            foreach (var item in list) { String value = WidgetHandler.findCtlValByCtlName(this, item); values.Add(value); }

            new CalculationHelper().calculateAverageExcludingNullAndEmpty(values, WidgetHandler.findControlByName(this, "txvAvgAvgScore"));
        }

    }
}

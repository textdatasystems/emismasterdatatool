﻿using Processor.ControlObjects;
using Processor.Entities.PM.systems;
using System;
using System.Windows.Forms;

namespace EmisTool.Code.ui.custom
{

    internal class AccordionPanelAuditSystems : AccordionPanel
    {

        public AccordionPanelAuditSystems(AppStateAuditTools appState)
        {

            DatabaseHandler databaseHandler = new DatabaseHandler();
            SQLite.SQLiteConnection dbConn = DatabaseHandler.dbConnection();

            //reset app state to remove old data
            AppStateAuditTools.auditSystemsToolSectionEvaluations.Clear();
            //reset the static value of the field count
            UiHelperAst.FieldCountPmAuditProcessTool = 0;


            //users
            var users = databaseHandler.allUsers(dbConn);

            //identification section         
            var section1 = UiHelperAst.GenerateIdentificationSectionTable(users);
            String sectionHeader = "IDENTIFICATION";
            accordion.Add(section1, sectionHeader, null, 1, false);

            //dynamic sections            
            var sections = databaseHandler.getPmAuditSystemToolSections(dbConn);

            var mgtLetterSections = databaseHandler.getManagementLetterSections(dbConn);
                      

            foreach (var section in sections)
            {

                //will hold the state for this section
                SectionEvaluationAst sectionEvaluation = null;
                TableLayoutPanel sectionTable = UiHelperAst.GenerateTableV1(section, out sectionEvaluation, appState, mgtLetterSections);

                String header = section.name;
                accordion.Add(sectionTable, header, null, 1, false);

                //check if section does not exists in app state
                if (!AppStateAuditTools.sectionEvaluationAstExists(sectionEvaluation.sectionEmisId) && sectionEvaluation.sectionEmisId != 0)
                {
                    AppStateAuditTools.auditSystemsToolSectionEvaluations.Add(sectionEvaluation);
                }

            }

            //add section 13
            var section13 = UiHelperAst.GenerateSection13Table();
            String section13Header = "SUMMARY OF KEY FINDINGS, CONCLUSIONS, AND RECOMMENDATIONS";
            accordion.Add(section13, section13Header, null, 1, false);
            
        }

    }


}

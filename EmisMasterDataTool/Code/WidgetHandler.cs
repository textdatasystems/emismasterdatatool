﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Processor.Entities;
using Processor.ControlObjects;
using Processor.Entities.custom;
using System.Drawing;
using EmisTool.Code;

namespace EmisMasterDataTool.Code
{
    public class WidgetHandler
    {
        internal static void populateFinancialYearsDropdown(ComboBox comboxBox, List<FinacialYear> financialYears)
        {

            List<FinacialYear> years = new List<FinacialYear>();
            FinacialYear year = new FinacialYear(); year.financial_year = "Select Financial Year";

            years.Add(year);
            years.AddRange(financialYears);

            comboxBox.DataSource = years;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "financial_year";

        }

        internal static void populateCbPlansDropdown(ComboBox comboxBox, List<CbPlan> plans)
        {

            List<CbPlan> updatedPlans = new List<CbPlan>();

            CbPlan plan = new CbPlan();
            plan.displayName = "Select Plan";
            updatedPlans.Add(plan);

            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();
            foreach (var item in plans)
            {                
                var year = sQLiteConnection.Table<FinacialYear>().Where(v => v.emisId == item.financial_year_id).FirstOrDefault();
                item.displayName = year == null ? "Plan " + item.emisId : year.financial_year + " Plan " + item.emisId;
                updatedPlans.Add(item);
            }
            
            comboxBox.DataSource = updatedPlans;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "displayName";

        }

        internal static void populateEntities(ComboBox comboxBox, List<Entity> entities)
        {
            List<Entity> updatedEntities = new List<Entity>();
            Entity entity = new Entity(); entity.entityName = "Select entity";

            updatedEntities.Add(entity);
            updatedEntities.AddRange(entities);

            comboxBox.DataSource = updatedEntities;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "entityName";

            comboxBox.SelectedIndex = 0;
        }


        internal static void populateUsersCombo(ComboBox comboxBox, List<User> users)
        {

            List<User> updatedUsers = new List<User>();
            User user = new User(); user.emisId = 0; user.first_name = "Select user";

            updatedUsers.Add(user);
            updatedUsers.AddRange(users);

            comboxBox.DataSource = updatedUsers;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "FullName";

            comboxBox.SelectedIndex = 0;

        }


        internal static ComboBox populateUsersWithEmailValueCombo(ComboBox comboxBox, List<User> users)
        {

            List<User> updatedUsers = new List<User>();
            User user = new User(); user.emisId = 0; user.first_name = "Select user";

            updatedUsers.Add(user);
            updatedUsers.AddRange(users);

            comboxBox.DataSource = updatedUsers;
            comboxBox.ValueMember = "email";
            comboxBox.DisplayMember = "FullName";            
            comboxBox.DropDownStyle = ComboBoxStyle.DropDownList;

            return comboxBox;

        }

        internal static void populatePeoplesCombo(ComboBox comboxBox, List<People> people)
        {

            List<People> updatedPeople = new List<People>();
            People person = new People(); person.emisId = 0; person.firstName = "Select person";

            updatedPeople.Add(person);
            updatedPeople.AddRange(people);

            comboxBox.DataSource = updatedPeople;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "FullName";

            comboxBox.SelectedIndex = 0;

        }

        internal static void populatePmAuditMeetingRoleCombo(ComboBox comboxBox, List<PmAuditMeetingRole> roles)
        {

            List<PmAuditMeetingRole> updatedRoles = new List<PmAuditMeetingRole>();
            PmAuditMeetingRole role = new PmAuditMeetingRole(); role.emisId = 0; role.role_name = "Select role";

            updatedRoles.Add(role);
            updatedRoles.AddRange(roles);

            comboxBox.DataSource = updatedRoles;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "role_name";

            comboxBox.SelectedIndex = 0;

        }

        internal static void populateAttendeTypeCombo(ComboBox comboxBox, List<AttendeeType> types)
        {

            List<AttendeeType> updatedTypes = new List<AttendeeType>();
            AttendeeType attendeeType = new AttendeeType("None","Select attendee type");

            updatedTypes.Add(attendeeType);
            updatedTypes.AddRange(types);

            comboxBox.DataSource = updatedTypes;
            comboxBox.ValueMember = "commentableType";
            comboxBox.DisplayMember = "displayValue";

            comboxBox.SelectedIndex = 0;

        }

        internal static void populateProcurementRolesDropdown(ComboBox comboxBox, List<ProcurementRole> roles)
        {
            List<ProcurementRole> updatedRoles = new List<ProcurementRole>();
            ProcurementRole role = new ProcurementRole(0,"Select procurement role");

            updatedRoles.Add(role);
            updatedRoles.AddRange(roles);

            comboxBox.DataSource = updatedRoles;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "role";

            comboxBox.SelectedIndex = 0;

        }

        internal static void populatePmPlansDropdown(ComboBox comboxBox, List<PmPlan> plans)
        {

            List<PmPlan> updatedPlans = new List<PmPlan>();

            PmPlan plan = new PmPlan();
            plan.displayName = "Select Plan";
            updatedPlans.Add(plan);

            SQLite.SQLiteConnection sQLiteConnection = DatabaseHandler.dbConnection();
            foreach (var item in plans)
            {
                var year = sQLiteConnection.Table<FinacialYear>().Where(v => v.emisId == item.financial_year_id).FirstOrDefault();
                item.displayName = year == null ? "Plan " + item.emisId : year.financial_year + " Plan " + item.emisId;
                updatedPlans.Add(item);
            }

            comboxBox.DataSource = updatedPlans;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "displayName";

        }



        internal static void populateDistrictsDropdown(ComboBox comboxBox, List<District> districts)
        {
            List<District> districtsUpdated = new List<District>();
            District district = new District(); district.districtName = "Select District";

            districtsUpdated.Add(district);
            districtsUpdated.AddRange(districts);

            comboxBox.DataSource = districtsUpdated;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "districtName";
        }



        internal static ComboBox populateManagementLetterSectionsDropdown(ComboBox comboxBox, List<ManagementLetterSection> managementLetterSections)
        {

            List<ManagementLetterSection> sectionsUpdated = new List<ManagementLetterSection>();
            ManagementLetterSection section = new ManagementLetterSection();
            section.emisId = 0;
            section.section_title = "Select Option";

            sectionsUpdated.Add(section);
            sectionsUpdated.AddRange(managementLetterSections);

            comboxBox.DataSource = sectionsUpdated;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "section_title";

            return comboxBox;

        }

        internal static void populateManagementLetterSectionExceptionsDropdown(ComboBox comboxBox, List<ManagementLetterSectionException> exceptions)
        {
            List<ManagementLetterSectionException> exceptionsUpdated = new List<ManagementLetterSectionException>();
            ManagementLetterSectionException exception = new ManagementLetterSectionException();
            exception.emisId = 0;
            exception.exception_title = "Select Option";

            exceptionsUpdated.Add(exception);
            exceptionsUpdated.AddRange(exceptions);

            comboxBox.DataSource = exceptionsUpdated;
            comboxBox.ValueMember = "emisId";
            comboxBox.DisplayMember = "exception_title";
        }

        internal static void populateDropdownWithStringData(ComboBox comboxBox, List<String> dataList, String prompt = "Select Option")
        {

            List<String> updatedDataList = new List<String>();
            updatedDataList.Add(prompt);          
            updatedDataList.AddRange(dataList);
            comboxBox.DataSource = updatedDataList;

        }

        internal static void populateBudgetItemAmounts(List<CustomBudgetItemAmount> items, ListView listView)
        {

            listView.View = View.Details;

            listView.Columns.Add("#", 5);
            listView.Columns.Add("Item");
            listView.Columns.Add("Unit Price");
            listView.Columns.Add("Quantity");
            listView.Columns.Add("Amount");
            listView.Columns.Add("Note");
            listView.Columns[0].ListView.Font = new System.Drawing.Font(listView.Columns[0].ListView.Font, System.Drawing.FontStyle.Bold);

            foreach (ColumnHeader column in listView.Columns) { column.Width = -2; }

            double total = 0;

            for (int count = 0; count < items.Count; count++)
            {

                CustomBudgetItemAmount data = items[count];
                string[] itemData = new[] { (count + 1).ToString(), data.itemName, data.unit_price.ToString(), data.quantity.ToString(), data.amount.ToString(), data.note };
                ListViewItem listViewItem = new ListViewItem(itemData);
                listViewItem.Font = new System.Drawing.Font(listViewItem.Font, System.Drawing.FontStyle.Regular);
                listView.Items.Add(listViewItem);

                total += data.amount;

            }

            //spacer
            listView.Items.Add(new ListViewItem(new[] { "", "", "", "", "", "" }));

            string[] totalRecord = new[] { "", "Total", "", "", total.ToString(), "" };
            listView.Items.Add(new ListViewItem(totalRecord));

            listView.GridLines = true;            

        }


        internal static void populateExpectedOutcomes(List<CbExpectedOutcome> items, ListView listView)
        {

            listView.View = View.Details;

            listView.Columns.Add("#", 5);
            listView.Columns.Add("Outcome");

            foreach (ColumnHeader column in listView.Columns) { column.Width = -2; }
            
            for (int count = 0; count < items.Count; count++)
            {

                CbExpectedOutcome data = items[count];
                string[] itemData = new[] { (count + 1).ToString(), data.expected_outcome};
                listView.Items.Add(new ListViewItem(itemData));
                
            }
            
            listView.GridLines = true;

        }

        public static void showMessage(string message, bool isError = true, IWin32Window owner = null)
        {

            if (isError)
            {
                if(owner == null)
                {
                    DialogResult res = MessageBox.Show(message, "Operation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DialogResult res = MessageBox.Show(owner,message, "Operation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                

            }
            else
            {
                if (owner == null)
                {
                    DialogResult res = MessageBox.Show(message, "Operation Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DialogResult res = MessageBox.Show(owner, message, "Operation Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }

        }


        public static Control findControlByName(Control parentControl, String controlName)
        {

            var controlArr = parentControl.Controls.Find(controlName, true);
            if (controlArr != null && controlArr.Length > 0)
            {
                return controlArr[0];
            }
            return null;

        }

        public static String findCtlValByCtlName(Control parentControl, String controlName)
        {

            var controlArr = parentControl.Controls.Find(controlName, true);
            if (controlArr != null && controlArr.Length > 0)
            {
                return controlArr[0].Text;
            }
            return null;

        }
              

        public static void findAndFillCtlValByCtlName(Control parentControl, String controlName, String value)
        {

            var controlArr = parentControl.Controls.Find(controlName, true);
            if (controlArr != null && controlArr.Length > 0)
            {

                Control control = controlArr[0];
                if (control is ComboBox)
                {
                    ComboBox cbx = (ComboBox)control;
                    cbx.SelectedIndex = String.IsNullOrEmpty(value) ? 0 : cbx.FindString(value);

                    if(cbx.SelectedIndex == -1)
                    {
                        cbx.SelectedValue = value;
                    }

                }
                else if (control is Panel)
                {
                    //for yesnona responses, the radio buttons are in panel
                    var checkedButton = control.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Text == value);
                    if (checkedButton != null) { checkedButton.Checked = true; }
                }
                else
                {
                    control.Text = value;
                }
                
            }

        }


        public static void findAndFillComboBoxValByCtlName(Control parentControl, String controlName, long value)
        {

            var controlArr = parentControl.Controls.Find(controlName, true);
            if (controlArr != null && controlArr.Length > 0)
            {
                var control = controlArr[0];
                (control as ComboBox).SelectedValue = int.Parse(value.ToString());                              
            
            }

        }


        public static void showModal(Form formToShowAsModal, IWin32Window owner, bool open = true)
        {

            if (!open)
            {

                if (formToShowAsModal.Visible)
                {
                    formToShowAsModal.Close();
                }
                return;

            }


            formToShowAsModal.StartPosition = FormStartPosition.CenterParent;

            // Show dialog as a modal dialog and determine if DialogResult = OK.
            if (formToShowAsModal.ShowDialog(owner) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
            }
            else
            {
            }

            formToShowAsModal.Dispose();

        }

        public static Bitmap ConvertFromIconToBitmap(Icon icon, Size size)
        {

            Bitmap bmp = new Bitmap(size.Width, size.Height);
            using (Graphics gp = Graphics.FromImage(bmp))
            {
                gp.Clear(Color.Transparent);
                gp.DrawIcon(icon, new Rectangle(0, 0, size.Width, size.Height));
            }
            return bmp;
        }



    }

}

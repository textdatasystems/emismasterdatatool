﻿using Processor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmisMasterDataTool.Code
{
    public interface IInnerDialogCustomAddPerson
    {
        void personSuccessfullyCreated(People person);

    }

}

﻿using EmisMasterDataTool.Code;
using Processor.ControlObjects;
using Processor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmisMasterDataTool
{
    public partial class SubDialogAddBranch : Form
    {
        public ISubDialogAddBranch iSubDialogAddBranch { get; set; }

        private int entityEmisId { get; set; }
        public SubDialogAddBranch(int entityEmisId)
        {
            InitializeComponent();
            this.entityEmisId = entityEmisId;

            loadData();

        }

        private void loadData()
        {
            populateDistrictsDropDown();
        }

        private void populateDistrictsDropDown()
        {
            var districts = new DatabaseHandler().allDistricts(DatabaseHandler.dbConnection());
            WidgetHandler.populateDistrictsDropdown(cbxDistricts, districts);            
        }
            
        private void button1_Click_1(object sender, EventArgs e)
        {

            if (!validInput())
            {
                return;
            }

            String branchName = txvBranchName.Text;
            String town = txvTown.Text;
            int district = (int)cbxDistricts.SelectedValue;

            Branch branch = new Branch();
            branch.entityEmisId = entityEmisId;
            branch.branchName = branchName;
            branch.town = town;
            branch.districtEmisId = district;

            DatabaseHandler.addBranch(DatabaseHandler.dbConnection(), branch);

            //clear the fields
            clearFields();

            //show message
            WidgetHandler.showMessage("Branch successfully created", false);

            //alert parent
            iSubDialogAddBranch.branchSuccessfullyAdded();

        }

        private void clearFields()
        {
            txvBranchName.Text = "";
            txvTown.Text = "";
            cbxDistricts.SelectedIndex = 0;
        }

        private bool validInput()
        {
            String branchName = txvBranchName.Text;
            String town = txvTown.Text;
            int districtIndex = cbxDistricts.SelectedIndex;

            if (string.IsNullOrEmpty(branchName))
            {
                WidgetHandler.showMessage("Please enter the branch name", true);
                return false;
            }

            if (string.IsNullOrEmpty(town))
            {
                WidgetHandler.showMessage("Please enter the town", true);
                return false;
            }

            if (districtIndex == 0)
            {
                WidgetHandler.showMessage("Please a district", true);
                return false;
            }

            return true;

        }
    }
}

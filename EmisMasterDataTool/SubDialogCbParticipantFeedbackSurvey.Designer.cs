﻿namespace EmisMasterDataTool
{
    partial class SubDialogCbParticipantFeedbackSurvey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubDialogCbParticipantFeedbackSurvey));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOpenFileUploadForm = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Participant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Entity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActivityScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrainerScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToolScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.btnOpenFileUploadForm);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(1022, 474);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "PARTICIPANT SURVEY";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnOpenFileUploadForm
            // 
            this.btnOpenFileUploadForm.Location = new System.Drawing.Point(13, 34);
            this.btnOpenFileUploadForm.Name = "btnOpenFileUploadForm";
            this.btnOpenFileUploadForm.Size = new System.Drawing.Size(203, 23);
            this.btnOpenFileUploadForm.TabIndex = 1;
            this.btnOpenFileUploadForm.Text = "Upload Survey From Excel File";
            this.btnOpenFileUploadForm.UseVisualStyleBackColor = true;
            this.btnOpenFileUploadForm.Click += new System.EventHandler(this.btnOpenFileUploadForm_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Participant,
            this.Entity,
            this.ActivityScore,
            this.TrainerScore,
            this.ToolScore});
            this.dataGridView1.Location = new System.Drawing.Point(16, 70);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(993, 391);
            this.dataGridView1.TabIndex = 2;
            // 
            // Date
            // 
            this.Date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Date.FillWeight = 3F;
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // Participant
            // 
            this.Participant.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Participant.FillWeight = 4F;
            this.Participant.HeaderText = "Participant";
            this.Participant.Name = "Participant";
            this.Participant.ReadOnly = true;
            // 
            // Entity
            // 
            this.Entity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Entity.FillWeight = 4F;
            this.Entity.HeaderText = "Entity";
            this.Entity.Name = "Entity";
            this.Entity.ReadOnly = true;
            // 
            // ActivityScore
            // 
            this.ActivityScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ActivityScore.FillWeight = 2F;
            this.ActivityScore.HeaderText = "Activity Score";
            this.ActivityScore.Name = "ActivityScore";
            this.ActivityScore.ReadOnly = true;
            // 
            // TrainerScore
            // 
            this.TrainerScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TrainerScore.FillWeight = 2F;
            this.TrainerScore.HeaderText = "Trainer Score";
            this.TrainerScore.Name = "TrainerScore";
            this.TrainerScore.ReadOnly = true;
            // 
            // ToolScore
            // 
            this.ToolScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ToolScore.FillWeight = 2F;
            this.ToolScore.HeaderText = "Tool Score";
            this.ToolScore.Name = "ToolScore";
            this.ToolScore.ReadOnly = true;
            // 
            // SubDialogCbParticipantFeedbackSurvey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 494);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SubDialogCbParticipantFeedbackSurvey";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "PARTICIPANT FEEDBACK SURVEY";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOpenFileUploadForm;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Participant;
        private System.Windows.Forms.DataGridViewTextBoxColumn Entity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActivityScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrainerScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn ToolScore;
    }
}